var isV2 = "V2",
  ReleaseURL = "http://minkfoodiee.com/foodiee/",
  TestingURL = "http://minkfoodiee.com/foodiee-Akay/",
  LocalHostURL = "http://192.168.5.204:8084/foodiee/",
  domain = ReleaseURL,


  tokenInfoAPI = domain + "DateService" + isV2,

  loginAPI = domain + "LoginServiceController" + isV2,
  userRegisterAPI = domain + "UserRegisServiceController" + isV2,
  LoginWithFacebookAPI = domain + "UserRegistrationFBService" + isV2,
  forgetPasswordAPI = domain + "ForgetPassServiceController" + isV2,
  sendOtpApi = domain + "SendOTP" + isV2,

  getUserDetailsAPI = domain + "GetUserDetailServiceController" + isV2,


  cityListAPI = domain + "CityServiceController" + isV2,
  cityAreaListAPI = domain + "CityAreaServiceController" + isV2,


  cuisineAPI = domain + "CuisinesServiceController" + isV2,
  specialCuisineAPI = domain + "SpecialCuisineServiceController" + isV2,


  userUpdateImageAPI = domain + "UpdateUserProfileServiceController" + isV2,
  userUpdateDetailsAPI = domain + "UpdateUserDetailService" + isV2,
  userUpdateMobileAPI = domain + "UpdateMobileService" + isV2,
  userUpdateEmailAPI = domain + "UpdateEmailService" + isV2,
  userChangePasswordAPI = domain + "ChangePasswordService" + isV2,

  RestaurantDetailsOnlyAPI = domain + "RestaurantDetailServiceByRestaurant" + isV2,
  menuDetailsAPI = domain + "RestaurantDetailAndMenuService" + isV2,
  restaurantDetailsAPI = domain + "RestaurantDetailServiceController" + isV2,
  AddUpdateCartItemsAPI = domain + "AddUpdateCartItemService" + isV2,
  reserveTableAPI = domain + "ReserveTableServiceController" + isV2,
  AvailableTableAPI = domain + "ListTableReservationService" + isV2,
  userCartListAPI = domain + "UserCartListServiceController" + isV2,
  userCartItemIncDecAPI = domain + "CartItemIncDecServiceController" + isV2,
  userCartListTaxAPI = domain + "UserCartListTaxServiceController" + isV2,
  addToCartAPI = domain + "AddToCartServiceController" + isV2,
  checkoutAPI = domain + "CheckoutServiceController" + isV2,
  recommendToFriendAPI = domain + "RecommendToFriendService" + isV2,

  toppingCommentAPI = domain + "SaveUpdateToppingCommentService" + isV2,
  listToppingByIdAPI = domain + "ListToppingByCartId" + isV2,
  commonCommentAPI = domain + "CommonCommentService" + isV2,
  preorderAPI = domain + "PreorderServiceController" + isV2,
  placeOrderAPI = domain + "PlaceOrderServiceController" + isV2,
  verificationCodeAPI = domain + "VerificationServiceController" + isV2,
  saveUserOrderAPI = domain + "SaveUserOrderServiceController" + isV2,



  restaurantQMListAPI = domain + "RestaurantQMList" + isV2,
  QMPrefrencesAPI = domain + "QmPreferenceController";
  userQMListAPI = domain + "UserQMList" + isV2,
  QManagerAPI = domain + "QueueManagerService" + isV2,
  QManagerCancelRequestAPI = domain + "QMRequestRejectByUser" + isV2,
  restaurantAPI = domain + "RestaurantServiceController" + isV2,

  restaurantSearchAPI = domain + "SearchRestaurantServiceController" + isV2,
  restaurantSortAPI = domain + "SortRestaurantListService" + isV2,
  listRestaurantByCategoryAPI = domain + "ListRestaurantByCategoryService" + isV2,



  //Inhouse Food Services
  inHouseCheckRestaurantAPI = domain + "InhouseCheckRestaurantService" + isV2,
  inHouseOrderDetailsAPI = domain + "InhouseOrderListDetail",
  inHouseDetailAPI = domain + "GetInhouseDetailService" + isV2,
  cartListInHouseAPI = domain + "UserCartListInhouseServiceController" + isV2,
  completeInHouseOredrAPI = domain + "CompleteInhouseOrderService" + isV2,
  saveUserOrderInHouseAPI = domain + "SaveUserOrderInhouseServiceController" + isV2,
  inHouseOederPayNowAPI = domain + "InhouseOrderPayNowService" + isV2,
  inHouseOrderListById = domain + "InhouseOrderListByInhouseIdService" + isV2,
  userCartInHouseIncDecAPI = domain + "CartItemIncDecInhouseServiceController" + isV2,
  addToCartInHouseAPI = domain + "AddToCartInhouseServiceController" + isV2,
  userCartListTaxInHouseAPI = domain + "UserCartListTaxInhouseServiceController" + isV2,
  PayGenrateInhouseAPI = domain + "PayGenrateInhouseBill" + isV2,
  cartInHouseDetailsAPI = domain + "InhouseOrderDetailByInhouseId" + isV2,
  ItemWiseBillSplitSubmitAPI = domain + "InhouseBillSplitItemwise" + isV2,
  InhouseItemPricewisePayCheckAPI = domain + "InhouseItemPricewisePayersList" + isV2,



  //Inhouse Bartabs (Normal) Services
  BarOrderRestaurantAPI = domain + "BarOrderRestaurantService" + isV2,
  BarOrderRequestAPI = domain + "BarOrderRequestService" + isV2,
  BarOrderRestaurantMenuAPI = domain + "BarOrderRestaurantMenuService" + isV2,
  AddToCartBarOrderServiceAPI = domain + "AddToCartBarOrderService" + isV2,
  UserCartListBarOrderAPI = domain + "UserCartListBarOrder" + isV2,
  BarOrderSendToRestaurantServiceAPI = domain + "BarOrderSendToRestaurantService" + isV2,
  BarOrderBillServiceAPI = domain + "BarOrderBillService" + isV2,
  BarOrderPaymentServiceAPI = domain + "BarOrderPaymentService" + isV2,
  BarOrderHistoryAPI = domain + "BarOrderHistory" + isV2,


  BarCoverChargeAPI = domain + "BarCoverChargeService" + isV2,

  //Inhouse Bar Exchange Services
  BarExchangeRequestAPI = domain + "BarExchangeRequestService" + isV2,
  BarExchangeRestaurantAPI = domain + "BarExchangeRestaurantService" + isV2,
  BarExchangeRestaurantMenuAPI = domain + "BarExchangeRestaurantMenuService" + isV2,
  AddToCartBarExchangeAPI = domain + "AddToCartBarExchangeService" + isV2,
  UserCartListBarExchangeAPI = domain + "UserCartListBarExchange" + isV2,
  BarExchangeSendToRestaurantAPI = domain + "BarExchangeSendToRestaurantService" + isV2,
  BarExchangeBillAPI = domain + "BarExchangeBillService" + isV2,
  BarExchangePaymentAPI = domain + "BarExchangePaymentService" + isV2,
  BarExchangeHistoryAPI = domain + "BarExchangeHistory" + isV2,

  CurrentActiveBarAPI = domain + "CurrentBarStatusService" + isV2,


  wishListAPI = domain + "WishlistServiceController" + isV2,
  addWishListAPI = domain + "AddWishlistServiceController" + isV2,
  userReviewAPI = domain + "UserReviewService" + isV2,
  userReviewListAPI = domain + "ListUsersReviews" + isV2,

  orderHistoryAPI = domain + "OrderHistoryServiceController" + isV2,
  oredrHistoryDetailsAPI = domain + "OrderHisDetailSerController" + isV2,
  tableReservationHistoryAPI = domain + "TableReservationHistoryV2",
  cancelOrderAPI = domain + "CancelOrderServiceControllerV2",


  sendSmsAPI = domain + "SendSMSService" + isV2,
  SendReceiptViaEmailAPI = domain + "SendOrderReceipt" + isV2,



  topCatAPI = domain + "TopCategoriesService" + isV2,
  topCatHomeAPI = domain + "TopCategoriesOnHomeService" + isV2,


  notificationHistoryAPI = domain + "NotificationHistory",


  UpdateUserDeviceTokenAPI = domain + "UpdateUserDeviceTokenV2",
  GeoFencingSendNotificationAPI = domain + "GeoFencingSendNotification" + isV2,
  SellerImagesAPI = domain + "SellerImages" + isV2;



//Deleted services
//  loginFbAPI = domain + "LoginFBService" + isV2,
//reserveTableInHouseAPI = domain + "ReserveInhouseTableServiceController" + isV2,
//    listRestaurantForAPI = domain + "ListRestaurantForV2",
//    paymentResponseAPI = domain + "user/FoodieePaymentResponseService" + isV2,


function caeserCipher(str, num) {
	num = num % 26; 
	var lowerCaseString = str.toLowerCase();
  var alphabet = 'abcdefghiklmnopqrstuvwxyz'.split('');
  var newString = '';
    
  for (var i = 0; i < lowerCaseString.length; i++) {
    var currentLetter = lowerCaseString[i];
      if (currentLetter === ' ') {
        newString += currentLetter;
        continue;
      }
      var currentIndex = alphabet.indexOf(currentLetter);
      var newIndex = currentIndex + num;
      if (newIndex > 25) newIndex = newIndex - 26;
      if (newIndex < 0 ) newIndex = 26 + newIndex;
      
      // if original string has some UPPERCASE letters
      if (str[i] === str[i].toUpperCase()) {
        newString += alphabet[newIndex].toUpperCase();
      } else newString += alphabet[newIndex];
  }
  return newString;
}

// -9 == 17
console.log(caeserCipher('Holaa', 17));
(function () {
  'use strict';

  angular
    .module('ElValidation', [])
    .config(['$validatorProvider', function ($validatorProvider) {

      $validatorProvider.setDefaults({
        debug: true,
        errorElement: 'span',
        errorClass: 'alert'
      });

      $.validator.addMethod('integer', function (value, element) {
        return this.optional(element) || /^\d+$/.test(value);
      });

      $.validator.addMethod('string', function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z_\s-]+$/.test(value);
      });

      $validatorProvider.addMethod("validEmail", function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $validatorProvider.addMethod("validMobileNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });

      $validatorProvider.addMethod("indianMobNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });
      $.validator.addMethod('strongPassword', function (value, element) {
        return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d).{8,}$/.test(value);
      });
      $.validator.addMethod('emailOrMobile', function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}|^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $.validator.addMethod('selectRequired', function (value) {
        return value != '0';
      });

      $.validator.addMethod('queuePeople', function (value, element) {
        return (value > 0 && value <= 30 && /^\d+$/.test(value));
      });

      $.validator.addMethod('greaterThanToday', function (value, element) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        var date = new Date(value.split('-').join('/'));
        return (date >= today);
      });
    }]);

})();


(function () {
  'use strict';

  //Waves effect configs
  var wavesConfig = {
    duration: 1200,
    delay: 50
  };
  Waves.init(wavesConfig);

  angular.module('MinkFoodiee', [

    //app dependancies
    'ionic',
    'ngCordova',
    'ngValidate',

    //3rd third-party modules
    'ionic-toast',
    'LocalStorageModule',
    'ngRateIt',
    'ElValidation',
    'angular-md5',
    'countdown',
    "ion-datetime-picker",

    //app pages and services
    'components-module',
    'pages-module',


  ]).run(AppRun);

  AppRun.$inject = ["$ionicPlatform", "$state", "$ionicHistory", "$ionicPopup", "$rootScope", "$Session", 'Loading', '$ionicPickerI18n'];
  
  
  function AppRun($ionicPlatform, $state, $ionicHistory, $ionicPopup, $rootScope, $Session, Loading, $ionicPickerI18n) {

    $ionicPlatform.ready(function () {

      $ionicPickerI18n.okClass = "button-assertive";
      $ionicPickerI18n.cancelClass = "button-stable";
      $ionicPickerI18n.arrowButtonClass = "button-assertive";

      function exitAppConfirm() {
        var confirmPopup = $ionicPopup.confirm({
          title: "Exit",
          template: "Are you sure you want to exit Mink Foodiee?",
          cancelText: "Cancel",
          cancelType: "button-default",
          okText: "I am sure",
          okType: "button-assertive"
        });

        confirmPopup.then(function(res) {
          return (res) ? navigator.app.exitApp() : false; 
        });
      };

      function backFromOnlinePaymentHandler() {
        var confirmPopup = $ionicPopup.confirm({
            title: "Back",
            template: "Order details will be lost if you click back. Are you sure?",
            cancelText: "Close",
            cancelType: "button-assertive",
            okText: "Yes, sure",
            okType: "button-default"
        });
        confirmPopup.then(function (res) {
          return (res) ? $state.go("app.checkout") : false;
        });
    }

      $rootScope.$on('$stateChangeStart', 
        function(event, toState, toParams, fromState, fromParams, options) { 
          // console.log(event,fromState);
          Loading.hide();
      });

      //Disable/Enable Hardware backbutton in Android
      $ionicPlatform.registerBackButtonAction(function (event) {
        Loading.hide();
        if ($state.current.name == "app.restList" || $state.current.name == "home") {
          event.preventDefault();
          exitAppConfirm();
        } else if (
          $state.current.name == "app.profile" ||
          $state.current.name == "app.notification" ||
          $state.current.name == "app.favourites" ||
          $state.current.name == "app.history" ||
          $state.current.name == "app.tableBookingList" ||
          $state.current.name == "app.inhouseFoodBarList" ||
          $state.current.name == "app.queuemanager") {
          $ionicHistory.nextViewOptions({
            historyRoot: true,
            disableBack: true
          });
          $state.go("app.restList");
        } else if ($state.current.name == "app.payment") {
          event.preventDefault();
          backFromOnlinePaymentHandler();
        }
        else if ($state.current.name == "app.checkout") {
          event.preventDefault();
        }
        else if ($state.current.name == "thankyou") {
          event.preventDefault();
        } else {
          // $ionicHistory.backView();
          navigator.app.backHistory();
        }
      }, 100);


      //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
      //Keep in mind the function will return null if the token has not been established yet.
      if (typeof FCMPlugin !== 'undefined') {
        FCMPlugin.getToken(function (registrationId) {
          //alert("registrationId: " + registrationId);
          var oldRegId = $Session.user.getDeviceToken();
          if (oldRegId !== registrationId) {
            // Save new registration ID
            $Session.user.setDeviceToken(registrationId);
            // localStorage.setItem("device_token", registrationId);
          }
        }, function (err) {
          //alert("error retrieving token: " + err);
        });

        FCMPlugin.onNotification(function (data) {
          if (data.wasTapped) {
            //Notification was received on device tray and tapped by the user.
            // alert("Tapped: " + JSON.stringify(data));
          } else {
            //Notification was received in foreground. Maybe the user needs to be notified.
            // alert("Not tapped: " + JSON.stringify(data));
          }
        }, function (msg) {
          //alert("onNotification callback successfully registered: " + msg);
        }, function (err) {
          //alert("Error registering onNotification callback: " + err);
        });
      } else console.warn("FCM is not defined, maybe You are using webview");
      

    });
    
  }

})();
(function () {
  'use strict';

  angular.module('components-module', [
    'error-content-component-module',
    'loading-content-component-module',
    'qm-history-component-module',
    'rest-card-component-module',
    'table-book-history-component-module',
    'star-rating-component-module',
    'order-history-component-module',
  ]);
})();

(function () {
  'use strict';

  angular.module('pages-module', [
    'order-history-list-module',
    'order-history-details-module',
    'home-module',
    'table-booking-module',
    // 'inhouse-module',
    'login-module',
    'notification-module',
    'profile-module',
    'queue-module',
    'restaurant-list-module',
    'signup-module',
    'splash-module',
    'welcome-module'
  ]);
})();

(function() {
    'use strict';

    angular.module('error-content-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('order-history-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('rest-card-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('qm-history-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('loading-content-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('star-rating-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('table-book-history-component-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('home-module', [

    ])
})();
(function () {
  'use strict';

  angular.module('inhouse-module', [
      'inhouse-food-module',
      'inhouse-bar-module',
      'inhouse-bar-ex-module'
    ])

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('app.inhouseFoodBarList', {
          url: '/inhouseFoodBarList',
          views: {
            'menuContent': {
              templateUrl: 'app/pages/Inhouse/list/inhouse-food-bar-list.html',
              controller: 'InhouseFoodBarListCTRL'
            }
          }
        });
    }]);

})();

(function () {
  'use strict';

  angular.module('login-module',[

  ])

}());

(function () {
  'use strict';

  angular
    .module('notification-module', [

    ])
}());

(function () {
  'use strict';

  angular.module('profile-module', [

  ])
})();

(function () {
  'use strict';

  angular.module('queue-module', [
    
  ])
})();

(function () {
  'use strict';

  angular.module('restaurant-list-module', [

  ])
})();
(function() {
    'use strict';

    angular.module('signup-module', [

    ])
})();
(function() {
    'use strict';

    angular.module('splash-module', [
        
    ])
})();
(function() {
    'use strict';

    angular.module('table-booking-module', [
        
    ])
})();
(function() {
    'use strict';

    angular.module('welcome-module', [

    ])
})();
(function () {
  'use strict';

  angular.module('order-history-details-module', [

  ])
})();

(function () {
  'use strict';

  angular.module('order-history-list-module', [

  ])
})();

(function() {
    'use strict';

    angular.module('inhouse-bar-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider
                .state('app.inhouseBarRequest', {
                    url: '/inhouseBarRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/request/bar-request.html',
                            controller: 'InhouseBarRequestCTRL'
                        }
                    }
                })
                .state('app.inhouseBarMenu', {
                    url: '/inhouseBarMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/menu/bar-menu.html',
                            controller: 'InhouseBarMenuCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouseBar', {
                    url: '/viewCartInhouseBar',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/cart/bar-cart.html',
                            controller: 'InhouseBarCartCTRL'
                        }
                    }
                })
                .state('app.inhouseBarCheckout', {
                    url: '/inhouseBarCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/checkout/bar-checkout.html',
                            controller: 'InhouseBarCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseBarPayMyself', {
                    url: '/inhouseBarPayMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/pay-myself/bar-pay-myself.html',
                            controller: 'InhouseBarPayMyselfCTRL'
                        }
                    }
                });

        }]);
})();
(function() {
    'use strict';

    angular.module('inhouse-food-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider
                .state('app.inhouseFoodRequest', {
                    url: '/inhouseFoodRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/request/request.html',
                            controller: 'InhouseFoodRequestCTRL'
                        }
                    }
                })
                .state('app.inHouseFoodMenu', {
                    url: '/inHouseFoodMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/menu/menu.html',
                            controller: 'InhouseFoodMenuCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouseModal', {
                    url: '/viewCartInhouseModal',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/cart-not-served/cart-not-served.html',
                            controller: 'InhouseFoodCartNotServedCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouse', {
                    url: '/viewCartInhouse',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/cart-served/cart-served.html',
                            controller: 'InhouseFoodCartServedCTRL'
                        }
                    }
                })
                .state('app.inhouseCheckout', {
                    url: '/inhouseCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/checkout/checkout.html',
                            controller: 'InhouseFoodCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayMyself', {
                    url: '/payMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-myself/pay-myself.html',
                            controller: 'InhouseFoodPayMyselfCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayOnline', {
                    url: '/payOnline',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-online/pay-online.html',
                            controller: 'InhouseFoodPayOnlineCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayItemWise', {
                    url: '/payItemWise',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-itemwise/pay-itemwise.html',
                            controller: 'InhouseFoodPayItemWiseCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayPriceWise', {
                    url: '/payPriceWise',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-pricewise/pay-pricewise.html',
                            controller: 'InhouseFoodPayPriceWiseCTRL'
                        }
                    }
                });

        }]);
})();
(function() {
    'use strict';

    angular.module('inhouse-bar-ex-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider

                .state('app.inhouseExBarRequest', {
                    url: '/inhouseExBarRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/request/bar-ex-request.html',
                            controller: 'InhouseBarExRequestCTRL'
                        }
                    }
                })
                .state('app.inHouseBarExMenu', {
                    url: '/inHouseBarExMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/menu/bar-ex-menu.html',
                            controller: 'InHouseBarExMenuCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExCart', {
                    url: '/inhouseBarExCart',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/cart/bar-ex-cart.html',
                            controller: 'InhouseBarExCartCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExCheckout', {
                    url: '/inhouseBarExCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/checkout/bar-ex-checkout.html',
                            controller: 'InhouseBarExCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExPayMyself', {
                    url: '/inhouseBarExPayMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/pay-myself/bar-ex-pay-myself.html',
                            controller: 'InhouseBarExPayMyselfCTRL'
                        }
                    }
                })

            ;

        }]);
})();
(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .controller('AppController', AppController);

  AppController.$inject = ['$scope', '$Easyajax', '$Session', '$state'];

  function AppController($scope, $Easyajax, $Session, $state) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

      $scope.$on('$ionicView.enter', function () {
        getUserDetails();
      });

      function getUserDetails() {
        var userId = $Session.user.getId() || "0";
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (res) {
          if (res.status == "success") {
            // Append details to show up in the scope
            $scope.userDetail = res.userDetail;
            $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res);

            // Show user image according to 
            var image = $Session.user.getImage();
            if (image == "" || image == "http://minkchatter.com/mink-chatter-images/app/uploads/") {
              $('.userImage').attr('src', 'assets/img/profile.png');
            } else  {
              $('.userImage').attr('src', image);
            }
          } else {
            console.log("Error getting user information!", res);
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.goProfile = function() {
        $state.go("app.profile")
      }

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }

      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }


    };
  };
})();

(function () {
  "use strict";

  angular
    .module("MinkFoodiee")
    .config(AppConfig);

  AppConfig.$inject = ["$urlRouterProvider", "$ionicConfigProvider", "$stateProvider"];

  function AppConfig($urlRouterProvider, $ionicConfigProvider, $stateProvider) {
    $ionicConfigProvider.backButton.text("").previousTitleText(false);
    $ionicConfigProvider.navBar.alignTitle("center");
    // $ionicConfigProvider.views.transition("ios");

    //App Routing base
    $stateProvider
      .state('app', {
        cache: false,
        url: '/app',
        abstract: true,
        templateUrl: 'app/app.html',
        controller: 'AppController',
        controllerAs: 'vm'
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise("/index");

  }

})();

(function() {
    "use strict";
  
    angular.module("MinkFoodiee").factory("Area", AreaFactory);
  
    AreaFactory.$inject = ["Session"];
  
    function AreaFactory(Session) {
      var data = {
        session: {
          id: "mf_location_city_area_id",
          name: "mf_location_city_area_name"
        }
      };
  
      var service = {
        getId: getId,
        setId: setId,
        removeId: removeId,
        getName: getName,
        setName: setName,
        removeName: removeName,
        clearSession: clearSession
      };
  
      return service;
  
      function getId() {
        return Session.get(data.session.id);
      };
  
      function setId(id) {
        Session.save(data.session.id, id);
      };
  
      function removeId() {
        Session.remove(data.session.id);
      };
  
      function getName() {
        return Session.get(data.session.name);
      };
  
      function setName(name) {
        Session.save(data.session.name, name);
      };
  
      function removeName() {
        Session.remove(data.session.name);
      };
  
      /**
       * Remove all data related to city location in the current session
       * @function clearSession
       * @memberof City
       */
      function clearSession() {
        this.removeId();
        this.removeName();
      };
    }
  })();
(function() {
    "use strict";
  
    angular.module("MinkFoodiee").factory("City", cityFactory);
  
    cityFactory.$inject = ["Session"];
  
    function cityFactory(Session) {
      /**
       * @private {data}
       * @member {cityFactory}
       */
      var data = {
        session: {
          id: "mf_location_city_id",
          name: "mf_location_city_name"
        }
      };
  
      return {
        getId: getId,
        setId: setId,
        removeId: removeId,
        getName: getName,
        setName: setName,
        removeName: removeName,
        clearSession: clearSession
      };
  
      function getId() {
        return Session.get(data.session.id);
      }
  
      function setId(id) {
        Session.save(data.session.id, id);
      }
  
      function removeId() {
        Session.remove(data.session.id);
      }
  
      function getName() {
        return Session.get(data.session.name);
      }
  
      function setName(name) {
        Session.save(data.session.name, name);
      }
  
      function removeName() {
        Session.remove(data.session.name);
      }
  
      /**
       * Remove all data related to city location in the current session
       * @function clearSession
       * @memberof City
       */
      function clearSession() {
        removeId();
        removeName();
      }
    }
  })();
(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .filter('Dates', filter)

  function filter() {

    var errorMessages = {
      invalidDate: "An attempt of defining a new date failed due a wrong date string being passed as a parameter",
      invalidDay: "It wasn't possible to get the day",
      invalidMonth: "It wasn't possible to get the month",
      invalidYear: "It wasn't possible to get the year"
    };

    return dateFilter;

     function dateFilter(input, day) {

      /**
       * Declaring the months as a string
       * @param {*} month 
       */
      function convertMonthAtString(month) {
        var months = [
          "JAN", // 0
          "FEB", // 1
          "MAR", // 2
          "APR", // 3
          "MAY", // 4
          "JUN", // 5
          "JULY", // 6
          "AUG", // 7
          "SEP", // 8
          "OCT", // 9
          "NOV", // 10
          "DEC" // 11
        ];
        return months[month];
      }

      function buildDate(date) {
        var d = new Date(date);
        if (d.toString().toLowerCase() === "invalid date") {
          var correctDate = moment(date, 'DD.MM.YYYY').format();
          // throw new Error(errorMessages.invalidDate);
          return new Date(correctDate);
        }
        return d;
      };

      function getDay(date) {
        var day = date.getDate();
        if (!$.isNumeric(day)) throw new Error(errorMessages.invalidDay);
        return day;
      };

      function getMonth(date) {
        var month = date.getMonth();
        if (!$.isNumeric(month)) throw new Error(errorMessages.invalidMonth);
        return month;
      };

      function getFullYear(date) {
        var year = date.getFullYear();
        if (!$.isNumeric(year)) throw new Error(errorMessages.invalidYear);
        return year;
      };

      function joinMonthYear(month, year) {
        return convertMonthAtString(month) + ", " + year;
      };

      function getTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        return {
          hours: hours,
          minutes: minutes
        }
      };

      function convertHours(hours) {
        // AM
        if (hours < 12) {
          // Adjust if the hour is equal midnight
          hours = (hours === 0) ? 12 : hours;
          // Just returns the number + am
          return {
            hours: hours,
            period: "am"
          };
        }
        // PM
        else {
          // Adjusting if the hours is equal noon
          hours = (hours === 12) ? hours : hours - 12;
          // Returns the number in a 12 hours format + pm
          return {
            hours: hours,
            period: "pm"
          };
        }
      }

      function addLeftZero(number) {
        // Slide -2 will return the last two digits of the number
        return ("0" + number).slice(-2);
      }

      function stringfyTime(time) {
        // Converting hours in a 12 hours format
        var hours = convertHours(time.hours);
        // Returns the result as a string
        return addLeftZero(hours.hours) + ":" + addLeftZero(time.minutes) + hours.period;
      };

      function extractDay(stringDate) {
        var date = buildDate(stringDate);
        var day = getDay(date);
        return day;
      }

      function extractMonthYear(data) {
        var date = buildDate(data);
        var month = getMonth(date);
        var year = getFullYear(date);
        var monthYear = joinMonthYear(month, year);
        return monthYear;
      }

      function extractTime(data) {
        var date = buildDate(data);
        var time = getTime(date);
        var stringTime = stringfyTime(time);
        return stringTime;
      }

      /**
       * @return {value} depending on filter {day} variable
       ***********************************/
      switch (day) {
        case 'mmyy':
          var outputmy = extractMonthYear(input);
          return outputmy;
        case 'd':
          var outputday = extractDay(input);
          return outputday;
        case 'time':
          var outputtime = extractTime(input);
          return outputtime;
        default:
          console.log('Sorry, we are out of ' + day + '.');
      }

    }
  }

}());
(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('httpRequest', service)

  service.$inject = ['Loading', 'ionicToast', '$q'];

  function service(Loading, ionicToast, $q) {
    var headers = {
      'contentType': 'application/x-www-form-urlencoded',
      'username': 'foodiee',
      'password': 'MTIzNDU=',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, username, password',
      'Access-Control-Allow-Credentials': 'true'
            // .append('Access-Control-Allow-Origin', '*')
      // .append('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
      // .append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization')
      // .append('Access-Control-Allow-Credentials', 'true')
    };
    /**
     * @function makeRequest
     * @param configRequest: configirations is being passed to this function
     * @todo assign values of config to private variables, and check if the request is not for token , so we can
     *       insert it on every AJAX request 
     * @return value of request is returned to makeRequest() function
     * */
    var makeRequest = function (configRequest) {

      var typeRequest = configRequest.typeRequest;
      var urlRequest = configRequest.urlRequest;
      var dataRequest = configRequest.dataRequest || {};
      var sucesCallbackRequest = configRequest.sucesCallbackRequest || function (response) {
        console.log(response)
      };
      var errorCallbackRequest = configRequest.errorCallbackRequest || function (err) {
        console.error("Error while trying to make AJAX request.", err);
        Loading.hide();
        ionicToast.show("Opss! Try again in a while!", 3000, '#A71212');
      };
      var token = localStorage.getItem("Token");
      if (urlRequest != tokenInfoAPI) {
        dataRequest.Token = token;
      }

      $.ajax({
        type: typeRequest,
        url: urlRequest,
        headers: headers,
        data: dataRequest,
        success: sucesCallbackRequest,
        error: errorCallbackRequest,
        timeout: 10000000,
        dataType: "json",
        crossDomain: true,
      });

    };


    this.validateNotNullData = function (DataObjectToTest, typeRequest) {
      if (typeRequest === "POST") {
        if (typeof DataObjectToTest !== "object") {
          throw new Error("POST request, you are trying to sent data that is not an object");
        }
      }
    }

    this.validateTypeRequest = function (typeRequest) {
      if (typeRequest !== "POST" && typeRequest !== "GET") {
        throw new Error("The request Type should be POST/GET");
      }
    }

    this.validateCallback = function (callback) {
      if (typeof callback !== "undefined") {
        if (typeof callback !== "function") {
          throw new Error("The callback function is not a function, make sure that callback is really a function");
        }
      }
    }

    this.getResponse = function (configRequest) {
      makeRequest(configRequest);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('$Easyajax', factory)

  factory.$inject = ['httpRequest', '$ionicPopup', 'Loading'];

  function factory(httpRequest, $ionicPopup, Loading) {
    var service = {
      startRequest: startRequest
    }

    return service;

    function startRequest(configRequest) {



      //Test if the data are valid in case of a POST
      httpRequest.validateNotNullData(configRequest.dataRequest, configRequest.typeRequest);

      //Test if the Request Type is POST or GET
      httpRequest.validateTypeRequest(configRequest.typeRequest);

      //Test if the callback is a function
      httpRequest.validateCallback(configRequest.sucesCallbackRequest);

      //Finally, get configurations and start doing request and return the response
      if (!navigator.onLine) {
        // console.log("Device connected to Internet? >> " + navigator.onLine);
        Loading.hide();
        showAlert();
        return false;
      } else {
        // console.log("Device connected to Internet? >> " + navigator.onLine);
        var response = httpRequest.getResponse(configRequest);
        return response;
      }

    }

    function showAlert() {
      $ionicPopup.show({
        title: 'Oops!',
        template: 'You are offline! Check your internet connection',
        buttons: [{
          text: 'REFRESH',
          type: 'button-assertive',
          onTap: function (e) {
            console.log("e > ", e);
            // e.preventDefault()
            // location.reload();
            startRequest(configRequest);
          }
        }]
      });
    }



  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('Facebook', service)

  service.$inject = ['$q'];

  function service($q) {

    // return 
    this.getFacebookProfileInfo = getFacebookProfileInfo;
    this.login = login;
    this.logout = logout;


    function getFacebookProfileInfo(authResponse) {
      var info = $q.defer();
      facebookConnectPlugin.api('/me?fields=email,name,picture,first_name,last_name&access_token=' + authResponse.accessToken, null,
        function (response) {
          console.log(response);
          info.resolve(response);
        },
        function (response) {
          console.log(response);
          info.reject(response);
        }
      );
      return info.promise;
    };


    function login(successCallback, errorCallback) {
      facebookConnectPlugin.login(["email", "public_profile"], successCallback, errorCallback);
    }


    function logout(successCallback, errorCallback) {
      facebookConnectPlugin.logout(successCallback, errorCallback);
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('Geolocation', service)

  service.$inject = ['$ionicPopup'];

  function service($ionicPopup) {
    this.validateCallbackFunction = function (callback) {
      if (typeof callback !== "function") {
        throw new Error("You must pass a function as a parameter");
      }
    }

    this.validateGpsActivation = function (callback) {

      function success_getLocation(enabled) {
        if (!enabled) {

          var confirmPopup = $ionicPopup.confirm({
            title: 'Location detect',
            template: 'We need to access your location to show near restaurants',
            cancelText: 'Cancel',
            cancelType: 'button-default',
            okText: 'OK',
            okType: 'button-balanced'
          });

          confirmPopup.then(function (res) {
            if (res) {
              cordova.plugins.diagnostic.switchToLocationSettings();
            } else {
              console.log('You are not sure');
            }
          });

        } else {
          //alert("Location is already ENABLED");
          // //getting stored values of lang and lat from localStorage
          // var storagelatitude = localStorage.getItem("user_first_latitude");
          // var storagelongitude = localStorage.getItem("user_first_longitude");
          // callback(storagelatitude, storagelongitude);
        }
      }

      function error_getLocation(error) {
        console.error("The following error occurred while trying to open Location: " + error);
      }

      cordova.plugins.diagnostic.isLocationEnabled(success_getLocation, error_getLocation);

    }

    this.getGeolocation = function (callback) {
      function successCallback(position) {
        //alert("Position Object for First Time After success oopening location: ", position);
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        localStorage.setItem("user_first_latitude", latitude);
        localStorage.setItem("user_first_longitude", longitude);
        //alert("latitude Value stored in local: " + latitude);
        //alert("longitude Value stored in local: " + longitude);
        callback(latitude, longitude);
      }

      function errorCallback(error) {
        alert(JSON.stringify(data));
        /*switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("The user has not allowed access to his position");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("The user's location could not be determined");
                break;
            case error.TIMEOUT:
                alert("The service did not respond in time");
                break;
        }*/
        return;
      }

      var options = {
        enableHighAccuracy: true
      };

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback, options);
      } else {
        alert("Something went wrong @getGeolocation   navigator.geolocation.getCurrentPosition ");
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('$Geolocation', factory)

  factory.$inject = ['Geolocation'];

  function factory(Geolocation) {

    return {
      requestGeolocation: requestGetGeolocation
    }

    function requestGetGeolocation(callback) {
      Geolocation.validateGpsActivation();
      Geolocation.validateCallbackFunction(callback);
      Geolocation.getGeolocation(callback);
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('Loading', factory)

  factory.$inject = ['$ionicLoading'];

  function factory($ionicLoading) {
    var service = {
      show: show,
      hide: hide,
      showDish: showDish,
      hideDish: hideDish
    };

    return service;

    function show() {
      $ionicLoading.show({
        template: '',
        animation: 'fade-in',
        showBackdrop: false
      });
    };

    function hide() {
      $ionicLoading.hide();
    }

    function showDish() {
      $ionicLoading.show({
        // <ion-spinner class="spinner-balanced"></ion-spinner>
        template: '',
        animation: 'fade-in',
        showBackdrop: false
      }).then(function () {
        //console.log("loading is shown");
      });
    };

    function hideDish() {
      $ionicLoading.hide().then(function () {
        //console.log("loading is hidden");
      });
    }


  }
})();

(function () {
  "use strict";

  angular.module("MinkFoodiee").factory("Order", OrderFactory);

  OrderFactory.$inject = ["Session"];

  function OrderFactory(Session) {

    var data = {
      session: {
        id: "mf_order_id",
        reviewOrderId: "mf_order_review_id",
        menuItem: "mf_order_menu_item_for_data",
        totalAmount: "mf_total_amount",
        minCharge: "mf_min_charge",
        type: "mf_order_type",
        cart: "mf_order_cart"
      }
    };

    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getReviewOrderId: getReviewOrderId,
      setReviewOrderId: setReviewOrderId,
      removeReviewOrderId: removeReviewOrderId,

      getMinDeliveryCharges: getMinDeliveryCharges,
      setMinDeliveryCharges: setMinDeliveryCharges,
      removeMinDeliveryCharges: removeMinDeliveryCharges,

      getTotalAmount: getTotalAmount,
      setTotalAmount: setTotalAmount,
      removeTotalAmount: removeTotalAmount,

      getType: getType,
      setType: setType,
      removeType: removeType,

      getMenuItemType: getMenuItemType,
      setMenuItemType: setMenuItemType,
      removeMenuItemType: removeMenuItemType,

      getCartObject: getCartObject,
      setCartObject: setCartObject,
      removeCartObject: removeCartObject,

      clearSession: clearSession
    };

    return service;

    /**
     * Responding the id of the order, this value is filled when the user adds
     * the first item to the cart
     * @function getId
     * @memberof OrderEntity
     * @returns {integer}
     */
    function getId() {
      return Session.get(data.session.id);
    };

    /**
     * Setting the id of the order in the localStorage, this information should be
     * set when the user adds the first item to the cart
     * @function setId
     * @memberof OrderEntity
     * @param {integer} id - Id coming from the backend
     */
    function setId(id) {
      Session.save(data.session.id, id);
    };

    /**
     * Removing order id from the localStorage, generally because the user
     * got to the end of the Checkout flow, this information should be deleted
     * to avoid inconsistency in the future
     * @function removeId
     * @memberof OrderEntity
     */
    function removeId() {
      Session.remove(data.session.id);
    };

    /**
     * Responding the id of the order, this value is filled when the user adds
     * the first item to the cart
     * @function getReviewOrderId
     * @memberof Order
     * @returns {integer} reviewOrderId
     */
    function getReviewOrderId() {
      return Session.get(data.session.reviewOrderId);
    };

    /**
     * Setting the id of the order in the localStorage, this information should be
     * set when the user adds the first item to the cart
     * @function setReviewOrderId
     * @memberof Order
     * @param {integer} id - Id coming from the backend
     */
    function setReviewOrderId(id) {
      Session.save(data.session.reviewOrderId, id);
    };

    /**
     * Removing order id from the localStorage, generally because the user
     * got to the end of the Checkout flow, this information should be deleted
     * to avoid inconsistency in the future
     * @function removeReviewOrderId
     * @memberof OrderEntity
     */
    function removeReviewOrderId() {
      Session.remove(data.session.reviewOrderId);
    };

    /**
     * Getting the minimum delivery charge from the localStorage before proceed to
     * Checkout Order screen. This value comes originally from the database and
     * change for each restaurant.
     * @function getMinDeliveryCharges
     * @memberof OrderEntity
     * @returns {string}
     */
    function getMinDeliveryCharges() {
      return Session.get(data.session.minCharge);
    };

    /**
     * Setting the Minimum Delivery Charges on the localStorage, this value
     * comes from backend and changes according to the restaurant.
     * @function setMinDeliveryCharges
     * @param {integer} minCharge
     */
    function setMinDeliveryCharges(minCharge) {
      Session.save(data.session.minCharge, minCharge);
    };

    /**
     * Removing the Minimum Delivery Charges from the localStorage, due avoiding
     * inconsistencies or locked values, this value is dynamic and change for
     * each restaurant, it's highly recommended to remove if it's not in use anymore
     * @function removeMinDeliveryCharges
     * @memberof OrderEntity
     */
    function removeMinDeliveryCharges() {
      Session.remove(data.session.minCharge);
    };

    /**
     * Getting the sum of all items the user added to the cart.
     * @function getTotalAmount
     * @returns {float}
     */
    function getTotalAmount() {
      return Session.get(data.session.totalAmount);
    };

    /**
     * Setting the sum of the price of all items the user has on the cart
     * @function setTotalAmount
     * @memberof OrderEntity
     * @param {totalAmount}
     */
    function setTotalAmount(totalAmount) {
      Session.save(data.session.totalAmount, totalAmount);
    };

    /**
     * Removing the total amount from the localStorage, this value is associated
     * to a dynamic value representing the sum of the items in a certain transaction,
     * it's highly recommended to delete this value if it's not been used anymore
     * @function removeTotalAmount
     * @memberof OrderEntity
     */
    function removeTotalAmount() {
      Session.remove(data.session.totalAmount);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getType() {
      return Session.get(data.session.type);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setType(type) {
      Session.save(data.session.type, type);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeType() {
      Session.remove(data.session.type);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getMenuItemType() {
      return Session.get(data.session.menuItem);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setMenuItemType(type) {
      Session.save(data.session.menuItem, type);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeMenuItemType() {
      Session.remove(data.session.menuItem);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getCartObject() {
      return Session.get(data.session.cart);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setCartObject(cartString) {
      Session.save(data.session.cart, cartString);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeCartObject() {
      Session.remove(data.session.cart);
    };

    /**
     * Remove all data related to order in the current session
     * @function clearSession
     * @memberof Order
     */
    function clearSession() {
      this.removeMenuItemType();
      this.removeMinDeliveryCharges();
      this.removeId();
      this.removeReviewOrderId();
      this.removeType();
      this.removeTotalAmount();
    };



  }
})();

(function() {
  "use strict";

  angular.module("MinkFoodiee").factory("SellerBar", SellerBarFactory);

  SellerBarFactory.$inject = ["Session"];

  function SellerBarFactory(Session) {
    var data = {
      session: {
        id: "mf_seller_inhouse_bar_id",
        tableNumber: "mf_seller_inhouse_bar_table_number",
        token: "mf_seller_inhouse_bar_token",
        sellerId: "mf_seller_inhouse_bar_rest_id",
        sellerName: "mf_seller_inhouse_bar_name",
        sellerMinCharge: "mf_seller_inhouse_bar_min_charge",
        sellerMinDiscount: "mf_seller_inhouse_bar_min_discount",
        sellerDiscountPercent: "mf_seller_inhouse_bar_discount_percent",
        sellerDiscountAmount: "mf_seller_inhouse_bar_discount_amount",
        orderTotalAmount: "mf_seller_inhouse_bar_total_amount"
      }
    };

    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getTableNumber: getTableNumber,
      setTableNumber: setTableNumber,
      removeTableNumber: removeTableNumber,

      getSellerId: getSellerId,
      setSellerId: setSellerId,
      removeSellerId: removeSellerId,

      getSellerName: getSellerName,
      setSellerName: setSellerName,
      removeSellerName: removeSellerName,

      getSellerDiscountAmount: getSellerDiscountAmount,
      setSellerDiscountAmount: setSellerDiscountAmount,
      removeSellerDiscountAmount: removeSellerDiscountAmount,

      getSellerDiscountPercent: getSellerDiscountPercent,
      setSellerDiscountPercent: setSellerDiscountPercent,
      removeSellerDiscountPercent: removeSellerDiscountPercent,

      getSellerMinDiscount: getSellerMinDiscount,
      setSellerMinDiscount: setSellerMinDiscount,
      removeSellerMinDiscount: removeSellerMinDiscount,

      getSellerMinCharge: getSellerMinCharge,
      setSellerMinCharge: setSellerMinCharge,
      removeSellerMinCharge: removeSellerMinCharge,

      getOrderTotalAmount: getOrderTotalAmount,
      setOrderTotalAmount: setOrderTotalAmount,
      removeOrderTotalAmount: removeOrderTotalAmount,

      getToken: getToken,
      setToken: setToken,
      removeToken: removeToken
    };

    return service;

    /**
     * Getting the InHouse id, generally to identify the transaction and
     * search data from the database, such as items on the cart.
     * @function getId
     * @memberof InHouseEntity
     * @returns {integer} - Returns inHouse id used at the inHouse transactions
     */
    function getId() {
      return Session.get(data.session.id);
    }

    /**
     * Saving the inHouse id in the localStorage, this information will be used
     * to identify which transaction the user is executing.
     * @function setId
     * @memberof InHouseEntity
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    }

    /**
     * Removing InHouse id from the localStorage, generally called at the end
     * of a transaction to avoid conflicts with future transactions
     * @memberof InHouseEntity
     * @function removeId
     */
    function removeId() {
      Session.remove(data.session.id);
    }

    /**
     * Getting the InHouse Seller id from the localStorage, this information is
     * used to consulte data from the restaurant, like products available
     * in the InHouse flow for that particular restaurant
     * @memberof InHouseEntity
     * @function getSellerId
     * @returns {string}
     */
    function getSellerId() {
      return Session.get(data.session.sellerId);
    }

    /**
     * Setting the Seller Id of the current transaction, this function
     * generally is called at the beginning of the flow, when the user
     * selected a restaurant after clicking in In House menu
     * @function setSellerId
     * @memberof InHouseEntity
     * @param {integer} sellerId
     */
    function setSellerId(sellerId) {
      Session.save(data.session.sellerId, sellerId);
    }

    /**
     * Removing Seller Id from the localStorage, this function generally is
     * called at the end of the InHouse flow, avoiding inconsistency when
     * the user executes the same flow
     * @function removeSellerId
     * @memberof InHouseEntity
     */
    function removeSellerId() {
      Session.remove(data.session.sellerId);
    }

    function getToken() {
      return Session.get(data.session.token);
    }
    function setToken(token) {
      Session.save(data.session.token, token);
    }
    function removeToken() {
      Session.remove(data.session.token);
    }

    function getTableNumber() {
      return Session.get(data.session.tableNumber);
    }
    function setTableNumber(tableNumber) {
      Session.save(data.session.tableNumber, tableNumber);
    }
    function removeTableNumber() {
      Session.remove(data.session.tableNumber);
    }

    function getSellerName() {
      return Session.get(data.session.sellerName);
    }
    function setSellerName(name) {
      Session.save(data.session.sellerName, name);
    }
    function removeSellerName() {
      Session.remove(data.session.sellerName);
    }

    function getSellerMinCharge() {
      return Session.get(data.session.sellerMinCharge);
    }
    function setSellerMinCharge(amount) {
      Session.save(data.session.sellerMinCharge, amount);
    }
    function removeSellerMinCharge() {
      Session.remove(data.session.sellerMinCharge);
    }

    function getSellerMinDiscount() {
      return Session.get(data.session.sellerMinDiscount);
    }
    function setSellerMinDiscount(discount) {
      Session.save(data.session.sellerMinDiscount, discount);
    }
    function removeSellerMinDiscount() {
      Session.remove(data.session.sellerMinDiscount);
    }

    function getSellerDiscountPercent() {
      return Session.get(data.session.sellerDiscountPercent);
    }
    function setSellerDiscountPercent(percent) {
      Session.save(data.session.sellerDiscountPercent, percent);
    }
    function removeSellerDiscountPercent() {
      Session.remove(data.session.sellerDiscountPercent);
    }

    function getSellerDiscountAmount() {
      return Session.get(data.session.sellerDiscountAmount);
    }
    function setSellerDiscountAmount(amount) {
      Session.save(data.session.sellerDiscountAmount, amount);
    }
    function removeSellerDiscountAmount() {
      Session.remove(data.session.sellerDiscountAmount);
    }

    function getOrderTotalAmount() {
      return Session.get(data.session.orderTotalAmount);
    }
    function setOrderTotalAmount(amount) {
      Session.save(data.session.orderTotalAmount, amount);
    }
    function removeOrderTotalAmount() {
      Session.remove(data.session.orderTotalAmount);
    }
  }
})();

(function() {
  "use strict";

  angular
    .module("MinkFoodiee")
    .factory("SellerInhouse", SellerInhouseFactory);

  SellerInhouseFactory.$inject = ["Session"];

  function SellerInhouseFactory(Session) {
    var data = {
      session: {
        id: "mf_seller_inhouse_id",
        tableNumber: "mf_seller_inhouse_table_number",
        sellerId: "mf_seller_inhouse_rest_id",
        sellerName: "mf_seller_inhouse_name",
        sellerMinCharge: "mf_seller_inhouse_min_charge",
        sellerMinDiscount: "mf_seller_inhouse_min_discount",
        sellerDiscountPercent: "mf_seller_inhouse_discount_percent",
        sellerDiscountAmount: "mf_seller_inhouse_discount_amount",
        orderTotalAmount: "mf_seller_inhouse_total_amount"
      }
    };
    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getTableNumber: getTableNumber,
      setTableNumber: setTableNumber,
      removeTableNumber: removeTableNumber,

      getSellerId: getSellerId,
      setSellerId: setSellerId,
      removeSellerId: removeSellerId,

      getSellerName: getSellerName,
      setSellerName: setSellerName,
      removeSellerName: removeSellerName,

      getSellerDiscountAmount: getSellerDiscountAmount,
      setSellerDiscountAmount: setSellerDiscountAmount,
      removeSellerDiscountAmount: removeSellerDiscountAmount,

      getSellerDiscountPercent: getSellerDiscountPercent,
      setSellerDiscountPercent: setSellerDiscountPercent,
      removeSellerDiscountPercent: removeSellerDiscountPercent,

      getSellerMinDiscount: getSellerMinDiscount,
      setSellerMinDiscount: setSellerMinDiscount,
      removeSellerMinDiscount: removeSellerMinDiscount,

      getSellerMinCharge: getSellerMinCharge,
      setSellerMinCharge: setSellerMinCharge,
      removeSellerMinCharge: removeSellerMinCharge,

      getOrderTotalAmount: getOrderTotalAmount,
      setOrderTotalAmount: setOrderTotalAmount,
      removeOrderTotalAmount: removeOrderTotalAmount,

      clearSession: clearSession
    };

    return service;

    /**
     * Getting the InHouse id, generally to identify the transaction and
     * search data from the database, such as items on the cart.
     * @function getId
     * @memberof InHouseEntity
     * @returns {integer} - Returns inHouse id used at the inHouse transactions
     */
    function getId() {
      return Session.get(data.session.id);
    }

    /**
     * Saving the inHouse id in the localStorage, this information will be used
     * to identify which transaction the user is executing.
     * @function setId
     * @memberof InHouseEntity
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    }

    /**
     * Removing InHouse id from the localStorage, generally called at the end
     * of a transaction to avoid conflicts with future transactions
     * @memberof InHouseEntity
     * @function removeId
     */
    function removeId() {
      Session.remove(data.session.id);
    }

    /**
     * Getting the InHouse Seller id from the localStorage, this information is
     * used to consulte data from the restaurant, like products available
     * in the InHouse flow for that particular restaurant
     * @memberof InHouseEntity
     * @function getSellerId
     * @returns {string}
     */
    function getSellerId() {
      return Session.get(data.session.sellerId);
    }

    /**
     * Setting the Seller Id of the current transaction, this function
     * generally is called at the beginning of the flow, when the user
     * selected a restaurant after clicking in In House menu
     * @function setSellerId
     * @memberof InHouseEntity
     * @param {integer} sellerId
     */
    function setSellerId(sellerId) {
      Session.save(data.session.sellerId, sellerId);
    }

    /**
     * Removing Seller Id from the localStorage, this function generally is
     * called at the end of the InHouse flow, avoiding inconsistency when
     * the user executes the same flow
     * @function removeSellerId
     * @memberof InHouseEntity
     */
    function removeSellerId() {
      Session.remove(data.session.sellerId);
    }

    function getTableNumber() {
      return Session.get(data.session.tableNumber);
    }
    function setTableNumber(tableNumber) {
      Session.save(data.session.tableNumber, tableNumber);
    }
    function removeTableNumber() {
      Session.remove(data.session.tableNumber);
    }

    function getSellerName() {
      return Session.get(data.session.sellerName);
    }
    function setSellerName(name) {
      Session.save(data.session.sellerName, name);
    }
    function removeSellerName() {
      Session.remove(data.session.sellerName);
    }

    function getSellerMinCharge() {
      return Session.get(data.session.sellerMinCharge);
    }
    function setSellerMinCharge(amount) {
      Session.save(data.session.sellerMinCharge, amount);
    }
    function removeSellerMinCharge() {
      Session.remove(data.session.sellerMinCharge);
    }

    function getSellerMinDiscount() {
      return Session.get(data.session.sellerMinDiscount);
    }
    function setSellerMinDiscount(discount) {
      Session.save(data.session.sellerMinDiscount, discount);
    }
    function removeSellerMinDiscount() {
      Session.remove(data.session.sellerMinDiscount);
    }

    function getSellerDiscountPercent() {
      return Session.get(data.session.sellerDiscountPercent);
    }
    function setSellerDiscountPercent(percent) {
      Session.save(data.session.sellerDiscountPercent, percent);
    }
    function removeSellerDiscountPercent() {
      Session.remove(data.session.sellerDiscountPercent);
    }

    function getSellerDiscountAmount() {
      return Session.get(data.session.sellerDiscountAmount);
    }
    function setSellerDiscountAmount(amount) {
      Session.save(data.session.sellerDiscountAmount, amount);
    }
    function removeSellerDiscountAmount() {
      Session.remove(data.session.sellerDiscountAmount);
    }

    function getOrderTotalAmount() {
      return Session.get(data.session.orderTotalAmount);
    }
    function setOrderTotalAmount(amount) {
      Session.save(data.session.orderTotalAmount, amount);
    }
    function removeOrderTotalAmount() {
      Session.remove(data.session.orderTotalAmount);
    }

    /**
     * Remove all data related to city location in the current session
     * @function clearSession
     * @memberof City
     */
    function clearSession() {
      removeId();
      removeSellerId();
      removeTableNumber();
      removeSellerName();
      removeSellerMinCharge();
      removeSellerMinDiscount();
      removeSellerDiscountPercent();
      removeSellerDiscountAmount();
      removeOrderTotalAmount();
    }
  }
})();

(function() {
  "use strict";

  angular.module("MinkFoodiee").factory("Seller", SellerFactory);

  SellerFactory.$inject = ["Session"];

  function SellerFactory(Session) {
    var data = {
      session: {
        id: "mf_seller_seller_id",
        name: "mf_seller_name",
        type: "mf_seller_veg_type",
        cuisineName: "mf_seller_cuisine_name",
        minDeliveryCharges: "mf_seller_min_delivery_charges",
        minPrice: "mf_seller_min_price",
        deliveryTime: "mf_seller_delivery_time",
        deliveryAvailable: "mf_seller_delivery_available",
        orderAvailable: "mf_seller_open_or_closed",
        tableBookOption: "mf_seller_reserve_table",
        vatPercent: "mf_seller_vat_percent",
        servicePercent: "mf_seller_service_percent",
        rate: "mf_seller_rate",
        coupon: "mf_seller_coupon",
        menu: "mf_seller_active_menu",
        SurgeIndication: "mf_seller_surge_indication",
        SurgeMode: "mf_seller_surge_mode",
        SurgePricePercent: "mf_seller_surge_percent",
        Images: "mf_seller_images",
        UserCanAddProduct: "mf_seller_user_can_add",
        attendanceTime: {
          from: "mf_seller_attendance_time_from",
          to: "mf_seller_attendance_time_to"
        }
      }
    };

    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getCuisineName: getCuisineName,
      setCuisineName: setCuisineName,
      removeCuisineName: removeCuisineName,

      getName: getName,
      setName: setName,
      removeName: removeName,

      getType: getType,
      setType: setType,
      removeType: removeType,

      getMinDeliveryCharges: getMinDeliveryCharges,
      setMinDeliveryCharges: setMinDeliveryCharges,
      removeMinDeliveryCharges: removeMinDeliveryCharges,

      getMinPrice: getMinPrice,
      setMinPrice: setMinPrice,
      removeMinPrice: removeMinPrice,

      getDeliveryTime: getDeliveryTime,
      setDeliveryTime: setDeliveryTime,
      removeDeliveryTime: removeDeliveryTime,

      isDeliveryAvailable: isDeliveryAvailable,
      setDeliveryAvailable: setDeliveryAvailable,
      removeDeliveryAvailable: removeDeliveryAvailable,

      getAttendanceTime: getAttendanceTime,
      setAttendanceTime: setAttendanceTime,
      removeAttendanceTime: removeAttendanceTime,
      extractAttendanceTime: extractAttendanceTime,

      getOrderAvailable: getOrderAvailable,
      setOrderAvailable: setOrderAvailable,
      removeOrderAvailable: removeOrderAvailable,

      getTableBookOption: getTableBookOption,
      setTableBookOption: setTableBookOption,
      removeTableBookOption: removeTableBookOption,

      getVatPercent: getVatPercent,
      setVatPercent: setVatPercent,
      removeVatPercent: removeVatPercent,

      getServicePercent: getServicePercent,
      setServicePercent: setServicePercent,
      removeServicePercent: removeServicePercent,

      getRating: getRating,
      setRating: setRating,
      removeRating: removeRating,

      getCouponExist: getCouponExist,
      setCouponExist: setCouponExist,
      removeCouponExist: removeCouponExist,

      getActiveMenuList: getActiveMenuList,
      setActiveMenuList: setActiveMenuList,
      removeActiveMenuList: removeActiveMenuList,

      getSurgeIndication: getSurgeIndication,
      setSurgeIndication: setSurgeIndication,
      removeSurgeIndication: removeSurgeIndication,

      getSurgeMode: getSurgeMode,
      setSurgeMode: setSurgeMode,
      removeSurgeMode: removeSurgeMode,

      getSurgePricePercent: getSurgePricePercent,
      setSurgePricePercent: setSurgePricePercent,
      removeSurgePricePercent: removeSurgePricePercent,

      getImages: getImages,
      setImages: setImages,
      removeImages: removeImages,

      getUserCanAddProduct: getUserCanAddProduct,
      setUserCanAddProduct: setUserCanAddProduct,
      removeUserCanAddProduct: removeUserCanAddProduct,

      clearSession: clearSession
    };

    return service;

    /**
     * @private {member}
     * @function convertAttendanceTime
     * @description  Getting the response of the server with many properties, figuring out which day is today
     * and returning an object with just the beginning and the end values of the today attendance time
     * @param {object} data - Response coming from the server
     * @return {object} - Formatted date with from and to properties
     */
    function convertAttendanceTime(data) {
      // Figuring out which day is today
      var currentDate = new Date();
      var today = currentDate.getDay();

      if (today == 0) return formatFromTo(data.sunTimeFrom, data.sunTimeTo);
      if (today == 1) return formatFromTo(data.monTimeFrom, data.monTimeTo);
      if (today == 2) return formatFromTo(data.tueTimeFrom, data.tueTimeTo);
      if (today == 3) return formatFromTo(data.wedTimeFrom, data.wedTimeTo);
      if (today == 4) return formatFromTo(data.thuTimeFrom, data.thuTimeTo);
      if (today == 5) return formatFromTo(data.friTimeFrom, data.friTimeTo);
      if (today == 6) return formatFromTo(data.satTimeFrom, data.satTimeTo);
    }

    /**
     * @private {member}
     * @function formatFromTo
     * @description Avoiding boilerplate to format the response, putting into a pattern that
     * could be changed easily
     * @param {string} dateFrom
     * @param {string} dateTo
     * @returns {object}
     */
    function formatFromTo(dateFrom, dateTo) {
      return {
        from: dateFrom,
        to: dateTo
      };
    }

    /**
     * Getting the id of the seller, generally when the user is at the restaurant page
     * @function getId
     * @memberof Seller
     * @returns {string}
     */
    function getId() {
      return Session.get(data.session.id);
    }

    /**
     * Setting the seller id, generally when the user is gonna be redirected to
     * the restaurant screen
     * @memberof Seller
     * @function setId
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    }

    /**
     * Removing seller id, this information is dynamic, it's highly recommended to
     * call this method when the information it's not been used anymore
     * @memberof Seller
     * @function removeId
     */
    function removeId() {
      Session.remove(data.session.id);
    }

    /**
     * Getting the cuisine name of the seller, which means the type of the cuisine,
     * the restaurant specialities, like Chinese, South Indian, and so on
     * @memberof Seller
     * @function getCuisineName
     * @returns {string}
     */
    function getCuisineName() {
      return Session.get(data.session.cuisineName);
    }

    /**
     * Setting the cuisine name, generally when the user is gonna be redirected to
     * the restaurant screen
     * @memberof Seller
     * @function setCuisineName
     * @returns {string}
     */
    function setCuisineName(name) {
      Session.save(data.session.cuisineName, name);
    }

    /**
     * Removing the cuisine name, this value is dynicamic, so this method should be
     * called whenever the user has a potential change of changing the current restaurant,
     * going to a restaurant list or clicking on In House modal for example
     * @memberof Seller
     * @function removeCuisineName
     */
    function removeCuisineName() {
      Session.remove(data.session.cuisineName);
    }

    /**
     * Setting if this restaurant is currently accepting delivery
     * @memberof Seller
     * @function setDeliveryAvailable
     * @param {boolean} available
     */
    function setDeliveryAvailable(available) {
      Session.save(data.session.deliveryAvailable, available);
    }

    /**
     * Checking in the localStorage if the restaurant is available for delivery
     * @memberof Seller
     * @function isDeliveryAvailable
     * @returns {string}
     */
    function isDeliveryAvailable() {
      return Session.get(data.session.deliveryAvailable);
    }

    /**
     * Removing the key that tells if a restaurant is available for delivery
     * from the localStorage, this is recommended when the user is gonna access
     * a screen that potentially will make him go to another restaurant
     * @memberof Seller
     * @function removeDeliveryAvailable
     */
    function removeDeliveryAvailable() {
      Session.remove(data.session.deliveryAvailable);
    }

    /**
     * Getting the attendance time of the restaurant, this will be used to
     * decide if the restaurant can or cannot receive order at that day and time
     * @memberof Seller
     * @function getAttendanceTime
     * @returns {string}
     */
    function getAttendanceTime() {
      return {
        from: Session.get(data.session.attendanceTime.from),
        to: Session.get(data.session.attendanceTime.to)
      };
    }

    /**
     * Setting the time the restaurant is opened, this will be considered to
     * accept or not orders at that time
     * @memberof Seller
     * @function setAttendanceTime
     * @param {object} time
     */
    function setAttendanceTime(time) {
      Session.save(data.session.attendanceTime.from, time.from);
      Session.save(data.session.attendanceTime.to, time.to);
    }

    /**
     * Removing the attendance time of a restaurant when the user is going out
     * of the checkout flow
     * @memberof Seller
     * @function removeAttendanceTime
     */
    function removeAttendanceTime() {
      Session.remove(data.session.attendanceTime.from);
      Session.remove(data.session.attendanceTime.to);
    }

    /**
     * @memberof Seller
     * @function extractAttendanceTime
     * @param {object} data - Response coming from the server with the date unformatted
     * @returns {object} - Today attendance period into an object with "from" and "to" properties
     */
    function extractAttendanceTime(data) {
      // Getting the response that has all days besides other stuffs and extracting the today's data
      var date = convertAttendanceTime(data);
      return date;
    }

    function setOrderAvailable(available) {
      Session.save(data.session.orderAvailable, available);
    }

    function getOrderAvailable() {
      return Session.get(data.session.orderAvailable);
    }

    function removeOrderAvailable() {
      Session.remove(data.session.orderAvailable);
    }

    function setMinPrice(price) {
      Session.save(data.session.minPrice, price);
    }

    function getMinPrice() {
      return Session.get(data.session.minPrice);
    }

    function removeMinPrice() {
      Session.remove(data.session.minPrice);
    }

    function setMinDeliveryCharges(charge) {
      Session.save(data.session.minDeliveryCharges, charge);
    }

    function getMinDeliveryCharges() {
      return Session.get(data.session.minDeliveryCharges);
    }

    function removeMinDeliveryCharges() {
      Session.remove(data.session.minDeliveryCharges);
    }

    function setDeliveryTime(time) {
      Session.save(data.session.deliveryTime, time);
    }

    function getDeliveryTime() {
      return Session.get(data.session.deliveryTime);
    }

    function removeDeliveryTime() {
      Session.remove(data.session.deliveryTime);
    }

    function setName(name) {
      Session.save(data.session.name, name);
    }

    function getName() {
      return Session.get(data.session.name);
    }

    function removeName() {
      Session.remove(data.session.name);
    }

    function setType(type) {
      Session.save(data.session.type, type);
    }

    function getType() {
      return Session.get(data.session.type);
    }

    function removeType() {
      Session.remove(data.session.type);
    }

    function setTableBookOption(option) {
      Session.save(data.session.tableBookOption, option);
    }

    function getTableBookOption() {
      return Session.get(data.session.tableBookOption);
    }

    function removeTableBookOption() {
      Session.remove(data.session.tableBookOption);
    }

    function setTableBookOption(option) {
      Session.save(data.session.tableBookOption, option);
    }

    function getTableBookOption() {
      return Session.get(data.session.tableBookOption);
    }

    function removeTableBookOption() {
      Session.remove(data.session.tableBookOption);
    }

    function setRating(rate) {
      Session.save(data.session.rate, rate);
    }

    function getRating() {
      return Session.get(data.session.rate);
    }

    function removeRating() {
      Session.remove(data.session.rate);
    }

    function setServicePercent(taxPercent) {
      Session.save(data.session.servicePercent, taxPercent);
    }

    function getServicePercent() {
      return Session.get(data.session.servicePercent);
    }

    function removeServicePercent() {
      Session.remove(data.session.servicePercent);
    }

    function setVatPercent(taxPercent) {
      Session.save(data.session.vatPercent, taxPercent);
    }

    function getVatPercent() {
      return Session.get(data.session.vatPercent);
    }

    function removeVatPercent() {
      Session.remove(data.session.vatPercent);
    }

    function setCouponExist(coupon) {
      Session.save(data.session.coupon, coupon);
    }

    function getCouponExist() {
      return Session.get(data.session.coupon);
    }

    function removeCouponExist() {
      Session.remove(data.session.coupon);
    }

    function setActiveMenuList(menu) {
      Session.save(data.session.menu, menu);
    }

    function getActiveMenuList() {
      return Session.get(data.session.menu);
    }

    function removeActiveMenuList() {
      Session.remove(data.session.menu);
    }

    function setSurgeIndication(value) {
      Session.save(data.session.surgeIndication, value);
    }

    function getSurgeIndication() {
      return Session.get(data.session.surgeIndication);
    }

    function removeSurgeIndication() {
      Session.remove(data.session.surgeIndication);
    }

    function setSurgeMode(value) {
      Session.save(data.session.SurgeMode, value);
    }

    function getSurgeMode() {
      return Session.get(data.session.SurgeMode);
    }

    function removeSurgeMode() {
      Session.remove(data.session.SurgeMode);
    }

    function setSurgePricePercent(value) {
      Session.save(data.session.SurgePricePercent, value);
    }

    function getSurgePricePercent() {
      return Session.get(data.session.SurgePricePercent);
    }

    function removeSurgePricePercent() {
      Session.remove(data.session.SurgePricePercent);
    }

    function setImages(value) {
      Session.save(data.session.Images, value);
    }

    function getImages() {
      return Session.get(data.session.Images);
    }

    function removeImages() {
      Session.remove(data.session.Images);
    }

    function setUserCanAddProduct(value) {
      Session.save(data.session.UserCanAddProduct, value);
    }

    function getUserCanAddProduct() {
      return Session.get(data.session.UserCanAddProduct);
    }

    function removeUserCanAddProduct() {
      Session.remove(data.session.UserCanAddProduct);
    }

    /**
     * Remove all data related to Seller in the current session
     * @function clearSession
     * @memberof Seller
     */
    function clearSession() {
      removeDeliveryAvailable();
      removeAttendanceTime();
      removeOrderAvailable();
      removeMinDeliveryCharges();
      removeName();
      removeType();
      removeMinPrice();
      removeDeliveryTime();
      removeTableBookOption();
      removeRating();
      removeServicePercent();
      removeVatPercent();
      removeActiveMenuList();
      removeCouponExist();
      removeImages();
      removeId();
      removeSurgeIndication();
      removeSurgeMode();
      removeSurgePricePercent();
      removeUserCanAddProduct();
    }
  }
})();

(function() {
  "use strict";

  angular.module("MinkFoodiee").factory("$Session", factory);

  factory.$inject = ["City", "Area", "User", "Order", "Seller", "SellerInhouse", "SellerBar"];

  function factory(City, Area, User, Order, Seller, SellerInhouse, SellerBar) {

    return {
      user: User,
      order: Order,
      city: City,
      area: Area,
      seller: Seller,
      inHouse: SellerInhouse,
      inHouseBar: SellerBar,
      kill: kill
    };

    function kill() {
        // user session
        User.clearSession();
  
        // city location session
        Area.clearSession();
  
        // area location session
        City.clearSession();
  
        // seller session
        Seller.clearSession();
  
        // order session
        Order.clearSession();
  
        // inhouse session
        SellerInhouse.clearSession();
    };
  }
})();


(function () {
    "use strict";
  
    angular.module("MinkFoodiee").service("Session", service);
  
    service.$inject = ["localStorageService"];
  
    function service(localStorageService) {
  
      this.get = get;
      this.save = save;
      this.remove = remove;
  
      function checkValue(value) {
        if (!checkParam(value)) return "";
        return value;
      }
  
      function checkParam(param) {
        if (typeof param !== "undefined" && param.toString() !== "undefined")
          return true;
        else return false;
      }
  
      function get(name) {
        if (checkParam(name)) return localStorageService.get(name);
      }
  
      function save(name, value) {
        if (!checkParam(name)) return false;
        value = checkValue(value);
        localStorageService.set(name, value);
      }
  
      function remove(name) {
        if (checkParam(name)) localStorageService.remove(name);
      }
    }
  })();
(function () {
  "use strict";

  angular.module("MinkFoodiee").factory("User", UserFactory);

  UserFactory.$inject = ["Session"];

  function UserFactory(Session) {

    var data = {
      session: {
        id: "mf_user_id",
        accessToken: "mf_fb_user_access_token",
        name: "mf_user_name",
        email: "mf_user_email",
        address: "mf_user_address",
        address2: "mf_user_address_2",
        city: "mf_user_city",
        area: "mf_user_area",
        mobile: "mf_user_mobile",
        image: "mf_user_image",
        pin: "mf_user_pin",
        deviceToken: "mf_user_device_token",
        canOrderCOD: "mf_user_can_order"
      }
    };

    var service = {
      getAccessToken: getAccessToken,
      setAccessToken: setAccessToken,
      removeAccessToken: removeAccessToken,

      getId: getId,
      setId: setId,
      removeId: removeId,

      getName: getName,
      setName: setName,
      removeName: removeName,

      getEmail: getEmail,
      setEmail: setEmail,
      removeEmail: removeEmail,

      getAddress: getAddress,
      setAddress: setAddress,
      removeAddress: removeAddress,

      getAddress2: getAddress2,
      setAddress2: setAddress2,
      removeAddress2: removeAddress2,

      getCity: getCity,
      setCity: setCity,
      removeCity: removeCity,

      getArea: getArea,
      setArea: setArea,
      removeArea: removeArea,

      getMobile: getMobile,
      setMobile: setMobile,
      removeMobile: removeMobile,

      getPin: getPin,
      setPin: setPin,
      removePin: removePin,

      getImage: getImage,
      setImage: setImage,
      removeImage: removeImage,

      getUserCanCod: getUserCanCod,
      setUserCanCod: setUserCanCod,
      removeUserCanCod: removeUserCanCod,

      getDeviceToken: getDeviceToken,
      setDeviceToken: setDeviceToken,
      removeDeviceToken: removeDeviceToken,

      isLoggedIn: isLoggedIn,
      isLoggedInFromFacebook: isLoggedInFromFacebook,
      
      setUserInfo: setUserInfo,
      clearSession: clearSession,
    };

    return service;

    // Facebook Part

    /**
     * Getting the Id of the logged user from the localStorage
     * @function getAccessToken
     * @memberof User
     * @returns {accessToken}
     */
    function getAccessToken() {
      return Session.get(data.session.accessToken);
    };

    /**
     * Set userId in the localStorage, generally after the login
     * @function setAccessToken
     * @memberof User
     * @param {integer} token
     */
    function setAccessToken(token) {
      Session.save(data.session.accessToken, token);
    };

    /**
     * Removing userId from the localStorage, generally when the user logout
     * @function removeAccessToken
     * @memberof User
     */
    function removeAccessToken() {
      Session.remove(data.session.accessToken);
    };

    /**
     * Getting the Id of the logged user from the localStorage
     * @function getId
     * @memberof User
     * @returns {string}
     */
    function getId() {
      return Session.get(data.session.id);
    };

    /**
     * Set userId in the localStorage, generally after the login
     * @function setId
     * @memberof User
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    };

    /**
     * Removing userId from the localStorage, generally when the user logout
     * @function removeId
     * @memberof User
     */
    function removeId() {
      Session.remove(data.session.id);
    };

    /**
     * Getting name of the logged user from the localStorage
     * @function getName
     * @memberof User
     * @returns {string}
     */
    function getName() {
      return Session.get(data.session.name);
    };

    /**
     * Setting the user name in the localStorage, generally after login
     * @function
     * @memberof User
     * @param {string} name
     */
    function setName(name) {
      Session.save(data.session.name, name);
    };

    /**
     * Removing the userName from the localStorage, generally after logout
     * @function
     * @memberof User
     */
    function removeName() {
      Session.remove(data.session.name);
    };

    /**
     * Getting the userEmail from the localStorage
     * @function getEmail
     * @memberof User
     * @returns {string}
     */
    function getEmail() {
      return Session.get(data.session.email);
    };

    /**
     * Setting email of the user in the localStorage, generally after login
     * @function setEmail
     * @memberof User
     * @param {string} email
     */
    function setEmail(email) {
      Session.save(data.session.email, email);
    };

    /**
     * Removing email from the localStorage, generally after logout
     * @function removeEmail
     * @memberof User
     */
    function removeEmail() {
      Session.remove(data.session.email);
    };

    /**
     * Getting the main user address from the localStorage
     * @function getAddress
     * @memberof User
     * @returns {string}
     */
    function getAddress() {
      return Session.get(data.session.address);
    };

    /**
     * Setting the user address in the localStorage, generally after login
     * @function setAddress
     * @memberof User
     * @param {string} address
     */
    function setAddress(address) {
      Session.save(data.session.address, address);
    };

    /**
     * Remove main address from the localStorage, generally after logout
     * @function removeAddress
     * @memberof User
     */
    function removeAddress() {
      Session.remove(data.session.address);
    };

    /**
     * Getting the second user address from the localStorage
     * @function getAddress2
     * @memberof User
     * @returns {string}
     */
    function getAddress2() {
      return Session.get(data.session.address2);
    };

    /**
     * Setting the second user address in the localStorage, generally after login
     * @function setAddress2
     * @memberof User
     * @param {string} address
     */
    function setAddress2(address) {
      Session.save(data.session.address2, address);
    };

    /**
     * Remove second address from the localStorage, generally after logout
     * @function removeAddress2
     * @memberof User
     */
    function removeAddress2() {
      Session.remove(data.session.address2);
    };

    /**
     * Getting the user city from the locaStorage
     * @function getCity
     * @memberof User
     * @returns {string}
     */
    function getCity() {
      return Session.get(data.session.city);
    };

    /**
     * Setting the user city in the localStorage, generally after login
     * @function setCity
     * @memberof User
     * @param {string} city
     */
    function setCity(city) {
      Session.save(data.session.city, city);
    };

    /**
     * Removing user city from the localStorage, generally after logout
     * @function removeCity
     * @memberof User
     */
    function removeCity() {
      Session.remove(data.session.city);
    };

    /**
     * Getting user area from the localStorage
     * @function getArea
     * @memberof User
     * @returns {string}
     */
    function getArea() {
      return Session.get(data.session.area);
    };

    /**
     * Setting user area in the localStorage, generally after login
     * @function setArea
     * @memberof User
     * @param {string} area
     */
    function setArea(area) {
      Session.save(data.session.area, area);
    };

    /**
     * Removing user area from the localStorage, generally after logout
     * @function removeArea
     * @memberof User
     */
    function removeArea() {
      Session.remove(data.session.area);
    };

    /**
     * Getting the user mobile number from the localStorage
     * @function getMobile
     * @memberof User
     * @returns {string}
     */
    function getMobile() {
      return Session.get(data.session.mobile);
    };

    /**
     * Set the user mobile number, generally after login
     * @function setMobile
     * @memberof User
     * @param {string} mobile
     */
    function setMobile(mobile) {
      Session.save(data.session.mobile, mobile);
    };

    /**
     * Removing user mobile number from the localStorage, generally after logout
     * @function removeMobile
     * @memberof User
     */
    function removeMobile() {
      Session.remove(data.session.mobile);
    };

    /**
     * Getting path to the user image from the localStorage
     * @function getImage
     * @memberof User
     * @returns {string}
     */
    function getImage() {
      return Session.get(data.session.image);
    };

    /**
     * Set the path of the user image in the localStorage, after login or edit picture
     * @function setImage
     * @memberof User
     * @param {string} image
     */
    function setImage(image) {
      Session.save(data.session.image, image);
    };

    /**
     * Removing path of the user image from the localStorage, generally after logout
     * @function removeImage
     * @memberof User
     */
    function removeImage() {
      Session.remove(data.session.image);
    };

    /**
     * Getting pin code of the user from the localStorage
     * @function getPin
     * @memberof User
     * @returns {string}
     */
    function getPin() {
      return Session.get(data.session.pin);
    };

    /**
     * Setting the user pin code in the localStorage, generally after login
     * @function setPin
     * @memberof User
     * @param {string} pin
     */
    function setPin(pin) {
      Session.save(data.session.pin, pin);
    };

    /**
     * Removing the user pin code from the localStorage, generally after logout
     * @function removePin
     * @memberof User
     */
    function removePin() {
      Session.remove(data.session.pin);
    };

    

        /**
     * Getting deviceToken of the user from the localStorage
     * @function getDeviceToken
     * @memberof User
     * @returns {string}
     */
    function getDeviceToken() {
      return Session.get(data.session.deviceToken);
    };

    /**
     * Setting the user getDeviceToken in the localStorage, generally after login
     * @function setDeviceToken
     * @memberof User
     * @param {string} deviceToken
     */
    function setDeviceToken(deviceToken) {
      Session.save(data.session.deviceToken, deviceToken);
    };

    /**
     * Removing the user deviceToken from the localStorage, generally after disbaling push notification
     * @function deviceToken
     * @memberof User
     */
    function removeDeviceToken() {
      Session.remove(data.session.deviceToken);
    };

    /**
     * Getting pin code of the user from the localStorage
     * @function getUserCanCod
     * @memberof User
     * @returns {string}
     */
    function getUserCanCod() {
      return Session.get(data.session.canOrderCOD);
    }

    /**
     * Setting the user pin code in the localStorage, generally after login
     * @function setUserCanCod
     * @memberof User
     * @param {boolean} canCod
     */
    function setUserCanCod(canCod) {
      Session.save(data.session.canOrderCOD, canCod);
    }

    /**
     * Removing the user pin code from the localStorage, generally after logout
     * @function removeUserCanCod
     * @memberof User
     */
    function removeUserCanCod() {
      Session.remove(data.session.canOrderCOD);
    }

    /**
     * @function isLoggedIn
     * @memberof User
     * @return {boolean}
     */
    function isLoggedIn() {
      return getId() ? true : false;
    };

    /**
     * @function isLoggedInFromFacebook
     * @memberof User
     * @return {boolean}
     */
    function isLoggedInFromFacebook() {
      return getAccessToken() ? true : false;
    };

    /**
     * Setting a bunch of common user configuration in just one shot
     * This function receives a parameter with a key, value structure that
     * allows that interaction with multiple user session values at once
     * @function setUserInfo
     * @memberof User
     * @param {array} list
     * @example
     * $session.user.setUserInfo([
     *   { key: 'name', value: 'John' },
     *   { key: 'email', value: 'example@email.com' }
     * ]);
     */
    function setUserInfo(list) {
      Object.keys(list).forEach(function (key) {
        var value = list[key];
        Session.save(data.session[key], value);
      });
    };

    /**
     * Remove all data related to user in the current session
     * @function clearSession
     * @memberof User
     */
    function clearSession() {
      
      removeId();
      removeName();
      removeEmail();
      removeMobile();
      removeAddress();
      removeAddress2();
      removeImage();
      removeCity();
      removeArea();
      removeUserCanCod();
      removePin();
      removeDeviceToken();

      //user Facebook session
      removeAccessToken();
    }


  }


})();

(function () {
  'use strict';

  angular
    .module('error-content-component-module')
    .directive('ionErrorContent', directive);

  directive.$inject = ['$window'];

  function directive($window) {
    // Usage:
    //     <ion-error-content></ion-error-content>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/error-content/error-content.html',
      link: link,
      restrict: 'EA',
      scope: {
        onRefresh: '&',
        message: '='
      }

      // changed message from @ to = 30-8
    };
    return directive;

    function link(scope, element, attrs) {}
  }

})();

(function () {
    'use strict';

    angular
        .module ('order-history-component-module')
        .directive ('orderHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <order-history></order-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/order-history/order-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('rest-card-component-module')
        .directive('restaurantCard', directive);

    directive.$inject = ["$compile"];

    function directive($compile) {

        /**
         * @Usage 
         *     <restaurant-card 
         *       ng-repeat="restaurant in restaurantsList
         *       restaurant="restaurant"
         *       isLiked="true or false"
         *       on-like="favouriteListChanged(sellerId)"
         *       on-review="showReviewsForThis(restaurant)"
         *       full-view="true or false"
         *       bookTableView="true or false"
         *       favouriteCardView="true or false"
         *     </restaurant-card>
         * 
         */
        var directive = {
            templateUrl: 'app/components/restaurant-card/restaurant-card.html',
            link: link,
            restrict: 'EA',
            scope: {
                restaurant: '=',
                isLiked: '=',
                onLike: '&',
                onReview: '&',
                onQueue: '&',
                favouriteCardView: "=",
                bookTableView: '=',
                fullView: '=',
                qmView: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            // $compile(element.contents())(scope.$new());
            // scope.gotoRestaurant = function(r) {
            //     console.log(r);
            //     $state.go('app.history');
            // }
            if (!attrs.fullView) { attrs.fullView = false; }
            if (!attrs.bookTableView) { attrs.bookTableView = false; }
            if (!attrs.favouriteCardView) { attrs.favouriteCardView = false; }
            if (!attrs.qmView) { attrs.qmView = false; }
        }
    }

})();
(function () {
    'use strict';

    angular
        .module ('qm-history-component-module')
        .directive ('qmHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <qm-history></qm-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/qm-history/qm-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();
(function () {
  'use strict';

  angular
    .module('loading-content-component-module')
    .directive('loadingContent', directive);

  function directive() {
    // Usage:
    //     <loading-content></loading-content>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/loading-content/loading-content.html',
      link: link,
      restrict: 'EA',
      scope: {
        onRefresh: '&'
      }
    };
    return directive;

    function link(scope) {
      scope.loadingItems = [1,2,3,4];
    }
  }

})();

(function() {
    'use strict';

    angular
        .module('ionic')
        .directive('ionSlidesTabs', ionSlidesTabsDirective)
        .directive('ionSlideTabLabel', ionSlideTabLabelDirective);

    ionSlidesTabsDirective.$inject = ['$timeout', '$compile', '$interval', '$ionicSlideBoxDelegate', '$ionicScrollDelegate', '$ionicGesture'];
    ionSlideTabLabelDirective.$inject = [];


    function ionSlidesTabsDirective($timeout, $compile, $interval, $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicGesture) {
        // Usage:
        //     <ion-slides ion-slides-tabs></ion-slides>
        // Creates:
        //
        var directive = {
            require: "^ionSlides",
            link: link,
            restrict: 'A',
            controller: ['$scope', function($scope) {
                this.addTab = function($content) {
                    $timeout(function() {
                        if ($scope.addTabContent) {
                            $scope.addTabContent($content);
                        }
                    });
                }
            }]
        };
        return directive;

        function link(scope, element, attrs, parent) {
            var ionicSlideBoxDelegate;
            var ionicScrollDelegate;
            var ionicScrollDelegateID;
            var slideTabs;
            var indicator;
            var slider;
            var tabsBar;
            var options = {
                "slideTabsScrollable": true
            }

            var init = function() {
                if (angular.isDefined(attrs.slideTabsScrollable) && attrs.slideTabsScrollable === "false") {
                    options.slideTabsScrollable = false;
                }
                var tabItems = '<li ng-repeat="(key, value) in tabs" ng-click="onTabTabbed($event, {{key}})" class="slider-slide-tab" ng-bind-html="value"></li>';
                if (options.slideTabsScrollable) {
                    ionicScrollDelegateID = "ion-slide-tabs-handle-" + Math.floor((Math.random() * 10000) + 1);
                    tabsBar = angular.element('<ion-scroll delegate-handle="' + ionicScrollDelegateID + '" class="slidingTabs" direction="x" scrollbar-x="false"><ul>' + tabItems + '</ul> <div class="tab-indicator-wrapper"><div class="tab-indicator"></div></div> </ion-scroll>');
                } else {
                    tabsBar = angular.element('<div class="slidingTabs"><ul>' + tabItems + '</ul> <div class="tab-indicator-wrapper"><div class="tab-indicator"></div></div> </div>');
                }
                slider = angular.element(element);
                var compiled = $compile(tabsBar);
                slider.parent().prepend(tabsBar);
                compiled(scope);
                indicator = angular.element(tabsBar[0].querySelector(".tab-indicator"));
                var slideHandle = slider.attr('delegate-handle');
                var scrollHandle = tabsBar.attr('delegate-handle');

                if (options.slideTabsScrollable) {
                    ionicScrollDelegate = $ionicScrollDelegate;
                    if (scrollHandle) {
                        ionicScrollDelegate = ionicScrollDelegate.$getByHandle(scrollHandle);
                    }
                }
                addEvents();
                setTabBarWidth();
                slideToCurrentPosition();
            };

            var addEvents = function() {
                scope.$on("$ionicSlides.sliderInitialized", function(event, data) {
                    ionicSlideBoxDelegate = data.slider;
                    ionicSlideBoxDelegate.on('sliderMove', scope.onSlideMove);
                    ionicSlideBoxDelegate.on('onSlideChangeStart', scope.onSlideChange);
                    angular.element(window).on('resize', scope.onResize);
                });
            }

            var setTabBarWidth = function() {
                if (!angular.isDefined(slideTabs) || slideTabs.length == 0) {
                    return false;
                }
                var tabsList = tabsBar.find("ul");
                var tabsWidth = 0;
                angular.forEach(slideTabs, function(currentElement, index) {
                    var currentLi = angular.element(currentElement);
                    tabsWidth += currentLi[0].offsetWidth;
                });
                if (options.slideTabsScrollable) {
                    angular.element(tabsBar[0].querySelector(".scroll")).css("width", tabsWidth + 1 + "px");
                } else {
                    slideTabs.css("width", tabsList[0].offsetWidth / slideTabs.length + "px");
                }
                slideToCurrentPosition();
            };

            var addTabTouchAnimation = function(event, currentElement) {
                var ink = angular.element(currentElement[0].querySelector(".ink"));
                if (!angular.isDefined(ink) || ink.length == 0) {
                    ink = angular.element("<span class='ink'></span>");
                    currentElement.prepend(ink);
                }
                ink.removeClass("animate");
                if (!ink.offsetHeight && !ink.offsetWidth) {

                    var d = Math.max(currentElement[0].offsetWidth, currentElement[0].offsetHeight);
                    ink.css("height", d + "px");
                    ink.css("width", d + "px");
                }
                var x = event.offsetX - ink[0].offsetWidth / 2;
                var y = event.offsetY - ink[0].offsetHeight / 2;
                ink.css("top", y + 'px');
                ink.css("left", x + 'px');
                ink.addClass("animate");
            }

            var slideToCurrentPosition = function() {
                if (!angular.isDefined(slideTabs) || slideTabs.length === 0) {
                    return false;
                }
                var targetSlideIndex = ionicSlideBoxDelegate.activeIndex;
                var targetTab = angular.element(slideTabs[targetSlideIndex]);
                var targetLeftOffset = targetTab.prop("offsetLeft");
                var targetWidth = targetTab[0].offsetWidth;
                indicator.css({
                    "-webkit-transition-duration": "300ms",
                    "-webkit-transform": "translate(" + targetLeftOffset + "px,0px)",
                    "width": targetWidth + "px"
                });
                if (options.slideTabsScrollable && ionicScrollDelegate) {
                    var scrollOffset = Math.round((targetTab.parent().parent().parent()[0].offsetWidth / 2) - (targetWidth / 2));
                    ionicScrollDelegate.scrollTo(targetLeftOffset - scrollOffset, 0, true);
                }
                slideTabs.removeClass("tab-active");
                targetTab.addClass("tab-active");
            };

            var setIndicatorPosition = function(currentSlideIndex, targetSlideIndex, position, slideDirection) {
                var targetTab = angular.element(slideTabs[targetSlideIndex]);
                var currentTab = angular.element(slideTabs[currentSlideIndex]);
                var targetLeftOffset = targetTab.prop("offsetLeft");
                var currentLeftOffset = currentTab.prop("offsetLeft");
                var offsetLeftDiff = Math.abs(targetLeftOffset - currentLeftOffset);
                var slidesCount = ionicSlideBoxDelegate.slides.length;
                if (currentSlideIndex == 0 && targetSlideIndex == slidesCount - 1 && slideDirection == "right" ||
                    targetSlideIndex == 0 && currentSlideIndex == slidesCount - 1 && slideDirection == "left") {
                    return;
                }
                var targetWidth = targetTab[0].offsetWidth;
                var currentWidth = currentTab[0].offsetWidth;
                var widthDiff = targetWidth - currentWidth;
                var indicatorPos = 0;
                var indicatorWidth = 0;
                if (currentSlideIndex > targetSlideIndex) {
                    indicatorPos = targetLeftOffset - (offsetLeftDiff * (position - 1));
                    indicatorWidth = targetWidth - ((widthDiff * (1 - position)));
                } else if (targetSlideIndex > currentSlideIndex) {
                    indicatorPos = targetLeftOffset + (offsetLeftDiff * (position - 1));
                    indicatorWidth = targetWidth + ((widthDiff * (position - 1)));
                }
                indicator.css({
                    "-webkit-transition-duration": "0ms",
                    "-webkit-transform": "translate(" + indicatorPos + "px,0px)",
                    "width": indicatorWidth + "px"
                });
                if (options.slideTabsScrollable && ionicScrollDelegate) {
                    var scrollOffset = Math.round((targetTab.parent().parent().parent()[0].offsetWidth / 2) - (targetWidth / 2));
                    ionicScrollDelegate.scrollTo(indicatorPos - scrollOffset, 0, false);
                }
            }

            scope.onTabTabbed = function(event, index) {
                addTabTouchAnimation(event, angular.element(event.currentTarget));
                ionicSlideBoxDelegate.slideTo(index);
                slideToCurrentPosition();
            }

            scope.tabs = [];

            scope.addTabContent = function($content) {
                scope.tabs.push($content);
                scope.$apply();
                $timeout(function() {
                    slideTabs = angular.element(tabsBar[0].querySelector("ul").querySelectorAll(".slider-slide-tab"));
                    slideToCurrentPosition();
                    setTabBarWidth()
                })
            }

            scope.onResize = function() {
                slideToCurrentPosition();
            }

            scope.onSlideChange = function(slideIndex) {
                slideToCurrentPosition();
            };

            scope.onSlideMove = function() {
                var scrollDiv = ionicSlideBoxDelegate.slides;
                var totalSlides = scrollDiv.length;
                var currentSlideIndex = ionicSlideBoxDelegate.activeIndex;
                var currentSlide = angular.element(scrollDiv[currentSlideIndex]);
                var currentSlideLeftOffset = (((ionicSlideBoxDelegate.progress * (totalSlides - 1)) - currentSlideIndex) * ionicSlideBoxDelegate.size * -1);
                var targetSlideIndex = (currentSlideIndex + 1) % scrollDiv.length;
                if (currentSlideLeftOffset > slider.prop("offsetLeft")) {
                    targetSlideIndex = currentSlideIndex - 1;
                    if (targetSlideIndex < 0) {
                        targetSlideIndex = scrollDiv.length - 1;
                    }
                }
                var targetSlide = angular.element(scrollDiv[targetSlideIndex]);
                var position = currentSlideLeftOffset / slider[0].offsetWidth;
                var slideDirection = position > 0 ? "right" : "left";
                position = Math.abs(position);
                setIndicatorPosition(currentSlideIndex, targetSlideIndex, position, slideDirection);
            };

            init();

        }
    }

    function ionSlideTabLabelDirective() {
        // Usage:
        //     <ion-slide-page ion-slide-tab-label="My Label Name"></ion-slide-page>
        // Creates:
        //
        var directive = {
            require: "^ionSlidesTabs",
            link: link
        };
        return directive;

        function link($scope, $element, $attrs, $parent) {
            $parent.addTab($attrs.ionSlideTabLabel);
        }
    }

})();
(function () {
  'use strict';

  angular
    .module('star-rating-component-module')
    .directive('starRating', directive);

  directive.$inject = ['$window'];

  function directive($window) {
    // Usage:
    //     <star-rating></star-rating>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/star-rating/star-rating.html',
      link: link,
      restrict: 'EA',
      scope: {
        onView: '&',
        rates: '='
      }
    };
    return directive;

    function link(scope, element, attrs) {
      // unwrap the function
      console.log(scope)
      // scope.onView = scope.onView();
    //   element.bind("click",function() {
    //     scope.$apply(function() {
    //       onView(data);    
    //       scope.onView = scope.onView();                    // ...or this way
    //     });
    // });
    }
  }

})();

(function () {
    'use strict';

    angular
        .module ('table-book-history-component-module')
        .directive ('tableBookHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <table-book-history></table-book-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/table-book-history/table-book-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();
(function () {
  'use strict';
  angular
    .module('home-module')
    .controller('homeCTRL', controller)

  controller.$inject = ['$scope', '$Easyajax', '$Session', '$state', 'Loading', 'ionicToast'];

  function controller($scope, $Easyajax, $Session, $state, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

        var $citylist = $("#cityList");
        var $cityArea = $("#cityArea");

      $scope.$on('$ionicView.enter', function (e) {
        registerDeviceToken();
        getCityList();
        $Session.city.removeId();
        $Session.area.removeId();
        $Session.city.removeName();
        $Session.area.removeName();
      });

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: '1'
        };
        var callback = function (data) {
          console.log(data);
        }
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      //get city list
      function getCityList() {
        Loading.show();
        var callback = function (res) {
          var citylistObj = res.cityList;
          $citylist.empty().append($("<option selected ></option>").val("-9999").html("Please select city"));
          $cityArea.empty().append($("<option value='-9999'></option>").val("-9999").html("Select your delivery area"));

          for (var i in citylistObj) {
            var cityID = citylistObj[i].id;
            var cityName = citylistObj[i].cityName;
            $citylist.append($("<option></option>").val(cityID).html(cityName));
          }
          Loading.hide();
          getCityAreaList();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: cityListAPI,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      //get city area list
      function getCityAreaList() {

        $citylist.change(function () {
          Loading.show();
          var cityId = $citylist.val();
          var callback = function (res) {
            var cityAreaList = res.cityAreaList;
            if (res.status == "success") {
              $cityArea.empty().append($("<option value='-9999'></option>").val("-9999").html("Select your delivery area"));
              for (var i in cityAreaList) {
                var cityID = cityAreaList[i].id;
                var cityName = cityAreaList[i].cityArea;
                $cityArea.append($("<option></option>").val(cityID).html(cityName));
              }
              Loading.hide();
            } else {
              $cityArea.empty().append($("<option value='0'></option>").val("-9999").html("Make sure you selected a city name"));
              Loading.hide();
            }
          };
          var config = {
            typeRequest: "GET",
            urlRequest: cityAreaListAPI,
            dataRequest: {
              cityId: cityId
            },
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        });
      }

      var valid = function () {
        var cityId = $("#cityList").val();
        var cityArea = $("#cityArea").val();
        if (cityId == "-9999") {
          ionicToast.show('Please select city!', 1500, '#b50905');
          return false;
        }
        if (cityArea == "-9999" && cityId != "-9999") {
          ionicToast.show('Please select city area!', 1500, '#b50905');
          return false;
        }
        return true;
      };
      $scope.findRest = function () {

        if (valid()) {
          var cityId = $("#cityList").val();
          var cityArea = $("#cityArea").val();

          var cityText = $("#cityList option:selected").text();
          var cityAreaText = $("#cityArea option:selected").text();

          $Session.city.setId(cityId);
          $Session.area.setId(cityArea);

          $Session.city.setName(cityText);
          $Session.area.setName(cityAreaText);

          $state.go('app.restList');
        }
      };

    }
  }
})();

(function () {
  'use strict';

  angular
    .module('home-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('home', {
        url: '/home',
        templateUrl: 'app/pages/Home/home.html',
        controller: 'homeCTRL',
        controllerAs: 'vm'
    });
  }

}());

(function () {
  'use strict';

  angular
    .module('login-module')
    .controller('LoginCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$ionicPopup', '$state', '$Easyajax', 'ionicToast', 'Facebook', 'Loading'];

  function controller($scope, $Session, $ionicPopup, $state, $Easyajax, ionicToast, Facebook, Loading) {
    /* jshint validthis:true */
    var vm = this;
    $scope.resetPasswordData = {};
    $scope.loginData = {};


    activate();

    function activate() {

      /************************
       * Login configs
       ************************/

      $scope.loginConfigs = {
        rules: {
          emailId: {
            required: true,
            emailOrMobile: true
          },
          passwordId: {
            required: true
          }
        },
        // validation configs - error messages
        messages: {
          emailId: {
            required: "Please enter your registered email or mobile number",
            emailOrMobile: "Please enter valid email or 10 digits mobile number"
          },
          passwordId: {
            required: "Enter password"
          }
        }
      };

      function setUserInfoSignin(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
        $state.go('home');
      }

      vm.doLogin = function (form) {
        if (form.validate()) {
          Loading.show();
          var params = {
            emailId: $scope.loginData.email,
            passwordId: $scope.loginData.password
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              $scope.loginData = {};
              setUserInfoSignin(data);
            } else {
              ionicToast.show(data.reason, 3500, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: loginAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      vm.goCreateAccount = function () {
        $state.go('signup');
      }
      
      /************************
       * Forget Password popup 
       ************************/
      vm.showResetpasswordPopup = function () {
        
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="email" autofocus name="resetEmail" id="resetEmail" ng-model="resetPasswordData.email" placeholder="Email"/>',
          title: 'Reset Password',
          subTitle: 'Please enter your email..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel',
              type: 'button-default',
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.resetPasswordData.email || !/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test($scope.resetPasswordData.email)) {
                  e.preventDefault();
                  ionicToast.show('Enter a valid email Address', 63000, '#b50905');
                } else {
                  doresetPassword();
                }
              }
            }
          ]
        });
      };

      function doresetPassword() {
        Loading.show();
        var params = {
          emailId: $scope.resetPasswordData.email
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('New Password has been sent to your email address!', 3500, '#26A65B') : ionicToast.show(data.reason, 3500, '#b50905');
        };
        var config = {
          typeRequest: "POST",
          urlRequest: forgetPasswordAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };



      // Login with facebook logic 














    }
  }
})();

(function () {
  'use strict';

  angular
    .module('login-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/pages/Login/login.html',
        controller: 'LoginCTRL',
        controllerAs: 'vm'
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('notification-module')
    .controller('notificationCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;
    $scope.errorText = '';
    $scope.fenotification = true;


    activate();

    function activate() {

      
      getNotificationlist();

      $scope.doRefresh = function () {
        getNotificationlist();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getNotificationlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.fenotification = false;
          $scope.errorText = "No Notifications! You are not logged in";
          return false;
        }
        Loading.show();
        var params = {
          user_id: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            $scope.fenotification = true;
            $scope.unreadnotificationList = data.activeNoti;
            $scope.notificationList = data.inActiveNoti;
          } else {
            $scope.fenotification = false;
            $scope.errorText = "No Notifications here!";
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: notificationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.removeFromUnread = function (notificationId) {
        Loading.show();
        var params = {
          wnId: notificationId
        };
        var callback = function (data) {
          if (data.status == "success") {
            getNotificationlist();
            ionicToast.show('Marked as read!', 3000, '#26A65B')
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "POST",
          urlRequest: notificationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('notification-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.notification', {
        url: '/notification',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Notification/notification.html',
            controller: 'notificationCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('signup-module')
    .controller('signupCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$ionicPopup', '$state', '$Easyajax', 'Loading', 'ionicToast', 'Facebook'];

  function controller($scope, $Session, $ionicPopup, $state, $Easyajax, Loading, ionicToast, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    $scope.signupData = {};
    $scope.otpData = {};

    activate();

    function activate() {

      /************************
       * Signup configs
       ************************/
      $scope.signupConfigs = {
        // validation configs - rules
        rules: {
          firstName: {
            required: true,
            string: true
          },
          lastName: {
            required: true,
            string: true
          },
          emailIdup: {
            required: true,
            validEmail: true
          },
          passwordup: {
            required: true,
            strongPassword: true
          },
          confirmPassword: {
            equalTo: "#passwordup",
            required: true
          },
          mobileNo: {
            required: true,
            indianMobNumber: true
          },
        },
        // validation configs - error messages
        messages: {
          firstName: {
            required: "First name is required",
            string: "First name should contain only letters"
          },
          lastName: {
            required: "Last name is required",
            string: "Last name should contain only letters"
          },
          emailIdup: {
            required: "Email address is required",
            validEmail: "Valid email address is required"
          },
          passwordup: {
            required: "Create your password",
            strongPassword: "Enter password with 8 to 12 length, should contain at least one letter and one number"
          },
          confirmPassword: {
            equalTo: "Passwords are not matched!",
            required: "Confirm password is required"
          },
          mobileNo: {
            required: "Mobile number is required",
            indianMobNumber: "Enter valid 8 or 10 digits mobile number"
          },
        }
      };

      vm.doSignup = function(form) {
        if (form.validate()) {
          Loading.show();
          var params = {
            event: "send_signup_otp",
            mobile_number: $scope.signupData.mobile,
            emailId: $scope.signupData.email
          };
          var callback = function (data) {
            Loading.hide();
            data.status === 'success' ? showSignupOTPPopup() : ionicToast.show(data.reason, 3500, '#b50905');
          };
          var config = {
            typeRequest: "POST",
            urlRequest: sendOtpApi,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      /************************
       * Signup OTP popup 
       ************************/
       function showSignupOTPPopup() {
        
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="number" name="otpCode" id="otpCode" ng-model="otpData.code" placeholder="OTP Code" required/>',
          title: 'Complete Registration',
          subTitle: 'Please enter 4 digits OTP code sent to your mobile',
          cssClass: "signupOTPPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.otpData.code || !/^\d{4}$/.test($scope.otpData.code)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid 4 digits OTP', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  verifyAndCreateAccount();
                }
              }
            }
          ]
        });
      };

       function verifyAndCreateAccount() {
        Loading.show();
        var params = {
          fromApp: "1",
          emailId: $scope.signupData.email,
          mobileNo: $scope.signupData.mobile,
          password: $scope.signupData.password,
          firstName: $scope.signupData.firstName,
          lastName: $scope.signupData.lastName,
          passcode: $scope.otpData.code
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            setUserInfoSignup(data);
          } else {
            ionicToast.show(data.reason, 3500, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userRegisterAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function setUserInfoSignup(data) {
        $Session.user.setUserInfo({
          id: data.userId,
          name: $scope.signupData.firstName + ' ' + $scope.signupData.lastName,
          image: data.userImage,
          mobile: $scope.signupData.mobile,
          address: data.address,
          email: $scope.signupData.email,
          city: data.cityName,
          area: data.cityArea,
          pin: data.pincode
        });
        $scope.signupData = {};
        $scope.otpData = {};
        $state.go('home');
      }

      vm.goLogin = function() {
        $state.go('login');
      }



    }
  }
})();

(function () {
  'use strict';

  angular
    .module('signup-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/pages/Signup/signup.html',
        controller: 'signupCTRL',
        controllerAs: 'vm'
      });
  }

}());

(function () {
  "use strict";

  angular.module("splash-module").controller("splashCTRL", controller);

  controller.$inject = ["$scope", "$Session", "$state", "$timeout", "$Easyajax", "md5"];

  function controller($scope, $Session, $state, $timeout, $Easyajax, md5) {

    activate();

    function activate() {
      $scope.$on("$ionicView.enter", function (e) {
        getTokenValue();
        $timeout(function () {
          testUser();
        }, 1800);
      });

      function getTokenValue() {
        var callback = function (data) {
          if (data.status == "failure") {
            console.error("Error getting tokenInfoAPI");
          } else {
            var day = parseInt(data.day);
            var month = parseInt(data.month);
            var year = parseInt(data.year);
            var token = day * 33 + month * 67 + year * 9;
            var t = token.toString();
            var hashedToken = md5.createHash(t);
            var Oldtoken = localStorage.getItem("Token");
            if (Oldtoken !== hashedToken) {
              localStorage.setItem("Token", hashedToken);
              console.log("NEW Token has been generated: " + hashedToken);
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: tokenInfoAPI,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: "1"
        };
        var callback = function (data) {
          console.log(data);
        };
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function testUser() {
        var userId = $Session.user.getId();
        var cityId = $Session.city.getId();
        var cityArea = $Session.area.getId();
        if (cityId != null && cityArea != null) {
          console.log("Redirecting to Restaurants list... ");
          registerDeviceToken();
          $state.go("app.restList");
        } else if (userId === "" || userId == null || userId === "0") {
          console.log("Redirecting to Login... ");
          $state.go("welcome");
        } else {
          console.log("Redirecting to Location select... ");
          registerDeviceToken();
          $state.go("home");
        }
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('splash-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('index', {
        url: '/index',
        templateUrl: 'app/pages/Splash/splash.html',
        controller: 'splashCTRL'
    });
  }

}());

(function () {
  "use strict";

  angular
    .module("welcome-module")
    .controller("welcomeCTRL", WelcomeController);

  WelcomeController.$inject = ["$scope", "$Session", "$state", "$Easyajax", "Facebook", "Loading", "ionicToast"];

  function WelcomeController($scope, $Session, $state, $Easyajax, Facebook, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

      vm.options = {
        loop: true,
        speed: 300
      };

      vm.loginAsGuest = function () {
        $Session.user.setUserInfo({
          id: "0",
          name: "Guest",
          image: "",
          mobile: "",
          address: "",
          address2: "",
          email: "",
          city: "",
          area: "",
          pin: ""
        });
        $state.go("home");
      }

      //This method is executed when the user press the "Login with facebook" button
      vm.facebookSignIn = function() {
        Facebook.login(fbLoginSuccess, fbLoginError);
      }

      function fbLoginSuccess(response) {
        var authResponse = response.authResponse;
        if (!authResponse) {
          alert("Auth response is not defined");
          return false;
        }
        Facebook.getFacebookProfileInfo(authResponse).then(
          function (profileInfo) {
            //Save user information in Session Entity
            $Session.user.setAccessToken(authResponse.accessToken);
            $Session.user.setId(profileInfo.id);
            $Session.user.setName(profileInfo.first_name);
            $Session.user.setEmail(profileInfo.email);
            $Session.user.setImage(profileInfo.picture.data.url);
            doFaceBookLogin();
          },
          function (err) {
            console.log(JSON.stringify(err));
          }
        );
      }

      function fbLoginError(error) {
        // ionicToast.show("Our servers are currenly busy! Try Login again", 3500, '#b50905');
        //Here you should handle if user signed in with different user. Check the response from FB Devs
        console.log(JSON.stringify(error));
        Loading.hide();
      }

      function setUserInfoSignin(data) {
        $Session.user.setUserInfo({
          id: data.userId,
          name: data.firstName,
          // image: data.userImage,
          mobile: data.mobileNumber,
          address: data.address,
          address2: data.shipAddress,
          email: data.email,
          city: data.cityName,
          area: data.cityArea,
          pin: data.pincode
        });
        $state.go("home");
      }

      function doFaceBookLogin() {
        Loading.show();
        var deviceToken = $Session.user.getDeviceToken();
        var fbId = $Session.user.getId();
        var fbToken = $Session.user.getAccessToken();
        var emailId = $Session.user.getEmail();
        var fname = $Session.user.getName();
        var params = {
          fbId: fbId,
          fbToken: fbToken,
          emailId: emailId,
          password: "Dd123456",
          firstName: fname,
          lastName: "",
          countryCode: "+91",
          deviceToken: deviceToken,
          deviceType: 1
        };
        var callback = function (res) {
          console.log(res);
          Loading.hide();
          if (res.status == "success") {
            setUserInfoSignin(res.userDetail);
          } else {
            ionicToast.show(JSON.stringify(res), 3000, "#42b");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: LoginWithFacebookAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



    }
  }
})();

(function () {
  'use strict';

  angular
    .module('welcome-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'app/pages/Welcome/welcome.html',
        controller: 'welcomeCTRL',
        controllerAs: 'vm'
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('order-history-details-module')
    .controller('OrderHistoryDetailsCTRL', HistoryDetailsController)

    HistoryDetailsController.$inject = ['$scope', '$Session', '$Easyajax', 'Loading', '$stateParams', 'ionicToast', '$ionicPopup'];

  function HistoryDetailsController($scope, $Session, $Easyajax, Loading, $stateParams, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    $scope.orderisCancelable = false;
    $scope.restName = '';

    activate();

    function activate() {

      getOrderDetails();

      function getOrderDetails() {
        Loading.show();
        var params = {
          orderId: $stateParams.orderId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            $scope.orderDetails = data.orderListAndDetail.orderDetail;
            $scope.orderDetailsTaxes = data.orderListAndDetail.adTaxList;
            $scope.cartList = data.orderListAndDetail.orderList;
            $scope.listToppings = data.orderListAndDetail.topping;
            $scope.restName = data.orderListAndDetail.restaurantName;
            
            ($scope.orderDetails.deliveryType == "cod") ? $scope.orderDetails.deliveryType = "Cash On Delivery" : $scope.orderDetails.deliveryType = "Online";
            ($scope.orderDetails.review == "no") ? $scope.orderDetails.review = "Order has not been reviewed" : $scope.orderDetails.review = "Order has been reviewed";
            ($scope.orderDetails.status.toLocaleLowerCase() == 'pending') ? $scope.orderisCancelable = true : $scope.orderisCancelable = false;


          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: oredrHistoryDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


      $scope.sendEmail = function () {
        Loading.show();
        var params = {
          orderId: $stateParams.orderId
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('Receipt sent to your email!', 3000, '#68a581') : ionicToast.show('Sorry! Please try to send again', 3000, '#33cd5f');
        };
        var config = {
          typeRequest: "GET",
          urlRequest: SendReceiptViaEmailAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.cancelOrder = function () {
        $scope.cancelOrderData = {};
        $ionicPopup.show({
          template: '<label class="item item-input item-floating-label"><textarea ng-model="cancelOrderData.message" placeholder="Why you want to cancel your order ..?" rows="4" cols="50"></textarea></label>',
          title: 'Cancel Order',
          subTitle: 'Please mention cancellation reason',
          cssClass: 'cancel-order',
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-assertive',
              onTap: function (e) {
                if (!$scope.cancelOrderData.message) {
                  e.preventDefault();
                  ionicToast.show('Please mention cancellation reason!', 3000, '#b50905');
                } else {
                  cancelMyOrder();
                }
              }
            }
          ]
        });
      }

      function cancelMyOrder() {
        if(userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          orderId: $stateParams.orderId,
          uId: userId,
          event: "1",
          reason: $scope.cancelOrderData.message
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('Order cancelled!', 3000, '#33cd5f') : ionicToast.show('Could not cancel order!', 3000, '#b50905');
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cancelOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      

    }
  }
})();

(function () {
  'use strict';

  angular
    .module('order-history-details-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('app.orderHistory', {
      url: '/history/:orderId',
      views: {
        'menuContent': {
          templateUrl: 'app/pages/History/order-history-details/order-history-details.html',
          controller: 'OrderHistoryDetailsCTRL',
          controllerAs: 'vm'
        }
      }
    })
  }

}());

(function () {
  'use strict';

  angular
    .module('order-history-list-module')
    .controller('OrderHistoryListCTRL', HistoryController)

  HistoryController.$inject = ['$scope', '$Session', '$Easyajax', 'Loading'];

  function HistoryController($scope, $Session, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    vm.errorMessage = 'No active orders!';

    activate();

    function activate() {


      // Calls
      getAllHistories();


      $scope.doRefreshOrder = function () {
        getHistoryList();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.doRefreshBarOrder = function () {
        getInhouseBarHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.doRefreshBarExOrder = function () {
        getInhouseBarExHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getAllHistories() {
        getHistoryList();
        getInhouseBarHistory();
        getInhouseBarExHistory();
      }

      function getHistoryList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          vm.errorMessage = 'Please login to check the order history!';
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            $scope.orderListOld = data.oldOrderList;
            $scope.orderListNew = data.newOrderList;
            $scope.orderListInhouse = data.completeInhouseList;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: orderHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function getInhouseBarHistory() {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          userId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListInhouseBar = data.listBars || {};
          for (var i in $scope.orderListInhouseBar) {
            var objectElement = $scope.orderListInhouseBar[i];
            switch (objectElement.barStatus) {
              case 0:
                objectElement.orderStatus = "Not Paid";
                break;
              case 1:
                objectElement.orderStatus = "Paid by myself";
                break;
              case 2:
                objectElement.orderStatus = "Paid Cash to restaurant";
                break;
              case 3:
                objectElement.orderStatus = "Choose Online";
                break;
              case 4:
                objectElement.orderStatus = "Paid Online";
                break;
              case 5:
                objectElement.orderStatus = "Paid by Bill Split";
                break;
              default:
                console.log('Sorry, we are out of ' + objectElement.barStatus + '.');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getInhouseBarExHistory() {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          userId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListInhouseBarEx = data.listBars;
          for (var i in $scope.orderListInhouseBarEx) {
            var objectElement = $scope.orderListInhouseBarEx[i];
            if (objectElement.barStatus == 0) {
              objectElement.orderStatus = "Not Paid";
            } else if (objectElement.barStatus == 1) {
              objectElement.orderStatus = "Paid by myself";
            } else if (objectElement.barStatus == 2) {
              objectElement.orderStatus = "Paid Cash to restaurant";
            } else if (objectElement.barStatus == 3) {
              objectElement.orderStatus = "Choose Online";
            } else if (objectElement.barStatus == 4) {
              objectElement.orderStatus = "Paid Online";
            } else if (objectElement.barStatus == 5) {
              objectElement.orderStatus = "Paid by Bill Split";
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('order-history-list-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.history', {
        url: '/history',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/History/order-history-list/order-history-list.html',
            controller: 'OrderHistoryListCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('inhouse-module')
    .controller('InhouseFoodBarListCTRL', controller);

  controller.$inject = ['$scope', '$Session', '$state', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var cityId = $Session.city.getId() ||"0" ;
    var areaId = $Session.area.getId() ||"0" ;
    var userId = $Session.user.getId() ||"0" ;

    activate();

    ////////////////

    function activate() {




      // Calls
      getInhouseRestList();
      getBarList();
      getBarExchangeList();


      $scope.onRefreshInhouseFood = function () {
        getInhouseRestList();
        $scope.$broadcast('scroll.refreshComplete');
      };
      $scope.onRefreshInhouseBar = function () {
        getBarList();
        $scope.$broadcast('scroll.refreshComplete');
      };
      $scope.onRefreshInhouseBarEx = function () {
        getBarExchangeList();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getInhouseRestList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sKey: "IHO",
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function getBarList() {
        Loading.show();
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getBarExchangeList() {
        Loading.show();
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barExList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.openBarReq = function () {
        $state.go("app.inhouseBarRequest");
      }
      $scope.openBarExReq = function () {
        $state.go("app.inhouseExBarRequest");
      }


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/about-minkfoodiee/about-minkfoodiee.html'
          }
        }
      })
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('AddressesCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    
    $scope.addres1tesxt = $Session.user.getAddress();
    $scope.addres2tesxt = $Session.user.getAddress2();


    $scope.idAddressII = false;
    $scope.isAddressI = false;
    $scope.newAddressData = {
      address: $Session.user.getAddress(),
      address2: $Session.user.getAddress2()
    };
    activate();

    function activate() {




      /********************************************************************************
       * @name update 
       * @type {Address}
       *******************************************************************************/
      
      $scope.updateAddress = function () {

        $ionicPopup.show({
          templateUrl: 'app/partials/addresses-popup.html',
          title: 'Update Addresses',
          // subTitle: 'Please enter your address..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newAddressData.address) {
                  //don't allow the user to close unless he enters address
                  ionicToast.show('Make sure you entered your address', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  updateAddress();
                }
              }
            }
          ]
        });
      }

      function updateAddress() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          name: $Session.user.getName(),
          address: $scope.newAddressData.address,
          address2: $scope.newAddressData.address2,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show("Address Updated!", 3000, '#249E21');
            getUserDetails();
            $scope.addres1tesxt = $scope.newAddressData.address;
            $scope.addres2tesxt = $scope.newAddressData.address2;
          } else {
            ionicToast.show(data.message, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userUpdateDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (res) {
          Loading.hide();
          $scope.userDetail = res.userDetail;
          $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res) ;
          var image = $Session.user.getImage();
          if (image == "" || image =="http://minkchatter.com/mink-chatter-images/app/uploads/") {
            $('#userProfileImage').attr('src', 'assets/img/profile.png');
          } else {
            $('#userProfileImage').attr('src', image);
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.addresses', {
        url: '/addresses',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/addresses/addresses.html',
            controller: 'AddressesCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('ChangePasswordCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Session', '$Easyajax', 'Loading', 'ionicToast'];

  function controller($scope, $state, $Session, $Easyajax, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    activate();

    function activate() {

      $scope.isFacebookUser = $Session.user.isLoggedInFromFacebook();

      $scope.changePasswordData = {};
      $scope.changePasswordConfigs = {
        rules: {
          oldPassword: {
            required: true,
          },
          newPassword: {
            notEqualTo: "#oldPassword",
            required: true,
            strongPassword: true
          },
          confirmNewPassword: {
            equalTo: "#newPassword",
            required: true
          }
        },
        // validation configs - error messages
        messages: {
          oldPassword: {
            required: "Please enter your old password",
          },
          newPassword: {
            notEqualTo: "Please Enter a new password different than your old password",
            required: "New password is required",
            strongPassword: "Please enter password with length 8 to 12 characters, should contain at least one letter and one number"
          },
          confirmNewPassword: {
            equalTo: "Passwords are not matched!",
            required: "Confirm new password is required"
          }
        }
      };

      $scope.changePassword = function (form) {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          Loading.show();
          var params;

          if($scope.isFacebookUser) {
            params = {
              oldPass: "Dd123456",
              newPass: $scope.changePasswordData.newPassword,
              uId: userId
            }
          } else {
            params = {
              oldPass: $scope.changePasswordData.oldPassword,
              newPass: $scope.changePasswordData.newPassword,
              uId: userId
            };
          }

          var callback = function (res) {
            Loading.hide();
            if (res.status == "success") {
              $scope.changePasswordData = {};
              $Session.kill();
              $state.go("welcome");
              ionicToast.show(res.reason, 3000, '#249E21');
            } else {
              ionicToast.show(res.reason, 3000, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userChangePasswordAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.changePassword', {
        url: '/changePassword',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/change-password/change-password.html',
            controller: 'ChangePasswordCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.privacy', {
        url: '/privacy',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/privacy/privacy.html'
          }
        }
      })
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('ProfileCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup', 'Facebook'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;

    activate();

    function activate() {

      
      getUserDetails();

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (res) {
          Loading.hide();

          $scope.userDetail = res.userDetail;
          $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res) ;

          var image = $Session.user.getImage();
          if (image == "" || image =="http://minkchatter.com/mink-chatter-images/app/uploads/") {
            $('#userProfileImage').attr('src', 'assets/img/profile.png');
          } else {
            $('#userProfileImage').attr('src', image);
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



      $scope.gotoFavourites = function () {
        $state.go('app.favourites');
      }
      $scope.gotoHistory = function () {
        $state.go('app.history');
      }
      $scope.gotoAddresses = function() {
        $state.go('app.addresses');
      }
      $scope.aboutus = function () {
        $state.go('app.about');
      }
      $scope.terms = function () {
        $state.go('app.terms');
      }
      $scope.privacy = function () {
        $state.go('app.privacy');
      }
      $scope.faq = function () {
        $state.go('app.faq');
      }
      $scope.gotoQMPrefrences = function() {
        $state.go('app.queue-preferences');
      }
      vm.gotoSettings = function() {
        $state.go("app.settings");
      }


      /********************************************************************************
       * @name update 
       * @type {Logout}
       *******************************************************************************/
      $scope.logout = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Mink Foodiee',
          template: 'Are you sure you want to logout?',
          cssClass: "resetPasswordPopup",
          cancelText: 'No, Stay',
          cancelType: 'button-balanced',
          okText: 'I am sure',
          okType: 'button-default'
        });
        confirmPopup.then(function (res) {
          if (res) {
            doLogout();
          }
        });
      }

      function doLogout() {
        $Session.user.isLoggedInFromFacebook() ? Facebook.logout(doLogoutCallback, doLogoutCallback) : doLogoutCallback() ;
      }

      function doLogoutCallback() {
        $Session.kill();
        $state.go("welcome");
      }




      /********************************************************************************
       * @name update 
       * @type {Username}
       *******************************************************************************/
      $scope.addUserName = function () {
        $scope.newNameData = {
          username: $Session.user.getName()
        };
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="text" name="username" ng-model="newNameData.username" placeholder="New name..." autofocus/>',
          title: 'Update Name',
          subTitle: 'Please enter your name..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                //don't allow the user to close unless he enters valid name
                if (!$scope.newNameData.username || !/^[^-\s][a-zA-Z_\s-]+$/.test($scope.newNameData.username)) {
                  ionicToast.show('Enter a valid name without special characters', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newNameData.username == $Session.user.getName()) {
                  ionicToast.show('Make sure new name is diffrent than old!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setUsername();
                }
              }
            }
          ]
        });
      }

      function setUsername() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          name: $scope.newNameData.username,
          address: $Session.user.getAddress(),
          address2: $Session.user.getAddress2(),
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show("Username updated!", 3000, '#249E21');
            getUserDetails();
          } else {
            ionicToast.show(data.message, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userUpdateDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }






      //   $scope.showImagePreview = function (input) {
      //     alert("hola");
      //     var userImg = document.getElementById("userProfilePic").files[0];

      //     var token = localStorage.getItem("Token");
      //     var reader = new FileReader();

      //     reader.onload = function (e) {
      //       $('#imguser').attr('src', e.target.result);
      //     };
      //     reader.readAsDataURL(input.files[0]);

      //     var formdata = new FormData();
      //     formdata.append('image', input.files[0]);
      //     formdata.append('uId', uid);
      //     formdata.append("event", "UploadImage");
      //     formdata.append("Token", token);

      //     console.log(formdata);

      //     $.ajax({
      //       url: userUpdateImageAPI,
      //       headers: {
      //         'contentType': 'application/x-www-form-urlencoded',
      //         'username': 'foodiee',
      //         'password': 'MTIzNDU='
      //       },
      //       data: formdata,
      //       processData: false,
      //       contentType: false,
      //       type: 'POST',
      //       dataType: 'json',
      //       success: function (data) {
      //         console.log(data);
      //         if (data.status === "success") {

      //           console.info("success");
      //         } else {

      //           console.info("failure");
      //         }
      //       },
      //       error: function (xhr, ajaxOptions, thrownError) {
      //         console.error(xhr.status);
      //         console.error(thrownError);
      //       }
      //     });


      //   }







    }
  }
})();

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/profile/profile.html',
            controller: 'ProfileCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('SettingsCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    $scope.langToggle = {
      DE: true
    }
    activate();

    function activate() {

      $scope.changePassword = function () {
        $state.go('app.changePassword');
      }

      $scope.togglePushNotification = function(scope) {
        console.log(scope);
        console.log($scope.langToggle.DE);

        if($scope.langToggle.DE) {
          registerDeviceToken();
        } else {
          ionicToast.show('Push notification disabled!', 3000, '#b50905');
        }
      }

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: '1'
        };
        var callback = function (data) {
          console.log(data);
          ionicToast.show('Push notification enabled!', 3000, '#b50905');
        }
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      /********************************************************************************
       * @name update 
       * @type {Email Address}
       *******************************************************************************/
      $scope.addEmailAddress = function () {
        $scope.newEmailData = {};
        $ionicPopup.show({
          template: '<input type="email" name="resetEmail" ng-model="newEmailData.email" placeholder="New Email..." autofocus/>',
          title: 'Update Email',
          subTitle: 'Please enter your email!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newEmailData.email || !/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test($scope.newEmailData.email)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid email address', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newEmailData.email == $Session.user.getEmail()) {
                  ionicToast.show('Make sure new email is diffrent than old one!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setEmailAddress();
                }
              }
            }
          ]
        });
      }

      function setEmailAddress() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          emailId: $scope.newEmailData.email,
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show('OTP sent to ' + $scope.newEmailData.email, 3000, '#42b');
            showOTPemail();
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateEmailAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


      function showOTPemail() {
        $scope.dataMobileOTP = {};
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="emailOTP" placeholder="OTP Code..."  ng-model="dataMobileOTP.emailOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verif your new email!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-positive',
              onTap: function (e) {
                e.preventDefault();
                if (!$scope.dataMobileOTP.emailOTP || !/^\d{4}$/.test($scope.dataMobileOTP.emailOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                } else {
                  vfEmail();
                }
              }
            }
          ]
        });

        function vfEmail() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          Loading.show();
          var params = {
            emailId: $scope.newEmailData.email,
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.emailOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your email updated succesfully!", 3000, '#42b');
              myPopup.close();
              //getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateEmailAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };


      /********************************************************************************
       * @name update 
       * @type {Mobile Number}
       *******************************************************************************/
      $scope.addMobileNumber = function () {
        $scope.newMobileData = {};
        $ionicPopup.show({
          template: '<input type="number" name="resetEmail" ng-model="newMobileData.number" placeholder="New mobile number..." autofocus />',
          title: 'Update Mobile Number',
          subTitle: 'Please enter your new mobile number!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newMobileData.number || !/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test($scope.newMobileData.number)) {
                  // don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid mobile number', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newMobileData.number == $Session.user.getMobile()) {
                  ionicToast.show('Make sure new mobile number is diffrent than old number!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setMobileNumber();
                }
              }
            }
          ]
        });
      };

      function setMobileNumber() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          mobileNum: $scope.newMobileData.number,
          countryCode: "91",
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            showOTPMobile();
            ionicToast.show('Passcode sent to ' + $scope.newMobileData.number, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateMobileAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function showOTPMobile() {
        $scope.dataMobileOTP = {};
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="mobileOTP" placeholder="OTP Code..." ng-model="dataMobileOTP.mobileOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verify your new mobile number',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-positive',
              onTap: function (e) {
                if (!$scope.dataMobileOTP.mobileOTP || !/^\d{4}$/.test($scope.dataMobileOTP.mobileOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  vfMobile();
                }
              }
            }
          ]
        });

        function vfMobile() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          var Elnew = $scope.newMobileData.number;
          Loading.show();
          var params = {
            mobileNum: Elnew,
            countryCode: "91",
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.mobileOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your mobile number updated succesfully!", 3000, '#249E21');
              myPopup.close();
              // getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateMobileAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };




      // $scope.gotoAboutUs = function () {
      //   $state.go('app.aboutus');
      // }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/settings/settings.html',
            controller: 'SettingsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.terms', {
        url: '/terms',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/terms/terms.html'
          }
        }
      })
  }

}());

(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.faq', {
        url: '/faq',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/faq/faq.html'
          }
        }
      })
  }

}());

(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('queueManagerCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$ionicPopup', '$Session', '$Easyajax', '$ionicModal', '$Geolocation', 'ionicToast', '$state'];

  function controller($scope, Loading, $ionicPopup, $Session, $Easyajax, $ionicModal, $Geolocation, ionicToast, $state) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    vm.errorMessage = 'No restaurant found in your nearest location for Queue Manager';
    vm.errorMessageHistory = 'There is no active requests';
    $scope.feQMRest = true;
    $scope.inputs = [{
      value: ""
    }];


    activate();

    function getloactionForQM(latitude, longitude) {
      // lat: "30.642550",
      // log: "76.817336"
      Loading.show();

      // UNCOMMENT TO REAL QM
      var params = {
          lat: latitude,
          log: longitude
      };
      // var params = {
      //   lat: "30.642550",
      //   log: "76.817336"
      // };
      var callback = function (data) {
        Loading.hide();
        if (data.status == "success") {
          $scope.feQMRest = true;
          $scope.restaurantList = data.restaurantList;
        } else {
          $scope.feQMRest = false;
        }
      }
      var config = {
        typeRequest: "POST",
        urlRequest: restaurantQMListAPI,
        dataRequest: params,
        sucesCallbackRequest: callback
      }
      $Easyajax.startRequest(config);
    }


    function activate() {


      $Geolocation.requestGeolocation(getloactionForQM);
      // getloactionForQM();
      getQueueHistory();


      $scope.doRefresh = function () {
        $Geolocation.requestGeolocation(getloactionForQM);
        // getloactionForQM();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.doRefreshHistory = function () {
        getQueueHistory();
        $scope.$broadcast('scroll.refreshComplete');
      };

      // start queue prefrences popup
      $ionicModal.fromTemplateUrl('app/partials/queue-request-popup.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeQueue = function () {
        $scope.modal.hide();
      };
      $scope.openQueue = function () {
        $scope.modal.show();
      };


      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.reviewModal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.reviewModal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.reviewModal.show();
      };
      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.openPrefQueue = function() {
        $state.go("app.queue-preferences")
      }


      $scope.getInQueue = function (queueSId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          // Check before create order and send to backend without userId 
          var confirmPopup = $ionicPopup.confirm({
            title: 'Mink Foodiee',
            template: 'Please Login or Create Account to get in the queue!',
            cancelText: 'Later',
            cancelType: 'button-default',
            okText: 'Login',
            okType: 'button-balanced'
          });
          confirmPopup.then(function (res) {
            if (res) {
              $state.go('login');
            }
          });
          return false;
        }
        getUserDetails();
        $scope.openQueue();
        $scope.queueRestId = queueSId;
      }


      $scope.addInput = function () {
        $scope.inputs.push({
          value: ""
        });
      }
      $scope.removeInput = function (index) {
        $scope.inputs.splice(index, 1);
      }

      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.queueRequestData.userName = data.userDetail.firstName;
          $scope.queueRequestData.phoneNumber = data.userDetail.mobileNumber;
          $scope.queueRequestData.emailId = data.userDetail.email;
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.queueRequestData = {};
      $scope.queueRequestConfigs = {
        rules: {
          userName: {
            required: true,
            string: true
          },
          emailId: {
            required: true,
            validEmail: true
          },
          phoneNumber: {
            required: true,
            indianMobNumber: true
          },
          noOfEntry: {
            required: true,
            queuePeople: true
          }
        },
        messages: {
          userName: {
            required: "Please enter your name",
            string: "Your name should contain only letters"
          },
          emailId: {
            required: "Email address is required",
            validEmail: "Please enter a valid email"
          },
          phoneNumber: {
            required: "Mobile number is required",
            indianMobNumber: "Make sure you entered 10 digits mobile number"
          },
          noOfEntry: {
            required: "Please enter the number of people",
            queuePeople: "Make sure number of people is between 1-30 people"
          }
        }
      };
      $scope.confirmQueueRequest = function (form) {
        var guestNumbersArr = [];
        var guestNumbers = '';
        $scope.inputs.map(function(item) {
          guestNumbersArr.push("guest^" + item['value'])
        });
        guestNumbers = guestNumbersArr.join('@');
        if (form.validate()) {
          Loading.show();
          var params = {
            sid: $scope.queueRestId,
            uid: userId,
            name: $scope.queueRequestData.userName,
            email: $scope.queueRequestData.emailId,
            mobile: $scope.queueRequestData.phoneNumber,
            numofentries: $scope.queueRequestData.noOfEntry,
            guest: guestNumbers
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              $scope.closeQueue();
              $scope.queueRequestData = {};
              ionicToast.show('Your request has been sent successfully!', 3000, '#26A65B');
              $scope.doRefreshHistory();
            } else {
              ionicToast.show(data.reason, 3000, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: QManagerAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      
      $scope.cancelQueue = function (queueId) {
        Loading.show();
        var params = {
          uId: userId,
          queueId: queueId,
          reason: "I want to cancel my queue order",
          status: "4"
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            getQueueHistory();
            ionicToast.show('Queue order has been cancelled successfully ', 3000, '#42b');
          } else {
            ionicToast.show('Something wrong happened.. Try to cancel again', 3000, '#42b');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: QManagerCancelRequestAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getQueueHistory() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          vm.errorMessageHistory = 'Please login to check queue history!';
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListNew = data.newQueue;
          $scope.orderListOld = data.oldQueue;
          var startDay = moment().format('DD-MMM-YYYY hh:mm: A');
          var endDay = moment().subtract(2, 'days').endOf('day').format('DD-MMM-YYYY hh:mm: A');
          if (data.status == "success") {
            for (var i in $scope.orderListOld) {
              var status = moment($scope.orderListOld.QueueDateTime).isBetween(endDay, startDay);
              if (!status) {
                $scope.orderListOld[i].queueStatus = "Cancelled";
              }
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userQMListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }






    }
  }
})();

(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queuemanager', {
        url: '/queuemanager',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-manager-list/queue-manager-list.html',
            controller: 'queueManagerCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('queueMenuListController', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', '$ionicPopup', 'menuList', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, menuList, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    var sellerId = $Session.seller.getId();
    $scope.$on('$ionicView.enter', function (e) {

      $scope.menu = menuList;

    });


    activate();

    function activate() {

      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      }
      
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      }

    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('queue-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.queueMenuList', {
        url: '/queueMenuList',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-menu-list/queue-menu-list.html',
            controller: 'queueMenuListController',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('QueuePreferencesCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicModal', 'Facebook'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicModal, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;
    $scope.errorText = "";
    $scope.fePref = false;
    activate();

    function activate() {

      getQueueManagerPrefernces();

      $scope.doRefresh = function () {
        getQueueManagerPrefernces();
        $scope.$broadcast('scroll.refreshComplete');
      };

      // start queue prefrences popup
      $ionicModal.fromTemplateUrl('app/partials/queue-prefrences-popup.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.prefModal = modal;
      });
      $scope.closePrefQueue = function () {
        $scope.prefModal.hide();
        getQueueManagerPrefernces();
      };
      $scope.openPrefQueue = function () {
        $scope.prefModal.show();
      };




      function getQueueManagerPrefernces() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          evt: "1",
          mb: $Session.user.getMobile()
        };
        var callback = function (res) {
          Loading.hide();
          if(res.status == 'success') {
            $scope.answers = JSON.parse(res.guestp);
            $scope.fePref = true;
            console.log( $scope.answers);
            $scope.$apply();
          } else {
            $scope.fePref = false;
            $scope.errorText = "No prefrences yet";
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: QMPrefrencesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      } 


      $scope.confirmQueuePrefRequest = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your preferences', 3000, '#b50905');
          return false;
        }

        var qaArr = [];
        var qaArrStr = ''
        for(var j in $scope.answers) {
          var q = $scope.answers[j].pQuestion;
          var a = $scope.answers[j].pAnswer;
          qaArr.push(q + "^" + a + "@");
        }
        qaArrStr = qaArr.join("");
        var qaString = qaArrStr.substring(0, qaArrStr.lastIndexOf("@"));
        Loading.show();
        var params = {
          qa: qaString,
          mb: $Session.user.getMobile().substring(3),
          evt: "3"
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status !== "failure") {
            $scope.closePrefQueue();
            ionicToast.show('Success!', 5000, '#26A65B');
          } else {
            ionicToast.show('Failed to save!', 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: QMPrefrencesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
        
      };


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queue-preferences', {
        url: '/queue-preferences',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-preferences/queue-preferences.html',
            controller: 'QueuePreferencesCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('queueRestDetailsCTRL', controller)

  controller.$inject = ['$scope', '$state', '$stateParams', 'Loading', '$Easyajax', '$ionicModal', '$Session'];

  function controller($scope, $state, $stateParams, Loading, $Easyajax, $ionicModal, $Session) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $stateParams.sellerId;
    $scope.sellerId = $stateParams.sellerId;

    $scope.feRestaurantDetails = true;
    $scope.options = {
      loop: true,
      effect: 'fade',
      speed: 500,
    }

    // Review Modal
    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function (modal) {
        $scope.reviewModal = modal;
      });
    $scope.closeReviewsList = function () {
      $scope.reviewModal.hide();
    };
    $scope.openReviewsList = function () {
      $scope.reviewModal.show();
    };

    activate();

    function activate() {

      $scope.doRefresh = function () {
        getRestDetails();
        // $Geolocation.requestGeolocation(getloactionForQM);
        $scope.$broadcast('scroll.refreshComplete');
      };
      

      //calls
      getRestDetails()


      function getRestDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (res) {
          Loading.hide();
          if(res.status == "failure") {
            $scope.feRestaurantDetails = false;
            return;
          }
          $scope.restaurantDetail = res.restaurantDetail;
          $scope.feRestaurantDetails = true;
          $scope.menuCodOnline = res.menuCodOnline;
          $scope.menuPreorder = res.menuPreorder;
          $scope.menuTakeaway = res.menuTakeaway;
          $scope.listMenuTR = res.listMenuTR;
          $scope.ourServices = res.restaurantDetail.ourServices;
          var aa = res.deliveryAreas[0].cityAreas;
          $scope.deliveryAreas = aa.replace(/\s\s+/g, ' ').substr(aa.indexOf(",")+1).split(',');
          // update qm seller entity
          $Session.seller.setActiveMenuList($scope.menuCodOnline);

          setTodayTiming(res.restaurantDetail.currentDay);
        }
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $(".item.mon").addClass("active-day");
            break;
          case "Tus":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            $(".item.mon").addClass("active-day");
            break;
        }
      }


      // Toggle Restaurant Delivery Area
      $scope.shownGroup = null; // set to null to make it hidden at first time
      $scope.toggleGroup = function (group) {
        $scope.isGroupShown(group) ? ($scope.shownGroup = null) : ($scope.shownGroup = group);
      };
      $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
      };

      // Toggle Restaurant timings (show more details)
      $scope.shownItem = null; // set to null to make it hidden at first time
      $scope.toggleItem = function (item) {
        $scope.isItemShown(item) ? ($scope.shownItem = null) : ($scope.shownItem = item);
      };
      $scope.isItemShown = function (item) {
        return $scope.shownItem === item;
      };


      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.viewMenu = function () {
        $state.go("app.queueMenuList");
      };


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queuerestdetails', {
        url: '/queuerestdetails/:sellerId',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-rest-details/queue-rest-details.html',
            controller: 'queueRestDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('cartCTRL', cartController)

  cartController.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast', '$ionicModal', '$ionicPopup'];

  function cartController($scope, Loading, $Session, $Easyajax, $state, ionicToast, $ionicModal, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $Session.seller.getId();
    var userId = $Session.user.getId() || "0";

    vm.errorMessage = 'There is no items in cart! Go back and add more items from our delicious menu!';
    $scope.feCart = false;

    //checkout OTP Modal configs
    $ionicModal.fromTemplateUrl('app/partials/toppings-popup.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modalTopping = modal;
    });
    $scope.closeTopping = function () {
      $scope.modalTopping.hide();
    };
    $scope.openTopping = function () {
      $scope.modalTopping.show();
    };

    activate();

    function activate() {

      $scope.doRefreshCart = function () {
        getCartList();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.$on('$ionicView.enter', function () {
        getCartList();
      });
      

      function getCartList() {
        // if (userId === "0") {
        //   console.log("Guest User mode is ON!");
        //   return false;
        // }
        var mif = $Session.order.getMenuItemType();
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: mif
        };
        var callback = function (data) {
          Loading.hide();
          $scope.topplis = [];
          
          if (data.status == "success") {
            $scope.feCart = true;
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            $scope.listToppings = data.cartlist.listToppings;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
          } else {
            $scope.feCart = false;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getToppingCommentAndAppend() {
        Loading.show();
        var params = {
          cartId: $scope.itemIdForTopping
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getToppingCommentAndAppend ", data);
        }
        var config = {
          typeRequest: "GET",
          urlRequest: listToppingByIdAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.addCommentTopping = function (item) {
        $scope.comment = {
          commentOnItem: item.commentOnFood
        };
        $scope.itemIdForTopping = item.cartId;
        Loading.show();
        var params = {
          cartId: item.cartId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openTopping();
          if (data.status == "failure") {
            $scope.feTopping = false;
          } else {
            $scope.feTopping = true;
            $scope.toppingList = data.toppingList;
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: listToppingByIdAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.submitToppingComment = function () {
        $scope.toppingValues = [];
        angular.forEach($scope.toppingList, function (topping) {
          if (!!topping.selected) {
            $scope.toppingValues.push(topping.toppingId);
          }
        });
        var toppingList = $scope.toppingValues.join(",");

        //check if to submit toppings with comments, otherwise submit comment only 
        //change event value 0 or 1
        if ($scope.feTopping) {
          console.log("Submit when feTopping is ", $scope.feTopping);
          Loading.show();
          var params = {
            cartId: $scope.itemIdForTopping,
            event: "1",
            toppingList: toppingList,
            commentOnfood: $scope.comment.commentOnItem,
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              $scope.closeTopping();
              ionicToast.show('Topping added successfully!', 3000, '#ef473a');
            } else {
              ionicToast.show('Please select at least one topping!', 3000, '#ef473a');
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: toppingCommentAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        } else {
          console.log("Submit when feTopping is ", $scope.feTopping);
          Loading.show();
          var params = {
            cartId: $scope.itemIdForTopping,
            event: "0",
            commentOnfood: $scope.comment.commentOnItem
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              //getToppingCommentAndAppend();
              $scope.closeTopping();
              ionicToast.show('Comment added successfully!', 3000, '#ef473a');
            } else {
              ionicToast.show('Please add comment on your food item!', 3000, '#ef473a');
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: toppingCommentAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
        getCartList();
      }

      $scope.deleteCartItem = function(itemId) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Mink Foodiee',
          template: 'Do you want to remove this item?',
          cancelText: 'No',
          cancelType: 'button-default',
          okText: 'Yes',
          okType: 'button-balanced'
        });
        confirmPopup.then(function (res) {
          if(res) {
            $scope.incDecItems(itemId, 2);
          } 
        });
      }

      $scope.incDecItems = function (oid, type) {
        // if (userId === "0") {
        //   console.log("Guest User mode is ON!");
        //   return false;
        // }
        var mif = $Session.order.getMenuItemType();
        Loading.show();
        var params = {
          uId: userId,
          oId: oid,
          calcType: type,
          menuItemFor: mif
        };
        var callback = function (data) {
          getCartList();
        }
        var config = {
          typeRequest: "POST",
          urlRequest: userCartItemIncDecAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.applyPromoCode = function() {
        // TODO: Apply promo code here
      }

      $scope.gotoPayment = function () {
        var minDCharge = parseFloat($Session.order.getMinDeliveryCharges());
        var totAmount = parseFloat($Session.order.getTotalAmount());
        console.log("minDCharge is ", minDCharge, ", and totAmount is ", totAmount);
        totAmount >= minDCharge ? $state.go('app.checkout') : ionicToast.show('Minimum amount not reached', 3000, '#42b');
      }



    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.cart', {
        url: '/cart',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/cart/cart.html',
            controller: 'cartCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function() {
  "use strict";

  angular.module("restaurant-list-module").controller("checkoutCTRL", checkoutController);

  checkoutController.$inject = ["$scope", "$Session", "$Easyajax", "$state", "Loading", "$ionicPopup", "ionicToast", "$rootScope", "$ionicHistory"];

  function checkoutController($scope, $Session, $Easyajax, $state, Loading, $ionicPopup, ionicToast, $rootScope, $ionicHistory) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $Session.seller.getId();
    var userId = $Session.user.getId() || "0";
    var cityId = $Session.city.getId();
    var areaId = $Session.area.getId();
    var paymentVariable = "";


    $scope.mifd = $Session.order.getMenuItemType();
    $scope.commonComment = "";
    $scope.errorText = "";
    $scope.feCart = false;
    $scope.checkoutData = {
      userdate: new Date(),
      usertime: new Date(),
      username: $Session.user.getName(),
      usermobile: $Session.user.getMobile(),
      useraddress: $Session.user.getAddress(),
      useraddressII: $Session.user.getAddress2(),
      userlocation: $Session.city.getName() + ", " + $Session.area.getName()
    }

    $scope.langToggle = {
      EN: true,
      FR: false,
      DE: false,
      SP: false
    }


    function updateMobileNumberFirst() {

    }

    activate();

    function activate() {
      // $rootScope.dateValue = new Date();
      // $rootScope.datetimeValue = new Date();
      
      // $scope.mifd = mifd;

      $scope.$on("$ionicView.enter", function() {
        $scope.mifd = $Session.order.getMenuItemType();
        if($scope.mifd == '1') {
          paymentVariable = "cod";
          $scope.langToggle = {
            EN: true,
            FR: false,
            DE: false,
            SP: false
          }
        } else if ($scope.mifd == '2') {
          paymentVariable = "OnlinePreOrder";
          $scope.langToggle = {
            EN: false,
            FR: false,
            DE: true,
            SP: false
          }
        } else if($scope.mifd == '3') {
          paymentVariable = "online";
          $scope.langToggle = {
            EN: false,
            FR: false,
            DE: false,
            SP: true
          }
        }
        
        $scope.checkoutData = {
          userdate: new Date(),
          usertime: new Date(),
          username: $Session.user.getName(),
          usermobile: $Session.user.getMobile(),
          useraddress: $Session.user.getAddress(),
          useraddressII: $Session.user.getAddress2(),
          userlocation: $Session.city.getName() + ", " +  $Session.area.getName()
        }
        getCartList();

        if($Session.user.getMobile() === '' || $Session.user.getMobile() != $scope.checkoutData.usermobile) {
          ionicToast.show("Please verify your mobile number !", 3000, "#A71212");
          $scope.addMobileNumber()
        } 
      });
  
      $scope.doRefresh = function () {
        getCartList();
        $scope.$broadcast('scroll.refreshComplete');
      };



      /********************************************************************************
       * @name update 
       * @type {Mobile Number}
       *******************************************************************************/
      $scope.addMobileNumber = function () {
        $scope.newMobileData = {};
        $ionicPopup.show({
          template: '<input type="number" name="resetEmail" ng-model="newMobileData.number" placeholder="Mobile number..." autofocus />',
          title: 'Update Mobile Number',
          subTitle: 'Please enter your mobile number!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newMobileData.number || !/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test($scope.newMobileData.number)) {
                  // don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid mobile number', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setMobileNumber();
                }
              }
            }
          ]
        });
      };

      function setMobileNumber() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          mobileNum: $scope.newMobileData.number,
          countryCode: "91",
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            showOTPMobile();
            ionicToast.show('Passcode sent to ' + $scope.newMobileData.number, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateMobileAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function showOTPMobile() {
        $scope.dataMobileOTP = {};
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="mobileOTP" placeholder="OTP Code..." ng-model="dataMobileOTP.mobileOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verify your new mobile number',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.dataMobileOTP.mobileOTP || !/^\d{4}$/.test($scope.dataMobileOTP.mobileOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  vfMobile();
                }
              }
            }
          ]
        });

        function vfMobile() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          Loading.show();
          var params = {
            mobileNum: $scope.newMobileData.number,
            countryCode: "91",
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.mobileOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your mobile number updated succesfully!", 3000, '#249E21');
              $scope.checkoutData.usermobile = $scope.newMobileData.number;
              $Session.user.setMobile($scope.newMobileData.number);
              myPopup.close();
              // getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateMobileAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };


      // Calls
      getCartList();

      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: $scope.mifd
        };
        var callback = function(data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.feCart = true;
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
          } else {
            $scope.feCart = false;
            $scope.errorText = "Your cart is empty! Go back and add some items";
            $Session.order.setTotalAmount("0");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function submitCommonComment() {
        if ($scope.commonComment === "") {
          console.log("common comment is empty :)");
          return;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          comments: $scope.commonComment,
          menuItem: $scope.mifd
        };
        var callback = function(data) {
          if (data.status == "success") {
            $scope.commonComment = "";
          } else {
            console.log("Common Comment Failed to add ");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: commonCommentAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function unFocusElement() {
        angular.element('#commonComment').blur()
        // document.getElementById("commonComment").disabled = true;    
        // document.getElementById("commonComment").disabled = false;
      }


      $scope.payCash = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.EN = true;
        scope.langToggle.FR = false;
        unFocusElement()
      };
      $scope.payOnline = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.EN = false;
        scope.langToggle.FR = true;
        unFocusElement()
      };
      $scope.prePayOnline = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.DE = true;
        unFocusElement()
      };
      $scope.payOnlineTakeaway = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.SP = true;
        unFocusElement()
      };

      $scope.checkoutConfigs = {
        rules: {
          nameid: {
            required: true
          },
          mobileid: {
            required: true,
            indianMobNumber: true
          },
          addressid: {
            required: true
          },
          locationId: {
            required: true
          },
          date_pick: {
            required: true,
            greaterThanToday: true
          },
          timeFrom: {
            selectRequired: true
          }
        },
        messages: {
          nameid: {
            required: "Please enter your name"
          },
          mobileid: {
            required: "Please enter your mobile number",
            indianMobNumber: "Enter your mobile number e.g. +91xxxxxxxxxx"
          },
          addressid: {
            required: "Address is required"
          },
          locationId: {
            required: "Location is required"
          },
          date_pick: {
            required: "Date is required",
            greaterThanToday: "Please select a valid date"
          },
          timeFrom: {
            selectRequired: "Please select time"
          }
        }
      };

      $scope.placeOrderHandler = function(form) {
        var mifd = $scope.mifd;
        unFocusElement();

        if($Session.user.getMobile() === '' || $Session.user.getMobile() != $scope.checkoutData.usermobile) {
          ionicToast.show("Please verify your mobile number !", 3000, "#A71212");
          $scope.addMobileNumber()
          return;
        } 


        if (form.validate()) {
          if (mifd == 2) {
            $scope.completePreOrder();
          } else if (mifd == 3) {
            $scope.completeOrderTakeaway();
          } else {
            $scope.completeOrder();
          }
        }
      };

      $scope.completePreOrder = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please Select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        var d = $scope.checkoutData.userdate;
        var t = $scope.checkoutData.usertime;
        var formattedDate = moment(d).format("MM/DD/YYYY");
        var formattedTime = moment(t).format("hh:mm a");
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          cname: $Session.city.getName(),
          carea: $Session.area.getName(),
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          predate: formattedDate,
          fromtime: formattedTime
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          localStorage.removeItem("mf_orderId_pre");
          localStorage.removeItem("mf_orderAmount_pre");
          Loading.hide();
          if (data.status == "failure") {
            ionicToast.show(data.reason || "Reason NULL", 5000, "#A71212");
            $scope.feCart = false;
            return false;
          }
          if (data.reason == "Please check restaurant timing") {
            ionicToast.show("Please check restaurant timing", 3000, "#A71212");
          } else if (data.reason == "You may send request next 2 days") {
            ionicToast.show("Make sure you send your request within two days from today", 5000, "#A71212");
          } else {
            submitCommonComment();
            localStorage.setItem("paymentLink", data.link);
            localStorage.setItem("mf_orderId_pre", data.onlinePreorderId);
            localStorage.setItem("mf_orderAmount_pre", data.onlineOrderAmount);

            // cordova.plugins.browsertab.isAvailable(function(result) {
            //   if (!result) {
            //     cordova.InAppBrowser.open(data.link, '_blank');
            //   } else {
            //     cordova.plugins.browsertab.openUrl(
            //       data.link,
            //       function(successResp) {console.log(successResp);},
            //       function(failureResp) {console.error(failureResp);});
            //   }
            // },
            // function(isAvailableError) {
            //   console.error(isAvailableError)
            // });
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go("app.payment");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: preorderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.completeOrderTakeaway = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        Loading.show();
        var params = {
          delType: "online",
          agree: 1,
          sId: sellerId,
          uId: userId,
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          cname: cityId,
          carea: areaId,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          takeaway: 3,
          orderFor: 3
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          localStorage.removeItem("mf_orderId");
          Loading.hide();
          if (data.status == "success") {
            localStorage.setItem("paymentLink", data.link);
            localStorage.setItem("mf_orderId", data.orderId);
            // cordova.plugins.browsertab.isAvailable(function(result) {
            //   if (!result) {
            //     cordova.InAppBrowser.open(data.link, '_blank');
            //   } else {
            //     cordova.plugins.browsertab.openUrl(
            //       data.link,
            //       function(successResp) {console.log(successResp);},
            //       function(failureResp) {console.error(failureResp);});
            //   }
            // },
            // function(isAvailableError) {
            //   console.error(isAvailableError)
            // });

            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go("app.payment");
            submitCommonComment();
          } else {
            $scope.feCart = false;
            ionicToast.show(data.reason, 3000, "#A71212");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: placeOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.completeOrder = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please Select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        Loading.show();
        var params = {
          delType: paymentVariable,
          agree: 1,
          sId: sellerId,
          uId: userId,
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          cname: cityId,
          carea: areaId,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          takeaway: 1,
          orderFor: 1
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          Loading.hide();
          if (data.status == "success") {


            //if payment type is cod
            if (paymentVariable == "cod") {
              if (data.mobileBit == "1") {
                saveAndPlaceOrder();
                submitCommonComment();
              } else if (data.mobileBit == "0") {
                $scope.showCheckoutOTPPopup();
              }
            }

            //if payemnt type online
            else if (paymentVariable == "online") {
              localStorage.setItem("paymentLink", data.link);

              // cordova.plugins.browsertab.isAvailable(function(result) {
              //   if (!result) {
              //     cordova.InAppBrowser.open(data.link, '_blank');
              //   } else {
              //     cordova.plugins.browsertab.openUrl(
              //       data.link,
              //       function(successResp) {console.log(successResp);},
              //       function(failureResp) {console.error(failureResp);});
              //   }
              // },
              // function(isAvailableError) {
              //   console.error(isAvailableError)
              // });


              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go("app.payment");
              submitCommonComment();
            }

            //if no payment variable assigned (Frontend error)
            else {
              ionicToast.show("paymentVariable is missing", 3000, "#A71212");
              alert("Payment Variable is null or undefined");
            }
          } else {
            ionicToast.show(data.reason, 3000, "#A71212");
            $scope.feCart = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: placeOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      };

      //checkout OTP Modal configs
      $scope.showCheckoutOTPPopup = function() {
        $scope.checkoutOTPdata = {};
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="otpCheckoutCode" id="otpCheckoutCode" ng-model="checkoutOTPdata.code" placeholder="OTP Code" required/>',
          title: "Verify",
          subTitle: "Please enter 4 digits OTP code sent to your mobile",
          cssClass: "showCheckoutOTPPopup",
          scope: $scope,
          buttons: [
            {
              text: "Cancel"
            },
            {
              text: "<b>Submit</b>",
              type: "button-balanced",
              onTap: function(e) {
                console.log(e);
                if (!$scope.checkoutOTPdata.code || !/^\d{4}$/.test($scope.checkoutOTPdata.code)) {
                  //don't allow the user to close unless he valid 4 digits OTP
                  ionicToast.show("Enter a valid 4 digits OTP", 2500, "#b50905");
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  // return $scope.checkoutOTPdata.code;
                  // $scope.verifyAndCompleteOrder($scope.checkoutOTPdata.code);
                  var params = {
                    passcode: $scope.checkoutOTPdata.code,
                    uId: userId,
                    sId: sellerId
                  };
                  var callback = function(data) {
                    if (data.status != "success") {
                      ionicToast.show(data.reason, 2500, "#b50905");
                      
                      return false;
                    }
                    myPopup.close();
                    saveAndPlaceOrder();
                    submitCommonComment();
                  };
                  var config = {
                    typeRequest: "GET",
                    urlRequest: verificationCodeAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                  };
                  $Easyajax.startRequest(config);
                }
              }
            }
          ]
        });

      };



    }



    function saveAndPlaceOrder() {
      Loading.show();
      var params = {
        sId: sellerId,
        uId: userId,
        menuItemFor: $scope.mifd,
        delType: 'cod',
      };
      var callback = function (data) {
        Loading.hide();
        if (data.status == "success") {
          $Session.order.setType("Online");
          ionicToast.show('You have placed an order succesfully!!', 5000, '#A71212');
          $state.go("thankyou");
        } else {
          ionicToast.show(data.reason, 5000, '#A71212');
        }
      }
      var config = {
        typeRequest: "POST",
        urlRequest: saveUserOrderAPI,
        dataRequest: params,
        sucesCallbackRequest: callback
      };
      $Easyajax.startRequest(config);
    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.checkout', {
        url: '/checkout',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/checkout/checkout.html',
            controller: 'checkoutCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('favouritesCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$ionicModal'];

  function controller($scope, Loading, $Session, $Easyajax, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    activate();

    function activate() {

      $scope.feReviews = true;
      $scope.fefavourites = true;
      $scope.errorText = "";

      $scope.$on('$ionicView.beforeEnter', function () {
        getWishlist();
      });

      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.modal.show();
      };

      $scope.doRefresh = function () {
        getWishlist();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
            getWishlist();
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.errorText = "No Favourites! Try login or create account";
          $scope.fefavourites = false;
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.fefavourites = true;
            $scope.restList = data.restaurantList;
            for (var i in data.restaurantList) {
              var sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          } else {
            $scope.fefavourites = false;
            $scope.errorText = "No Restaurants added to Favourites yet!";

          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.favourites', {
        url: '/favourites',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/favourites/favourites.html',
            controller: 'favouritesCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('FilteredRestListCTRL', controller)

  controller.$inject = ['$scope', '$state'];

  function controller($scope, $state) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {
      $scope.queuemanager = function () {
        $state.go('app.queuemanager');
      }

      $scope.sortRestaurant = function (key) {
        $state.go('app.restListSorted', {
          'sortKey': key
        });
      }

    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.filter', {
        url: '/filter',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/filter-rest-list/filter-rest-list.html',
            controller: 'FilteredRestListCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('menuListController', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', '$ionicPopup', 'menuList', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, menuList, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var mifd = $Session.order.getMenuItemType();
    console.log(mifd);
    var sellerId = $Session.seller.getId();
    
    var cart = {
      cartItems: [],
      cartDetail: {
        uId: userId,
        sId: sellerId
      }
    }
    var Oldcart = {
      cartItems: [],
      cartDetail: {
        uId: userId,
        sId: sellerId
      }
    }

    activate();

    function activate() {

      // if( $('#target').hasClass('animal') ) {
      //   $(document.body).addClass('dog');
      // }

      $scope.$on('$ionicView.enter', function (e) {
        cart.cartItems = [];
        getCartList();
        $scope.menu = menuList;
        // $("#addDefault" + proid).show(300);
        $("span[id^='addDefault']").show(300);
        Loading.hide();
      });
      // $scope.menu = $Session.seller.getActiveMenuList();

      $scope.totalAmount = 0;
      $scope.isHappyHour = false;
      $scope.surgePercent = $Session.seller.getSurgePricePercent();

      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: mifd
        };
        var callback = function (data) {
          if (data.status == "success") {
            Oldcart.cartItems = data.cartlist.list;
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
            $scope.totalAmount = $Session.order.getTotalAmount();
            console.log("getTotalAmount from getCartList() >> ", $Session.order.getTotalAmount());
          } else {
            Oldcart.cartItems = [];
            $Session.order.setTotalAmount(0);
            $scope.totalAmount = $Session.order.getTotalAmount();
            console.log("getTotalAmount from getCartList() >> ", $Session.order.getTotalAmount());
          }
          updateCartStatus();
          Loading.hide();
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function animateItYawala() {
        var el = $("#dishcount");
        var newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      //Add-Remove to-from cart object and calling calculating taxes function
      $scope.addItemJsonFIRST = function (item) {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $ionicPopup.alert({
            title: 'Restaurant Closed',
            template: 'Restaurant is closed now. Please check restaurant timings!',
            okType: 'button-assertive'
          });
        } else {
          $("#addDefault" + item.proId).hide(300);
          $scope.addItemJson(item);
        }
      };

      $scope.addItemJson = function (item) {
        animateItYawala();
        var hasMatch = false;

        // if (item.hpHourStatus == 'yes') {
        //     console.log("HAPPY HOUR!!! 💪 👺");
        //     $scope.isHappyHour = true;
        //     addItemHappyHour(item)
        //     return true;
        // }
        $scope.isHappyHour = false;
        console.log("NO HAPPY HOUR!!!🙄 ");
        for (var index = 0; index < cart.cartItems.length; index++) {
          var product = cart.cartItems[index];
          if (product.proId == item.proId) {
            hasMatch = true;
            cart.cartItems[index].qty = (parseInt(cart.cartItems[index].qty) + 1).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "1",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });

          hasMatch = false;
        }
        calcCartTaxes();
      };

      function addItemHappyHour(item) {
        var hasMatch = false;
        for (var index = 0; index < cart.cartItems.length; index++) {
          var product = cart.cartItems[index];
          if (product.proId == item.proId) {
            hasMatch = true;
            cart.cartItems[index].qty = (parseInt(cart.cartItems[index].qty) + 2).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "2",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });

          hasMatch = false;
        }
        calcCartTaxes();
      }

      $scope.removeItemJson = function (item) {
        animateItYawala();
        // if (item.hpHourStatus == 'yes') {
        //     console.log("HAPPY HOUR!!! 💪 👺");
        //     $scope.isHappyHour = true;
        //     removeItemHappyHour(item);
        //     return true;
        // }
        $scope.isHappyHour = false;
        console.log("NO HAPPY HOUR!!!🙄");
        for (var i = 0; i < cart.cartItems.length; i++) {
          if (cart.cartItems[i].proId == item.proId) {
            cart.cartItems[i].qty = (parseInt(cart.cartItems[i].qty) - 1).toString();
            if (cart.cartItems[i].qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcCartTaxes();
        }
      };


      function removeItemHappyHour(item) {
        for (var i = 0; i < cart.cartItems.length; i++) {
          if (cart.cartItems[i].proId == item.proId) {
            cart.cartItems[i].qty = (parseInt(cart.cartItems[i].qty) - 2).toString();
            if (cart.cartItems[i].qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcCartTaxes();
        }
      }

      ////////////////////////////////////////////////////////////////////////////////
      /**
       * @todo
       * update add to cart login, rm unsed variables
       */
      var totalElTotal = 0;

      function calcTotalAmount() {
        var sumTotal = 0;
        var cart_length = cart.cartItems.length;
        if (cart_length > 0) {

          if ($scope.isHappyHour) {
            for (var i = 0; i < cart_length; i++) {
              var pid = cart.cartItems[i].proId;
              var qt = cart.cartItems[i].qty;
              var price = cart.cartItems[i].productPrice;
              sumTotal += parseInt(qt / 2) * parseInt(price);
              $("#itemNumber" + pid).val(qt);
            }
            return sumTotal;
          } else {
            for (var i = 0; i < cart_length; i++) {
              var pid = cart.cartItems[i].proId;
              var qt = cart.cartItems[i].qty;
              var price = cart.cartItems[i].productPrice;
              sumTotal += parseInt(qt) * parseInt(price);
              $("#itemNumber" + pid).val(qt);
            }
            return sumTotal;
          }

        } else {
          //console.log("sumTotal  (length < 0)   == " + sumTotal);
          return sumTotal;
        }
      }

      function calcCartTaxes() {
        var totalSum = 0;
        var oldaTotal = $Session.order.getTotalAmount() || 0;
        var sum = calcTotalAmount();
        totalSum = parseFloat(sum) + parseFloat(oldaTotal);
        totalElTotal = totalSum;
        $scope.totalAmount = totalElTotal;
        console.log("totalElTotal >> ", totalSum);
        updateCartStatus();
      }

      function updateCartStatus() {
        var dishCount = 0;
        var cartSumQty = 0;
        var oldCartSumQty = 0;
        for (var i in cart.cartItems) {
          cartSumQty = parseInt(cartSumQty) + parseInt(cart.cartItems[i].qty);
        }
        for (var j in Oldcart.cartItems) {
          oldCartSumQty = parseInt(oldCartSumQty) + parseInt(Oldcart.cartItems[j].proQty);
        }
        dishCount = parseInt(cartSumQty) + parseInt(oldCartSumQty);
        dishCount == 0 ? $("#cart-footer-bar").hide().addClass('hidden') : $("#cart-footer-bar").show().removeClass('hidden');
        $("#dishcount").text(dishCount);
      }


      /////////////////////////////////////////////////////////////////////////////////////////////

      function isTotalMoreThanMinChar() {
        var minDCharge = parseFloat($Session.order.getMinDeliveryCharges());
        var totAmount = totalElTotal;
        // console.log("minDCharge >> ", minDCharge, " totalElTotal >> ", totAmount);
        if (totAmount >= minDCharge) {
          $Session.order.setTotalAmount(totalElTotal);
          console.log("$ Total amount stored in order session = ", $Session.order.getTotalAmount());
          return true;
        } else {
          return false;
        }
      }


      function showReviewAlert() {
        $scope.reviewOrderdata = {};
        var myPopup = $ionicPopup.show({
          template: '<div class="item item-input"><ng-rate-it class="center" ng-model="reviewOrderdata.stars" star-width="32" star-height="32" step="1" max="5" rated="ratedCallback"></ng-rate-it></div><label class="item item-input item-floating-label"><textarea placeholder="Review message (Optional)" ng-model="reviewOrderdata.message" rows="5" cols="50"></textarea></label>',
          title: $Session.seller.getName(),
          subTitle: 'Please review your previous order to complete your current order',
          cssClass: 'review-order-first',
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.reviewOrderdata.stars) {
                  //don't allow the user to close unless he enters wifi password
                  e.preventDefault();
                } else {
                  $scope.revieworder();
                }
              }
            }
          ]
        });
        myPopup.then(function (res) {
          console.log('Tapped!', res);
        });
      }


      $scope.revieworder = function () {
        var orderId = $Session.order.getReviewOrderId();
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          selId: sellerId,
          uId: userId,
          rates: $scope.reviewOrderdata.stars,
          review: $scope.reviewOrderdata.message,
          orderId: orderId
        };
        var callback = function (data) {
          if (data.status == 'success') {
            updateCart();
          } else {
            ionicToast.show('Error submitting a review. Try again!', 3000, '#A71212');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);

      };

      $scope.ratedCallback = function () {
        console.log("Total reviewOrderdata.stars >> ", $scope.reviewOrderdata.stars);
      }

      function updateOldCart() {
        Loading.show();
        var mifd = $Session.order.getMenuItemType();
        var params = {
          jsonUserCart: Oldcart,
          menuItemFor: mifd
        }
        var callback = function (data) {
          Loading.hide();
          $state.go('app.cart');
        }
        var config = {
          typeRequest: "POST",
          urlRequest: AddUpdateCartItemsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function addLocalItemsUpdateCart() {
        var mifd = $Session.order.getMenuItemType();
        // Loading.show();
        var params = {
          jsonUserCart: JSON.stringify(cart),
          menuItemFor: mifd
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $state.go('app.cart');
          } else {
            ionicToast.show('Not added to cart! Please select order type', 3000, '#A71212');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: AddUpdateCartItemsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function updateCart() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");

          // Check before create order and send to backend without userId 
          var confirmPopup = $ionicPopup.confirm({
            title: 'Mink Foodiee',
            template: 'Please Login or Create Account to complete your order!',
            cancelText: 'Later',
            cancelType: 'button-default',
            okText: 'Login',
            okType: 'button-balanced'
          });
          confirmPopup.then(function (res) {
            if (res) {
              $state.go('login');
            }
          });
          return false;
        }
        Loading.show();
        var callback = function (data) {
          Loading.hide();
          if (data.reason == "review on your order") {
            $Session.order.setReviewOrderId(data.orderId);
            showReviewAlert();
            return false;
          } else if (!isTotalMoreThanMinChar()) {
            ionicToast.show('Minimum order amount per person ' + '₹' + $Session.order.getMinDeliveryCharges(), 3000, '#A71212');
            return false;
          } else {
            //if all cases are passed, then send the local cart to backend
            addLocalItemsUpdateCart();
          }
        };
        var mifd = $Session.order.getMenuItemType();
        var params = {
          sId: sellerId,
          uId: userId,
          takeaway: mifd
        };
        var config = {
          typeRequest: "POST",
          urlRequest: checkoutAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.checkout = function () {
        if (cart.cartItems.length == 0 && Oldcart.cartItems.length == 0) {
          $ionicPopup.alert({
            title: 'Empty Cart',
            template: 'Please add whatever you like from our food Menu!',
            okType: 'button-assertive'
          });

        } else if (cart.cartItems.length == 0) {
          updateOldCart();
          console.log("called updateOldCart");
        } else {
          updateCart();
          console.log("called updateCart");
        }
      }


      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      };
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      };


    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.menuList', {
        url: '/menuList',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            Loading.show();
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/menu-list/menu-list.html',
            controller: 'menuListController',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function() {
    'use strict';

    angular
        .module('restaurant-list-module')
        .controller('paymentCTRL', PaymentController);

    PaymentController.$inject = ['Loading','$scope', '$state', '$ionicPopup'];

    function PaymentController(Loading, $scope, $state, $ionicPopup) {
        var vm = this;

        $scope.$on("$ionicView.enter", function() {
            activate();
          });

        activate();

        ////////////////

        function activate() {
            var link = localStorage.getItem("paymentLink");
            $("#payment").attr("src", link);
            // localStorage.removeItem("paymentLink");
            // Loading.show();
            // $(document).ready(function() {
            // $('#payment').ready(function() {
            //     Loading.hide();
            // });
            // $('#payment').load(function() {
            //     Loading.hide();
            // });
            // });
            function backFromOnlinePaymentHandler() {
                var confirmPopup = $ionicPopup.confirm({
                    title: "Back",
                    template: "Order details will be lost if you click back. Are you sure?",
                    cancelText: "Close",
                    cancelType: "button-assertive",
                    okText: "Yes, sure",
                    okType: "button-default"
                });
                confirmPopup.then(function (res) {
                    if (res) {
                    // $ionicHistory.backView();
                    $state.go("app.checkout")
                    } else return;
                });
            }
            $scope.goback =function() {
                backFromOnlinePaymentHandler()
            }

        }
    }
})();
(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.payment', {
        url: '/payment',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/online-payment/online-payment.html',
            controller: 'paymentCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('recieptCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var sellerId = $Session.seller.getId();
    var menuItemType = $Session.order.getMenuItemType();
    activate();

    function activate() {
      

      $scope.checkoutDetails = {
        firstName: localStorage.getItem("chusername"),
        mobileNumber: localStorage.getItem("chusermobile"),
        address: localStorage.getItem("chuseraddress")
      };

      // Calls
      getCartList();


      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: menuItemType
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            $scope.listToppings = data.cartlist.listToppings;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
          } else {
            console.log("Error happened calling userCartList ");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.completeOrder = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: menuItemType,
          delType: 'cod',
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $Session.order.setType("Online");
            ionicToast.show('You have placed an order succesfully!!', 5000, '#42b');
            $state.go("thankyou");
          } else {
            ionicToast.show(data.reason, 5000, '#42b');
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: saveUserOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.reciept', {
        url: '/reciept',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/order-reciept/order-reciept.html',
            controller: 'recieptCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function() {
  "use strict";

  angular.module("restaurant-list-module").controller("restDetailsCTRL", controller);

  controller.$inject = ["$scope", "$stateParams", "Loading", "$Session", "$Easyajax", "$state", "$ionicModal", "$ionicScrollDelegate"];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicScrollDelegate) {
    /* jshint validthis:true */
    var vm = this;
    $Session.seller.setId($stateParams.sellerId);
    var orderType = $stateParams.orderType || '';
    var sellerId = $Session.seller.getId();
    $scope.ferestaurantDetails = false;
    $scope.feReviews = false;
    $scope.isOpen = true;
    $scope.mifd = $Session.order.getMenuItemType() || '1';
    $scope.langToggle = {
      FR: false,
      DE: false,
      EN: false
    };
    // Review Modal
    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function(modal) {
        $scope.reviewModal = modal;
      });
    $scope.closeReviewsList = function() {
      $scope.reviewModal.hide();
    };
    $scope.openReviewsList = function() {
      $scope.reviewModal.show();
    };


    

    activate();

    function activate() {
      // Variables

      $scope.$on("$ionicView.enter", function() {
        getMIF();
      });

      // Calls
      getRestaurantsDetails();

      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          if (data.status == "success") {
            $scope.restaurantDetail = data.restaurantDetail;
            $scope.ferestaurantDetails = true;
            $scope.deliveryAreas = data.deliveryAreas;

            //Settings Seller information to Session
            $Session.seller.setDeliveryAvailable(data.restaurantDetail.userCanAddProduct);
            $Session.seller.setOrderAvailable(data.restaurantDetail.openOrClose);
            $Session.seller.setMinPrice(data.restaurantDetail.minPriceStart);
            $Session.seller.setMinDeliveryCharges(data.restaurantDetail.minDeliveryCharges);
            $Session.seller.setServicePercent(data.restaurantDetail.serviceTax);
            $Session.seller.setVatPercent(data.restaurantDetail.vatTax);
            $Session.seller.setDeliveryTime(data.restaurantDetail.deliveryTime);
            $Session.seller.setTableBookOption(data.restaurantDetail.reserveTable);
            $Session.seller.setSurgeIndication(data.restaurantDetail.surgeIndication);
            $Session.seller.setSurgeMode(data.restaurantDetail.surgeMode);
            $Session.seller.setSurgePricePercent(data.restaurantDetail.surgePricePercent);
            $Session.seller.setRating(data.restaurantDetail.totalRates);
            $Session.seller.setImages(data.restImages.image);
            $Session.seller.setUserCanAddProduct(data.restaurantDetail.userCanAddProduct);
            $Session.seller.setType(data.restaurantDetail.restaurantTypeTitle);
            $Session.seller.setName(data.restaurantDetail.restaurantName);
            $Session.seller.setCouponExist(data.restaurantDetail.couponExist);

            // Setting Order information (Minimum amount for order)
            $Session.order.setMinDeliveryCharges(data.restaurantDetail.minOrder);

            $scope.menuCodOnline = data.menuCodOnline;
            $scope.menuPreorder = data.menuPreorder;
            $scope.menuTakeaway = data.menuTakeaway;
            $scope.listMenuTR = data.listMenuTR;
            var aa = data.deliveryAreas[0].cityAreas;
            $scope.deliveryAreas = aa.replace(/\s\s+/g, ' ').substr(aa.indexOf(",")+1).split(',');
            $scope.ourServices = data.restaurantDetail.ourServices;

            setTodayTiming(data.restaurantDetail.currentDay);
            getMIF();
            checkAvailablity();
            Loading.hide();
          } else {
            $scope.ferestaurantDetails = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function checkAvailablity() {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $scope.isOpen = false
        } else $scope.isOpen = true;
      }

      function getMIF() {

        if(orderType == '') {
          if ($scope.mifd == '1') {
            $scope.langToggle.DE = !false;
            $scope.langToggle.EN = false;
            $scope.langToggle.FR = false;
            $Session.seller.setActiveMenuList($scope.menuCodOnline);
          } else if ($scope.mifd == '2') {
            $scope.langToggle.EN = !false;
            $scope.langToggle.DE = false;
            $scope.langToggle.FR = false;
            $Session.seller.setActiveMenuList($scope.menuPreorder);
          } else if ($scope.mifd == '3') {
            $scope.langToggle.FR = !false;
            $scope.langToggle.DE = false;
            $scope.langToggle.EN = false;
            $Session.seller.setActiveMenuList($scope.menuTakeaway);
          }
        } else if(orderType == 'takeaway') {
          $scope.langToggle.FR = !false;
          $scope.langToggle.DE = false;
          $scope.langToggle.EN = false;
          $Session.seller.setActiveMenuList($scope.menuTakeaway);
          $Session.order.setMenuItemType('3');
          
        } else if(orderType == 'online') {
          $scope.langToggle.DE = !false;
          $scope.langToggle.EN = false;
          $scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuCodOnline);
          $Session.order.setMenuItemType('1');
        }

      }

      function getSellerImages() {
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.sellerImages = data.sellerImages;
          } else {
            $scope.sellerImages = [];
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: SellerImagesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $(".item.mon").addClass("active-day");
            break;
          case "Tus":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            $(".item.mon").addClass("active-day");
            break;
        }
      }

      // setTimeout(function () {
      //   $ionicSlideBoxDelegate.slide(0);
      //   $ionicSlideBoxDelegate.update();
      //   $scope.$apply();
      // });
      // $scope.options = {
      //   loop: false,
      //   effect: 'fade',
      //   speed: 500,
      //   pagination: false
      // }

      $scope.setOrderType = function(scope, type) {
        // Make sure MenuItemType is updated
        $Session.order.setMenuItemType(type);
        // Get value of MenuItemType and append to $scope.mifd
        $scope.mifd = $Session.order.getMenuItemType();

        if (type === "1") {
          // Change radio-button value
          scope.langToggle.DE = !false;
          scope.langToggle.EN = false;
          scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuCodOnline);
          $Session.order.setMenuItemType('1');
          $ionicScrollDelegate.scrollBottom();
        } else if (type === "2") {
          // Change radio-button value
          scope.langToggle.EN = !false;
          scope.langToggle.DE = false;
          scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuPreorder);
          $Session.order.setMenuItemType('2');
          $ionicScrollDelegate.scrollBottom();
        } else if (type === "3") {
          // Change radio-button value
          scope.langToggle.FR = !false;
          scope.langToggle.DE = false;
          scope.langToggle.EN = false;
          $Session.seller.setActiveMenuList($scope.menuTakeaway);
          $Session.order.setMenuItemType('3');
          $ionicScrollDelegate.scrollBottom();
        }
      };

      // Toggle Restaurant Delivery Area
      $scope.shownGroup = null; // set to null to make it hidden at first time
      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? ($scope.shownGroup = null) : ($scope.shownGroup = group);
      };
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      };

      // Toggle Restaurant timings (show more details)
      $scope.shownItem = null; // set to null to make it hidden at first time
      $scope.toggleItem = function(item) {
        $scope.isItemShown(item) ? ($scope.shownItem = null) : ($scope.shownItem = item);
      };
      $scope.isItemShown = function(item) {
        return $scope.shownItem === item;
      };

      $scope.showReviewsForThis = function(sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.viewMenu = function() {
        // $Session.order.setMenuItemType($scope.mifd);
        $state.go("app.menuList");
      };
    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restaurant', {
        url: '/restList/:sellerId/:orderType',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/rest-details/rest-details.html',
            controller: 'restDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('restListCTRL', restListCTRL)

  restListCTRL.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function restListCTRL($scope, $Session, $Easyajax, $state, Loading, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() ||"0";
    var cityId = $Session.city.getId();
    var areaId = $Session.area.getId();
    var cityName = $Session.city.getName();
    var areaName = $Session.area.getName();

    activate();

    function activate() {

      // $scope.slides = [
      //   {
      //     title: 'First',
      //     imageUrl: 'assets/img/banner/1.jpg',
      //   },
      //   {
      //     title: 'Second',
      //     imageUrl: 'assets/img/banner/2.jpg',
      //   },
      //   {
      //     title: 'Third',
      //     imageUrl: 'assets/img/banner/3.jpg',
      //   },
      //   {
      //     title: 'Fourth',
      //     imageUrl: 'assets/img/banner/4.jpg',
      //   }
      // ];

      // $scope.options = {
      //   loop: true,
      //   autoplay: 1000,
      //   effect: 'fade',
      //   speed: 300,
      // }

      // $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      //   // data.slider is the instance of Swiper
      //   $scope.slider = data.slider;
      //   console.log($scope.slider);
      // });

      $scope.restList = [];
      $scope.feList = false;
      $scope.feReviews = true; 
      $scope._isLiked = false;
      $Session.seller.removeId();
      $scope.cityName = cityName;
      $scope.areaName = areaName;

      // Calls
      getRestaurantsList(cityId, areaId);


      $scope.$on('$ionicView.enter', function () {
        var c = $Session.city.getId();
        var a = $Session.area.getId();
        if (cityId != c || areaId != a) {
          cityId = c;
          areaId = a;
          getRestaurantsList(c, a);
        }
      });

      $scope.openHomePage = function () {
        $state.go('home');
      };
      $scope.openFilterPage = function () {
        $state.go('app.filter');
      }
      $scope.restSearch = {
        restaurantName: ""
      };


      $scope.clearSearch = function (event) {
        event.preventDefault();
        $scope.restSearch.restaurantName = "";
      }

      $scope.openSearch = function (form, key) {
        if (form.validate()) {
          $state.go('app.restListSearch', {
            'searchKey': key
          });
        } else {
          $ionicPopup.alert({
            title: 'Restaurant Search',
            template: 'Make sure search input is not empty!',
            okType: 'button-assertive'
          });
        }
      }

      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });

      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };

      $scope.openReviewsList = function () {
        $scope.modal.show();
      };


      $scope.doRefresh = function () {
        getRestaurantsList(cityId, areaId);
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getRestaurantsList(cityId, areaId) {
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          if (data.status == "success") {
            $scope.feList = true;
            $scope.restList = data.restaurantList;
            $scope.$apply();
            getWishlist();
          } else {
            $scope.feList = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };



    }
  }

})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restList', {
        url: '/restList',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/rest-list/rest-list.html',
            controller: 'restListCTRL',
            controllerAs: 'vm'
          }
        }
      })
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('SortedRestListCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$ionicModal', 'Loading', '$stateParams', 'ionicToast'];

  function controller($scope, $Session, $Easyajax, $ionicModal, Loading, $stateParams, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var areaId = $Session.area.getId();
    activate();

    function activate() {

      $scope.feReviews = true;
      $scope.feRestaurants = true;

      // Calls
      sortRestaurants();


      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.modal.show();
      };


      $scope.doRefresh = function () {
        sortRestaurants();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          } else {
            console.log("wishListAPI Failure");
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function sortRestaurants() {
        Loading.show();
        $scope.sk = $stateParams.sortKey;
        var sk = $scope.sk;
        if (sk == "B") $scope.sk = "table booking restaurants";
        if (sk == "TS") $scope.sk = "restaurants offering tiffin service";
        if (sk == "PO") $scope.sk = "order scheduling restaurants";
        if (sk == "TK") $scope.sk = "takeaway restaurants";
        if (sk == "C") $scope.sk = "restaurants with cash payment options";
        if (sk == "ORD") $scope.sk = "restaurants with online payment options";
        if (sk == "CO") $scope.sk = "restaurants with cash and online payment options";
        if (sk == "IHO") $scope.sk = "restaurants delivering in-house orders";
        if (sk == "QM") $scope.sk = "restaurants with queue manager facility";
        if (sk == "MONC") $scope.sk = "most ordered restaurants";
        if (sk == "MDNC") $scope.sk = "restaurants delivering near you";
        if (sk == "MRNC") $scope.sk = "restaurants residing near you";
        if (sk == "OOD") $scope.sk = "Srestaurants offering online order discount";
        if (sk == "APPD") $scope.sk = "restaurants offering app based discount";
        if (sk == "Buffet") $scope.sk = "restaurants with buffet facility";


        var key = $stateParams.sortKey;
        var params = {
          sKey: key,
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.feRestaurants = true;
            $scope.restList = data.restaurantList;
            $scope.$apply();
            getWishlist();
          } else {
            $scope.feRestaurants = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };





    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restListSorted', {
        url: '/restListSorted',
        params: {
          'sortKey': ""
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/sort-rest-list/sort-rest-list.html',
            controller: 'SortedRestListCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module("restaurant-list-module")
    .controller("thankyouCTRL", thankyouCTRL);

  thankyouCTRL.$inject = ["$scope", "$Session", "$state", "$timeout"];

  function thankyouCTRL($scope, $Session, $state, $timeout) {

    $scope.$on('$ionicView.enter', function () {
      activate();
    });


    function activate() {

      cleanSession();
      navigateToHistory();

      function cleanSession() {
        //inhouse session
        $Session.inHouse.removeId();
        $Session.inHouse.removeSellerId();
        $Session.inHouse.removeTableNumber();
        $Session.inHouse.removeSellerName();
        $Session.inHouse.removeSellerMinCharge();
        $Session.inHouse.removeSellerMinDiscount();
        $Session.inHouse.removeSellerDiscountPercent();
        $Session.inHouse.removeSellerDiscountAmount();
        $Session.inHouse.removeOrderTotalAmount();

        //order session
        $Session.order.removeMenuItemType();
        $Session.order.removeMinDeliveryCharges();
        $Session.order.removeId();
        $Session.order.removeReviewOrderId();
        $Session.order.removeType();
        $Session.order.removeTotalAmount();

        //seller session
        $Session.seller.removeDeliveryAvailable();
        $Session.seller.removeAttendanceTime();
        $Session.seller.removeOrderAvailable();
        $Session.seller.removeMinDeliveryCharges();
        $Session.seller.removeName();
        $Session.seller.removeType();
        $Session.seller.removeMinPrice();
        $Session.seller.removeDeliveryTime();
        $Session.seller.removeTableBookOption();
        $Session.seller.removeRating();
        $Session.seller.removeServicePercent();
        $Session.seller.removeVatPercent();
      }

      function navigateToHistory() {
        $timeout(function () {
          $state.go("app.history");
        }, 5000);
      }


    }
  }
})();

(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('thankyou', {
        url: '/thankyou',
        templateUrl: 'app/pages/Restaurant/thank-you/thank-you.html',
        controller: 'thankyouCTRL',
        controllerAs: 'vm'
      });
    }
  
  }());
  
(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .controller('TableBookingListCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$ionicPopup', '$Easyajax', 'Loading', '$ionicModal'];

  function controller($scope, $Session, $state, $ionicPopup, $Easyajax, Loading, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var areaId = $Session.area.getId();
    $scope.feTableList = false;
    $scope.errorText = "";
    $scope.isLiked =  false;
    $scope.tableHistoryList = [];
    $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.closeReviewsList = function () {
      $scope.modal.hide();
    };

    $scope.openReviewsList = function () {
      $scope.modal.show();
    };

    activate();

    function activate() {
      

      // Calls
      sortRestaurants();
      getTableBookingHistory();
      

      function sortRestaurants() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.feTableList = false;
          $scope.errorText = "No Restaurants! You are not logged in";
          return false;
        }
        Loading.show();
        var params = {
          sKey: "B",
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restList = data.restaurantList;
            $scope.feTableList = true;
            getWishlist()
          } else {
            $scope.feTableList = false;
            $scope.errorText = "No Restaurants found!";
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.changelike = function(sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            
            if (data.color == "red") {
              $scope.isLiked = true;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getTableBookingHistory() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status = "success") {
            $scope.tableHistoryList = data.tableReserveList;
          } else {
            $scope.tableHistoryList = [];
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: tableReservationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.doRefresh = function () {
        sortRestaurants();
        $scope.$broadcast('scroll.refreshComplete');
      }

      $scope.doRefreshBookingHistory = function () {
        getTableBookingHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }


    }
  }
})();

(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingList', {
        url: '/tableBookingList',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-list/table-booking-list.html',
            controller: 'TableBookingListCTRL',
            controllerAs: 'vm'
          }
        }
      })
  }

}());

(function () {
  "use strict";

  angular
    .module("table-booking-module")
    .controller("TableBookingRequestCTRL", controller);

  controller.$inject = ["$scope", "$ionicHistory", "$Session", "$state", "$stateParams", "ionicToast", "$Easyajax", "menuList", "Loading"];

  function controller($scope, $ionicHistory, $Session, $state, $stateParams, ionicToast, $Easyajax, menuList, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    /**
     * @member {module} AddToCart
     ***************************/
    var cart = {
      cartItems: [],
      cartDetail: {
        uId: userId
      }
    }
    $scope.totalAmount = 0;
    $scope.totalCartProducts = 0;
    $scope.isHappyHour = false;
    $scope.surgePercent = $Session.seller.getSurgePricePercent();
    
    
    /**
     * @member {module} BookTable 
     *******************************/
    $scope.feTablesList = false;
    $scope.feReviews = true;
    $scope.timeValues = [
      "12:00 AM",
      "12:30 AM",
      "1:00 AM",
      "1:30 AM",
      "2:00 AM",
      "2:30 AM",
      "3:00 AM",
      "3:30 AM",
      "4:00 AM",
      "4:30 AM",
      "5:00 AM",
      "5:30 AM",
      "6:00 AM",
      "6:30 AM",
      "7:00 AM",
      "7:30 AM",
      "8:00 AM",
      "8:30 AM",
      "9:00 AM",
      "9:30 AM",
      "10:00 AM",
      "10:30 AM",
      "11:00 AM",
      "11:30 AM",
      "12:00 PM",
      "12:30 PM",
      "1:00 PM",
      "1:30 PM",
      "2:00 PM",
      "2:30 PM",
      "3:00 PM",
      "3:30 PM",
      "4:00 PM",
      "4:30 PM",
      "5:00 PM",
      "5:30 PM",
      "6:00 PM",
      "6:30 PM",
      "7:00 PM",
      "7:30 PM",
      "8:00 PM",
      "8:30 PM",
      "9:00 PM",
      "9:30 PM",
      "10:00 PM",
      "10:30 PM",
      "11:00 PM",
      "11:30 PM"
    ];


    $scope.$on('$ionicView.enter', function (e) {
      cart.cartItems = [];
      $scope.menu = menuList;
      $("span[id^='addDefault']").show(300);
    });

    activate();

    function activate() {

      /**
       * @member {module} AddToCart 
       *******************************/


      // Toggle Item part (show more details)
      $scope.shownItem = null;
      $scope.toggleItem = function (item) {
        if ($scope.isItemShown(item)) {
          $scope.shownItem = null;
        } else {
          $scope.shownItem = item;
        }
      };
      $scope.isItemShown = function (item) {
        return $scope.shownItem === item;
      };


      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      }
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      }

      function animateItYawala() {
        var el = $("#dishcount");
        var newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      //Add-Remove to-from cart object and calling calculating taxes function
      $scope.addItemJsonFIRST = function (item) {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $ionicPopup.alert({
            title: 'Restaurant Closed',
            template: 'Restaurant is closed now. Please check restaurant timings!',
            okType: 'button-assertive'
          });
        } else {
          $("#addDefault" + item.proId).hide(300);
          $scope.addItemJson(item);
        }
      };
      $scope.addItemJson = function (item) {
        animateItYawala();
        var hasMatch = false;
        $scope.isHappyHour = false;
        for (var i = 0; i < cart.cartItems.length; i++) {
          var product = cart.cartItems[i];
          if (product.proId == item.proId) {
            hasMatch = true;
            product.qty = (parseInt(product.qty) + 1).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "1",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });
          hasMatch = false;
        }
        calcTotalAmount();
      };


      $scope.removeItemJson = function (item) {
        animateItYawala();
        $scope.isHappyHour = false;
        for (var i = 0; i < cart.cartItems.length; i++) {
          var product = cart.cartItems[i];
          if (product.proId == item.proId) {
            product.qty = (parseInt(cart.cartItems[i].qty) - 1).toString();
            if (product.qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcTotalAmount();
        }
      }

      function calcTotalAmount() {
        var sumTotal = 0;
        var sumtotalQty = 0;
        var cart_length = cart.cartItems.length;
        if (cart_length == 0) {
          $scope.totalAmount = 0;
          $("#cart-footer-bar").hide().addClass('hidden');
          return;
        } else {
          for (var i = 0; i < cart_length; i++) {
            var product = cart.cartItems[i];
            var productId = product.proId;
            var productQty = parseInt(product.qty);
            var productPrice = parseInt(product.productPrice);

            sumTotal += productQty * productPrice;
            sumtotalQty =  sumtotalQty + productQty;

            // Update the view
            $("#itemNumber" + productId).val(productQty);
          }

          $scope.totalAmount = sumTotal;
          $scope.totalCartProducts = sumtotalQty;

          $("#cart-footer-bar").show().removeClass('hidden');
          $("#dishcount").text($scope.totalCartProducts);
        }
      }

      $scope.checkout = function() {
        console.log(JSON.stringify(cart));
      }

      /**
       * @member {module} BookTable 
       *******************************/
      //Event when confirm datetime
      $scope.datetimepickerEvent = function (e) {
        // console.log("Event changed, ", e.tableBookingData.tableBookingDate);
        $scope.getTableList();
      };

      $scope.getTableList = function () {
        console.log("$scope.tableBookingData.tableBookingTime ", $scope.tableBookingData.tableBookingTime);
        console.log("$scope.tableBookingData.tableBookingDate ", $scope.tableBookingData.tableBookingDate);
        var d = $scope.tableBookingData.tableBookingDate;
        var formattedDate = moment(d).format("YYYY-MM-DD");

        var t = $scope.tableBookingData.tableBookingTime;
        var formattedTime = moment(t).format("HH:mm ");
        var params = {
          sId: $stateParams.sellerId,
          trTime: formattedTime,
          trDate: formattedDate
        };
        var callback = function (data) {
          console.log("getTableList : ", data);
          if (data.status == "success") {
            $scope.feTablesList = true;
            $scope.availableList = data.list;
          } else {
            $scope.feTablesList = false;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: AvailableTableAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.tableBookingData = {
        tableBookingDate: new Date(),
        tableBookingTime: new Date(),
        tableBookingPeopleNo: 0
      };
      $scope.tableBookingConfigs = {
        rules: {
          tableBookingDate: {
            required: true,
            greaterThanToday: true
          },
          tableBookingTime: {
            selectRequired: true
          },
          tableBookingPeopleNo: {
            selectRequired: true
          },
          table: {
            selectRequired: true
          }
        },
        messages: {
          tableBookingDate: {
            required: "Date is required",
            greaterThanToday: "Please select a valid date"
          },
          tableBookingTime: {
            selectRequired: "Time is required"
          },
          tableBookingPeopleNo: {
            selectRequired: "Number of people is required"
          },
          table: {
            selectRequired: true
          }
        }
      };

      $scope.findTable = function (form) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if ($scope.tableBookingData.tableBookingPeopleNo == "0") {
          ionicToast.show("Please select how many persons", 5000, "#b50905");
          return false;
        }
        // if ($scope.tableBookingData.table == "0") {
        //   ionicToast.show("Please select how many people", 5000, "#b50905");
        //   return false;
        // }
        var d = $scope.tableBookingData.tableBookingDate;
        var formattedDate = moment(d).format("YYYY-MM-DD");

        var t = $scope.tableBookingData.tableBookingTime;
        var formattedTime = moment(t).format("HH:mm ");


        if (form.validate()) {
          Loading.show();
          var params = {
            sId: $stateParams.sellerId,
            uId: userId,
            reserDate: formattedDate,
            timeFrom: formattedTime,
            np: $scope.tableBookingData.tableBookingPeopleNo,
            tableIds: "0", //$scope.tableBookingData.table,
            seatingHours: "1",
            jsonUserCart: JSON.stringify(cart),
            event: "Save"
          };
          var callback = function (data) {
            Loading.hide();
            console.log(data);
            if (data.status === 'success') {
              ionicToast.show("Your Booking request has been received please wait while it gets approved", 3000, "#249E21");
              
              $ionicHistory.nextViewOptions({
                historyRoot: true,
                disableBack: true
              });
              $state.go("app.tableBookingList");
            } else {
              ionicToast.show(data.reason, 5000, "#b50905");
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: reserveTableAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };



    }
  }
})();

(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingRequest', {
        url: '/tableBookingRequest/:sellerId',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-request/table-booking-request.html',
            controller: 'TableBookingRequestCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function() {
  "use strict";

  angular
    .module("table-booking-module")
    .controller("TableBookingRestDetailsCTRL", controller);

  controller.$inject = [ "$scope", "$ionicHistory", "$Session","$state", "$stateParams", "ionicToast", "$Easyajax", "$ionicModal", "Loading" ];

  function controller( $scope, $ionicHistory, $Session, $state, $stateParams, ionicToast, $Easyajax, $ionicModal, Loading) {
    /* jshint validthis:true */
    var vm = this;


    $scope.feReviews = true;
    $scope.restaurantId = $stateParams.sellerId;


    activate();
    
    function activate() {

      getRestaurantsDetails();

      // Toggle Item part (show more details)
      $scope.shownItem = null;
      $scope.toggleItem = function(item) {
        if ($scope.isItemShown(item)) {
          $scope.shownItem = null;
        } else {
          $scope.shownItem = item;
        }
      };
      $scope.isItemShown = function(item) {
        return $scope.shownItem === item;
      };

      // Get restaurant details accoding to Id
      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function(res) {
          if (res.status == "success") {
            Loading.hide();
            $scope.restaurantDetail = res.restaurantDetail;
            $scope.deliveryAreas = res.deliveryAreas;
            $scope.ourServices = res.restaurantDetail.ourServices
              .split(",")
              .join(", ");

              $scope.menuCodOnline = res.menuCodOnline;
              $scope.menuPreorder = res.menuPreorder;
              $scope.menuTakeaway = res.menuTakeaway;
              $scope.listMenuTR = res.listMenuTR;
              $scope.deliveryAreas = res.deliveryAreas;
    
              // update qm seller entity
              $Session.seller.setActiveMenuList($scope.menuCodOnline);

            setTodayTiming(res.restaurantDetail.currentDay);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $scope.activeDay = "Mon";
            $(".item.mon").addClass("active-day");
            break;
          case "Tue":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            //$scope.activeDay = true;
            console.log("default");
            break;
        }
      }






    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function(modal) {
        $scope.reviewModal = modal;
      });

      $scope.closeReviewsList = function() {
        $scope.reviewModal.hide();
      };

      $scope.openReviewsList = function() {
        $scope.reviewModal.show();
      };

      $scope.showReviewsForThis = function(sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingRestDetails', {
        url: '/tableBookingRestDetails/:sellerId',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-rest-details/table-booking-rest-details.html',
            controller: 'TableBookingRestDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('inhouse-bar-module')
    .controller('InhouseBarCartCTRL', controller)

  controller.$inject = ['$scope', 'ionicToast', '$state', '$Session', '$Easyajax', 'Loading'];

  function controller($scope, ionicToast, $state, $Session, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {
      var barID = $Session.inHouseBar.getId();
      var barSellerId = $Session.inHouseBar.getSellerId();


      $scope.$on('$ionicView.enter', function () {
        getCartItems();
      });

      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.placedlist.list || [];
            $scope.oc_adTaxList = data.placedlist.adTaxList || [];
            $scope.oc_listInfo = data.placedlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

          if (data.orderStatus == 3) {
            getCartItemsAfterBill();
          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getCartItemsAfterBill() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.deliveredlist.list || [];
            $scope.oc_adTaxList = data.deliveredlist.adTaxList || [];
            $scope.oc_listInfo = data.deliveredlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: UserCartListBarOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



      function generateBill() {
        Loading.show()
        var params = {
          barId: barID,
          sId: barSellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("generateBill >> ", data);
          if (data.status == "success") {
            $state.go('app.inhouseBarCheckout');
            ionicToast.show('Bill Generated successfully', 3000, '#42b');
          } else if (data.reason == "Bill already generated") {
            $state.go('app.inhouseBarCheckout');
            ionicToast.show(data.reason, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderBillServiceAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.placeOrder = function () {
        submitOrderToBar()
      }

      $scope.generateBill = function () {
        generateBill();
      }

    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-bar-module')
        .controller('InhouseBarCheckoutCTRL', controller)

    controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast'];

    function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {
            $scope.paymentLabel = '1';
            $scope.setOnline = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.DE = !false;
                howa.langToggle.EN = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            };

            $scope.setItemWise = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.EN = !false;
                howa.langToggle.DE = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            }

            $scope.setPriceWise = function(howa, type) {
                console.log("Order Type value:", type);
                //console.log("howa:", howa);
                howa.langToggle.FR = !false;
                howa.langToggle.DE = false;
                howa.langToggle.EN = false;
                $scope.paymentLabel = type;
            }

            $scope.gotoPayment = function() {

                if ($scope.paymentLabel == 1) {
                    $state.go("app.inhouseBarPayMyself");
                } else if ($scope.paymentLabel == 2) {
                    ionicToast.show("Pay Item wise option is not available", 3000, '#42B');
                    //$state.go("app.payItemWise");
                } else if ($scope.paymentLabel == 3) {
                    ionicToast.show("Pay Price wise option is not available", 3000, '#42B');
                    //$state.go("app.payPriceWise");
                }
            }


        }
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-bar-module')
    .controller('InhouseBarMenuCTRL', controller)

  controller.$inject = ['$scope', '$ionicModal', '$ionicSlideBoxDelegate', '$state', '$rootScope', 'Loading', '$Session', '$Easyajax', 'ionicToast'];

  function controller($scope, $ionicModal, $ionicSlideBoxDelegate, $state, $rootScope, Loading, $Session, $Easyajax, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var barID = $Session.inHouseBar.getId();
    var barSellerId = $Session.inHouseBar.getSellerId();
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {
      
      // Toggle Group part
      $scope.toggleGroup = function (group) {
        if ($scope.isGroupShown(group)) {
          $scope.shownGroup = null;
        } else {
          $scope.shownGroup = group;
        }
      };
      $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
      };



      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.$on('$ionicView.beforeEnter', function (e) {
        getCartItems();
      });

      getNormalBarDetails();

      function getNormalBarDetails() {
        Loading.show()

        var params = {
          sId: barSellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getNormalBarDetails >> ", data);
          if (data.status == "success") {
            $scope.barmenu = data.barMenu;
            $scope.barDetails = data.restaurantDetail;
            var restImages = JSON.parse(data.restImages.image)
            $scope.images = restImages
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderRestaurantMenuAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.nc_cartlist = [];
            $scope.nc_adTaxList = [];
            $scope.nc_listInfo = {};
            $scope.oc_placedlist = [];
            $scope.oc_adTaxList = [];
            $scope.oc_listInfo = {};
            $scope.nc_listInfo.Subtotal = $scope.nc_listInfo.Subtotal || 0;
            $scope.oc_listInfo.Subtotal = $scope.oc_listInfo.Subtotal || 0;


            if (data.cartlist.status == "success") {
              $scope.nc_cartlist = data.cartlist.list || [];
              $scope.nc_adTaxList = data.cartlist.adTaxList || [];
              $scope.nc_listInfo = data.cartlist.listInfo || {};
            }

            if (data.placedlist.status == "success") {
              $scope.oc_placedlist = data.placedlist.list || [];
              $scope.oc_adTaxList = data.placedlist.adTaxList || [];
              $scope.oc_listInfo = data.placedlist.listInfo || {};
            }
            for (var i in $scope.nc_cartlist) {
              var objectElement = $scope.nc_cartlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            console.log("nC sbtot ", $scope.nc_listInfo.Subtotal);
            console.log("OC sbtot ", $scope.oc_listInfo.Subtotal);
            $scope.totalElSubtotal = parseFloat($scope.nc_listInfo.Subtotal) + parseFloat($scope.oc_listInfo.Subtotal);

          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.addToCart = function (itemId) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          uId: userId,
          proId: itemId,
          barId: barID,
          // qty: 1
        };
        var callback = function (data) {
          Loading.hide();
          console.log("addToCart >> ", data);
          if (data.status == "success") {
            animateItYawala();
            getCartItems();
          } else {
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }

        var config = {
          typeRequest: "GET",
          urlRequest: AddToCartBarOrderServiceAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function animateItYawala() {
        var el = $("#dishcount"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }


      $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/Inhouse-bar-cart.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });

      // Triggered in the login modal to close it
      $scope.closeInhouseCartModal = function () {
        $scope.modal.hide();
      };

      // Open the login modal
      $scope.openInhouseCartModal = function () {
        $scope.modal.show();
      };


      $scope.checkout = function () {

        if ($scope.nc_cartlist.length == 0) {
          $state.go("app.viewCartInhouseBar");
        } else {
          $scope.openInhouseCartModal();
        }
      }


      function submitOrderToBar() {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          barId: barID,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitOrderToBar >> ", data);

          if (data.status == "success") {

            $scope.closeInhouseCartModal();
            $state.go('app.viewCartInhouseBar');
          } else {
            $scope.closeInhouseCartModal();
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderSendToRestaurantServiceAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.submitOrderToBar = function () {
        submitOrderToBar();
      }


    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-bar-module')
        .controller('InhouseBarPayMyselfCTRL', controller)

    controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast'];

    function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            var barID = $Session.inHouseBar.getId();
            var barSellerId = $Session.inHouseBar.getSellerId();

            $scope.paymentType = "";


            $scope.payCash = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = !false;
                howa.langToggle.FR = false;
                $scope.paymentType = type;
            };

            $scope.payOnline = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = false;
                howa.langToggle.FR = !false;
                $scope.paymentType = type;
            }


            $scope.doRefresh = function() {
                getCartItems();
                $scope.$broadcast('scroll.refreshComplete');
            };
            getCartItems();

            function getCartItems() {
                Loading.show()
                var params = {
                    barId: barID,
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("getCartItems >> ", data);
                    if (data.status == "success") {

                        $scope.oc_placedlist = data.deliveredlist.list || [];
                        $scope.oc_adTaxList = data.deliveredlist.adTaxList || [];
                        $scope.oc_listInfo = data.deliveredlist.listInfo || {};
                        for (var i in $scope.oc_placedlist) {
                            var objectElement = $scope.oc_placedlist[i];
                            objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
                        }

                    }

                }
                var config = {
                    typeRequest: "POST",
                    urlRequest: UserCartListBarOrderAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };
                $Easyajax.startRequest(config);
            }

            $scope.completeOrder = function() {
                if ($scope.paymentType == "") {
                    ionicToast.show("Please select payment type", 3000, '#42B');
                    return;
                }

                Loading.show();
                var params = {
                    paytype: $scope.paymentType,
                    sId: barSellerId,
                    barId: barID,
                    event: 1
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("completeOrder : ", data);

                    if (data.status == "success" && data.paymentType == "cash") {
                        $Session.order.setType("inHouseBar");
                        ionicToast.show("You have already Paid this Bill through Online payment", 3000, '#42B');
                        $state.go("thankyou");
                    } else if (data.status == "success" && data.paymentType == "online") {
                        localStorage.setItem("InhousePaymentLink", data.link);
                        $state.go("app.inhouseBarPayOnline");
                    } else {
                        ionicToast.show(data.reason, 3000, '#249E21');
                    }

                }
                var config = {
                    typeRequest: "GET",
                    urlRequest: BarOrderPaymentServiceAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };

                $Easyajax.startRequest(config);
            }

        }
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-bar-module')
    .controller('InhouseBarRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {

      // Calls
      getBarDetails();
      getCoverCharges()



      function getBarDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getCoverCharges() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId,
          bar_or_exchange: 0
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == 'success') {}
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarCoverChargeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.inhouseBarData = {};
      $scope.inhouseBarData.inhouseBarNum = $stateParams.sellerId;
      $scope.inhouseBarConfigs = {
        rules: {
          inhouseBarNum: {
            required: true
          },
          inhouseBarKey: {
            required: true
          },
          inhouseBarTableNum: {
            required: true
          }
        },
        messages: {
          inhouseBarNum: {
            required: "Please enter restuarant number"
          },
          inhouseBarKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseBarTableNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }
      }

      $scope.gotoInhouseBar = function (form) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          var params = {
            restNumId: $scope.inhouseBarData.inhouseBarNum,
            passKey: $scope.inhouseBarData.inhouseBarKey,
            tableNum: $scope.inhouseBarData.inhouseBarTableNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              ionicToast.show('Enjoy!', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($stateParams.sellerId);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarData.inhouseBarTableNum);
              $state.go("app.inhouseBarMenu");
            } else if (data.reason == "exist") {
              ionicToast.show('You already exist', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($stateParams.sellerId);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarData.inhouseBarTableNum);
              $state.go("app.inhouseBarMenu");
            } else {
              ionicToast.show(data.reason, 3000, '#42b');
            }
          }
          var config = {
            typeRequest: "GET",
            urlRequest: BarOrderRequestAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }

      }

    }
  }
})();

(function () {
  // 'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodCartNotServedCTRL', controller)

  controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";



    activate();

    function activate() {

      $scope.$on('$ionicView.enter', function (e) {
        inhouseCartList();
      });

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.cartList = data.userCartList.list;
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;
            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            // updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.increaseDecrease = function (cartId, type) {
        var params = {
          inhouseId: inhouseId,
          cartId: cartId,
          calcType: type
        };
        var callback = function (data) {
          console.log("increaseDecrease : ", data);
          inhouseCartList();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userCartInHouseIncDecAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.submitInhouseOrder = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId,
          afterEdit: 'AF'
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitInhouseOrder : ", data);
          if (data.status == 'success') {
            $state.go("app.viewCartInhouse");
          } else {
            alert("Minimum order");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: completeInHouseOredrAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.goToCart = function () {
        $state.go("app.viewCartInhouse");
      }

    }
  }
})();

(function () {
  // 'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodCartServedCTRL', controller)

  controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";


    activate();



    function activate() {


      $scope.noItems = true;
      $scope.$on('$ionicView.enter', function (e) {
        inhouseCartList();
      });

      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else if (data.status == 'success' && $scope.cartListOld == {}) {
            $scope.noItems = true;
          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      }

      $scope.generateBill = function (cartId, type) {
        Loading.show();
        var params = {
          sId: sellerId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $state.go("app.inhouseCheckout");
          } else if (data.reason == "Payment is pending and bill has been generated") {
            ionicToast.show("Payment is pending and bill has been generated", 3000, '#249E21');
            $state.go("app.inhouseCheckout");
          } else {
            ionicToast.show(data.reason, 3000, '#42B');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: PayGenrateInhouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();

(function () {
  "use strict";

  angular
    .module("inhouse-food-module")
    .controller("InhouseFoodMenuCTRL", controller);

  controller.$inject = ["$scope", "Loading", "$Session", "$Easyajax", "$state", "$ionicPopup", "ionicToast"];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";
    $scope.totalCartItems = 0;
    $scope.cartList = [];
    $scope.cartListOld = [];

    activate();

    function activate() {
      $scope.$on("$ionicView.enter", function (event, data) {
        // getInhouseRestaurantDetails();
        updateInhouseCartWithoutLoader();
      });

      // Toggle part
      // $scope.toggleGroup = function(group) {
      //     if ($scope.isGroupShown(group)) {
      //         $scope.shownGroup = null;
      //     } else {
      //         $scope.shownGroup = group;
      //     }
      // };
      // $scope.isGroupShown = function(group) {
      //     return $scope.shownGroup === group;
      // };
      //
      getInhouseRestaurantDetails();


      function getInhouseRestaurantDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getInhouseRestaurantDetails >> ", data);

          $scope.restaurantDetail = data.restaurantDetail;
          $scope.menuCodOnline = data.menu;

          //Setting restaurant details to Session
          $Session.inHouse.setSellerDiscountPercent(
            data.restaurantDetail.discountInPercent
          );
          $Session.inHouse.setSellerDiscountAmount(
            data.restaurantDetail.discountInRs
          );
          $Session.inHouse.setSellerName(data.restaurantDetail.restaurantName);
          $Session.inHouse.setSellerMinCharge(data.restaurantDetail.minOrder);
          $Session.inHouse.setSellerMinDiscount(
            data.restaurantDetail.minAmountForDis
          );
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function updateInhouseCart() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("update cart : ", data);
          if (data.status == "success") {
            $scope.cartList = data.userCartList.list || [];
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;

            $scope.cartListOld = data.userCartTaxOld.list || [];
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            console.log("$scope.cartList >> ", $scope.cartList);
            console.log("$scope.cartListOld >> ", $scope.cartListOld);
            updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function updateInhouseCartWithoutLoader() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          console.log("update cart : ", data);
          if (data.status == "success") {
            $scope.cartList = data.userCartList.list || [];
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;

            $scope.cartListOld = data.userCartTaxOld.list || [];
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            console.log("$scope.cartList >> ", $scope.cartList);
            console.log("$scope.cartListOld >> ", $scope.cartListOld);
            updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



      function updateCartBadge(list, oldList) {
        var newLength = list.length || 0;
        var oldLength = oldList.length || 0;
        $scope.totalCartItems = newLength + oldLength;
      }

      $scope.addToCart = function (proId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        // animateItYawala();
        Loading.show();
        var params = {
          proId: proId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          console.log("addToCart : ", data);
          if (data.status == "success") {
            updateInhouseCart();
          } else {
            ionicToast.show(data.reason, 3000, "#42B");
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: addToCartInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function animateItYawala() {
        var el = $("#Bar-Cart h5 span"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      $scope.submitOrgenerate = function () {
        if ($scope.cartList == undefined && $scope.cartListOld == undefined) {
          $ionicPopup.alert({
            title: "Empty Cart",
            template: "Please add whatever you like from our food Menu!",
            okType: "button-assertive"
          });
          return;
        }
        if ($scope.cartList == undefined) {
          $state.go("app.viewCartInhouse");
        } else {
          $state.go("app.viewCartInhouseModal");
        }
      };
    }
  }
})();

/***
 * Code Trash
 * 
 * 
 * 
function getInhousedetail() {
var params = {
    sId: sellerId,
    uId: userId,
    inhouseId: inhouseId
};
var callback = function(data) {
    console.log("getInhousedetail >> ", data);
};
var config = {
    typeRequest: "POST",
    urlRequest: inHouseDetailAPI,
    dataRequest: params,
    sucesCallbackRequest: callback
};
$Easyajax.startRequest(config);
};


// function checkMinimumCharge() {
//     var total = parseInt($Session.inHouse.getOrderTotalAmount());
//     var minCharge = parseInt($Session.inHouse.getSellerMinCharge());
//     if (total < minCharge) {
//         alert("check total amount < min charge");
//         return false;
//     } else {
//         return true;
//     }
// }




 * 
 * 
 * 
 */

(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodPayItemWiseCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast', '$cordovaContacts', '$ionicPlatform', '$ionicModal'];

  function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast, $cordovaContacts, $ionicPlatform, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var mobileNum_G, memberName_G;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var userId = $Session.user.getId() ||"0" ;

    activate();
    $scope.feSelect = false;
    $scope.checkItems = {}
    $scope.checkedBoxes = []
    $scope.cart = {
      items: []
    };

    $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/contacts-itemwise-modal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeContactsModal = function () {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.openContactsModal = function () {
      $scope.modal.show();
    };

    function activate() {


      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      }


      $scope.selectItemWise = function (model) {
        console.log("Model item > ", model.item);

        var __CheckedBoxes = [];
        angular.forEach($scope.checkItems, function (value, key) {
          if (value) {
            __CheckedBoxes.push(parseInt(key));
            $scope.checkedBoxes.push(parseInt(key));
          }
        });
        $scope.checkedBoxes = __CheckedBoxes;
        $scope.feSelect = getSelection(__CheckedBoxes);
        console.log(" $scope.checkedBoxes")
        console.log($scope.checkedBoxes)

      }

      var getSelection = function (checkedList) {
        if (checkedList.length != 0) {
          return true;
        } else {
          return false;
        }
      }

      $scope.sendItemTo = function () {
        $scope.openContactsModal();
      }


      $scope.pickContactUsingNativeUI = function () {
        $scope.contacts = {
          member: []
        };
        $cordovaContacts.pickContact().then(function (contactPicked) {
            var contactsLength = $scope.contacts.member.length;
            if (contactsLength == 0) {
              var contactNo = contactPicked.phoneNumbers[0].value.toString().replace(/\(|\)|-|\s/g, '');
              var validContact = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(contactNo);

              if (validContact) {
                $scope.contacts.member.push({
                  "memberName": contactPicked.displayName.toString(),
                  "memberNumber": contactNo
                });
                $scope.selectedContactName = contactPicked.displayName.toString();
              } else {

                ionicToast.show("Please select Valid contact number", 3000, '#42B');
              }
            } else if (contactsLength >= 1) {
              ionicToast.show("Only one contact is allowed to add per item", 3000, '#42B');
            }


            mobileNum_G = $scope.contacts.member["0"].memberNumber;
            memberName_G = $scope.contacts.member["0"].memberName;
          },
          function (err) {
            console.error("Error happend when pickup contact ", err);
          });
      }

      // $scope.getAllContacts = function() {
      //     var opts = {
      //         multiple: true,
      //     };
      //     $cordovaContacts.find(opts).then(function(allContacts) {
      //         $scope.contacts = allContacts;
      //     }, function(err) {
      //         alert(err);
      //     });
      // };


      $scope.submitNumber = function () {
        if ($scope.contacts.member.length == 0) {
          ionicToast.show("Please add mobile number from your contacts", 3000, '#42B');
        } else {

          for (var i in $scope.checkedBoxes) {

            console.log(i);
            var hiddenitemPrice = $(".class_" + $scope.checkedBoxes[i]).val();
            $scope.cart.items.push({
              "inhouseId": inhouseId,
              "cartId": $scope.checkedBoxes[i],
              "price": hiddenitemPrice,
              "mobileNum": mobileNum_G
            });
            $("#item_details" + $scope.checkedBoxes[i]).html("<h5>**This item will be paid by <span>" + memberName_G + "</span></h5>");

          }

          angular.forEach($scope.checkItems, function (value, key) {
            if (value) {
              $scope.checkItems[key] = false;
              $scope.feSelect = false;
            }
          });

          $scope.closeContactsModal();
        }
      }

      $scope.completeOrder = function () {
        if ($scope.cart.items.length === $scope.cartListOld.length) {

          Loading.show();
          var params = {
            jsonData: JSON.stringify($scope.cart),
            inhouseId: inhouseId,
            inhousePaytype: "1"
          };

          var callback = function (data) {
            Loading.hide();

            console.log("inhouseCartList : ", data);
            if (data.status == 'success') {
              ionicToast.show("Bill Paid and sent to your contacts", 3000, '#249E21');
              $state.go("thankyou");
            } else {
              alert(JSON.stringify(data))
              ionicToast.show("Try Again Later", 3000, '#42B');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: ItemWiseBillSplitSubmitAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };

          $Easyajax.startRequest(config);
        } else {
          ionicToast.show("Please make sure all items are assigned to a contact number", 3000, '#42b');
        }
      }
    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-food-module')
        .controller('InhouseFoodCheckoutCTRL', controller)

    controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast'];

    function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            $scope.paymentLabel = '1';
            $scope.setOnline = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.DE = !false;
                howa.langToggle.EN = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            };

            $scope.setItemWise = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.EN = !false;
                howa.langToggle.DE = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            }

            $scope.setPriceWise = function(howa, type) {
                console.log("Order Type value:", type);
                //console.log("howa:", howa);
                howa.langToggle.FR = !false;
                howa.langToggle.DE = false;
                howa.langToggle.EN = false;
                $scope.paymentLabel = type;
            }

            $scope.gotoPayment = function() {
                if ($scope.paymentLabel == 1) {
                    $state.go("app.inhouseFoodPayMyself");
                } else if ($scope.paymentLabel == 2) {
                    ionicToast.show("Pay Item wise option is not available", 3000, '#42B');
                    // $state.go("app.payItemWise");
                } else if ($scope.paymentLabel == 3) {
                    ionicToast.show("Pay Price wise option is not available", 3000, '#42B');
                    //$state.go("app.payPriceWise");
                }
            }


        }
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodPayMyselfCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast'];

  function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var userId = $Session.user.getId() ||"0" ;


    activate();

    function activate() {


      $scope.paymentType = "";


      $scope.payCash = function (howa, type) {
        console.log("Order Type value:", type);
        // console.log("howa:", howa);
        howa.langToggle.EN = !false;
        howa.langToggle.FR = false;
        $scope.paymentType = type;
      };

      $scope.payOnline = function (howa, type) {
        console.log("Order Type value:", type);
        // console.log("howa:", howa);
        howa.langToggle.EN = false;
        howa.langToggle.FR = !false;
        $scope.paymentType = type;
      }


      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.completeOrder = function () {
        if ($scope.paymentType == "") {
          ionicToast.show("Please select payment type", 3000, '#42B');
          return false;
        }
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          delType: $scope.paymentType,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();

          if (data.status == "failure") {
            ionicToast.show("Please contact restaurant regarding this issue", 3000, '#42B');
          } else if (data.status == "failure" && data.reason == "inhouse order items is missing") {
            ionicToast.show("You have already Paid this Bill through Online payment", 3000, '#42B');
            $state.go("app.history");
          } else if (data.status == "success") {
            if ($scope.paymentType == "cod") {
              ionicToast.show("Bill Paid", 3000, '#249E21');
              $state.go("thankyou");
              $Session.order.setType("inHouseFood");
            } else if ($scope.paymentType == "online") {
              localStorage.setItem("InhousePaymentLink", data.link);
              $state.go("app.inhouseFoodPayOnline");
            } else {
              ionicToast.show("you are trtying something wrong .. please check deltype", 3000, '#249E21');
            }
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: inHouseOederPayNowAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-food-module')
        .controller('InhouseFoodPayOnlineCTRL', paymentCTRL)


    function paymentCTRL() {
        var link = localStorage.getItem("InhousePaymentLink");
        $("#payment").attr("src", link);
    }

})();
(function() {
    'use strict';

    angular
        .module('inhouse-food-module')
        .controller('InhouseFoodPayPriceWiseCTRL', controller)

    controller.$inject = ['$location'];

    function controller($location) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {}
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', '$ionicPopup', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, $ionicPopup, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() ||"0" ;

    
    activate();

    function activate() {

      getRestaurantsDetails();


      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restaurantDetail = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.inhouseData = {};
      $scope.inhouseData.restNum = $stateParams.sellerId;
      $scope.inhouseConfigs = {
        rules: {
          inhouseRestNum: {
            required: true
          },
          inhouseRestKey: {
            required: true
          },
          inhouseTableNum: {
            required: true
          }
        },
        messages: {
          inhouseRestNum: {
            required: "Please enter restuarant number"
          },
          inhouseRestKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseTableNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }

      };

      $scope.gotoInhouseRest = function (form) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          var params = {
            restNumId: $scope.inhouseData.restNum,
            restPassKeyId: $scope.inhouseData.restKey,
            tableNumId: $scope.inhouseData.tableNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              $Session.inHouse.setId(data.inhouseId);
              $Session.inHouse.setSellerId($scope.inhouseData.restNum);
              $Session.inHouse.setTableNumber($scope.inhouseData.tableNum);
              $state.go("app.inHouseFoodMenu");
            } else if (data.status == "failure" && data.message == "Your have already reserved or sent request for your table" && data.paymentType == "Did not send order") {
              $Session.inHouse.setId(data.inhouseId);
              $state.go("app.inHouseFoodMenu");
            } else if (data.status == "failure" && data.message == "Your have already reserved or sent request for your table") {
              $scope.showConfirm(data);
            } else if (data.status == "failure" && data.reason == "Please ask for valid pass key") {
              ionicToast.show('Please ask for valid pass key', 3000, '#42b');
            } else if (data.status == 'failure' && data.inhouseId != undefined) {
              $Session.inHouse.setId(data.inhouseId);
              $state.go("app.inHouseFoodMenu");
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: inHouseCheckRestaurantAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      }

      // A confirm dialog
      $scope.showConfirm = function (data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Complete Order',
          cancelText: "Pay Now",
          cancelType: "button-assertive",
          okText: "Continue Shopping",
          okType: "button-balanced",
          template: data.message + ", and " + data.paymentType + " , Make your choice"
        });
        confirmPopup.then(function (res) {
          if (res) {
            $Session.inHouse.setId(data.inhouseId);
            $Session.inHouse.setSellerId($scope.inhouseData.restNum);
            $Session.inHouse.setTableNumber($scope.inhouseData.tableNum);
            $state.go("app.inHouseFoodMenu");
          } else {
            $state.go("app.inhouseCheckout");
          }
        });
      };

    }
  }
})();

(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InhouseBarExCartCTRL', controller)

  controller.$inject = ['$scope', 'ionicToast', '$state', '$Session', '$Easyajax', 'Loading'];

  function controller($scope, ionicToast, $state, $Session, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {
      var barID = $Session.inHouseBar.getId();
      var barSellerId = $Session.inHouseBar.getSellerId();


      $scope.$on('$ionicView.enter', function () {
        getCartItems()
      });

      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.placedlist.list || [];
            $scope.oc_adTaxList = data.placedlist.adTaxList || [];
            $scope.oc_listInfo = data.placedlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

          if (data.orderStatus == 3) {
            getCartItemsAfterBill();
          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getCartItemsAfterBill() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.placedlist.list || [];
            $scope.oc_adTaxList = data.placedlist.adTaxList || [];
            $scope.oc_listInfo = data.placedlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

          if (data.orderStatus == 3) {
            getCartItemsAfterBill();
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function generateBill() {
        Loading.show()
        var params = {
          barId: barID,
          sId: barSellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("generateBill >> ", data);
          if (data.status == "success") {
            $state.go('app.inhouseBarExCheckout');
            ionicToast.show('Bill Generated successfully', 3000, '#42b');
          } else if (data.reason == "Bill already generated") {
            $state.go('app.inhouseBarExCheckout');
            ionicToast.show(data.reason, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeBillAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.placeOrder = function () {
        submitOrderToBar()
      }

      $scope.generateBill = function () {
        generateBill();
      }

    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-bar-ex-module')
        .controller('InhouseBarExCheckoutCTRL', controller)

    controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast'];

    function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            $scope.paymentLabel = '1';
            $scope.setOnline = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.DE = !false;
                howa.langToggle.EN = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            };

            $scope.setItemWise = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.EN = !false;
                howa.langToggle.DE = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            }

            $scope.setPriceWise = function(howa, type) {
                console.log("Order Type value:", type);
                //console.log("howa:", howa);
                howa.langToggle.FR = !false;
                howa.langToggle.DE = false;
                howa.langToggle.EN = false;
                $scope.paymentLabel = type;
            }

            $scope.gotoPayment = function() {

                if ($scope.paymentLabel == 1) {
                    $state.go("app.inhouseBarExPayMyself");
                } else if ($scope.paymentLabel == 2) {
                    ionicToast.show("Pay Item wise option is not available", 3000, '#42B');
                    //$state.go("app.payItemWise");
                } else if ($scope.paymentLabel == 3) {
                    ionicToast.show("Pay Price wise option is not available", 3000, '#42B');
                    //$state.go("app.payPriceWise");
                }
            }


        }
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InHouseBarExMenuCTRL', controller)

  controller.$inject = ['$scope', '$ionicSlideBoxDelegate', 'ionicToast', '$ionicModal', '$state', '$rootScope', 'Loading', '$Session', '$Easyajax', 'CountdownTimer'];

  function controller($scope, $ionicSlideBoxDelegate, ionicToast, $ionicModal, $state, $rootScope, Loading, $Session, $Easyajax, CountdownTimer) {
    /* jshint validthis:true */
    var vm = this;
    var barID = $Session.inHouseBar.getId();
    var barSellerId = $Session.inHouseBar.getSellerId();
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {


      getBarExDetails();

      $scope.$on('$ionicView.enter', function () {
        getCartItems();
      });


      function getBarExDetails() {
        // Loading.show()
        var params = {
          sId: barSellerId,
          bar_or_exchange: 1
        };
        var callback = function (data) {
          // Loading.hide();
          console.log("getBarExDetails >> ", data);
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
            $scope.barMenu = data.barMenu;
            $scope.deliveryAreas = data.deliveryAreas;
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeRestaurantMenuAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500,
        pagination: false
      }
      $scope.images = [{
          MediaUrl: "assets/img/bar/1.png"
        },
        {
          MediaUrl: "assets/img/bar/2.png"
        },
        {
          MediaUrl: "assets/img/bar/3.png"
        },
        {
          MediaUrl: "assets/img/bar/4.png"
        },
        {
          MediaUrl: "assets/img/bar/5.png"
        }
      ]
      setTimeout(function () {
        $ionicSlideBoxDelegate.slide(0);
        $ionicSlideBoxDelegate.update();
        $scope.$apply();
      });


      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {
            $scope.nc_cartlist = [];
            $scope.nc_adTaxList = [];
            $scope.nc_listInfo = {};
            $scope.oc_placedlist = [];
            $scope.oc_adTaxList = [];
            $scope.oc_listInfo = {};
            $scope.nc_listInfo.Subtotal = $scope.nc_listInfo.Subtotal || 0;
            $scope.oc_listInfo.Subtotal = $scope.oc_listInfo.Subtotal || 0;
            if (data.cartlist.status == "success") {
              $scope.nc_cartlist = data.cartlist.list;
              $scope.nc_adTaxList = data.cartlist.adTaxList;
              $scope.nc_listInfo = data.cartlist.listInfo;
            }
            if (data.placedlist.status == "success") {
              $scope.oc_placedlist = data.placedlist.list;
              $scope.oc_adTaxList = data.placedlist.adTaxList;
              $scope.oc_listInfo = data.placedlist.listInfo;
            }
            for (var i in $scope.nc_cartlist) {
              var objectElement = $scope.nc_cartlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            console.log("nC sbtot ", $scope.nc_listInfo.Subtotal);
            console.log("OC sbtot ", $scope.oc_listInfo.Subtotal);
            $scope.totalElSubtotal = parseFloat($scope.nc_listInfo.Subtotal) + parseFloat($scope.oc_listInfo.Subtotal);

          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function startTime() {
        $scope.show = false
        setTimeout(function () {
          ionicToast.show("Prices are going to change after 60 seconds!", 1500, '#42b');
          $scope.show = true;
          $scope.timer = new CountdownTimer(60000);
          $scope.timer.start();
          $scope.timer.$$deferred.promise.then(function () {
            getBarExDetails();
            getCartItems();
            ionicToast.show("Prices CHANGED!", 1500, '#42b');
          })
        }, 5)
      }

      $scope.addToCart = function (itemId) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        startTime();
        Loading.show()
        var params = {
          uId: userId,
          proId: itemId,
          barId: barID,
          // qty: 1
        };
        var callback = function (data) {
          Loading.hide();
          console.log("addToCart >> ", data);
          if (data.status == "success") {
            animateItYawala();
            getCartItems();
            return;
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: AddToCartBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function animateItYawala() {
        var el = $("#dishcount"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }


      $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/Inhouse-bar-ex-cart.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });


      // Triggered in the login modal to close it
      $scope.closeInhouseCartModal = function () {
        $scope.modal.hide();
      };


      // Open the login modal
      $scope.openInhouseCartModal = function () {
        $scope.modal.show();
      };


      $scope.checkout = function () {
        if ($scope.nc_cartlist.length == 0) {
          $state.go("app.inhouseBarExCart");
        } else {
          $scope.openInhouseCartModal();
        }

      }


      function submitOrderToBar() {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          barId: barID,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitOrderToBar >> ", data);
          if (data.status == "success") {
            $scope.closeInhouseCartModal();
            $state.go('app.inhouseBarExCart');
          } else {
            $scope.closeInhouseCartModal();
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeSendToRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.submitOrderToBar = function () {
        submitOrderToBar();
      }


      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

    }
  }
})();

(function() {
    'use strict';

    angular
        .module('inhouse-bar-ex-module')
        .controller('InhouseBarExPayMyselfCTRL', controller)

    controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast'];

    function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            var barID = $Session.inHouseBar.getId();
            var barSellerId = $Session.inHouseBar.getSellerId();


            $scope.paymentType = "";


            $scope.payCash = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = !false;
                howa.langToggle.FR = false;
                $scope.paymentType = type;
            };

            $scope.payOnline = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = false;
                howa.langToggle.FR = !false;
                $scope.paymentType = type;
            }


            $scope.doRefresh = function() {
                getCartItems();
                $scope.$broadcast('scroll.refreshComplete');
            };
            getCartItems();

            function getCartItems() {
                Loading.show()
                var params = {
                    barId: barID,
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("getCartItems >> ", data);
                    if (data.status == "success") {

                        $scope.oc_placedlist = data.deliveredlist.list || [];
                        $scope.oc_adTaxList = data.deliveredlist.adTaxList || [];
                        $scope.oc_listInfo = data.deliveredlist.listInfo || {};
                        for (var i in $scope.oc_placedlist) {
                            var objectElement = $scope.oc_placedlist[i];
                            objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
                        }

                    }

                }
                var config = {
                    typeRequest: "POST",
                    urlRequest: UserCartListBarExchangeAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };
                $Easyajax.startRequest(config);
            }

            $scope.completeOrder = function() {
                if ($scope.paymentType == "") {
                    ionicToast.show("Please select payment type", 3000, '#42B');
                    return;
                }

                Loading.show();
                var params = {
                    paytype: $scope.paymentType,
                    sId: barSellerId,
                    barId: barID,
                    event: 1
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("completeOrder : ", data);

                    if (data.status == "success" && data.paymentType == "cash") {
                        $Session.order.setType("inHouseBarEx");
                        ionicToast.show("You have already Paid this Bill through Online payment", 3000, '#42B');
                        $state.go("thankyou");
                    } else if (data.status == "success" && data.paymentType == "online") {
                        localStorage.setItem("InhousePaymentLink", data.link);
                        $state.go("app.inhouseBarExPayOnline");
                    } else {
                        ionicToast.show(data.reason, 3000, '#249E21');
                    }

                }
                var config = {
                    typeRequest: "GET",
                    urlRequest: BarExchangePaymentAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };

                $Easyajax.startRequest(config);
            }

        }
    }
})();
(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InhouseBarExRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    
    activate();

    function activate() {

      // Calls
      getBarDetails();
      getCoverCharges();

      function getBarDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getCoverCharges() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId,
          bar_or_exchange: 1
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {

          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarCoverChargeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.inhouseBarExData = {}
      $scope.inhouseBarExData.inhouseBarExNum = $stateParams.sellerId;
      $scope.inhouseBarExConfigs = {
        rules: {
          inhouseBarExNum: {
            required: true
          },
          inhouseBarExKey: {
            required: true
          },
          inhouseTableBarExNum: {
            required: true
          }
        },
        messages: {
          inhouseBarExNum: {
            required: "Please enter restuarant number"
          },
          inhouseBarExKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseTableBarExNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }
      }

      $scope.gotoInhouseBarEx = function (form) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          Loading.show();
          var params = {
            restNumId: $scope.inhouseBarExData.inhouseBarExNum,
            passKey: $scope.inhouseBarExData.inhouseBarExKey,
            tableNum: $scope.inhouseBarExData.inhouseTableBarExNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              ionicToast.show('Enjoy!', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($scope.inhouseBarExData.inhouseBarExNum);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarExData.inhouseTableBarExNum);
              $state.go("app.inHouseBarExMenu");
            } else if (data.reason == 'exist') {
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($scope.inhouseBarExData.inhouseBarExNum);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarExData.inhouseTableBarExNum);
              ionicToast.show('Order exists!', 3000, '#42b');
              $state.go("app.inHouseBarExMenu");
            } else {
              ionicToast.show(data.reason, 3000, '#42b');
            }
          }
          var config = {
            typeRequest: "GET",
            urlRequest: BarExchangeRequestAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      }

    }
  }
})();
