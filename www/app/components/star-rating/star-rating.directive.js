(function () {
  'use strict';

  angular
    .module('star-rating-component-module')
    .directive('starRating', directive);

  directive.$inject = ['$window'];

  function directive($window) {
    // Usage:
    //     <star-rating></star-rating>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/star-rating/star-rating.html',
      link: link,
      restrict: 'EA',
      scope: {
        onView: '&',
        rates: '='
      }
    };
    return directive;

    function link(scope, element, attrs) {
      // unwrap the function
      console.log(scope)
      // scope.onView = scope.onView();
    //   element.bind("click",function() {
    //     scope.$apply(function() {
    //       onView(data);    
    //       scope.onView = scope.onView();                    // ...or this way
    //     });
    // });
    }
  }

})();
