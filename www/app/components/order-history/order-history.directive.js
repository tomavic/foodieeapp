(function () {
    'use strict';

    angular
        .module ('order-history-component-module')
        .directive ('orderHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <order-history></order-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/order-history/order-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();