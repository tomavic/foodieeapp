(function() {
    'use strict';

    angular
        .module('rest-card-component-module')
        .directive('restaurantCard', directive);

    directive.$inject = ["$compile"];

    function directive($compile) {

        /**
         * @Usage 
         *     <restaurant-card 
         *       ng-repeat="restaurant in restaurantsList
         *       restaurant="restaurant"
         *       isLiked="true or false"
         *       on-like="favouriteListChanged(sellerId)"
         *       on-review="showReviewsForThis(restaurant)"
         *       full-view="true or false"
         *       bookTableView="true or false"
         *       favouriteCardView="true or false"
         *     </restaurant-card>
         * 
         */
        var directive = {
            templateUrl: 'app/components/restaurant-card/restaurant-card.html',
            link: link,
            restrict: 'EA',
            scope: {
                restaurant: '=',
                isLiked: '=',
                onLike: '&',
                onReview: '&',
                onQueue: '&',
                favouriteCardView: "=",
                bookTableView: '=',
                fullView: '=',
                qmView: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            // $compile(element.contents())(scope.$new());
            // scope.gotoRestaurant = function(r) {
            //     console.log(r);
            //     $state.go('app.history');
            // }
            if (!attrs.fullView) { attrs.fullView = false; }
            if (!attrs.bookTableView) { attrs.bookTableView = false; }
            if (!attrs.favouriteCardView) { attrs.favouriteCardView = false; }
            if (!attrs.qmView) { attrs.qmView = false; }
        }
    }

})();