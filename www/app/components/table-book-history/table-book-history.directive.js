(function () {
    'use strict';

    angular
        .module ('table-book-history-component-module')
        .directive ('tableBookHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <table-book-history></table-book-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/table-book-history/table-book-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();