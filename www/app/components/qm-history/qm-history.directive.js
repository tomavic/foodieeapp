(function () {
    'use strict';

    angular
        .module ('qm-history-component-module')
        .directive ('qmHistory', directive);

    directive.$inject = ['$compile'];

    function directive($compile) {
        // Usage:
        //     <qm-history></qm-history>
        // Creates:
        //
        var directive = {
            templateUrl: 'app/components/qm-history/qm-history.html',
            link: link,
            restrict: 'EA',
            scope: {
              order: '='
          }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();