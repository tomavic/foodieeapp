(function () {
  'use strict';

  angular
    .module('error-content-component-module')
    .directive('ionErrorContent', directive);

  directive.$inject = ['$window'];

  function directive($window) {
    // Usage:
    //     <ion-error-content></ion-error-content>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/error-content/error-content.html',
      link: link,
      restrict: 'EA',
      scope: {
        onRefresh: '&',
        message: '='
      }

      // changed message from @ to = 30-8
    };
    return directive;

    function link(scope, element, attrs) {}
  }

})();
