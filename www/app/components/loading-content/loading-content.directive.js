(function () {
  'use strict';

  angular
    .module('loading-content-component-module')
    .directive('loadingContent', directive);

  function directive() {
    // Usage:
    //     <loading-content></loading-content>
    // Creates:
    //
    var directive = {
      templateUrl: 'app/components/loading-content/loading-content.html',
      link: link,
      restrict: 'EA',
      scope: {
        onRefresh: '&'
      }
    };
    return directive;

    function link(scope) {
      scope.loadingItems = [1,2,3,4];
    }
  }

})();
