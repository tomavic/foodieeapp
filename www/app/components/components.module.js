(function () {
  'use strict';

  angular.module('components-module', [
    'error-content-component-module',
    'loading-content-component-module',
    'qm-history-component-module',
    'rest-card-component-module',
    'table-book-history-component-module',
    'star-rating-component-module',
    'order-history-component-module',
  ]);
})();
