(function () {
  'use strict';

  angular
    .module('signup-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/pages/Signup/signup.html',
        controller: 'signupCTRL',
        controllerAs: 'vm'
      });
  }

}());
