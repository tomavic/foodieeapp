(function () {
  'use strict';

  angular
    .module('signup-module')
    .controller('signupCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$ionicPopup', '$state', '$Easyajax', 'Loading', 'ionicToast', 'Facebook'];

  function controller($scope, $Session, $ionicPopup, $state, $Easyajax, Loading, ionicToast, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    $scope.signupData = {};
    $scope.otpData = {};

    activate();

    function activate() {

      /************************
       * Signup configs
       ************************/
      $scope.signupConfigs = {
        // validation configs - rules
        rules: {
          firstName: {
            required: true,
            string: true
          },
          lastName: {
            required: true,
            string: true
          },
          emailIdup: {
            required: true,
            validEmail: true
          },
          passwordup: {
            required: true,
            strongPassword: true
          },
          confirmPassword: {
            equalTo: "#passwordup",
            required: true
          },
          mobileNo: {
            required: true,
            indianMobNumber: true
          },
        },
        // validation configs - error messages
        messages: {
          firstName: {
            required: "First name is required",
            string: "First name should contain only letters"
          },
          lastName: {
            required: "Last name is required",
            string: "Last name should contain only letters"
          },
          emailIdup: {
            required: "Email address is required",
            validEmail: "Valid email address is required"
          },
          passwordup: {
            required: "Create your password",
            strongPassword: "Enter password with 8 to 12 length, should contain at least one letter and one number"
          },
          confirmPassword: {
            equalTo: "Passwords are not matched!",
            required: "Confirm password is required"
          },
          mobileNo: {
            required: "Mobile number is required",
            indianMobNumber: "Enter valid 8 or 10 digits mobile number"
          },
        }
      };

      vm.doSignup = function(form) {
        if (form.validate()) {
          Loading.show();
          var params = {
            event: "send_signup_otp",
            mobile_number: $scope.signupData.mobile,
            emailId: $scope.signupData.email
          };
          var callback = function (data) {
            Loading.hide();
            data.status === 'success' ? showSignupOTPPopup() : ionicToast.show(data.reason, 3500, '#b50905');
          };
          var config = {
            typeRequest: "POST",
            urlRequest: sendOtpApi,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      /************************
       * Signup OTP popup 
       ************************/
       function showSignupOTPPopup() {
        
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="number" name="otpCode" id="otpCode" ng-model="otpData.code" placeholder="OTP Code" required/>',
          title: 'Complete Registration',
          subTitle: 'Please enter 4 digits OTP code sent to your mobile',
          cssClass: "signupOTPPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.otpData.code || !/^\d{4}$/.test($scope.otpData.code)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid 4 digits OTP', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  verifyAndCreateAccount();
                }
              }
            }
          ]
        });
      };

       function verifyAndCreateAccount() {
        Loading.show();
        var params = {
          fromApp: "1",
          emailId: $scope.signupData.email,
          mobileNo: $scope.signupData.mobile,
          password: $scope.signupData.password,
          firstName: $scope.signupData.firstName,
          lastName: $scope.signupData.lastName,
          passcode: $scope.otpData.code
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            setUserInfoSignup(data);
          } else {
            ionicToast.show(data.reason, 3500, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userRegisterAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function setUserInfoSignup(data) {
        $Session.user.setUserInfo({
          id: data.userId,
          name: $scope.signupData.firstName + ' ' + $scope.signupData.lastName,
          image: data.userImage,
          mobile: $scope.signupData.mobile,
          address: data.address,
          email: $scope.signupData.email,
          city: data.cityName,
          area: data.cityArea,
          pin: data.pincode
        });
        $scope.signupData = {};
        $scope.otpData = {};
        $state.go('home');
      }

      vm.goLogin = function() {
        $state.go('login');
      }



    }
  }
})();
