(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('queueMenuListController', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', '$ionicPopup', 'menuList', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, menuList, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    var sellerId = $Session.seller.getId();
    $scope.$on('$ionicView.enter', function (e) {

      $scope.menu = menuList;

    });


    activate();

    function activate() {

      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      }
      
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      }

    }
  }
})();
