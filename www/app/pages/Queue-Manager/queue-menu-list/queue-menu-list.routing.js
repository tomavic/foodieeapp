(function () {
    'use strict';
  
    angular
      .module('queue-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.queueMenuList', {
        url: '/queueMenuList',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-menu-list/queue-menu-list.html',
            controller: 'queueMenuListController',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  