(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queuerestdetails', {
        url: '/queuerestdetails/:sellerId',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-rest-details/queue-rest-details.html',
            controller: 'queueRestDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
