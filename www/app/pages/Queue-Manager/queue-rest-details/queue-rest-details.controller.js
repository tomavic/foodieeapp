(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('queueRestDetailsCTRL', controller)

  controller.$inject = ['$scope', '$state', '$stateParams', 'Loading', '$Easyajax', '$ionicModal', '$Session'];

  function controller($scope, $state, $stateParams, Loading, $Easyajax, $ionicModal, $Session) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $stateParams.sellerId;
    $scope.sellerId = $stateParams.sellerId;

    $scope.feRestaurantDetails = true;
    $scope.options = {
      loop: true,
      effect: 'fade',
      speed: 500,
    }

    // Review Modal
    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function (modal) {
        $scope.reviewModal = modal;
      });
    $scope.closeReviewsList = function () {
      $scope.reviewModal.hide();
    };
    $scope.openReviewsList = function () {
      $scope.reviewModal.show();
    };

    activate();

    function activate() {

      $scope.doRefresh = function () {
        getRestDetails();
        // $Geolocation.requestGeolocation(getloactionForQM);
        $scope.$broadcast('scroll.refreshComplete');
      };
      

      //calls
      getRestDetails()


      function getRestDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (res) {
          Loading.hide();
          if(res.status == "failure") {
            $scope.feRestaurantDetails = false;
            return;
          }
          $scope.restaurantDetail = res.restaurantDetail;
          $scope.feRestaurantDetails = true;
          $scope.menuCodOnline = res.menuCodOnline;
          $scope.menuPreorder = res.menuPreorder;
          $scope.menuTakeaway = res.menuTakeaway;
          $scope.listMenuTR = res.listMenuTR;
          $scope.ourServices = res.restaurantDetail.ourServices;
          var aa = res.deliveryAreas[0].cityAreas;
          $scope.deliveryAreas = aa.replace(/\s\s+/g, ' ').substr(aa.indexOf(",")+1).split(',');
          // update qm seller entity
          $Session.seller.setActiveMenuList($scope.menuCodOnline);

          setTodayTiming(res.restaurantDetail.currentDay);
        }
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $(".item.mon").addClass("active-day");
            break;
          case "Tus":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            $(".item.mon").addClass("active-day");
            break;
        }
      }


      // Toggle Restaurant Delivery Area
      $scope.shownGroup = null; // set to null to make it hidden at first time
      $scope.toggleGroup = function (group) {
        $scope.isGroupShown(group) ? ($scope.shownGroup = null) : ($scope.shownGroup = group);
      };
      $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
      };

      // Toggle Restaurant timings (show more details)
      $scope.shownItem = null; // set to null to make it hidden at first time
      $scope.toggleItem = function (item) {
        $scope.isItemShown(item) ? ($scope.shownItem = null) : ($scope.shownItem = item);
      };
      $scope.isItemShown = function (item) {
        return $scope.shownItem === item;
      };


      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.viewMenu = function () {
        $state.go("app.queueMenuList");
      };


    }
  }
})();
