(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('QueuePreferencesCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicModal', 'Facebook'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicModal, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;
    $scope.errorText = "";
    $scope.fePref = false;
    activate();

    function activate() {

      getQueueManagerPrefernces();

      $scope.doRefresh = function () {
        getQueueManagerPrefernces();
        $scope.$broadcast('scroll.refreshComplete');
      };

      // start queue prefrences popup
      $ionicModal.fromTemplateUrl('app/partials/queue-prefrences-popup.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.prefModal = modal;
      });
      $scope.closePrefQueue = function () {
        $scope.prefModal.hide();
        getQueueManagerPrefernces();
      };
      $scope.openPrefQueue = function () {
        $scope.prefModal.show();
      };




      function getQueueManagerPrefernces() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          evt: "1",
          mb: $Session.user.getMobile()
        };
        var callback = function (res) {
          Loading.hide();
          if(res.status == 'success') {
            $scope.answers = JSON.parse(res.guestp);
            $scope.fePref = true;
            console.log( $scope.answers);
            $scope.$apply();
          } else {
            $scope.fePref = false;
            $scope.errorText = "No prefrences yet";
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: QMPrefrencesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      } 


      $scope.confirmQueuePrefRequest = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your preferences', 3000, '#b50905');
          return false;
        }

        var qaArr = [];
        var qaArrStr = ''
        for(var j in $scope.answers) {
          var q = $scope.answers[j].pQuestion;
          var a = $scope.answers[j].pAnswer;
          qaArr.push(q + "^" + a + "@");
        }
        qaArrStr = qaArr.join("");
        var qaString = qaArrStr.substring(0, qaArrStr.lastIndexOf("@"));
        Loading.show();
        var params = {
          qa: qaString,
          mb: $Session.user.getMobile().substring(3),
          evt: "3"
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status !== "failure") {
            $scope.closePrefQueue();
            ionicToast.show('Success!', 5000, '#26A65B');
          } else {
            ionicToast.show('Failed to save!', 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: QMPrefrencesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
        
      };


    }
  }
})();
