(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queue-preferences', {
        url: '/queue-preferences',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-preferences/queue-preferences.html',
            controller: 'QueuePreferencesCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
