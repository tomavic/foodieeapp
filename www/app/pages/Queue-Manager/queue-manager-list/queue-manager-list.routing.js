(function () {
  'use strict';

  angular
    .module('queue-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.queuemanager', {
        url: '/queuemanager',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Queue-Manager/queue-manager-list/queue-manager-list.html',
            controller: 'queueManagerCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
