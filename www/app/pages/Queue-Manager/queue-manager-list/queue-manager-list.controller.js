(function () {
  'use strict';

  angular
    .module('queue-module')
    .controller('queueManagerCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$ionicPopup', '$Session', '$Easyajax', '$ionicModal', '$Geolocation', 'ionicToast', '$state'];

  function controller($scope, Loading, $ionicPopup, $Session, $Easyajax, $ionicModal, $Geolocation, ionicToast, $state) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    vm.errorMessage = 'No restaurant found in your nearest location for Queue Manager';
    vm.errorMessageHistory = 'There is no active requests';
    $scope.feQMRest = true;
    $scope.inputs = [{
      value: ""
    }];


    activate();

    function getloactionForQM(latitude, longitude) {
      // lat: "30.642550",
      // log: "76.817336"
      Loading.show();

      // UNCOMMENT TO REAL QM
      var params = {
          lat: latitude,
          log: longitude
      };
      // var params = {
      //   lat: "30.642550",
      //   log: "76.817336"
      // };
      var callback = function (data) {
        Loading.hide();
        if (data.status == "success") {
          $scope.feQMRest = true;
          $scope.restaurantList = data.restaurantList;
        } else {
          $scope.feQMRest = false;
        }
      }
      var config = {
        typeRequest: "POST",
        urlRequest: restaurantQMListAPI,
        dataRequest: params,
        sucesCallbackRequest: callback
      }
      $Easyajax.startRequest(config);
    }


    function activate() {


      $Geolocation.requestGeolocation(getloactionForQM);
      // getloactionForQM();
      getQueueHistory();


      $scope.doRefresh = function () {
        $Geolocation.requestGeolocation(getloactionForQM);
        // getloactionForQM();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.doRefreshHistory = function () {
        getQueueHistory();
        $scope.$broadcast('scroll.refreshComplete');
      };

      // start queue prefrences popup
      $ionicModal.fromTemplateUrl('app/partials/queue-request-popup.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeQueue = function () {
        $scope.modal.hide();
      };
      $scope.openQueue = function () {
        $scope.modal.show();
      };


      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.reviewModal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.reviewModal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.reviewModal.show();
      };
      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.openPrefQueue = function() {
        $state.go("app.queue-preferences")
      }


      $scope.getInQueue = function (queueSId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          // Check before create order and send to backend without userId 
          var confirmPopup = $ionicPopup.confirm({
            title: 'Mink Foodiee',
            template: 'Please Login or Create Account to get in the queue!',
            cancelText: 'Later',
            cancelType: 'button-default',
            okText: 'Login',
            okType: 'button-balanced'
          });
          confirmPopup.then(function (res) {
            if (res) {
              $state.go('login');
            }
          });
          return false;
        }
        getUserDetails();
        $scope.openQueue();
        $scope.queueRestId = queueSId;
      }


      $scope.addInput = function () {
        $scope.inputs.push({
          value: ""
        });
      }
      $scope.removeInput = function (index) {
        $scope.inputs.splice(index, 1);
      }

      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.queueRequestData.userName = data.userDetail.firstName;
          $scope.queueRequestData.phoneNumber = data.userDetail.mobileNumber;
          $scope.queueRequestData.emailId = data.userDetail.email;
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.queueRequestData = {};
      $scope.queueRequestConfigs = {
        rules: {
          userName: {
            required: true,
            string: true
          },
          emailId: {
            required: true,
            validEmail: true
          },
          phoneNumber: {
            required: true,
            indianMobNumber: true
          },
          noOfEntry: {
            required: true,
            queuePeople: true
          }
        },
        messages: {
          userName: {
            required: "Please enter your name",
            string: "Your name should contain only letters"
          },
          emailId: {
            required: "Email address is required",
            validEmail: "Please enter a valid email"
          },
          phoneNumber: {
            required: "Mobile number is required",
            indianMobNumber: "Make sure you entered 10 digits mobile number"
          },
          noOfEntry: {
            required: "Please enter the number of people",
            queuePeople: "Make sure number of people is between 1-30 people"
          }
        }
      };
      $scope.confirmQueueRequest = function (form) {
        var guestNumbersArr = [];
        var guestNumbers = '';
        $scope.inputs.map(function(item) {
          guestNumbersArr.push("guest^" + item['value'])
        });
        guestNumbers = guestNumbersArr.join('@');
        if (form.validate()) {
          Loading.show();
          var params = {
            sid: $scope.queueRestId,
            uid: userId,
            name: $scope.queueRequestData.userName,
            email: $scope.queueRequestData.emailId,
            mobile: $scope.queueRequestData.phoneNumber,
            numofentries: $scope.queueRequestData.noOfEntry,
            guest: guestNumbers
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              $scope.closeQueue();
              $scope.queueRequestData = {};
              ionicToast.show('Your request has been sent successfully!', 3000, '#26A65B');
              $scope.doRefreshHistory();
            } else {
              ionicToast.show(data.reason, 3000, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: QManagerAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      
      $scope.cancelQueue = function (queueId) {
        Loading.show();
        var params = {
          uId: userId,
          queueId: queueId,
          reason: "I want to cancel my queue order",
          status: "4"
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            getQueueHistory();
            ionicToast.show('Queue order has been cancelled successfully ', 3000, '#42b');
          } else {
            ionicToast.show('Something wrong happened.. Try to cancel again', 3000, '#42b');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: QManagerCancelRequestAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getQueueHistory() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          vm.errorMessageHistory = 'Please login to check queue history!';
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListNew = data.newQueue;
          $scope.orderListOld = data.oldQueue;
          var startDay = moment().format('DD-MMM-YYYY hh:mm: A');
          var endDay = moment().subtract(2, 'days').endOf('day').format('DD-MMM-YYYY hh:mm: A');
          if (data.status == "success") {
            for (var i in $scope.orderListOld) {
              var status = moment($scope.orderListOld.QueueDateTime).isBetween(endDay, startDay);
              if (!status) {
                $scope.orderListOld[i].queueStatus = "Cancelled";
              }
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userQMListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }






    }
  }
})();
