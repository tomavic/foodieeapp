(function () {
  "use strict";

  angular.module("splash-module").controller("splashCTRL", controller);

  controller.$inject = ["$scope", "$Session", "$state", "$timeout", "$Easyajax", "md5"];

  function controller($scope, $Session, $state, $timeout, $Easyajax, md5) {

    activate();

    function activate() {
      $scope.$on("$ionicView.enter", function (e) {
        getTokenValue();
        $timeout(function () {
          testUser();
        }, 1800);
      });

      function getTokenValue() {
        var callback = function (data) {
          if (data.status == "failure") {
            console.error("Error getting tokenInfoAPI");
          } else {
            var day = parseInt(data.day);
            var month = parseInt(data.month);
            var year = parseInt(data.year);
            var token = day * 33 + month * 67 + year * 9;
            var t = token.toString();
            var hashedToken = md5.createHash(t);
            var Oldtoken = localStorage.getItem("Token");
            if (Oldtoken !== hashedToken) {
              localStorage.setItem("Token", hashedToken);
              console.log("NEW Token has been generated: " + hashedToken);
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: tokenInfoAPI,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: "1"
        };
        var callback = function (data) {
          console.log(data);
        };
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function testUser() {
        var userId = $Session.user.getId();
        var cityId = $Session.city.getId();
        var cityArea = $Session.area.getId();
        if (cityId != null && cityArea != null) {
          console.log("Redirecting to Restaurants list... ");
          registerDeviceToken();
          $state.go("app.restList");
        } else if (userId === "" || userId == null || userId === "0") {
          console.log("Redirecting to Login... ");
          $state.go("welcome");
        } else {
          console.log("Redirecting to Location select... ");
          registerDeviceToken();
          $state.go("home");
        }
      }
    }
  }
})();
