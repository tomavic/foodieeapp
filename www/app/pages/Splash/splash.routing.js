(function () {
  'use strict';

  angular
    .module('splash-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('index', {
        url: '/index',
        templateUrl: 'app/pages/Splash/splash.html',
        controller: 'splashCTRL'
    });
  }

}());
