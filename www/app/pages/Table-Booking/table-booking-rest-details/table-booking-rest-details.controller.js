(function() {
  "use strict";

  angular
    .module("table-booking-module")
    .controller("TableBookingRestDetailsCTRL", controller);

  controller.$inject = [ "$scope", "$ionicHistory", "$Session","$state", "$stateParams", "ionicToast", "$Easyajax", "$ionicModal", "Loading" ];

  function controller( $scope, $ionicHistory, $Session, $state, $stateParams, ionicToast, $Easyajax, $ionicModal, Loading) {
    /* jshint validthis:true */
    var vm = this;


    $scope.feReviews = true;
    $scope.restaurantId = $stateParams.sellerId;


    activate();
    
    function activate() {

      getRestaurantsDetails();

      // Toggle Item part (show more details)
      $scope.shownItem = null;
      $scope.toggleItem = function(item) {
        if ($scope.isItemShown(item)) {
          $scope.shownItem = null;
        } else {
          $scope.shownItem = item;
        }
      };
      $scope.isItemShown = function(item) {
        return $scope.shownItem === item;
      };

      // Get restaurant details accoding to Id
      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function(res) {
          if (res.status == "success") {
            Loading.hide();
            $scope.restaurantDetail = res.restaurantDetail;
            $scope.deliveryAreas = res.deliveryAreas;
            $scope.ourServices = res.restaurantDetail.ourServices
              .split(",")
              .join(", ");

              $scope.menuCodOnline = res.menuCodOnline;
              $scope.menuPreorder = res.menuPreorder;
              $scope.menuTakeaway = res.menuTakeaway;
              $scope.listMenuTR = res.listMenuTR;
              $scope.deliveryAreas = res.deliveryAreas;
    
              // update qm seller entity
              $Session.seller.setActiveMenuList($scope.menuCodOnline);

            setTodayTiming(res.restaurantDetail.currentDay);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $scope.activeDay = "Mon";
            $(".item.mon").addClass("active-day");
            break;
          case "Tue":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            //$scope.activeDay = true;
            console.log("default");
            break;
        }
      }






    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function(modal) {
        $scope.reviewModal = modal;
      });

      $scope.closeReviewsList = function() {
        $scope.reviewModal.hide();
      };

      $scope.openReviewsList = function() {
        $scope.reviewModal.show();
      };

      $scope.showReviewsForThis = function(sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };
    }
  }
})();
