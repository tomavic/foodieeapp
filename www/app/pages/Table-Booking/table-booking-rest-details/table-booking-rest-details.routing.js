(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingRestDetails', {
        url: '/tableBookingRestDetails/:sellerId',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-rest-details/table-booking-rest-details.html',
            controller: 'TableBookingRestDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
