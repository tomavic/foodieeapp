(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingList', {
        url: '/tableBookingList',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-list/table-booking-list.html',
            controller: 'TableBookingListCTRL',
            controllerAs: 'vm'
          }
        }
      })
  }

}());
