(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .controller('TableBookingListCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$ionicPopup', '$Easyajax', 'Loading', '$ionicModal'];

  function controller($scope, $Session, $state, $ionicPopup, $Easyajax, Loading, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var areaId = $Session.area.getId();
    $scope.feTableList = false;
    $scope.errorText = "";
    $scope.isLiked =  false;
    $scope.tableHistoryList = [];
    $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.closeReviewsList = function () {
      $scope.modal.hide();
    };

    $scope.openReviewsList = function () {
      $scope.modal.show();
    };

    activate();

    function activate() {
      

      // Calls
      sortRestaurants();
      getTableBookingHistory();
      

      function sortRestaurants() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.feTableList = false;
          $scope.errorText = "No Restaurants! You are not logged in";
          return false;
        }
        Loading.show();
        var params = {
          sKey: "B",
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restList = data.restaurantList;
            $scope.feTableList = true;
            getWishlist()
          } else {
            $scope.feTableList = false;
            $scope.errorText = "No Restaurants found!";
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.changelike = function(sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            
            if (data.color == "red") {
              $scope.isLiked = true;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getTableBookingHistory() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status = "success") {
            $scope.tableHistoryList = data.tableReserveList;
          } else {
            $scope.tableHistoryList = [];
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: tableReservationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.doRefresh = function () {
        sortRestaurants();
        $scope.$broadcast('scroll.refreshComplete');
      }

      $scope.doRefreshBookingHistory = function () {
        getTableBookingHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }


    }
  }
})();
