(function () {
  "use strict";

  angular
    .module("table-booking-module")
    .controller("TableBookingRequestCTRL", controller);

  controller.$inject = ["$scope", "$ionicHistory", "$Session", "$state", "$stateParams", "ionicToast", "$Easyajax", "menuList", "Loading"];

  function controller($scope, $ionicHistory, $Session, $state, $stateParams, ionicToast, $Easyajax, menuList, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    /**
     * @member {module} AddToCart
     ***************************/
    var cart = {
      cartItems: [],
      cartDetail: {
        uId: userId
      }
    }
    $scope.totalAmount = 0;
    $scope.totalCartProducts = 0;
    $scope.isHappyHour = false;
    $scope.surgePercent = $Session.seller.getSurgePricePercent();
    
    
    /**
     * @member {module} BookTable 
     *******************************/
    $scope.feTablesList = false;
    $scope.feReviews = true;
    $scope.timeValues = [
      "12:00 AM",
      "12:30 AM",
      "1:00 AM",
      "1:30 AM",
      "2:00 AM",
      "2:30 AM",
      "3:00 AM",
      "3:30 AM",
      "4:00 AM",
      "4:30 AM",
      "5:00 AM",
      "5:30 AM",
      "6:00 AM",
      "6:30 AM",
      "7:00 AM",
      "7:30 AM",
      "8:00 AM",
      "8:30 AM",
      "9:00 AM",
      "9:30 AM",
      "10:00 AM",
      "10:30 AM",
      "11:00 AM",
      "11:30 AM",
      "12:00 PM",
      "12:30 PM",
      "1:00 PM",
      "1:30 PM",
      "2:00 PM",
      "2:30 PM",
      "3:00 PM",
      "3:30 PM",
      "4:00 PM",
      "4:30 PM",
      "5:00 PM",
      "5:30 PM",
      "6:00 PM",
      "6:30 PM",
      "7:00 PM",
      "7:30 PM",
      "8:00 PM",
      "8:30 PM",
      "9:00 PM",
      "9:30 PM",
      "10:00 PM",
      "10:30 PM",
      "11:00 PM",
      "11:30 PM"
    ];


    $scope.$on('$ionicView.enter', function (e) {
      cart.cartItems = [];
      $scope.menu = menuList;
      $("span[id^='addDefault']").show(300);
    });

    activate();

    function activate() {

      /**
       * @member {module} AddToCart 
       *******************************/


      // Toggle Item part (show more details)
      $scope.shownItem = null;
      $scope.toggleItem = function (item) {
        if ($scope.isItemShown(item)) {
          $scope.shownItem = null;
        } else {
          $scope.shownItem = item;
        }
      };
      $scope.isItemShown = function (item) {
        return $scope.shownItem === item;
      };


      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      }
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      }

      function animateItYawala() {
        var el = $("#dishcount");
        var newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      //Add-Remove to-from cart object and calling calculating taxes function
      $scope.addItemJsonFIRST = function (item) {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $ionicPopup.alert({
            title: 'Restaurant Closed',
            template: 'Restaurant is closed now. Please check restaurant timings!',
            okType: 'button-assertive'
          });
        } else {
          $("#addDefault" + item.proId).hide(300);
          $scope.addItemJson(item);
        }
      };
      $scope.addItemJson = function (item) {
        animateItYawala();
        var hasMatch = false;
        $scope.isHappyHour = false;
        for (var i = 0; i < cart.cartItems.length; i++) {
          var product = cart.cartItems[i];
          if (product.proId == item.proId) {
            hasMatch = true;
            product.qty = (parseInt(product.qty) + 1).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "1",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });
          hasMatch = false;
        }
        calcTotalAmount();
      };


      $scope.removeItemJson = function (item) {
        animateItYawala();
        $scope.isHappyHour = false;
        for (var i = 0; i < cart.cartItems.length; i++) {
          var product = cart.cartItems[i];
          if (product.proId == item.proId) {
            product.qty = (parseInt(cart.cartItems[i].qty) - 1).toString();
            if (product.qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcTotalAmount();
        }
      }

      function calcTotalAmount() {
        var sumTotal = 0;
        var sumtotalQty = 0;
        var cart_length = cart.cartItems.length;
        if (cart_length == 0) {
          $scope.totalAmount = 0;
          $("#cart-footer-bar").hide().addClass('hidden');
          return;
        } else {
          for (var i = 0; i < cart_length; i++) {
            var product = cart.cartItems[i];
            var productId = product.proId;
            var productQty = parseInt(product.qty);
            var productPrice = parseInt(product.productPrice);

            sumTotal += productQty * productPrice;
            sumtotalQty =  sumtotalQty + productQty;

            // Update the view
            $("#itemNumber" + productId).val(productQty);
          }

          $scope.totalAmount = sumTotal;
          $scope.totalCartProducts = sumtotalQty;

          $("#cart-footer-bar").show().removeClass('hidden');
          $("#dishcount").text($scope.totalCartProducts);
        }
      }

      $scope.checkout = function() {
        console.log(JSON.stringify(cart));
      }

      /**
       * @member {module} BookTable 
       *******************************/
      //Event when confirm datetime
      $scope.datetimepickerEvent = function (e) {
        // console.log("Event changed, ", e.tableBookingData.tableBookingDate);
        $scope.getTableList();
      };

      $scope.getTableList = function () {
        console.log("$scope.tableBookingData.tableBookingTime ", $scope.tableBookingData.tableBookingTime);
        console.log("$scope.tableBookingData.tableBookingDate ", $scope.tableBookingData.tableBookingDate);
        var d = $scope.tableBookingData.tableBookingDate;
        var formattedDate = moment(d).format("YYYY-MM-DD");

        var t = $scope.tableBookingData.tableBookingTime;
        var formattedTime = moment(t).format("HH:mm ");
        var params = {
          sId: $stateParams.sellerId,
          trTime: formattedTime,
          trDate: formattedDate
        };
        var callback = function (data) {
          console.log("getTableList : ", data);
          if (data.status == "success") {
            $scope.feTablesList = true;
            $scope.availableList = data.list;
          } else {
            $scope.feTablesList = false;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: AvailableTableAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.tableBookingData = {
        tableBookingDate: new Date(),
        tableBookingTime: new Date(),
        tableBookingPeopleNo: 0
      };
      $scope.tableBookingConfigs = {
        rules: {
          tableBookingDate: {
            required: true,
            greaterThanToday: true
          },
          tableBookingTime: {
            selectRequired: true
          },
          tableBookingPeopleNo: {
            selectRequired: true
          },
          table: {
            selectRequired: true
          }
        },
        messages: {
          tableBookingDate: {
            required: "Date is required",
            greaterThanToday: "Please select a valid date"
          },
          tableBookingTime: {
            selectRequired: "Time is required"
          },
          tableBookingPeopleNo: {
            selectRequired: "Number of people is required"
          },
          table: {
            selectRequired: true
          }
        }
      };

      $scope.findTable = function (form) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if ($scope.tableBookingData.tableBookingPeopleNo == "0") {
          ionicToast.show("Please select how many persons", 5000, "#b50905");
          return false;
        }
        // if ($scope.tableBookingData.table == "0") {
        //   ionicToast.show("Please select how many people", 5000, "#b50905");
        //   return false;
        // }
        var d = $scope.tableBookingData.tableBookingDate;
        var formattedDate = moment(d).format("YYYY-MM-DD");

        var t = $scope.tableBookingData.tableBookingTime;
        var formattedTime = moment(t).format("HH:mm ");


        if (form.validate()) {
          Loading.show();
          var params = {
            sId: $stateParams.sellerId,
            uId: userId,
            reserDate: formattedDate,
            timeFrom: formattedTime,
            np: $scope.tableBookingData.tableBookingPeopleNo,
            tableIds: "0", //$scope.tableBookingData.table,
            seatingHours: "1",
            jsonUserCart: JSON.stringify(cart),
            event: "Save"
          };
          var callback = function (data) {
            Loading.hide();
            console.log(data);
            if (data.status === 'success') {
              ionicToast.show("Your Booking request has been received please wait while it gets approved", 3000, "#249E21");
              
              $ionicHistory.nextViewOptions({
                historyRoot: true,
                disableBack: true
              });
              $state.go("app.tableBookingList");
            } else {
              ionicToast.show(data.reason, 5000, "#b50905");
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: reserveTableAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };



    }
  }
})();
