(function () {
  'use strict';

  angular
    .module('table-booking-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.tableBookingRequest', {
        url: '/tableBookingRequest/:sellerId',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Table-Booking/table-booking-request/table-booking-request.html',
            controller: 'TableBookingRequestCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
