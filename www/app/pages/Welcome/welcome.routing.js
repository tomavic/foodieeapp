(function () {
  'use strict';

  angular
    .module('welcome-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'app/pages/Welcome/welcome.html',
        controller: 'welcomeCTRL',
        controllerAs: 'vm'
      });
  }

}());
