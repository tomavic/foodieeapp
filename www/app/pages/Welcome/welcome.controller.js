(function () {
  "use strict";

  angular
    .module("welcome-module")
    .controller("welcomeCTRL", WelcomeController);

  WelcomeController.$inject = ["$scope", "$Session", "$state", "$Easyajax", "Facebook", "Loading", "ionicToast"];

  function WelcomeController($scope, $Session, $state, $Easyajax, Facebook, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

      vm.options = {
        loop: true,
        speed: 300
      };

      vm.loginAsGuest = function () {
        $Session.user.setUserInfo({
          id: "0",
          name: "Guest",
          image: "",
          mobile: "",
          address: "",
          address2: "",
          email: "",
          city: "",
          area: "",
          pin: ""
        });
        $state.go("home");
      }

      //This method is executed when the user press the "Login with facebook" button
      vm.facebookSignIn = function() {
        Facebook.login(fbLoginSuccess, fbLoginError);
      }

      function fbLoginSuccess(response) {
        var authResponse = response.authResponse;
        if (!authResponse) {
          alert("Auth response is not defined");
          return false;
        }
        Facebook.getFacebookProfileInfo(authResponse).then(
          function (profileInfo) {
            //Save user information in Session Entity
            $Session.user.setAccessToken(authResponse.accessToken);
            $Session.user.setId(profileInfo.id);
            $Session.user.setName(profileInfo.first_name);
            $Session.user.setEmail(profileInfo.email);
            $Session.user.setImage(profileInfo.picture.data.url);
            doFaceBookLogin();
          },
          function (err) {
            console.log(JSON.stringify(err));
          }
        );
      }

      function fbLoginError(error) {
        // ionicToast.show("Our servers are currenly busy! Try Login again", 3500, '#b50905');
        //Here you should handle if user signed in with different user. Check the response from FB Devs
        console.log(JSON.stringify(error));
        Loading.hide();
      }

      function setUserInfoSignin(data) {
        $Session.user.setUserInfo({
          id: data.userId,
          name: data.firstName,
          // image: data.userImage,
          mobile: data.mobileNumber,
          address: data.address,
          address2: data.shipAddress,
          email: data.email,
          city: data.cityName,
          area: data.cityArea,
          pin: data.pincode
        });
        $state.go("home");
      }

      function doFaceBookLogin() {
        Loading.show();
        var deviceToken = $Session.user.getDeviceToken();
        var fbId = $Session.user.getId();
        var fbToken = $Session.user.getAccessToken();
        var emailId = $Session.user.getEmail();
        var fname = $Session.user.getName();
        var params = {
          fbId: fbId,
          fbToken: fbToken,
          emailId: emailId,
          password: "Dd123456",
          firstName: fname,
          lastName: "",
          countryCode: "+91",
          deviceToken: deviceToken,
          deviceType: 1
        };
        var callback = function (res) {
          console.log(res);
          Loading.hide();
          if (res.status == "success") {
            setUserInfoSignin(res.userDetail);
          } else {
            ionicToast.show(JSON.stringify(res), 3000, "#42b");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: LoginWithFacebookAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



    }
  }
})();
