(function () {
  'use strict';
  angular
    .module('home-module')
    .controller('homeCTRL', controller)

  controller.$inject = ['$scope', '$Easyajax', '$Session', '$state', 'Loading', 'ionicToast'];

  function controller($scope, $Easyajax, $Session, $state, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

        var $citylist = $("#cityList");
        var $cityArea = $("#cityArea");

      $scope.$on('$ionicView.enter', function (e) {
        registerDeviceToken();
        getCityList();
        $Session.city.removeId();
        $Session.area.removeId();
        $Session.city.removeName();
        $Session.area.removeName();
      });

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: '1'
        };
        var callback = function (data) {
          console.log(data);
        }
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      //get city list
      function getCityList() {
        Loading.show();
        var callback = function (res) {
          var citylistObj = res.cityList;
          $citylist.empty().append($("<option selected ></option>").val("-9999").html("Please select city"));
          $cityArea.empty().append($("<option value='-9999'></option>").val("-9999").html("Select your delivery area"));

          for (var i in citylistObj) {
            var cityID = citylistObj[i].id;
            var cityName = citylistObj[i].cityName;
            $citylist.append($("<option></option>").val(cityID).html(cityName));
          }
          Loading.hide();
          getCityAreaList();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: cityListAPI,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      //get city area list
      function getCityAreaList() {

        $citylist.change(function () {
          Loading.show();
          var cityId = $citylist.val();
          var callback = function (res) {
            var cityAreaList = res.cityAreaList;
            if (res.status == "success") {
              $cityArea.empty().append($("<option value='-9999'></option>").val("-9999").html("Select your delivery area"));
              for (var i in cityAreaList) {
                var cityID = cityAreaList[i].id;
                var cityName = cityAreaList[i].cityArea;
                $cityArea.append($("<option></option>").val(cityID).html(cityName));
              }
              Loading.hide();
            } else {
              $cityArea.empty().append($("<option value='0'></option>").val("-9999").html("Make sure you selected a city name"));
              Loading.hide();
            }
          };
          var config = {
            typeRequest: "GET",
            urlRequest: cityAreaListAPI,
            dataRequest: {
              cityId: cityId
            },
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        });
      }

      var valid = function () {
        var cityId = $("#cityList").val();
        var cityArea = $("#cityArea").val();
        if (cityId == "-9999") {
          ionicToast.show('Please select city!', 1500, '#b50905');
          return false;
        }
        if (cityArea == "-9999" && cityId != "-9999") {
          ionicToast.show('Please select city area!', 1500, '#b50905');
          return false;
        }
        return true;
      };
      $scope.findRest = function () {

        if (valid()) {
          var cityId = $("#cityList").val();
          var cityArea = $("#cityArea").val();

          var cityText = $("#cityList option:selected").text();
          var cityAreaText = $("#cityArea option:selected").text();

          $Session.city.setId(cityId);
          $Session.area.setId(cityArea);

          $Session.city.setName(cityText);
          $Session.area.setName(cityAreaText);

          $state.go('app.restList');
        }
      };

    }
  }
})();
