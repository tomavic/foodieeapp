(function () {
  'use strict';

  angular
    .module('home-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('home', {
        url: '/home',
        templateUrl: 'app/pages/Home/home.html',
        controller: 'homeCTRL',
        controllerAs: 'vm'
    });
  }

}());
