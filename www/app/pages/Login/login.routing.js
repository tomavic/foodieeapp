(function () {
  'use strict';

  angular
    .module('login-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/pages/Login/login.html',
        controller: 'LoginCTRL',
        controllerAs: 'vm'
      });
  }

}());
