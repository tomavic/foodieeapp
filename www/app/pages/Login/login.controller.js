(function () {
  'use strict';

  angular
    .module('login-module')
    .controller('LoginCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$ionicPopup', '$state', '$Easyajax', 'ionicToast', 'Facebook', 'Loading'];

  function controller($scope, $Session, $ionicPopup, $state, $Easyajax, ionicToast, Facebook, Loading) {
    /* jshint validthis:true */
    var vm = this;
    $scope.resetPasswordData = {};
    $scope.loginData = {};


    activate();

    function activate() {

      /************************
       * Login configs
       ************************/

      $scope.loginConfigs = {
        rules: {
          emailId: {
            required: true,
            emailOrMobile: true
          },
          passwordId: {
            required: true
          }
        },
        // validation configs - error messages
        messages: {
          emailId: {
            required: "Please enter your registered email or mobile number",
            emailOrMobile: "Please enter valid email or 10 digits mobile number"
          },
          passwordId: {
            required: "Enter password"
          }
        }
      };

      function setUserInfoSignin(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
        $state.go('home');
      }

      vm.doLogin = function (form) {
        if (form.validate()) {
          Loading.show();
          var params = {
            emailId: $scope.loginData.email,
            passwordId: $scope.loginData.password
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              $scope.loginData = {};
              setUserInfoSignin(data);
            } else {
              ionicToast.show(data.reason, 3500, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: loginAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };

      vm.goCreateAccount = function () {
        $state.go('signup');
      }
      
      /************************
       * Forget Password popup 
       ************************/
      vm.showResetpasswordPopup = function () {
        
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="email" autofocus name="resetEmail" id="resetEmail" ng-model="resetPasswordData.email" placeholder="Email"/>',
          title: 'Reset Password',
          subTitle: 'Please enter your email..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel',
              type: 'button-default',
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.resetPasswordData.email || !/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test($scope.resetPasswordData.email)) {
                  e.preventDefault();
                  ionicToast.show('Enter a valid email Address', 63000, '#b50905');
                } else {
                  doresetPassword();
                }
              }
            }
          ]
        });
      };

      function doresetPassword() {
        Loading.show();
        var params = {
          emailId: $scope.resetPasswordData.email
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('New Password has been sent to your email address!', 3500, '#26A65B') : ionicToast.show(data.reason, 3500, '#b50905');
        };
        var config = {
          typeRequest: "POST",
          urlRequest: forgetPasswordAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };



      // Login with facebook logic 














    }
  }
})();
