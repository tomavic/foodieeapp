(function () {
  'use strict';

  angular
    .module('notification-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.notification', {
        url: '/notification',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Notification/notification.html',
            controller: 'notificationCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
