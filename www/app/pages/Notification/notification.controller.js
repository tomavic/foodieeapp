(function () {
  'use strict';

  angular
    .module('notification-module')
    .controller('notificationCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;
    $scope.errorText = '';
    $scope.fenotification = true;


    activate();

    function activate() {

      
      getNotificationlist();

      $scope.doRefresh = function () {
        getNotificationlist();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getNotificationlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.fenotification = false;
          $scope.errorText = "No Notifications! You are not logged in";
          return false;
        }
        Loading.show();
        var params = {
          user_id: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            $scope.fenotification = true;
            $scope.unreadnotificationList = data.activeNoti;
            $scope.notificationList = data.inActiveNoti;
          } else {
            $scope.fenotification = false;
            $scope.errorText = "No Notifications here!";
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: notificationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.removeFromUnread = function (notificationId) {
        Loading.show();
        var params = {
          wnId: notificationId
        };
        var callback = function (data) {
          if (data.status == "success") {
            getNotificationlist();
            ionicToast.show('Marked as read!', 3000, '#26A65B')
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "POST",
          urlRequest: notificationHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


    }
  }
})();
