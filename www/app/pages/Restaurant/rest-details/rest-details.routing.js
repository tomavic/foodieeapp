(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restaurant', {
        url: '/restList/:sellerId/:orderType',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/rest-details/rest-details.html',
            controller: 'restDetailsCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  