(function() {
  "use strict";

  angular.module("restaurant-list-module").controller("restDetailsCTRL", controller);

  controller.$inject = ["$scope", "$stateParams", "Loading", "$Session", "$Easyajax", "$state", "$ionicModal", "$ionicScrollDelegate"];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicScrollDelegate) {
    /* jshint validthis:true */
    var vm = this;
    $Session.seller.setId($stateParams.sellerId);
    var orderType = $stateParams.orderType || '';
    var sellerId = $Session.seller.getId();
    $scope.ferestaurantDetails = false;
    $scope.feReviews = false;
    $scope.isOpen = true;
    $scope.mifd = $Session.order.getMenuItemType() || '1';
    $scope.langToggle = {
      FR: false,
      DE: false,
      EN: false
    };
    // Review Modal
    $ionicModal
      .fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      })
      .then(function(modal) {
        $scope.reviewModal = modal;
      });
    $scope.closeReviewsList = function() {
      $scope.reviewModal.hide();
    };
    $scope.openReviewsList = function() {
      $scope.reviewModal.show();
    };


    

    activate();

    function activate() {
      // Variables

      $scope.$on("$ionicView.enter", function() {
        getMIF();
      });

      // Calls
      getRestaurantsDetails();

      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          if (data.status == "success") {
            $scope.restaurantDetail = data.restaurantDetail;
            $scope.ferestaurantDetails = true;
            $scope.deliveryAreas = data.deliveryAreas;

            //Settings Seller information to Session
            $Session.seller.setDeliveryAvailable(data.restaurantDetail.userCanAddProduct);
            $Session.seller.setOrderAvailable(data.restaurantDetail.openOrClose);
            $Session.seller.setMinPrice(data.restaurantDetail.minPriceStart);
            $Session.seller.setMinDeliveryCharges(data.restaurantDetail.minDeliveryCharges);
            $Session.seller.setServicePercent(data.restaurantDetail.serviceTax);
            $Session.seller.setVatPercent(data.restaurantDetail.vatTax);
            $Session.seller.setDeliveryTime(data.restaurantDetail.deliveryTime);
            $Session.seller.setTableBookOption(data.restaurantDetail.reserveTable);
            $Session.seller.setSurgeIndication(data.restaurantDetail.surgeIndication);
            $Session.seller.setSurgeMode(data.restaurantDetail.surgeMode);
            $Session.seller.setSurgePricePercent(data.restaurantDetail.surgePricePercent);
            $Session.seller.setRating(data.restaurantDetail.totalRates);
            $Session.seller.setImages(data.restImages.image);
            $Session.seller.setUserCanAddProduct(data.restaurantDetail.userCanAddProduct);
            $Session.seller.setType(data.restaurantDetail.restaurantTypeTitle);
            $Session.seller.setName(data.restaurantDetail.restaurantName);
            $Session.seller.setCouponExist(data.restaurantDetail.couponExist);

            // Setting Order information (Minimum amount for order)
            $Session.order.setMinDeliveryCharges(data.restaurantDetail.minOrder);

            $scope.menuCodOnline = data.menuCodOnline;
            $scope.menuPreorder = data.menuPreorder;
            $scope.menuTakeaway = data.menuTakeaway;
            $scope.listMenuTR = data.listMenuTR;
            var aa = data.deliveryAreas[0].cityAreas;
            $scope.deliveryAreas = aa.replace(/\s\s+/g, ' ').substr(aa.indexOf(",")+1).split(',');
            $scope.ourServices = data.restaurantDetail.ourServices;

            setTodayTiming(data.restaurantDetail.currentDay);
            getMIF();
            checkAvailablity();
            Loading.hide();
          } else {
            $scope.ferestaurantDetails = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function checkAvailablity() {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $scope.isOpen = false
        } else $scope.isOpen = true;
      }

      function getMIF() {

        if(orderType == '') {
          if ($scope.mifd == '1') {
            $scope.langToggle.DE = !false;
            $scope.langToggle.EN = false;
            $scope.langToggle.FR = false;
            $Session.seller.setActiveMenuList($scope.menuCodOnline);
          } else if ($scope.mifd == '2') {
            $scope.langToggle.EN = !false;
            $scope.langToggle.DE = false;
            $scope.langToggle.FR = false;
            $Session.seller.setActiveMenuList($scope.menuPreorder);
          } else if ($scope.mifd == '3') {
            $scope.langToggle.FR = !false;
            $scope.langToggle.DE = false;
            $scope.langToggle.EN = false;
            $Session.seller.setActiveMenuList($scope.menuTakeaway);
          }
        } else if(orderType == 'takeaway') {
          $scope.langToggle.FR = !false;
          $scope.langToggle.DE = false;
          $scope.langToggle.EN = false;
          $Session.seller.setActiveMenuList($scope.menuTakeaway);
          $Session.order.setMenuItemType('3');
          
        } else if(orderType == 'online') {
          $scope.langToggle.DE = !false;
          $scope.langToggle.EN = false;
          $scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuCodOnline);
          $Session.order.setMenuItemType('1');
        }

      }

      function getSellerImages() {
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.sellerImages = data.sellerImages;
          } else {
            $scope.sellerImages = [];
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: SellerImagesAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setTodayTiming(today) {
        switch (today) {
          case "Mon":
            $(".item.mon").addClass("active-day");
            break;
          case "Tus":
            $(".item.tus").addClass("active-day");
            break;
          case "Wed":
            $(".item.wed").addClass("active-day");
            break;
          case "Thu":
            $(".item.thu").addClass("active-day");
            break;
          case "Fri":
            $(".item.fri").addClass("active-day");
            break;
          case "Sat":
            $(".item.sat").addClass("active-day");
            break;
          case "Sun":
            $(".item.sun").addClass("active-day");
            break;
          default:
            $(".item.mon").addClass("active-day");
            break;
        }
      }

      // setTimeout(function () {
      //   $ionicSlideBoxDelegate.slide(0);
      //   $ionicSlideBoxDelegate.update();
      //   $scope.$apply();
      // });
      // $scope.options = {
      //   loop: false,
      //   effect: 'fade',
      //   speed: 500,
      //   pagination: false
      // }

      $scope.setOrderType = function(scope, type) {
        // Make sure MenuItemType is updated
        $Session.order.setMenuItemType(type);
        // Get value of MenuItemType and append to $scope.mifd
        $scope.mifd = $Session.order.getMenuItemType();

        if (type === "1") {
          // Change radio-button value
          scope.langToggle.DE = !false;
          scope.langToggle.EN = false;
          scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuCodOnline);
          $Session.order.setMenuItemType('1');
          $ionicScrollDelegate.scrollBottom();
        } else if (type === "2") {
          // Change radio-button value
          scope.langToggle.EN = !false;
          scope.langToggle.DE = false;
          scope.langToggle.FR = false;
          $Session.seller.setActiveMenuList($scope.menuPreorder);
          $Session.order.setMenuItemType('2');
          $ionicScrollDelegate.scrollBottom();
        } else if (type === "3") {
          // Change radio-button value
          scope.langToggle.FR = !false;
          scope.langToggle.DE = false;
          scope.langToggle.EN = false;
          $Session.seller.setActiveMenuList($scope.menuTakeaway);
          $Session.order.setMenuItemType('3');
          $ionicScrollDelegate.scrollBottom();
        }
      };

      // Toggle Restaurant Delivery Area
      $scope.shownGroup = null; // set to null to make it hidden at first time
      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? ($scope.shownGroup = null) : ($scope.shownGroup = group);
      };
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      };

      // Toggle Restaurant timings (show more details)
      $scope.shownItem = null; // set to null to make it hidden at first time
      $scope.toggleItem = function(item) {
        $scope.isItemShown(item) ? ($scope.shownItem = null) : ($scope.shownItem = item);
      };
      $scope.isItemShown = function(item) {
        return $scope.shownItem === item;
      };

      $scope.showReviewsForThis = function(sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function(data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.viewMenu = function() {
        // $Session.order.setMenuItemType($scope.mifd);
        $state.go("app.menuList");
      };
    }
  }
})();
