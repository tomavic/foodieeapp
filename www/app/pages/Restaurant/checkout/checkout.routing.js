(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.checkout', {
        url: '/checkout',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/checkout/checkout.html',
            controller: 'checkoutCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  