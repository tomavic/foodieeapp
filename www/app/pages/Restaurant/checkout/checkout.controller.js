(function() {
  "use strict";

  angular.module("restaurant-list-module").controller("checkoutCTRL", checkoutController);

  checkoutController.$inject = ["$scope", "$Session", "$Easyajax", "$state", "Loading", "$ionicPopup", "ionicToast", "$rootScope", "$ionicHistory"];

  function checkoutController($scope, $Session, $Easyajax, $state, Loading, $ionicPopup, ionicToast, $rootScope, $ionicHistory) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $Session.seller.getId();
    var userId = $Session.user.getId() || "0";
    var cityId = $Session.city.getId();
    var areaId = $Session.area.getId();
    var paymentVariable = "";


    $scope.mifd = $Session.order.getMenuItemType();
    $scope.commonComment = "";
    $scope.errorText = "";
    $scope.feCart = false;
    $scope.checkoutData = {
      userdate: new Date(),
      usertime: new Date(),
      username: $Session.user.getName(),
      usermobile: $Session.user.getMobile(),
      useraddress: $Session.user.getAddress(),
      useraddressII: $Session.user.getAddress2(),
      userlocation: $Session.city.getName() + ", " + $Session.area.getName()
    }

    $scope.langToggle = {
      EN: true,
      FR: false,
      DE: false,
      SP: false
    }


    function updateMobileNumberFirst() {

    }

    activate();

    function activate() {
      // $rootScope.dateValue = new Date();
      // $rootScope.datetimeValue = new Date();
      
      // $scope.mifd = mifd;

      $scope.$on("$ionicView.enter", function() {
        $scope.mifd = $Session.order.getMenuItemType();
        if($scope.mifd == '1') {
          paymentVariable = "cod";
          $scope.langToggle = {
            EN: true,
            FR: false,
            DE: false,
            SP: false
          }
        } else if ($scope.mifd == '2') {
          paymentVariable = "OnlinePreOrder";
          $scope.langToggle = {
            EN: false,
            FR: false,
            DE: true,
            SP: false
          }
        } else if($scope.mifd == '3') {
          paymentVariable = "online";
          $scope.langToggle = {
            EN: false,
            FR: false,
            DE: false,
            SP: true
          }
        }
        
        $scope.checkoutData = {
          userdate: new Date(),
          usertime: new Date(),
          username: $Session.user.getName(),
          usermobile: $Session.user.getMobile(),
          useraddress: $Session.user.getAddress(),
          useraddressII: $Session.user.getAddress2(),
          userlocation: $Session.city.getName() + ", " +  $Session.area.getName()
        }
        getCartList();

        if($Session.user.getMobile() === '' || $Session.user.getMobile() != $scope.checkoutData.usermobile) {
          ionicToast.show("Please verify your mobile number !", 3000, "#A71212");
          $scope.addMobileNumber()
        } 
      });
  
      $scope.doRefresh = function () {
        getCartList();
        $scope.$broadcast('scroll.refreshComplete');
      };



      /********************************************************************************
       * @name update 
       * @type {Mobile Number}
       *******************************************************************************/
      $scope.addMobileNumber = function () {
        $scope.newMobileData = {};
        $ionicPopup.show({
          template: '<input type="number" name="resetEmail" ng-model="newMobileData.number" placeholder="Mobile number..." autofocus />',
          title: 'Update Mobile Number',
          subTitle: 'Please enter your mobile number!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newMobileData.number || !/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test($scope.newMobileData.number)) {
                  // don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid mobile number', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setMobileNumber();
                }
              }
            }
          ]
        });
      };

      function setMobileNumber() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          mobileNum: $scope.newMobileData.number,
          countryCode: "91",
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            showOTPMobile();
            ionicToast.show('Passcode sent to ' + $scope.newMobileData.number, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateMobileAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function showOTPMobile() {
        $scope.dataMobileOTP = {};
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="mobileOTP" placeholder="OTP Code..." ng-model="dataMobileOTP.mobileOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verify your new mobile number',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.dataMobileOTP.mobileOTP || !/^\d{4}$/.test($scope.dataMobileOTP.mobileOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  vfMobile();
                }
              }
            }
          ]
        });

        function vfMobile() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          Loading.show();
          var params = {
            mobileNum: $scope.newMobileData.number,
            countryCode: "91",
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.mobileOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your mobile number updated succesfully!", 3000, '#249E21');
              $scope.checkoutData.usermobile = $scope.newMobileData.number;
              $Session.user.setMobile($scope.newMobileData.number);
              myPopup.close();
              // getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateMobileAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };


      // Calls
      getCartList();

      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: $scope.mifd
        };
        var callback = function(data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.feCart = true;
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
          } else {
            $scope.feCart = false;
            $scope.errorText = "Your cart is empty! Go back and add some items";
            $Session.order.setTotalAmount("0");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function submitCommonComment() {
        if ($scope.commonComment === "") {
          console.log("common comment is empty :)");
          return;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          comments: $scope.commonComment,
          menuItem: $scope.mifd
        };
        var callback = function(data) {
          if (data.status == "success") {
            $scope.commonComment = "";
          } else {
            console.log("Common Comment Failed to add ");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: commonCommentAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function unFocusElement() {
        angular.element('#commonComment').blur()
        // document.getElementById("commonComment").disabled = true;    
        // document.getElementById("commonComment").disabled = false;
      }


      $scope.payCash = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.EN = true;
        scope.langToggle.FR = false;
        unFocusElement()
      };
      $scope.payOnline = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.EN = false;
        scope.langToggle.FR = true;
        unFocusElement()
      };
      $scope.prePayOnline = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.DE = true;
        unFocusElement()
      };
      $scope.payOnlineTakeaway = function(scope, value) {
        // console.log(value);
        paymentVariable = value;
        scope.langToggle.SP = true;
        unFocusElement()
      };

      $scope.checkoutConfigs = {
        rules: {
          nameid: {
            required: true
          },
          mobileid: {
            required: true,
            indianMobNumber: true
          },
          addressid: {
            required: true
          },
          locationId: {
            required: true
          },
          date_pick: {
            required: true,
            greaterThanToday: true
          },
          timeFrom: {
            selectRequired: true
          }
        },
        messages: {
          nameid: {
            required: "Please enter your name"
          },
          mobileid: {
            required: "Please enter your mobile number",
            indianMobNumber: "Enter your mobile number e.g. +91xxxxxxxxxx"
          },
          addressid: {
            required: "Address is required"
          },
          locationId: {
            required: "Location is required"
          },
          date_pick: {
            required: "Date is required",
            greaterThanToday: "Please select a valid date"
          },
          timeFrom: {
            selectRequired: "Please select time"
          }
        }
      };

      $scope.placeOrderHandler = function(form) {
        var mifd = $scope.mifd;
        unFocusElement();

        if($Session.user.getMobile() === '' || $Session.user.getMobile() != $scope.checkoutData.usermobile) {
          ionicToast.show("Please verify your mobile number !", 3000, "#A71212");
          $scope.addMobileNumber()
          return;
        } 


        if (form.validate()) {
          if (mifd == 2) {
            $scope.completePreOrder();
          } else if (mifd == 3) {
            $scope.completeOrderTakeaway();
          } else {
            $scope.completeOrder();
          }
        }
      };

      $scope.completePreOrder = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please Select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        var d = $scope.checkoutData.userdate;
        var t = $scope.checkoutData.usertime;
        var formattedDate = moment(d).format("MM/DD/YYYY");
        var formattedTime = moment(t).format("hh:mm a");
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          cname: $Session.city.getName(),
          carea: $Session.area.getName(),
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          predate: formattedDate,
          fromtime: formattedTime
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          localStorage.removeItem("mf_orderId_pre");
          localStorage.removeItem("mf_orderAmount_pre");
          Loading.hide();
          if (data.status == "failure") {
            ionicToast.show(data.reason || "Reason NULL", 5000, "#A71212");
            $scope.feCart = false;
            return false;
          }
          if (data.reason == "Please check restaurant timing") {
            ionicToast.show("Please check restaurant timing", 3000, "#A71212");
          } else if (data.reason == "You may send request next 2 days") {
            ionicToast.show("Make sure you send your request within two days from today", 5000, "#A71212");
          } else {
            submitCommonComment();
            localStorage.setItem("paymentLink", data.link);
            localStorage.setItem("mf_orderId_pre", data.onlinePreorderId);
            localStorage.setItem("mf_orderAmount_pre", data.onlineOrderAmount);

            // cordova.plugins.browsertab.isAvailable(function(result) {
            //   if (!result) {
            //     cordova.InAppBrowser.open(data.link, '_blank');
            //   } else {
            //     cordova.plugins.browsertab.openUrl(
            //       data.link,
            //       function(successResp) {console.log(successResp);},
            //       function(failureResp) {console.error(failureResp);});
            //   }
            // },
            // function(isAvailableError) {
            //   console.error(isAvailableError)
            // });
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go("app.payment");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: preorderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.completeOrderTakeaway = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        Loading.show();
        var params = {
          delType: "online",
          agree: 1,
          sId: sellerId,
          uId: userId,
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          cname: cityId,
          carea: areaId,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          takeaway: 3,
          orderFor: 3
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          localStorage.removeItem("mf_orderId");
          Loading.hide();
          if (data.status == "success") {
            localStorage.setItem("paymentLink", data.link);
            localStorage.setItem("mf_orderId", data.orderId);
            // cordova.plugins.browsertab.isAvailable(function(result) {
            //   if (!result) {
            //     cordova.InAppBrowser.open(data.link, '_blank');
            //   } else {
            //     cordova.plugins.browsertab.openUrl(
            //       data.link,
            //       function(successResp) {console.log(successResp);},
            //       function(failureResp) {console.error(failureResp);});
            //   }
            // },
            // function(isAvailableError) {
            //   console.error(isAvailableError)
            // });

            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go("app.payment");
            submitCommonComment();
          } else {
            $scope.feCart = false;
            ionicToast.show(data.reason, 3000, "#A71212");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: placeOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.completeOrder = function() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (paymentVariable == "") {
          ionicToast.show("Please Select a payment method to complete order!", 5000, "#A71212");
          Loading.hide();
          return false;
        }
        Loading.show();
        var params = {
          delType: paymentVariable,
          agree: 1,
          sId: sellerId,
          uId: userId,
          name: $scope.checkoutData.username,
          mbnum: $scope.checkoutData.usermobile,
          cname: cityId,
          carea: areaId,
          addressline1: $scope.checkoutData.useraddress,
          addressline2: $scope.checkoutData.useraddressII,
          takeaway: 1,
          orderFor: 1
        };
        var callback = function(data) {
          localStorage.removeItem("paymentLink");
          Loading.hide();
          if (data.status == "success") {


            //if payment type is cod
            if (paymentVariable == "cod") {
              if (data.mobileBit == "1") {
                saveAndPlaceOrder();
                submitCommonComment();
              } else if (data.mobileBit == "0") {
                $scope.showCheckoutOTPPopup();
              }
            }

            //if payemnt type online
            else if (paymentVariable == "online") {
              localStorage.setItem("paymentLink", data.link);

              // cordova.plugins.browsertab.isAvailable(function(result) {
              //   if (!result) {
              //     cordova.InAppBrowser.open(data.link, '_blank');
              //   } else {
              //     cordova.plugins.browsertab.openUrl(
              //       data.link,
              //       function(successResp) {console.log(successResp);},
              //       function(failureResp) {console.error(failureResp);});
              //   }
              // },
              // function(isAvailableError) {
              //   console.error(isAvailableError)
              // });


              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go("app.payment");
              submitCommonComment();
            }

            //if no payment variable assigned (Frontend error)
            else {
              ionicToast.show("paymentVariable is missing", 3000, "#A71212");
              alert("Payment Variable is null or undefined");
            }
          } else {
            ionicToast.show(data.reason, 3000, "#A71212");
            $scope.feCart = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: placeOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      };

      //checkout OTP Modal configs
      $scope.showCheckoutOTPPopup = function() {
        $scope.checkoutOTPdata = {};
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="otpCheckoutCode" id="otpCheckoutCode" ng-model="checkoutOTPdata.code" placeholder="OTP Code" required/>',
          title: "Verify",
          subTitle: "Please enter 4 digits OTP code sent to your mobile",
          cssClass: "showCheckoutOTPPopup",
          scope: $scope,
          buttons: [
            {
              text: "Cancel"
            },
            {
              text: "<b>Submit</b>",
              type: "button-balanced",
              onTap: function(e) {
                console.log(e);
                if (!$scope.checkoutOTPdata.code || !/^\d{4}$/.test($scope.checkoutOTPdata.code)) {
                  //don't allow the user to close unless he valid 4 digits OTP
                  ionicToast.show("Enter a valid 4 digits OTP", 2500, "#b50905");
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  // return $scope.checkoutOTPdata.code;
                  // $scope.verifyAndCompleteOrder($scope.checkoutOTPdata.code);
                  var params = {
                    passcode: $scope.checkoutOTPdata.code,
                    uId: userId,
                    sId: sellerId
                  };
                  var callback = function(data) {
                    if (data.status != "success") {
                      ionicToast.show(data.reason, 2500, "#b50905");
                      
                      return false;
                    }
                    myPopup.close();
                    saveAndPlaceOrder();
                    submitCommonComment();
                  };
                  var config = {
                    typeRequest: "GET",
                    urlRequest: verificationCodeAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                  };
                  $Easyajax.startRequest(config);
                }
              }
            }
          ]
        });

      };



    }



    function saveAndPlaceOrder() {
      Loading.show();
      var params = {
        sId: sellerId,
        uId: userId,
        menuItemFor: $scope.mifd,
        delType: 'cod',
      };
      var callback = function (data) {
        Loading.hide();
        if (data.status == "success") {
          $Session.order.setType("Online");
          ionicToast.show('You have placed an order succesfully!!', 5000, '#A71212');
          $state.go("thankyou");
        } else {
          ionicToast.show(data.reason, 5000, '#A71212');
        }
      }
      var config = {
        typeRequest: "POST",
        urlRequest: saveUserOrderAPI,
        dataRequest: params,
        sucesCallbackRequest: callback
      };
      $Easyajax.startRequest(config);
    }
  }
})();
