(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('restListCTRL', restListCTRL)

  restListCTRL.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function restListCTRL($scope, $Session, $Easyajax, $state, Loading, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() ||"0";
    var cityId = $Session.city.getId();
    var areaId = $Session.area.getId();
    var cityName = $Session.city.getName();
    var areaName = $Session.area.getName();

    activate();

    function activate() {

      // $scope.slides = [
      //   {
      //     title: 'First',
      //     imageUrl: 'assets/img/banner/1.jpg',
      //   },
      //   {
      //     title: 'Second',
      //     imageUrl: 'assets/img/banner/2.jpg',
      //   },
      //   {
      //     title: 'Third',
      //     imageUrl: 'assets/img/banner/3.jpg',
      //   },
      //   {
      //     title: 'Fourth',
      //     imageUrl: 'assets/img/banner/4.jpg',
      //   }
      // ];

      // $scope.options = {
      //   loop: true,
      //   autoplay: 1000,
      //   effect: 'fade',
      //   speed: 300,
      // }

      // $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      //   // data.slider is the instance of Swiper
      //   $scope.slider = data.slider;
      //   console.log($scope.slider);
      // });

      $scope.restList = [];
      $scope.feList = false;
      $scope.feReviews = true; 
      $scope._isLiked = false;
      $Session.seller.removeId();
      $scope.cityName = cityName;
      $scope.areaName = areaName;

      // Calls
      getRestaurantsList(cityId, areaId);


      $scope.$on('$ionicView.enter', function () {
        var c = $Session.city.getId();
        var a = $Session.area.getId();
        if (cityId != c || areaId != a) {
          cityId = c;
          areaId = a;
          getRestaurantsList(c, a);
        }
      });

      $scope.openHomePage = function () {
        $state.go('home');
      };
      $scope.openFilterPage = function () {
        $state.go('app.filter');
      }
      $scope.restSearch = {
        restaurantName: ""
      };


      $scope.clearSearch = function (event) {
        event.preventDefault();
        $scope.restSearch.restaurantName = "";
      }

      $scope.openSearch = function (form, key) {
        if (form.validate()) {
          $state.go('app.restListSearch', {
            'searchKey': key
          });
        } else {
          $ionicPopup.alert({
            title: 'Restaurant Search',
            template: 'Make sure search input is not empty!',
            okType: 'button-assertive'
          });
        }
      }

      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });

      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };

      $scope.openReviewsList = function () {
        $scope.modal.show();
      };


      $scope.doRefresh = function () {
        getRestaurantsList(cityId, areaId);
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getRestaurantsList(cityId, areaId) {
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          if (data.status == "success") {
            $scope.feList = true;
            $scope.restList = data.restaurantList;
            $scope.$apply();
            getWishlist();
          } else {
            $scope.feList = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };



    }
  }

})();
