(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restList', {
        url: '/restList',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/rest-list/rest-list.html',
            controller: 'restListCTRL',
            controllerAs: 'vm'
          }
        }
      })
    }
  
  }());
  