(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('cartCTRL', cartController)

  cartController.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast', '$ionicModal', '$ionicPopup'];

  function cartController($scope, Loading, $Session, $Easyajax, $state, ionicToast, $ionicModal, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var sellerId = $Session.seller.getId();
    var userId = $Session.user.getId() || "0";

    vm.errorMessage = 'There is no items in cart! Go back and add more items from our delicious menu!';
    $scope.feCart = false;

    //checkout OTP Modal configs
    $ionicModal.fromTemplateUrl('app/partials/toppings-popup.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modalTopping = modal;
    });
    $scope.closeTopping = function () {
      $scope.modalTopping.hide();
    };
    $scope.openTopping = function () {
      $scope.modalTopping.show();
    };

    activate();

    function activate() {

      $scope.doRefreshCart = function () {
        getCartList();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.$on('$ionicView.enter', function () {
        getCartList();
      });
      

      function getCartList() {
        // if (userId === "0") {
        //   console.log("Guest User mode is ON!");
        //   return false;
        // }
        var mif = $Session.order.getMenuItemType();
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: mif
        };
        var callback = function (data) {
          Loading.hide();
          $scope.topplis = [];
          
          if (data.status == "success") {
            $scope.feCart = true;
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            $scope.listToppings = data.cartlist.listToppings;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
          } else {
            $scope.feCart = false;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getToppingCommentAndAppend() {
        Loading.show();
        var params = {
          cartId: $scope.itemIdForTopping
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getToppingCommentAndAppend ", data);
        }
        var config = {
          typeRequest: "GET",
          urlRequest: listToppingByIdAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.addCommentTopping = function (item) {
        $scope.comment = {
          commentOnItem: item.commentOnFood
        };
        $scope.itemIdForTopping = item.cartId;
        Loading.show();
        var params = {
          cartId: item.cartId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openTopping();
          if (data.status == "failure") {
            $scope.feTopping = false;
          } else {
            $scope.feTopping = true;
            $scope.toppingList = data.toppingList;
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: listToppingByIdAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.submitToppingComment = function () {
        $scope.toppingValues = [];
        angular.forEach($scope.toppingList, function (topping) {
          if (!!topping.selected) {
            $scope.toppingValues.push(topping.toppingId);
          }
        });
        var toppingList = $scope.toppingValues.join(",");

        //check if to submit toppings with comments, otherwise submit comment only 
        //change event value 0 or 1
        if ($scope.feTopping) {
          console.log("Submit when feTopping is ", $scope.feTopping);
          Loading.show();
          var params = {
            cartId: $scope.itemIdForTopping,
            event: "1",
            toppingList: toppingList,
            commentOnfood: $scope.comment.commentOnItem,
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              $scope.closeTopping();
              ionicToast.show('Topping added successfully!', 3000, '#ef473a');
            } else {
              ionicToast.show('Please select at least one topping!', 3000, '#ef473a');
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: toppingCommentAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        } else {
          console.log("Submit when feTopping is ", $scope.feTopping);
          Loading.show();
          var params = {
            cartId: $scope.itemIdForTopping,
            event: "0",
            commentOnfood: $scope.comment.commentOnItem
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "success") {
              //getToppingCommentAndAppend();
              $scope.closeTopping();
              ionicToast.show('Comment added successfully!', 3000, '#ef473a');
            } else {
              ionicToast.show('Please add comment on your food item!', 3000, '#ef473a');
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: toppingCommentAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
        getCartList();
      }

      $scope.deleteCartItem = function(itemId) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Mink Foodiee',
          template: 'Do you want to remove this item?',
          cancelText: 'No',
          cancelType: 'button-default',
          okText: 'Yes',
          okType: 'button-balanced'
        });
        confirmPopup.then(function (res) {
          if(res) {
            $scope.incDecItems(itemId, 2);
          } 
        });
      }

      $scope.incDecItems = function (oid, type) {
        // if (userId === "0") {
        //   console.log("Guest User mode is ON!");
        //   return false;
        // }
        var mif = $Session.order.getMenuItemType();
        Loading.show();
        var params = {
          uId: userId,
          oId: oid,
          calcType: type,
          menuItemFor: mif
        };
        var callback = function (data) {
          getCartList();
        }
        var config = {
          typeRequest: "POST",
          urlRequest: userCartItemIncDecAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.applyPromoCode = function() {
        // TODO: Apply promo code here
      }

      $scope.gotoPayment = function () {
        var minDCharge = parseFloat($Session.order.getMinDeliveryCharges());
        var totAmount = parseFloat($Session.order.getTotalAmount());
        console.log("minDCharge is ", minDCharge, ", and totAmount is ", totAmount);
        totAmount >= minDCharge ? $state.go('app.checkout') : ionicToast.show('Minimum amount not reached', 3000, '#42b');
      }



    }
  }
})();
