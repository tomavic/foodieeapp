(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.cart', {
        url: '/cart',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/cart/cart.html',
            controller: 'cartCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  