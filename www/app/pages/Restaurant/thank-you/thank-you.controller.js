(function () {
  'use strict';

  angular
    .module("restaurant-list-module")
    .controller("thankyouCTRL", thankyouCTRL);

  thankyouCTRL.$inject = ["$scope", "$Session", "$state", "$timeout"];

  function thankyouCTRL($scope, $Session, $state, $timeout) {

    $scope.$on('$ionicView.enter', function () {
      activate();
    });


    function activate() {

      cleanSession();
      navigateToHistory();

      function cleanSession() {
        //inhouse session
        $Session.inHouse.removeId();
        $Session.inHouse.removeSellerId();
        $Session.inHouse.removeTableNumber();
        $Session.inHouse.removeSellerName();
        $Session.inHouse.removeSellerMinCharge();
        $Session.inHouse.removeSellerMinDiscount();
        $Session.inHouse.removeSellerDiscountPercent();
        $Session.inHouse.removeSellerDiscountAmount();
        $Session.inHouse.removeOrderTotalAmount();

        //order session
        $Session.order.removeMenuItemType();
        $Session.order.removeMinDeliveryCharges();
        $Session.order.removeId();
        $Session.order.removeReviewOrderId();
        $Session.order.removeType();
        $Session.order.removeTotalAmount();

        //seller session
        $Session.seller.removeDeliveryAvailable();
        $Session.seller.removeAttendanceTime();
        $Session.seller.removeOrderAvailable();
        $Session.seller.removeMinDeliveryCharges();
        $Session.seller.removeName();
        $Session.seller.removeType();
        $Session.seller.removeMinPrice();
        $Session.seller.removeDeliveryTime();
        $Session.seller.removeTableBookOption();
        $Session.seller.removeRating();
        $Session.seller.removeServicePercent();
        $Session.seller.removeVatPercent();
      }

      function navigateToHistory() {
        $timeout(function () {
          $state.go("app.history");
        }, 5000);
      }


    }
  }
})();
