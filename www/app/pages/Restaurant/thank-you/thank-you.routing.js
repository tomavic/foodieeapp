(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('thankyou', {
        url: '/thankyou',
        templateUrl: 'app/pages/Restaurant/thank-you/thank-you.html',
        controller: 'thankyouCTRL',
        controllerAs: 'vm'
      });
    }
  
  }());
  