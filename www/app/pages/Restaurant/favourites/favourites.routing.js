(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.favourites', {
        url: '/favourites',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/favourites/favourites.html',
            controller: 'favouritesCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  