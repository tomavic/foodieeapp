(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('favouritesCTRL', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$ionicModal'];

  function controller($scope, Loading, $Session, $Easyajax, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    activate();

    function activate() {

      $scope.feReviews = true;
      $scope.fefavourites = true;
      $scope.errorText = "";

      $scope.$on('$ionicView.beforeEnter', function () {
        getWishlist();
      });

      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.modal.show();
      };

      $scope.doRefresh = function () {
        getWishlist();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
            getWishlist();
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          $scope.errorText = "No Favourites! Try login or create account";
          $scope.fefavourites = false;
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.fefavourites = true;
            $scope.restList = data.restaurantList;
            for (var i in data.restaurantList) {
              var sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          } else {
            $scope.fefavourites = false;
            $scope.errorText = "No Restaurants added to Favourites yet!";

          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();
