(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.menuList', {
        url: '/menuList',
        resolve: {
          menuList: ["$Session", "Loading", function ($Session, Loading) {
            Loading.show();
            return $Session.seller.getActiveMenuList();
          }]
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/menu-list/menu-list.html',
            controller: 'menuListController',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  