(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('menuListController', controller)

  controller.$inject = ['$scope', 'Loading', '$Session', '$Easyajax', '$state', '$ionicPopup', 'menuList', 'ionicToast'];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, menuList, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var mifd = $Session.order.getMenuItemType();
    console.log(mifd);
    var sellerId = $Session.seller.getId();
    
    var cart = {
      cartItems: [],
      cartDetail: {
        uId: userId,
        sId: sellerId
      }
    }
    var Oldcart = {
      cartItems: [],
      cartDetail: {
        uId: userId,
        sId: sellerId
      }
    }

    activate();

    function activate() {

      // if( $('#target').hasClass('animal') ) {
      //   $(document.body).addClass('dog');
      // }

      $scope.$on('$ionicView.enter', function (e) {
        cart.cartItems = [];
        getCartList();
        $scope.menu = menuList;
        // $("#addDefault" + proid).show(300);
        $("span[id^='addDefault']").show(300);
        Loading.hide();
      });
      // $scope.menu = $Session.seller.getActiveMenuList();

      $scope.totalAmount = 0;
      $scope.isHappyHour = false;
      $scope.surgePercent = $Session.seller.getSurgePricePercent();

      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: mifd
        };
        var callback = function (data) {
          if (data.status == "success") {
            Oldcart.cartItems = data.cartlist.list;
            $Session.order.setTotalAmount(data.cartlist.listInfo.subTotal);
            $scope.totalAmount = $Session.order.getTotalAmount();
            console.log("getTotalAmount from getCartList() >> ", $Session.order.getTotalAmount());
          } else {
            Oldcart.cartItems = [];
            $Session.order.setTotalAmount(0);
            $scope.totalAmount = $Session.order.getTotalAmount();
            console.log("getTotalAmount from getCartList() >> ", $Session.order.getTotalAmount());
          }
          updateCartStatus();
          Loading.hide();
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function animateItYawala() {
        var el = $("#dishcount");
        var newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      //Add-Remove to-from cart object and calling calculating taxes function
      $scope.addItemJsonFIRST = function (item) {
        if ($Session.seller.getOrderAvailable() != "Open") {
          $ionicPopup.alert({
            title: 'Restaurant Closed',
            template: 'Restaurant is closed now. Please check restaurant timings!',
            okType: 'button-assertive'
          });
        } else {
          $("#addDefault" + item.proId).hide(300);
          $scope.addItemJson(item);
        }
      };

      $scope.addItemJson = function (item) {
        animateItYawala();
        var hasMatch = false;

        // if (item.hpHourStatus == 'yes') {
        //     console.log("HAPPY HOUR!!! 💪 👺");
        //     $scope.isHappyHour = true;
        //     addItemHappyHour(item)
        //     return true;
        // }
        $scope.isHappyHour = false;
        console.log("NO HAPPY HOUR!!!🙄 ");
        for (var index = 0; index < cart.cartItems.length; index++) {
          var product = cart.cartItems[index];
          if (product.proId == item.proId) {
            hasMatch = true;
            cart.cartItems[index].qty = (parseInt(cart.cartItems[index].qty) + 1).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "1",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });

          hasMatch = false;
        }
        calcCartTaxes();
      };

      function addItemHappyHour(item) {
        var hasMatch = false;
        for (var index = 0; index < cart.cartItems.length; index++) {
          var product = cart.cartItems[index];
          if (product.proId == item.proId) {
            hasMatch = true;
            cart.cartItems[index].qty = (parseInt(cart.cartItems[index].qty) + 2).toString();
            break;
          }
        }
        if (!hasMatch) {
          cart.cartItems.push({
            "proId": item.proId.toString(),
            "qty": "2",
            "productPrice": item.originalPrice.toString(),
            "productName": item.proName.toString()
          });

          hasMatch = false;
        }
        calcCartTaxes();
      }

      $scope.removeItemJson = function (item) {
        animateItYawala();
        // if (item.hpHourStatus == 'yes') {
        //     console.log("HAPPY HOUR!!! 💪 👺");
        //     $scope.isHappyHour = true;
        //     removeItemHappyHour(item);
        //     return true;
        // }
        $scope.isHappyHour = false;
        console.log("NO HAPPY HOUR!!!🙄");
        for (var i = 0; i < cart.cartItems.length; i++) {
          if (cart.cartItems[i].proId == item.proId) {
            cart.cartItems[i].qty = (parseInt(cart.cartItems[i].qty) - 1).toString();
            if (cart.cartItems[i].qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcCartTaxes();
        }
      };


      function removeItemHappyHour(item) {
        for (var i = 0; i < cart.cartItems.length; i++) {
          if (cart.cartItems[i].proId == item.proId) {
            cart.cartItems[i].qty = (parseInt(cart.cartItems[i].qty) - 2).toString();
            if (cart.cartItems[i].qty <= 0) {
              cart.cartItems.splice(i, 1);
              $("#addDefault" + item.proId).show(300);
            }
          }
          calcCartTaxes();
        }
      }

      ////////////////////////////////////////////////////////////////////////////////
      /**
       * @todo
       * update add to cart login, rm unsed variables
       */
      var totalElTotal = 0;

      function calcTotalAmount() {
        var sumTotal = 0;
        var cart_length = cart.cartItems.length;
        if (cart_length > 0) {

          if ($scope.isHappyHour) {
            for (var i = 0; i < cart_length; i++) {
              var pid = cart.cartItems[i].proId;
              var qt = cart.cartItems[i].qty;
              var price = cart.cartItems[i].productPrice;
              sumTotal += parseInt(qt / 2) * parseInt(price);
              $("#itemNumber" + pid).val(qt);
            }
            return sumTotal;
          } else {
            for (var i = 0; i < cart_length; i++) {
              var pid = cart.cartItems[i].proId;
              var qt = cart.cartItems[i].qty;
              var price = cart.cartItems[i].productPrice;
              sumTotal += parseInt(qt) * parseInt(price);
              $("#itemNumber" + pid).val(qt);
            }
            return sumTotal;
          }

        } else {
          //console.log("sumTotal  (length < 0)   == " + sumTotal);
          return sumTotal;
        }
      }

      function calcCartTaxes() {
        var totalSum = 0;
        var oldaTotal = $Session.order.getTotalAmount() || 0;
        var sum = calcTotalAmount();
        totalSum = parseFloat(sum) + parseFloat(oldaTotal);
        totalElTotal = totalSum;
        $scope.totalAmount = totalElTotal;
        console.log("totalElTotal >> ", totalSum);
        updateCartStatus();
      }

      function updateCartStatus() {
        var dishCount = 0;
        var cartSumQty = 0;
        var oldCartSumQty = 0;
        for (var i in cart.cartItems) {
          cartSumQty = parseInt(cartSumQty) + parseInt(cart.cartItems[i].qty);
        }
        for (var j in Oldcart.cartItems) {
          oldCartSumQty = parseInt(oldCartSumQty) + parseInt(Oldcart.cartItems[j].proQty);
        }
        dishCount = parseInt(cartSumQty) + parseInt(oldCartSumQty);
        dishCount == 0 ? $("#cart-footer-bar").hide().addClass('hidden') : $("#cart-footer-bar").show().removeClass('hidden');
        $("#dishcount").text(dishCount);
      }


      /////////////////////////////////////////////////////////////////////////////////////////////

      function isTotalMoreThanMinChar() {
        var minDCharge = parseFloat($Session.order.getMinDeliveryCharges());
        var totAmount = totalElTotal;
        // console.log("minDCharge >> ", minDCharge, " totalElTotal >> ", totAmount);
        if (totAmount >= minDCharge) {
          $Session.order.setTotalAmount(totalElTotal);
          console.log("$ Total amount stored in order session = ", $Session.order.getTotalAmount());
          return true;
        } else {
          return false;
        }
      }


      function showReviewAlert() {
        $scope.reviewOrderdata = {};
        var myPopup = $ionicPopup.show({
          template: '<div class="item item-input"><ng-rate-it class="center" ng-model="reviewOrderdata.stars" star-width="32" star-height="32" step="1" max="5" rated="ratedCallback"></ng-rate-it></div><label class="item item-input item-floating-label"><textarea placeholder="Review message (Optional)" ng-model="reviewOrderdata.message" rows="5" cols="50"></textarea></label>',
          title: $Session.seller.getName(),
          subTitle: 'Please review your previous order to complete your current order',
          cssClass: 'review-order-first',
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.reviewOrderdata.stars) {
                  //don't allow the user to close unless he enters wifi password
                  e.preventDefault();
                } else {
                  $scope.revieworder();
                }
              }
            }
          ]
        });
        myPopup.then(function (res) {
          console.log('Tapped!', res);
        });
      }


      $scope.revieworder = function () {
        var orderId = $Session.order.getReviewOrderId();
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          selId: sellerId,
          uId: userId,
          rates: $scope.reviewOrderdata.stars,
          review: $scope.reviewOrderdata.message,
          orderId: orderId
        };
        var callback = function (data) {
          if (data.status == 'success') {
            updateCart();
          } else {
            ionicToast.show('Error submitting a review. Try again!', 3000, '#A71212');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);

      };

      $scope.ratedCallback = function () {
        console.log("Total reviewOrderdata.stars >> ", $scope.reviewOrderdata.stars);
      }

      function updateOldCart() {
        Loading.show();
        var mifd = $Session.order.getMenuItemType();
        var params = {
          jsonUserCart: Oldcart,
          menuItemFor: mifd
        }
        var callback = function (data) {
          Loading.hide();
          $state.go('app.cart');
        }
        var config = {
          typeRequest: "POST",
          urlRequest: AddUpdateCartItemsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function addLocalItemsUpdateCart() {
        var mifd = $Session.order.getMenuItemType();
        // Loading.show();
        var params = {
          jsonUserCart: JSON.stringify(cart),
          menuItemFor: mifd
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $state.go('app.cart');
          } else {
            ionicToast.show('Not added to cart! Please select order type', 3000, '#A71212');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: AddUpdateCartItemsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function updateCart() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");

          // Check before create order and send to backend without userId 
          var confirmPopup = $ionicPopup.confirm({
            title: 'Mink Foodiee',
            template: 'Please Login or Create Account to complete your order!',
            cancelText: 'Later',
            cancelType: 'button-default',
            okText: 'Login',
            okType: 'button-balanced'
          });
          confirmPopup.then(function (res) {
            if (res) {
              $state.go('login');
            }
          });
          return false;
        }
        Loading.show();
        var callback = function (data) {
          Loading.hide();
          if (data.reason == "review on your order") {
            $Session.order.setReviewOrderId(data.orderId);
            showReviewAlert();
            return false;
          } else if (!isTotalMoreThanMinChar()) {
            ionicToast.show('Minimum order amount per person ' + '₹' + $Session.order.getMinDeliveryCharges(), 3000, '#A71212');
            return false;
          } else {
            //if all cases are passed, then send the local cart to backend
            addLocalItemsUpdateCart();
          }
        };
        var mifd = $Session.order.getMenuItemType();
        var params = {
          sId: sellerId,
          uId: userId,
          takeaway: mifd
        };
        var config = {
          typeRequest: "POST",
          urlRequest: checkoutAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      $scope.checkout = function () {
        if (cart.cartItems.length == 0 && Oldcart.cartItems.length == 0) {
          $ionicPopup.alert({
            title: 'Empty Cart',
            template: 'Please add whatever you like from our food Menu!',
            okType: 'button-assertive'
          });

        } else if (cart.cartItems.length == 0) {
          updateOldCart();
          console.log("called updateOldCart");
        } else {
          updateCart();
          console.log("called updateCart");
        }
      }


      $scope.toggleGroup = function(group) {
        $scope.isGroupShown(group) ? $scope.shownGroup = null : $scope.shownGroup = group;
      };
      $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
      };


    }
  }
})();
