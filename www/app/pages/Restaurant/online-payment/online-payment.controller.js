(function() {
    'use strict';

    angular
        .module('restaurant-list-module')
        .controller('paymentCTRL', PaymentController);

    PaymentController.$inject = ['Loading','$scope', '$state', '$ionicPopup'];

    function PaymentController(Loading, $scope, $state, $ionicPopup) {
        var vm = this;

        $scope.$on("$ionicView.enter", function() {
            activate();
          });

        activate();

        ////////////////

        function activate() {
            var link = localStorage.getItem("paymentLink");
            $("#payment").attr("src", link);
            // localStorage.removeItem("paymentLink");
            // Loading.show();
            // $(document).ready(function() {
            // $('#payment').ready(function() {
            //     Loading.hide();
            // });
            // $('#payment').load(function() {
            //     Loading.hide();
            // });
            // });
            function backFromOnlinePaymentHandler() {
                var confirmPopup = $ionicPopup.confirm({
                    title: "Back",
                    template: "Order details will be lost if you click back. Are you sure?",
                    cancelText: "Close",
                    cancelType: "button-assertive",
                    okText: "Yes, sure",
                    okType: "button-default"
                });
                confirmPopup.then(function (res) {
                    if (res) {
                    // $ionicHistory.backView();
                    $state.go("app.checkout")
                    } else return;
                });
            }
            $scope.goback =function() {
                backFromOnlinePaymentHandler()
            }

        }
    }
})();