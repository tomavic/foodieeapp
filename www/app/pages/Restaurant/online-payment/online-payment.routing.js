(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.payment', {
        url: '/payment',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/online-payment/online-payment.html',
            controller: 'paymentCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  