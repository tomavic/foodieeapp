(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.reciept', {
        url: '/reciept',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/order-reciept/order-reciept.html',
            controller: 'recieptCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  