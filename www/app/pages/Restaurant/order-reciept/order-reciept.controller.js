(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('recieptCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var sellerId = $Session.seller.getId();
    var menuItemType = $Session.order.getMenuItemType();
    activate();

    function activate() {
      

      $scope.checkoutDetails = {
        firstName: localStorage.getItem("chusername"),
        mobileNumber: localStorage.getItem("chusermobile"),
        address: localStorage.getItem("chuseraddress")
      };

      // Calls
      getCartList();


      function getCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: menuItemType
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.cartList = data.cartlist.list;
            $scope.cartListInfo = data.cartlist.listInfo;
            $scope.cartTax = data.cartlist.adTaxList;
            $scope.listToppings = data.cartlist.listToppings;
            var cartlist = data.cartlist.list;
            for (var i in cartlist) {
              cartlist[i].totalItemPrice = cartlist[i].originalPrice * cartlist[i].proQty;
            }
          } else {
            console.log("Error happened calling userCartList ");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userCartListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.completeOrder = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          menuItemFor: menuItemType,
          delType: 'cod',
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $Session.order.setType("Online");
            ionicToast.show('You have placed an order succesfully!!', 5000, '#42b');
            $state.go("thankyou");
          } else {
            ionicToast.show(data.reason, 5000, '#42b');
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: saveUserOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();
