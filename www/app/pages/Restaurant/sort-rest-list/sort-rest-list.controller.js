(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('SortedRestListCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$ionicModal', 'Loading', '$stateParams', 'ionicToast'];

  function controller($scope, $Session, $Easyajax, $ionicModal, Loading, $stateParams, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    var areaId = $Session.area.getId();
    activate();

    function activate() {

      $scope.feReviews = true;
      $scope.feRestaurants = true;

      // Calls
      sortRestaurants();


      $ionicModal.fromTemplateUrl('app/partials/reviews-list.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });
      $scope.closeReviewsList = function () {
        $scope.modal.hide();
      };
      $scope.openReviewsList = function () {
        $scope.modal.show();
      };


      $scope.doRefresh = function () {
        sortRestaurants();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.changelike = function (sellerId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          ionicToast.show('Please login to save your favourites', 2000, '#b50905');
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            if (data.color == "red") {
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            } else {
              $('a.button p#like' + sellerId).removeClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart').addClass('fa-heart-o');
            }
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: addWishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.showReviewsForThis = function (sellerId, image) {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.openReviewsList();
          if (data.status == "success") {
            $scope.feReviews = true;
            $scope.simg = image;
            $scope.reviewsListSize = data.listUserReviewsSize;
            $scope.reviewsList = data.listUserReviews;
          } else {
            $scope.feReviews = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userReviewListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getWishlist() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          if (data.status == "success") {
            var sellerId = 0;
            for (var i in data.restaurantList) {
              sellerId = data.restaurantList[i].sellerId;
              $('a.button p#like' + sellerId).addClass('favourite');
              $('a.button p#like' + sellerId + " i.fa").removeClass('fa-heart-o').addClass('fa-heart');
            }
          } else {
            console.log("wishListAPI Failure");
          }
          Loading.hide();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: wishListAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function sortRestaurants() {
        Loading.show();
        $scope.sk = $stateParams.sortKey;
        var sk = $scope.sk;
        if (sk == "B") $scope.sk = "table booking restaurants";
        if (sk == "TS") $scope.sk = "restaurants offering tiffin service";
        if (sk == "PO") $scope.sk = "order scheduling restaurants";
        if (sk == "TK") $scope.sk = "takeaway restaurants";
        if (sk == "C") $scope.sk = "restaurants with cash payment options";
        if (sk == "ORD") $scope.sk = "restaurants with online payment options";
        if (sk == "CO") $scope.sk = "restaurants with cash and online payment options";
        if (sk == "IHO") $scope.sk = "restaurants delivering in-house orders";
        if (sk == "QM") $scope.sk = "restaurants with queue manager facility";
        if (sk == "MONC") $scope.sk = "most ordered restaurants";
        if (sk == "MDNC") $scope.sk = "restaurants delivering near you";
        if (sk == "MRNC") $scope.sk = "restaurants residing near you";
        if (sk == "OOD") $scope.sk = "Srestaurants offering online order discount";
        if (sk == "APPD") $scope.sk = "restaurants offering app based discount";
        if (sk == "Buffet") $scope.sk = "restaurants with buffet facility";


        var key = $stateParams.sortKey;
        var params = {
          sKey: key,
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.feRestaurants = true;
            $scope.restList = data.restaurantList;
            $scope.$apply();
            getWishlist();
          } else {
            $scope.feRestaurants = false;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };





    }
  }
})();
