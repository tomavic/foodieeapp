(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.restListSorted', {
        url: '/restListSorted',
        params: {
          'sortKey': ""
        },
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/sort-rest-list/sort-rest-list.html',
            controller: 'SortedRestListCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  