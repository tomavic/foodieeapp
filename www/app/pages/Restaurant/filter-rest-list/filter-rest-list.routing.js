(function () {
    'use strict';
  
    angular
      .module('restaurant-list-module')
      .config(Config)
  
    Config.$inject = ['$stateProvider'];
  
    function Config($stateProvider) {
      $stateProvider
      .state('app.filter', {
        url: '/filter',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Restaurant/filter-rest-list/filter-rest-list.html',
            controller: 'FilteredRestListCTRL',
            controllerAs: 'vm'
          }
        }
      });
    }
  
  }());
  