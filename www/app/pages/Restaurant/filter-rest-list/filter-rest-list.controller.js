(function () {
  'use strict';

  angular
    .module('restaurant-list-module')
    .controller('FilteredRestListCTRL', controller)

  controller.$inject = ['$scope', '$state'];

  function controller($scope, $state) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {
      $scope.queuemanager = function () {
        $state.go('app.queuemanager');
      }

      $scope.sortRestaurant = function (key) {
        $state.go('app.restListSorted', {
          'sortKey': key
        });
      }

    }
  }
})();
