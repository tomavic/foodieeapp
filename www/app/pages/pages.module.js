(function () {
  'use strict';

  angular.module('pages-module', [
    'order-history-list-module',
    'order-history-details-module',
    'home-module',
    'table-booking-module',
    // 'inhouse-module',
    'login-module',
    'notification-module',
    'profile-module',
    'queue-module',
    'restaurant-list-module',
    'signup-module',
    'splash-module',
    'welcome-module'
  ]);
})();
