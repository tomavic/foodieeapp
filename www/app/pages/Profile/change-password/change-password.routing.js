(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.changePassword', {
        url: '/changePassword',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/change-password/change-password.html',
            controller: 'ChangePasswordCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
