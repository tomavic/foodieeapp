(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('ChangePasswordCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Session', '$Easyajax', 'Loading', 'ionicToast'];

  function controller($scope, $state, $Session, $Easyajax, Loading, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    activate();

    function activate() {

      $scope.isFacebookUser = $Session.user.isLoggedInFromFacebook();

      $scope.changePasswordData = {};
      $scope.changePasswordConfigs = {
        rules: {
          oldPassword: {
            required: true,
          },
          newPassword: {
            notEqualTo: "#oldPassword",
            required: true,
            strongPassword: true
          },
          confirmNewPassword: {
            equalTo: "#newPassword",
            required: true
          }
        },
        // validation configs - error messages
        messages: {
          oldPassword: {
            required: "Please enter your old password",
          },
          newPassword: {
            notEqualTo: "Please Enter a new password different than your old password",
            required: "New password is required",
            strongPassword: "Please enter password with length 8 to 12 characters, should contain at least one letter and one number"
          },
          confirmNewPassword: {
            equalTo: "Passwords are not matched!",
            required: "Confirm new password is required"
          }
        }
      };

      $scope.changePassword = function (form) {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          Loading.show();
          var params;

          if($scope.isFacebookUser) {
            params = {
              oldPass: "Dd123456",
              newPass: $scope.changePasswordData.newPassword,
              uId: userId
            }
          } else {
            params = {
              oldPass: $scope.changePasswordData.oldPassword,
              newPass: $scope.changePasswordData.newPassword,
              uId: userId
            };
          }

          var callback = function (res) {
            Loading.hide();
            if (res.status == "success") {
              $scope.changePasswordData = {};
              $Session.kill();
              $state.go("welcome");
              ionicToast.show(res.reason, 3000, '#249E21');
            } else {
              ionicToast.show(res.reason, 3000, '#b50905');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userChangePasswordAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      };


    }
  }
})();
