(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('ProfileCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup', 'Facebook'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup, Facebook) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0" ;

    activate();

    function activate() {

      
      getUserDetails();

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (res) {
          Loading.hide();

          $scope.userDetail = res.userDetail;
          $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res) ;

          var image = $Session.user.getImage();
          if (image == "" || image =="http://minkchatter.com/mink-chatter-images/app/uploads/") {
            $('#userProfileImage').attr('src', 'assets/img/profile.png');
          } else {
            $('#userProfileImage').attr('src', image);
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



      $scope.gotoFavourites = function () {
        $state.go('app.favourites');
      }
      $scope.gotoHistory = function () {
        $state.go('app.history');
      }
      $scope.gotoAddresses = function() {
        $state.go('app.addresses');
      }
      $scope.aboutus = function () {
        $state.go('app.about');
      }
      $scope.terms = function () {
        $state.go('app.terms');
      }
      $scope.privacy = function () {
        $state.go('app.privacy');
      }
      $scope.faq = function () {
        $state.go('app.faq');
      }
      $scope.gotoQMPrefrences = function() {
        $state.go('app.queue-preferences');
      }
      vm.gotoSettings = function() {
        $state.go("app.settings");
      }


      /********************************************************************************
       * @name update 
       * @type {Logout}
       *******************************************************************************/
      $scope.logout = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Mink Foodiee',
          template: 'Are you sure you want to logout?',
          cssClass: "resetPasswordPopup",
          cancelText: 'No, Stay',
          cancelType: 'button-balanced',
          okText: 'I am sure',
          okType: 'button-default'
        });
        confirmPopup.then(function (res) {
          if (res) {
            doLogout();
          }
        });
      }

      function doLogout() {
        $Session.user.isLoggedInFromFacebook() ? Facebook.logout(doLogoutCallback, doLogoutCallback) : doLogoutCallback() ;
      }

      function doLogoutCallback() {
        $Session.kill();
        $state.go("welcome");
      }




      /********************************************************************************
       * @name update 
       * @type {Username}
       *******************************************************************************/
      $scope.addUserName = function () {
        $scope.newNameData = {
          username: $Session.user.getName()
        };
        // An elaborate, custom popup
        $ionicPopup.show({
          template: '<input type="text" name="username" ng-model="newNameData.username" placeholder="New name..." autofocus/>',
          title: 'Update Name',
          subTitle: 'Please enter your name..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                //don't allow the user to close unless he enters valid name
                if (!$scope.newNameData.username || !/^[^-\s][a-zA-Z_\s-]+$/.test($scope.newNameData.username)) {
                  ionicToast.show('Enter a valid name without special characters', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newNameData.username == $Session.user.getName()) {
                  ionicToast.show('Make sure new name is diffrent than old!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setUsername();
                }
              }
            }
          ]
        });
      }

      function setUsername() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          name: $scope.newNameData.username,
          address: $Session.user.getAddress(),
          address2: $Session.user.getAddress2(),
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show("Username updated!", 3000, '#249E21');
            getUserDetails();
          } else {
            ionicToast.show(data.message, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userUpdateDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }






      //   $scope.showImagePreview = function (input) {
      //     alert("hola");
      //     var userImg = document.getElementById("userProfilePic").files[0];

      //     var token = localStorage.getItem("Token");
      //     var reader = new FileReader();

      //     reader.onload = function (e) {
      //       $('#imguser').attr('src', e.target.result);
      //     };
      //     reader.readAsDataURL(input.files[0]);

      //     var formdata = new FormData();
      //     formdata.append('image', input.files[0]);
      //     formdata.append('uId', uid);
      //     formdata.append("event", "UploadImage");
      //     formdata.append("Token", token);

      //     console.log(formdata);

      //     $.ajax({
      //       url: userUpdateImageAPI,
      //       headers: {
      //         'contentType': 'application/x-www-form-urlencoded',
      //         'username': 'foodiee',
      //         'password': 'MTIzNDU='
      //       },
      //       data: formdata,
      //       processData: false,
      //       contentType: false,
      //       type: 'POST',
      //       dataType: 'json',
      //       success: function (data) {
      //         console.log(data);
      //         if (data.status === "success") {

      //           console.info("success");
      //         } else {

      //           console.info("failure");
      //         }
      //       },
      //       error: function (xhr, ajaxOptions, thrownError) {
      //         console.error(xhr.status);
      //         console.error(thrownError);
      //       }
      //     });


      //   }







    }
  }
})();
