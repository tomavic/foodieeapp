(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/profile/profile.html',
            controller: 'ProfileCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
