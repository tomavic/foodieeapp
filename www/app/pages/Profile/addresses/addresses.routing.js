(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.addresses', {
        url: '/addresses',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/addresses/addresses.html',
            controller: 'AddressesCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
