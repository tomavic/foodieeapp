(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('AddressesCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    
    $scope.addres1tesxt = $Session.user.getAddress();
    $scope.addres2tesxt = $Session.user.getAddress2();


    $scope.idAddressII = false;
    $scope.isAddressI = false;
    $scope.newAddressData = {
      address: $Session.user.getAddress(),
      address2: $Session.user.getAddress2()
    };
    activate();

    function activate() {




      /********************************************************************************
       * @name update 
       * @type {Address}
       *******************************************************************************/
      
      $scope.updateAddress = function () {

        $ionicPopup.show({
          templateUrl: 'app/partials/addresses-popup.html',
          title: 'Update Addresses',
          // subTitle: 'Please enter your address..',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newAddressData.address) {
                  //don't allow the user to close unless he enters address
                  ionicToast.show('Make sure you entered your address', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  updateAddress();
                }
              }
            }
          ]
        });
      }

      function updateAddress() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          name: $Session.user.getName(),
          address: $scope.newAddressData.address,
          address2: $scope.newAddressData.address2,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show("Address Updated!", 3000, '#249E21');
            getUserDetails();
            $scope.addres1tesxt = $scope.newAddressData.address;
            $scope.addres2tesxt = $scope.newAddressData.address2;
          } else {
            ionicToast.show(data.message, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userUpdateDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getUserDetails() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (res) {
          Loading.hide();
          $scope.userDetail = res.userDetail;
          $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res) ;
          var image = $Session.user.getImage();
          if (image == "" || image =="http://minkchatter.com/mink-chatter-images/app/uploads/") {
            $('#userProfileImage').attr('src', 'assets/img/profile.png');
          } else {
            $('#userProfileImage').attr('src', image);
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }
      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }


    }
  }
})();
