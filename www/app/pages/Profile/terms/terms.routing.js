(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.terms', {
        url: '/terms',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/terms/terms.html'
          }
        }
      })
  }

}());
