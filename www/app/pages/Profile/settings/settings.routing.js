(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/settings/settings.html',
            controller: 'SettingsCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
