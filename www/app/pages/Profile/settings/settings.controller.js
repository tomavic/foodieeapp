(function () {
  'use strict';

  angular
    .module('profile-module')
    .controller('SettingsCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$Easyajax', '$state', 'Loading', 'ionicToast', '$ionicPopup'];

  function controller($scope, $Session, $Easyajax, $state, Loading, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    $scope.langToggle = {
      DE: true
    }
    activate();

    function activate() {

      $scope.changePassword = function () {
        $state.go('app.changePassword');
      }

      $scope.togglePushNotification = function(scope) {
        console.log(scope);
        console.log($scope.langToggle.DE);

        if($scope.langToggle.DE) {
          registerDeviceToken();
        } else {
          ionicToast.show('Push notification disabled!', 3000, '#b50905');
        }
      }

      function registerDeviceToken() {
        var useremail = $Session.user.getEmail();
        var deviceToken = $Session.user.getDeviceToken();
        var params = {
          emailId: useremail,
          deviceToken: deviceToken,
          deviceType: '1'
        };
        var callback = function (data) {
          console.log(data);
          ionicToast.show('Push notification enabled!', 3000, '#b50905');
        }
        var config = {
          typeRequest: "POST",
          urlRequest: UpdateUserDeviceTokenAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      /********************************************************************************
       * @name update 
       * @type {Email Address}
       *******************************************************************************/
      $scope.addEmailAddress = function () {
        $scope.newEmailData = {};
        $ionicPopup.show({
          template: '<input type="email" name="resetEmail" ng-model="newEmailData.email" placeholder="New Email..." autofocus/>',
          title: 'Update Email',
          subTitle: 'Please enter your email!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newEmailData.email || !/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test($scope.newEmailData.email)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid email address', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newEmailData.email == $Session.user.getEmail()) {
                  ionicToast.show('Make sure new email is diffrent than old one!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setEmailAddress();
                }
              }
            }
          ]
        });
      }

      function setEmailAddress() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          emailId: $scope.newEmailData.email,
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            ionicToast.show('OTP sent to ' + $scope.newEmailData.email, 3000, '#42b');
            showOTPemail();
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateEmailAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


      function showOTPemail() {
        $scope.dataMobileOTP = {};
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="emailOTP" placeholder="OTP Code..."  ng-model="dataMobileOTP.emailOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verif your new email!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-positive',
              onTap: function (e) {
                e.preventDefault();
                if (!$scope.dataMobileOTP.emailOTP || !/^\d{4}$/.test($scope.dataMobileOTP.emailOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                } else {
                  vfEmail();
                }
              }
            }
          ]
        });

        function vfEmail() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          Loading.show();
          var params = {
            emailId: $scope.newEmailData.email,
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.emailOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your email updated succesfully!", 3000, '#42b');
              myPopup.close();
              //getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateEmailAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };


      /********************************************************************************
       * @name update 
       * @type {Mobile Number}
       *******************************************************************************/
      $scope.addMobileNumber = function () {
        $scope.newMobileData = {};
        $ionicPopup.show({
          template: '<input type="number" name="resetEmail" ng-model="newMobileData.number" placeholder="New mobile number..." autofocus />',
          title: 'Update Mobile Number',
          subTitle: 'Please enter your new mobile number!',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-balanced',
              onTap: function (e) {
                if (!$scope.newMobileData.number || !/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test($scope.newMobileData.number)) {
                  // don't allow the user to close unless he enters wifi password
                  ionicToast.show('Enter a valid mobile number', 3000, '#b50905');
                  e.preventDefault();
                } else if ($scope.newMobileData.number == $Session.user.getMobile()) {
                  ionicToast.show('Make sure new mobile number is diffrent than old number!', 3000, '#b50905');
                  e.preventDefault();
                } else {
                  setMobileNumber();
                }
              }
            }
          ]
        });
      };

      function setMobileNumber() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          mobileNum: $scope.newMobileData.number,
          countryCode: "91",
          event: "0",
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            showOTPMobile();
            ionicToast.show('Passcode sent to ' + $scope.newMobileData.number, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#b50905');
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: userUpdateMobileAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function showOTPMobile() {
        $scope.dataMobileOTP = {};
        var myPopup = $ionicPopup.show({
          template: '<input type="number" name="mobileOTP" placeholder="OTP Code..." ng-model="dataMobileOTP.mobileOTP" autofocus />',
          title: 'Enter OTP Code',
          subTitle: 'Please verify your new mobile number',
          cssClass: "resetPasswordPopup",
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-positive',
              onTap: function (e) {
                if (!$scope.dataMobileOTP.mobileOTP || !/^\d{4}$/.test($scope.dataMobileOTP.mobileOTP)) {
                  //don't allow the user to close unless he enters wifi password
                  ionicToast.show("Make sure you entered OTP code!", 3000, '#b50905');
                  e.preventDefault();
                } else {
                  e.preventDefault();
                  vfMobile();
                }
              }
            }
          ]
        });

        function vfMobile() {
          if (userId === "0") {
            console.log("Guest User mode is ON!");
            return false;
          }
          var Elnew = $scope.newMobileData.number;
          Loading.show();
          var params = {
            mobileNum: Elnew,
            countryCode: "91",
            event: "1",
            uId: userId,
            otpCode: $scope.dataMobileOTP.mobileOTP
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == "failure") {
              ionicToast.show(data.reason, 3000, '#b50905');
            } else {
              ionicToast.show("Your mobile number updated succesfully!", 3000, '#249E21');
              myPopup.close();
              // getUserDetails();
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: userUpdateMobileAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        };
      };




      // $scope.gotoAboutUs = function () {
      //   $state.go('app.aboutus');
      // }
    }
  }
})();
