(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.faq', {
        url: '/faq',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/faq/faq.html'
          }
        }
      })
  }

}());
