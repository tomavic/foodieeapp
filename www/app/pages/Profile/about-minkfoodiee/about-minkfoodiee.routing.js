(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/about-minkfoodiee/about-minkfoodiee.html'
          }
        }
      })
  }

}());
