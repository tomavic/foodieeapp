(function () {
  'use strict';

  angular
    .module('profile-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.privacy', {
        url: '/privacy',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/Profile/privacy/privacy.html'
          }
        }
      })
  }

}());
