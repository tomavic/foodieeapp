(function() {
    'use strict';

    angular.module('inhouse-bar-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider
                .state('app.inhouseBarRequest', {
                    url: '/inhouseBarRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/request/bar-request.html',
                            controller: 'InhouseBarRequestCTRL'
                        }
                    }
                })
                .state('app.inhouseBarMenu', {
                    url: '/inhouseBarMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/menu/bar-menu.html',
                            controller: 'InhouseBarMenuCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouseBar', {
                    url: '/viewCartInhouseBar',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/cart/bar-cart.html',
                            controller: 'InhouseBarCartCTRL'
                        }
                    }
                })
                .state('app.inhouseBarCheckout', {
                    url: '/inhouseBarCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/checkout/bar-checkout.html',
                            controller: 'InhouseBarCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseBarPayMyself', {
                    url: '/inhouseBarPayMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Bar/pay-myself/bar-pay-myself.html',
                            controller: 'InhouseBarPayMyselfCTRL'
                        }
                    }
                });

        }]);
})();