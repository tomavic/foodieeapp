(function () {
  'use strict';

  angular
    .module('inhouse-bar-module')
    .controller('InhouseBarMenuCTRL', controller)

  controller.$inject = ['$scope', '$ionicModal', '$ionicSlideBoxDelegate', '$state', '$rootScope', 'Loading', '$Session', '$Easyajax', 'ionicToast'];

  function controller($scope, $ionicModal, $ionicSlideBoxDelegate, $state, $rootScope, Loading, $Session, $Easyajax, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var barID = $Session.inHouseBar.getId();
    var barSellerId = $Session.inHouseBar.getSellerId();
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {
      
      // Toggle Group part
      $scope.toggleGroup = function (group) {
        if ($scope.isGroupShown(group)) {
          $scope.shownGroup = null;
        } else {
          $scope.shownGroup = group;
        }
      };
      $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
      };



      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.$on('$ionicView.beforeEnter', function (e) {
        getCartItems();
      });

      getNormalBarDetails();

      function getNormalBarDetails() {
        Loading.show()

        var params = {
          sId: barSellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getNormalBarDetails >> ", data);
          if (data.status == "success") {
            $scope.barmenu = data.barMenu;
            $scope.barDetails = data.restaurantDetail;
            var restImages = JSON.parse(data.restImages.image)
            $scope.images = restImages
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderRestaurantMenuAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.nc_cartlist = [];
            $scope.nc_adTaxList = [];
            $scope.nc_listInfo = {};
            $scope.oc_placedlist = [];
            $scope.oc_adTaxList = [];
            $scope.oc_listInfo = {};
            $scope.nc_listInfo.Subtotal = $scope.nc_listInfo.Subtotal || 0;
            $scope.oc_listInfo.Subtotal = $scope.oc_listInfo.Subtotal || 0;


            if (data.cartlist.status == "success") {
              $scope.nc_cartlist = data.cartlist.list || [];
              $scope.nc_adTaxList = data.cartlist.adTaxList || [];
              $scope.nc_listInfo = data.cartlist.listInfo || {};
            }

            if (data.placedlist.status == "success") {
              $scope.oc_placedlist = data.placedlist.list || [];
              $scope.oc_adTaxList = data.placedlist.adTaxList || [];
              $scope.oc_listInfo = data.placedlist.listInfo || {};
            }
            for (var i in $scope.nc_cartlist) {
              var objectElement = $scope.nc_cartlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            console.log("nC sbtot ", $scope.nc_listInfo.Subtotal);
            console.log("OC sbtot ", $scope.oc_listInfo.Subtotal);
            $scope.totalElSubtotal = parseFloat($scope.nc_listInfo.Subtotal) + parseFloat($scope.oc_listInfo.Subtotal);

          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.addToCart = function (itemId) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          uId: userId,
          proId: itemId,
          barId: barID,
          // qty: 1
        };
        var callback = function (data) {
          Loading.hide();
          console.log("addToCart >> ", data);
          if (data.status == "success") {
            animateItYawala();
            getCartItems();
          } else {
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }

        var config = {
          typeRequest: "GET",
          urlRequest: AddToCartBarOrderServiceAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function animateItYawala() {
        var el = $("#dishcount"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }


      $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/Inhouse-bar-cart.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });

      // Triggered in the login modal to close it
      $scope.closeInhouseCartModal = function () {
        $scope.modal.hide();
      };

      // Open the login modal
      $scope.openInhouseCartModal = function () {
        $scope.modal.show();
      };


      $scope.checkout = function () {

        if ($scope.nc_cartlist.length == 0) {
          $state.go("app.viewCartInhouseBar");
        } else {
          $scope.openInhouseCartModal();
        }
      }


      function submitOrderToBar() {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          barId: barID,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitOrderToBar >> ", data);

          if (data.status == "success") {

            $scope.closeInhouseCartModal();
            $state.go('app.viewCartInhouseBar');
          } else {
            $scope.closeInhouseCartModal();
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderSendToRestaurantServiceAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.submitOrderToBar = function () {
        submitOrderToBar();
      }


    }
  }
})();
