(function () {
  'use strict';

  angular
    .module('inhouse-bar-module')
    .controller('InhouseBarRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {

      // Calls
      getBarDetails();
      getCoverCharges()



      function getBarDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getCoverCharges() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId,
          bar_or_exchange: 0
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == 'success') {}
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarCoverChargeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.inhouseBarData = {};
      $scope.inhouseBarData.inhouseBarNum = $stateParams.sellerId;
      $scope.inhouseBarConfigs = {
        rules: {
          inhouseBarNum: {
            required: true
          },
          inhouseBarKey: {
            required: true
          },
          inhouseBarTableNum: {
            required: true
          }
        },
        messages: {
          inhouseBarNum: {
            required: "Please enter restuarant number"
          },
          inhouseBarKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseBarTableNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }
      }

      $scope.gotoInhouseBar = function (form) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          var params = {
            restNumId: $scope.inhouseBarData.inhouseBarNum,
            passKey: $scope.inhouseBarData.inhouseBarKey,
            tableNum: $scope.inhouseBarData.inhouseBarTableNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              ionicToast.show('Enjoy!', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($stateParams.sellerId);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarData.inhouseBarTableNum);
              $state.go("app.inhouseBarMenu");
            } else if (data.reason == "exist") {
              ionicToast.show('You already exist', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($stateParams.sellerId);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarData.inhouseBarTableNum);
              $state.go("app.inhouseBarMenu");
            } else {
              ionicToast.show(data.reason, 3000, '#42b');
            }
          }
          var config = {
            typeRequest: "GET",
            urlRequest: BarOrderRequestAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }

      }

    }
  }
})();
