(function() {
    'use strict';

    angular
        .module('inhouse-bar-module')
        .controller('InhouseBarPayMyselfCTRL', controller)

    controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast'];

    function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            var barID = $Session.inHouseBar.getId();
            var barSellerId = $Session.inHouseBar.getSellerId();

            $scope.paymentType = "";


            $scope.payCash = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = !false;
                howa.langToggle.FR = false;
                $scope.paymentType = type;
            };

            $scope.payOnline = function(howa, type) {
                console.log("Order Type value:", type);
                howa.langToggle.EN = false;
                howa.langToggle.FR = !false;
                $scope.paymentType = type;
            }


            $scope.doRefresh = function() {
                getCartItems();
                $scope.$broadcast('scroll.refreshComplete');
            };
            getCartItems();

            function getCartItems() {
                Loading.show()
                var params = {
                    barId: barID,
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("getCartItems >> ", data);
                    if (data.status == "success") {

                        $scope.oc_placedlist = data.deliveredlist.list || [];
                        $scope.oc_adTaxList = data.deliveredlist.adTaxList || [];
                        $scope.oc_listInfo = data.deliveredlist.listInfo || {};
                        for (var i in $scope.oc_placedlist) {
                            var objectElement = $scope.oc_placedlist[i];
                            objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
                        }

                    }

                }
                var config = {
                    typeRequest: "POST",
                    urlRequest: UserCartListBarOrderAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };
                $Easyajax.startRequest(config);
            }

            $scope.completeOrder = function() {
                if ($scope.paymentType == "") {
                    ionicToast.show("Please select payment type", 3000, '#42B');
                    return;
                }

                Loading.show();
                var params = {
                    paytype: $scope.paymentType,
                    sId: barSellerId,
                    barId: barID,
                    event: 1
                };
                var callback = function(data) {
                    Loading.hide();
                    console.log("completeOrder : ", data);

                    if (data.status == "success" && data.paymentType == "cash") {
                        $Session.order.setType("inHouseBar");
                        ionicToast.show("You have already Paid this Bill through Online payment", 3000, '#42B');
                        $state.go("thankyou");
                    } else if (data.status == "success" && data.paymentType == "online") {
                        localStorage.setItem("InhousePaymentLink", data.link);
                        $state.go("app.inhouseBarPayOnline");
                    } else {
                        ionicToast.show(data.reason, 3000, '#249E21');
                    }

                }
                var config = {
                    typeRequest: "GET",
                    urlRequest: BarOrderPaymentServiceAPI,
                    dataRequest: params,
                    sucesCallbackRequest: callback
                };

                $Easyajax.startRequest(config);
            }

        }
    }
})();