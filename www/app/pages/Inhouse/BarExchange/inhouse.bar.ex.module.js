(function() {
    'use strict';

    angular.module('inhouse-bar-ex-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider

                .state('app.inhouseExBarRequest', {
                    url: '/inhouseExBarRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/request/bar-ex-request.html',
                            controller: 'InhouseBarExRequestCTRL'
                        }
                    }
                })
                .state('app.inHouseBarExMenu', {
                    url: '/inHouseBarExMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/menu/bar-ex-menu.html',
                            controller: 'InHouseBarExMenuCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExCart', {
                    url: '/inhouseBarExCart',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/cart/bar-ex-cart.html',
                            controller: 'InhouseBarExCartCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExCheckout', {
                    url: '/inhouseBarExCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/checkout/bar-ex-checkout.html',
                            controller: 'InhouseBarExCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseBarExPayMyself', {
                    url: '/inhouseBarExPayMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/BarExchange/pay-myself/bar-ex-pay-myself.html',
                            controller: 'InhouseBarExPayMyselfCTRL'
                        }
                    }
                })

            ;

        }]);
})();