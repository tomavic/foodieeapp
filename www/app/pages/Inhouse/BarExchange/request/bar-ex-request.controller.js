(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InhouseBarExRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";

    
    activate();

    function activate() {

      // Calls
      getBarDetails();
      getCoverCharges();

      function getBarDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function getCoverCharges() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId,
          bar_or_exchange: 1
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {

          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarCoverChargeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.inhouseBarExData = {}
      $scope.inhouseBarExData.inhouseBarExNum = $stateParams.sellerId;
      $scope.inhouseBarExConfigs = {
        rules: {
          inhouseBarExNum: {
            required: true
          },
          inhouseBarExKey: {
            required: true
          },
          inhouseTableBarExNum: {
            required: true
          }
        },
        messages: {
          inhouseBarExNum: {
            required: "Please enter restuarant number"
          },
          inhouseBarExKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseTableBarExNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }
      }

      $scope.gotoInhouseBarEx = function (form) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          Loading.show();
          var params = {
            restNumId: $scope.inhouseBarExData.inhouseBarExNum,
            passKey: $scope.inhouseBarExData.inhouseBarExKey,
            tableNum: $scope.inhouseBarExData.inhouseTableBarExNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              ionicToast.show('Enjoy!', 3000, '#42b');
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($scope.inhouseBarExData.inhouseBarExNum);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarExData.inhouseTableBarExNum);
              $state.go("app.inHouseBarExMenu");
            } else if (data.reason == 'exist') {
              $Session.inHouseBar.setId(data.barOrderId);
              $Session.inHouseBar.setToken(data.token);
              $Session.inHouseBar.setSellerId($scope.inhouseBarExData.inhouseBarExNum);
              $Session.inHouseBar.setTableNumber($scope.inhouseBarExData.inhouseTableBarExNum);
              ionicToast.show('Order exists!', 3000, '#42b');
              $state.go("app.inHouseBarExMenu");
            } else {
              ionicToast.show(data.reason, 3000, '#42b');
            }
          }
          var config = {
            typeRequest: "GET",
            urlRequest: BarExchangeRequestAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      }

    }
  }
})();
