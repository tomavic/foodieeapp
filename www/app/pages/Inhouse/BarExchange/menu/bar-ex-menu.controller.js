(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InHouseBarExMenuCTRL', controller)

  controller.$inject = ['$scope', '$ionicSlideBoxDelegate', 'ionicToast', '$ionicModal', '$state', '$rootScope', 'Loading', '$Session', '$Easyajax', 'CountdownTimer'];

  function controller($scope, $ionicSlideBoxDelegate, ionicToast, $ionicModal, $state, $rootScope, Loading, $Session, $Easyajax, CountdownTimer) {
    /* jshint validthis:true */
    var vm = this;
    var barID = $Session.inHouseBar.getId();
    var barSellerId = $Session.inHouseBar.getSellerId();
    var userId = $Session.user.getId() || "0";


    activate();

    function activate() {


      getBarExDetails();

      $scope.$on('$ionicView.enter', function () {
        getCartItems();
      });


      function getBarExDetails() {
        // Loading.show()
        var params = {
          sId: barSellerId,
          bar_or_exchange: 1
        };
        var callback = function (data) {
          // Loading.hide();
          console.log("getBarExDetails >> ", data);
          if (data.status == "success") {
            $scope.barDetails = data.restaurantDetail;
            $scope.barMenu = data.barMenu;
            $scope.deliveryAreas = data.deliveryAreas;
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeRestaurantMenuAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500,
        pagination: false
      }
      $scope.images = [{
          MediaUrl: "assets/img/bar/1.png"
        },
        {
          MediaUrl: "assets/img/bar/2.png"
        },
        {
          MediaUrl: "assets/img/bar/3.png"
        },
        {
          MediaUrl: "assets/img/bar/4.png"
        },
        {
          MediaUrl: "assets/img/bar/5.png"
        }
      ]
      setTimeout(function () {
        $ionicSlideBoxDelegate.slide(0);
        $ionicSlideBoxDelegate.update();
        $scope.$apply();
      });


      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {
            $scope.nc_cartlist = [];
            $scope.nc_adTaxList = [];
            $scope.nc_listInfo = {};
            $scope.oc_placedlist = [];
            $scope.oc_adTaxList = [];
            $scope.oc_listInfo = {};
            $scope.nc_listInfo.Subtotal = $scope.nc_listInfo.Subtotal || 0;
            $scope.oc_listInfo.Subtotal = $scope.oc_listInfo.Subtotal || 0;
            if (data.cartlist.status == "success") {
              $scope.nc_cartlist = data.cartlist.list;
              $scope.nc_adTaxList = data.cartlist.adTaxList;
              $scope.nc_listInfo = data.cartlist.listInfo;
            }
            if (data.placedlist.status == "success") {
              $scope.oc_placedlist = data.placedlist.list;
              $scope.oc_adTaxList = data.placedlist.adTaxList;
              $scope.oc_listInfo = data.placedlist.listInfo;
            }
            for (var i in $scope.nc_cartlist) {
              var objectElement = $scope.nc_cartlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            console.log("nC sbtot ", $scope.nc_listInfo.Subtotal);
            console.log("OC sbtot ", $scope.oc_listInfo.Subtotal);
            $scope.totalElSubtotal = parseFloat($scope.nc_listInfo.Subtotal) + parseFloat($scope.oc_listInfo.Subtotal);

          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function startTime() {
        $scope.show = false
        setTimeout(function () {
          ionicToast.show("Prices are going to change after 60 seconds!", 1500, '#42b');
          $scope.show = true;
          $scope.timer = new CountdownTimer(60000);
          $scope.timer.start();
          $scope.timer.$$deferred.promise.then(function () {
            getBarExDetails();
            getCartItems();
            ionicToast.show("Prices CHANGED!", 1500, '#42b');
          })
        }, 5)
      }

      $scope.addToCart = function (itemId) {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        startTime();
        Loading.show()
        var params = {
          uId: userId,
          proId: itemId,
          barId: barID,
          // qty: 1
        };
        var callback = function (data) {
          Loading.hide();
          console.log("addToCart >> ", data);
          if (data.status == "success") {
            animateItYawala();
            getCartItems();
            return;
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: AddToCartBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      function animateItYawala() {
        var el = $("#dishcount"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }


      $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/Inhouse-bar-ex-cart.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });


      // Triggered in the login modal to close it
      $scope.closeInhouseCartModal = function () {
        $scope.modal.hide();
      };


      // Open the login modal
      $scope.openInhouseCartModal = function () {
        $scope.modal.show();
      };


      $scope.checkout = function () {
        if ($scope.nc_cartlist.length == 0) {
          $state.go("app.inhouseBarExCart");
        } else {
          $scope.openInhouseCartModal();
        }

      }


      function submitOrderToBar() {
        if(userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show()
        var params = {
          barId: barID,
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitOrderToBar >> ", data);
          if (data.status == "success") {
            $scope.closeInhouseCartModal();
            $state.go('app.inhouseBarExCart');
          } else {
            $scope.closeInhouseCartModal();
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeSendToRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.submitOrderToBar = function () {
        submitOrderToBar();
      }


      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

    }
  }
})();
