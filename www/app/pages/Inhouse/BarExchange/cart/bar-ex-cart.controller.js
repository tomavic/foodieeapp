(function () {
  'use strict';

  angular
    .module('inhouse-bar-ex-module')
    .controller('InhouseBarExCartCTRL', controller)

  controller.$inject = ['$scope', 'ionicToast', '$state', '$Session', '$Easyajax', 'Loading'];

  function controller($scope, ionicToast, $state, $Session, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {
      var barID = $Session.inHouseBar.getId();
      var barSellerId = $Session.inHouseBar.getSellerId();


      $scope.$on('$ionicView.enter', function () {
        getCartItems()
      });

      $scope.doRefresh = function () {
        getCartItems();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getCartItems() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.placedlist.list || [];
            $scope.oc_adTaxList = data.placedlist.adTaxList || [];
            $scope.oc_listInfo = data.placedlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

          if (data.orderStatus == 3) {
            getCartItemsAfterBill();
          }

        }
        var config = {
          typeRequest: "GET",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getCartItemsAfterBill() {
        Loading.show()
        var params = {
          barId: barID,
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getCartItems >> ", data);
          if (data.status == "success") {

            $scope.oc_placedlist = data.placedlist.list || [];
            $scope.oc_adTaxList = data.placedlist.adTaxList || [];
            $scope.oc_listInfo = data.placedlist.listInfo || {};
            for (var i in $scope.oc_placedlist) {
              var objectElement = $scope.oc_placedlist[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          }

          if (data.orderStatus == 3) {
            getCartItemsAfterBill();
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: UserCartListBarExchangeAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function generateBill() {
        Loading.show()
        var params = {
          barId: barID,
          sId: barSellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("generateBill >> ", data);
          if (data.status == "success") {
            $state.go('app.inhouseBarExCheckout');
            ionicToast.show('Bill Generated successfully', 3000, '#42b');
          } else if (data.reason == "Bill already generated") {
            $state.go('app.inhouseBarExCheckout');
            ionicToast.show(data.reason, 3000, '#42b');
          } else {
            ionicToast.show(data.reason, 3000, '#42b');
          }
        }
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeBillAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.placeOrder = function () {
        submitOrderToBar()
      }

      $scope.generateBill = function () {
        generateBill();
      }

    }
  }
})();
