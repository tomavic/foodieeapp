(function () {
  'use strict';

  angular.module('inhouse-module', [
      'inhouse-food-module',
      'inhouse-bar-module',
      'inhouse-bar-ex-module'
    ])

    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('app.inhouseFoodBarList', {
          url: '/inhouseFoodBarList',
          views: {
            'menuContent': {
              templateUrl: 'app/pages/Inhouse/list/inhouse-food-bar-list.html',
              controller: 'InhouseFoodBarListCTRL'
            }
          }
        });
    }]);

})();
