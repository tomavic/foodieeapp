(function () {
  'use strict';

  angular
    .module('inhouse-module')
    .controller('InhouseFoodBarListCTRL', controller);

  controller.$inject = ['$scope', '$Session', '$state', '$Easyajax', 'Loading'];

  function controller($scope, $Session, $state, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var cityId = $Session.city.getId() ||"0" ;
    var areaId = $Session.area.getId() ||"0" ;
    var userId = $Session.user.getId() ||"0" ;

    activate();

    ////////////////

    function activate() {




      // Calls
      getInhouseRestList();
      getBarList();
      getBarExchangeList();


      $scope.onRefreshInhouseFood = function () {
        getInhouseRestList();
        $scope.$broadcast('scroll.refreshComplete');
      };
      $scope.onRefreshInhouseBar = function () {
        getBarList();
        $scope.$broadcast('scroll.refreshComplete');
      };
      $scope.onRefreshInhouseBarEx = function () {
        getBarExchangeList();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getInhouseRestList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sKey: "IHO",
          cityAreaId: areaId,
          userEmailId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantSortAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function getBarList() {
        Loading.show();
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getBarExchangeList() {
        Loading.show();
        var params = {
          cityId: cityId,
          cityAreaId: areaId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.barExList = data.restaurantList;
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeRestaurantAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.openBarReq = function () {
        $state.go("app.inhouseBarRequest");
      }
      $scope.openBarExReq = function () {
        $state.go("app.inhouseExBarRequest");
      }


    }
  }
})();
