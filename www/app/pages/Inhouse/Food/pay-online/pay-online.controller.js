(function() {
    'use strict';

    angular
        .module('inhouse-food-module')
        .controller('InhouseFoodPayOnlineCTRL', paymentCTRL)


    function paymentCTRL() {
        var link = localStorage.getItem("InhousePaymentLink");
        $("#payment").attr("src", link);
    }

})();