(function() {
    'use strict';

    angular.module('inhouse-food-module', [])
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider
                .state('app.inhouseFoodRequest', {
                    url: '/inhouseFoodRequest/:sellerId',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/request/request.html',
                            controller: 'InhouseFoodRequestCTRL'
                        }
                    }
                })
                .state('app.inHouseFoodMenu', {
                    url: '/inHouseFoodMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/menu/menu.html',
                            controller: 'InhouseFoodMenuCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouseModal', {
                    url: '/viewCartInhouseModal',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/cart-not-served/cart-not-served.html',
                            controller: 'InhouseFoodCartNotServedCTRL'
                        }
                    }
                })
                .state('app.viewCartInhouse', {
                    url: '/viewCartInhouse',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/cart-served/cart-served.html',
                            controller: 'InhouseFoodCartServedCTRL'
                        }
                    }
                })
                .state('app.inhouseCheckout', {
                    url: '/inhouseCheckout',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/checkout/checkout.html',
                            controller: 'InhouseFoodCheckoutCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayMyself', {
                    url: '/payMyself',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-myself/pay-myself.html',
                            controller: 'InhouseFoodPayMyselfCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayOnline', {
                    url: '/payOnline',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-online/pay-online.html',
                            controller: 'InhouseFoodPayOnlineCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayItemWise', {
                    url: '/payItemWise',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-itemwise/pay-itemwise.html',
                            controller: 'InhouseFoodPayItemWiseCTRL'
                        }
                    }
                })
                .state('app.inhouseFoodPayPriceWise', {
                    url: '/payPriceWise',
                    views: {
                        'menuContent': {
                            templateUrl: 'app/pages/Inhouse/Food/pay-pricewise/pay-pricewise.html',
                            controller: 'InhouseFoodPayPriceWiseCTRL'
                        }
                    }
                });

        }]);
})();