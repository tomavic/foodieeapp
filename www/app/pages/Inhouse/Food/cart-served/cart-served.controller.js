(function () {
  // 'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodCartServedCTRL', controller)

  controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";


    activate();



    function activate() {


      $scope.noItems = true;
      $scope.$on('$ionicView.enter', function (e) {
        inhouseCartList();
      });

      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else if (data.status == 'success' && $scope.cartListOld == {}) {
            $scope.noItems = true;
          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      }

      $scope.generateBill = function (cartId, type) {
        Loading.show();
        var params = {
          sId: sellerId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $state.go("app.inhouseCheckout");
          } else if (data.reason == "Payment is pending and bill has been generated") {
            ionicToast.show("Payment is pending and bill has been generated", 3000, '#249E21');
            $state.go("app.inhouseCheckout");
          } else {
            ionicToast.show(data.reason, 3000, '#42B');
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: PayGenrateInhouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();
