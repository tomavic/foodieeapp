(function () {
  "use strict";

  angular
    .module("inhouse-food-module")
    .controller("InhouseFoodMenuCTRL", controller);

  controller.$inject = ["$scope", "Loading", "$Session", "$Easyajax", "$state", "$ionicPopup", "ionicToast"];

  function controller($scope, Loading, $Session, $Easyajax, $state, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";
    $scope.totalCartItems = 0;
    $scope.cartList = [];
    $scope.cartListOld = [];

    activate();

    function activate() {
      $scope.$on("$ionicView.enter", function (event, data) {
        // getInhouseRestaurantDetails();
        updateInhouseCartWithoutLoader();
      });

      // Toggle part
      // $scope.toggleGroup = function(group) {
      //     if ($scope.isGroupShown(group)) {
      //         $scope.shownGroup = null;
      //     } else {
      //         $scope.shownGroup = group;
      //     }
      // };
      // $scope.isGroupShown = function(group) {
      //     return $scope.shownGroup === group;
      // };
      //
      getInhouseRestaurantDetails();


      function getInhouseRestaurantDetails() {
        Loading.show();
        var params = {
          sId: sellerId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("getInhouseRestaurantDetails >> ", data);

          $scope.restaurantDetail = data.restaurantDetail;
          $scope.menuCodOnline = data.menu;

          //Setting restaurant details to Session
          $Session.inHouse.setSellerDiscountPercent(
            data.restaurantDetail.discountInPercent
          );
          $Session.inHouse.setSellerDiscountAmount(
            data.restaurantDetail.discountInRs
          );
          $Session.inHouse.setSellerName(data.restaurantDetail.restaurantName);
          $Session.inHouse.setSellerMinCharge(data.restaurantDetail.minOrder);
          $Session.inHouse.setSellerMinDiscount(
            data.restaurantDetail.minAmountForDis
          );
        };
        var config = {
          typeRequest: "POST",
          urlRequest: restaurantDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function updateInhouseCart() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("update cart : ", data);
          if (data.status == "success") {
            $scope.cartList = data.userCartList.list || [];
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;

            $scope.cartListOld = data.userCartTaxOld.list || [];
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            console.log("$scope.cartList >> ", $scope.cartList);
            console.log("$scope.cartListOld >> ", $scope.cartListOld);
            updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function updateInhouseCartWithoutLoader() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          console.log("update cart : ", data);
          if (data.status == "success") {
            $scope.cartList = data.userCartList.list || [];
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;

            $scope.cartListOld = data.userCartTaxOld.list || [];
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice =
                objectElement.originalPrice * objectElement.proQty;
            }
            console.log("$scope.cartList >> ", $scope.cartList);
            console.log("$scope.cartListOld >> ", $scope.cartListOld);
            updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }



      function updateCartBadge(list, oldList) {
        var newLength = list.length || 0;
        var oldLength = oldList.length || 0;
        $scope.totalCartItems = newLength + oldLength;
      }

      $scope.addToCart = function (proId) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        // animateItYawala();
        Loading.show();
        var params = {
          proId: proId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          console.log("addToCart : ", data);
          if (data.status == "success") {
            updateInhouseCart();
          } else {
            ionicToast.show(data.reason, 3000, "#42B");
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: addToCartInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function animateItYawala() {
        var el = $("#Bar-Cart h5 span"),
          newone = el.clone(true);
        el.before(newone);
        el.remove();
      }

      $scope.submitOrgenerate = function () {
        if ($scope.cartList == undefined && $scope.cartListOld == undefined) {
          $ionicPopup.alert({
            title: "Empty Cart",
            template: "Please add whatever you like from our food Menu!",
            okType: "button-assertive"
          });
          return;
        }
        if ($scope.cartList == undefined) {
          $state.go("app.viewCartInhouse");
        } else {
          $state.go("app.viewCartInhouseModal");
        }
      };
    }
  }
})();

/***
 * Code Trash
 * 
 * 
 * 
function getInhousedetail() {
var params = {
    sId: sellerId,
    uId: userId,
    inhouseId: inhouseId
};
var callback = function(data) {
    console.log("getInhousedetail >> ", data);
};
var config = {
    typeRequest: "POST",
    urlRequest: inHouseDetailAPI,
    dataRequest: params,
    sucesCallbackRequest: callback
};
$Easyajax.startRequest(config);
};


// function checkMinimumCharge() {
//     var total = parseInt($Session.inHouse.getOrderTotalAmount());
//     var minCharge = parseInt($Session.inHouse.getSellerMinCharge());
//     if (total < minCharge) {
//         alert("check total amount < min charge");
//         return false;
//     } else {
//         return true;
//     }
// }




 * 
 * 
 * 
 */
