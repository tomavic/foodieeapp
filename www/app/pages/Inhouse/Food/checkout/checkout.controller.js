(function() {
    'use strict';

    angular
        .module('inhouse-food-module')
        .controller('InhouseFoodCheckoutCTRL', controller)

    controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', 'ionicToast'];

    function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, ionicToast) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        function activate() {

            $scope.paymentLabel = '1';
            $scope.setOnline = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.DE = !false;
                howa.langToggle.EN = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            };

            $scope.setItemWise = function(howa, type) {
                console.log("Order Type value:", type);
                // console.log("howa:", howa);
                howa.langToggle.EN = !false;
                howa.langToggle.DE = false;
                howa.langToggle.FR = false;
                $scope.paymentLabel = type;
            }

            $scope.setPriceWise = function(howa, type) {
                console.log("Order Type value:", type);
                //console.log("howa:", howa);
                howa.langToggle.FR = !false;
                howa.langToggle.DE = false;
                howa.langToggle.EN = false;
                $scope.paymentLabel = type;
            }

            $scope.gotoPayment = function() {
                if ($scope.paymentLabel == 1) {
                    $state.go("app.inhouseFoodPayMyself");
                } else if ($scope.paymentLabel == 2) {
                    ionicToast.show("Pay Item wise option is not available", 3000, '#42B');
                    // $state.go("app.payItemWise");
                } else if ($scope.paymentLabel == 3) {
                    ionicToast.show("Pay Price wise option is not available", 3000, '#42B');
                    //$state.go("app.payPriceWise");
                }
            }


        }
    }
})();