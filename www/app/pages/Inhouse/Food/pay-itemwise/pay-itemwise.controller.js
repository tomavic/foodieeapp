(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodPayItemWiseCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast', '$cordovaContacts', '$ionicPlatform', '$ionicModal'];

  function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast, $cordovaContacts, $ionicPlatform, $ionicModal) {
    /* jshint validthis:true */
    var vm = this;
    var mobileNum_G, memberName_G;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var userId = $Session.user.getId() ||"0" ;

    activate();
    $scope.feSelect = false;
    $scope.checkItems = {}
    $scope.checkedBoxes = []
    $scope.cart = {
      items: []
    };

    $ionicModal.fromTemplateUrl('app/pages/inhouse/partials/contacts-itemwise-modal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeContactsModal = function () {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.openContactsModal = function () {
      $scope.modal.show();
    };

    function activate() {


      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };

        $Easyajax.startRequest(config);
      }


      $scope.selectItemWise = function (model) {
        console.log("Model item > ", model.item);

        var __CheckedBoxes = [];
        angular.forEach($scope.checkItems, function (value, key) {
          if (value) {
            __CheckedBoxes.push(parseInt(key));
            $scope.checkedBoxes.push(parseInt(key));
          }
        });
        $scope.checkedBoxes = __CheckedBoxes;
        $scope.feSelect = getSelection(__CheckedBoxes);
        console.log(" $scope.checkedBoxes")
        console.log($scope.checkedBoxes)

      }

      var getSelection = function (checkedList) {
        if (checkedList.length != 0) {
          return true;
        } else {
          return false;
        }
      }

      $scope.sendItemTo = function () {
        $scope.openContactsModal();
      }


      $scope.pickContactUsingNativeUI = function () {
        $scope.contacts = {
          member: []
        };
        $cordovaContacts.pickContact().then(function (contactPicked) {
            var contactsLength = $scope.contacts.member.length;
            if (contactsLength == 0) {
              var contactNo = contactPicked.phoneNumbers[0].value.toString().replace(/\(|\)|-|\s/g, '');
              var validContact = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(contactNo);

              if (validContact) {
                $scope.contacts.member.push({
                  "memberName": contactPicked.displayName.toString(),
                  "memberNumber": contactNo
                });
                $scope.selectedContactName = contactPicked.displayName.toString();
              } else {

                ionicToast.show("Please select Valid contact number", 3000, '#42B');
              }
            } else if (contactsLength >= 1) {
              ionicToast.show("Only one contact is allowed to add per item", 3000, '#42B');
            }


            mobileNum_G = $scope.contacts.member["0"].memberNumber;
            memberName_G = $scope.contacts.member["0"].memberName;
          },
          function (err) {
            console.error("Error happend when pickup contact ", err);
          });
      }

      // $scope.getAllContacts = function() {
      //     var opts = {
      //         multiple: true,
      //     };
      //     $cordovaContacts.find(opts).then(function(allContacts) {
      //         $scope.contacts = allContacts;
      //     }, function(err) {
      //         alert(err);
      //     });
      // };


      $scope.submitNumber = function () {
        if ($scope.contacts.member.length == 0) {
          ionicToast.show("Please add mobile number from your contacts", 3000, '#42B');
        } else {

          for (var i in $scope.checkedBoxes) {

            console.log(i);
            var hiddenitemPrice = $(".class_" + $scope.checkedBoxes[i]).val();
            $scope.cart.items.push({
              "inhouseId": inhouseId,
              "cartId": $scope.checkedBoxes[i],
              "price": hiddenitemPrice,
              "mobileNum": mobileNum_G
            });
            $("#item_details" + $scope.checkedBoxes[i]).html("<h5>**This item will be paid by <span>" + memberName_G + "</span></h5>");

          }

          angular.forEach($scope.checkItems, function (value, key) {
            if (value) {
              $scope.checkItems[key] = false;
              $scope.feSelect = false;
            }
          });

          $scope.closeContactsModal();
        }
      }

      $scope.completeOrder = function () {
        if ($scope.cart.items.length === $scope.cartListOld.length) {

          Loading.show();
          var params = {
            jsonData: JSON.stringify($scope.cart),
            inhouseId: inhouseId,
            inhousePaytype: "1"
          };

          var callback = function (data) {
            Loading.hide();

            console.log("inhouseCartList : ", data);
            if (data.status == 'success') {
              ionicToast.show("Bill Paid and sent to your contacts", 3000, '#249E21');
              $state.go("thankyou");
            } else {
              alert(JSON.stringify(data))
              ionicToast.show("Try Again Later", 3000, '#42B');
            }
          };
          var config = {
            typeRequest: "POST",
            urlRequest: ItemWiseBillSplitSubmitAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };

          $Easyajax.startRequest(config);
        } else {
          ionicToast.show("Please make sure all items are assigned to a contact number", 3000, '#42b');
        }
      }
    }
  }
})();
