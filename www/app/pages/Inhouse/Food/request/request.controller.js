(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodRequestCTRL', controller)

  controller.$inject = ['$scope', '$Session', '$state', '$stateParams', 'ionicToast', '$Easyajax', '$ionicPopup', 'Loading'];

  function controller($scope, $Session, $state, $stateParams, ionicToast, $Easyajax, $ionicPopup, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() ||"0" ;

    
    activate();

    function activate() {

      getRestaurantsDetails();


      function getRestaurantsDetails() {
        Loading.show();
        var params = {
          sId: $stateParams.sellerId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status == "success") {
            $scope.restaurantDetail = data.restaurantDetail;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: menuDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.inhouseData = {};
      $scope.inhouseData.restNum = $stateParams.sellerId;
      $scope.inhouseConfigs = {
        rules: {
          inhouseRestNum: {
            required: true
          },
          inhouseRestKey: {
            required: true
          },
          inhouseTableNum: {
            required: true
          }
        },
        messages: {
          inhouseRestNum: {
            required: "Please enter restuarant number"
          },
          inhouseRestKey: {
            required: "Please enter restuarant key (Ask the restaurant's staff)"
          },
          inhouseTableNum: {
            required: "Please enter a table number (Ask the restaurant's staff)"
          }
        }

      };

      $scope.gotoInhouseRest = function (form) {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        if (form.validate()) {
          var params = {
            restNumId: $scope.inhouseData.restNum,
            restPassKeyId: $scope.inhouseData.restKey,
            tableNumId: $scope.inhouseData.tableNum,
            uId: userId
          };
          var callback = function (data) {
            Loading.hide();
            if (data.status == 'success') {
              $Session.inHouse.setId(data.inhouseId);
              $Session.inHouse.setSellerId($scope.inhouseData.restNum);
              $Session.inHouse.setTableNumber($scope.inhouseData.tableNum);
              $state.go("app.inHouseFoodMenu");
            } else if (data.status == "failure" && data.message == "Your have already reserved or sent request for your table" && data.paymentType == "Did not send order") {
              $Session.inHouse.setId(data.inhouseId);
              $state.go("app.inHouseFoodMenu");
            } else if (data.status == "failure" && data.message == "Your have already reserved or sent request for your table") {
              $scope.showConfirm(data);
            } else if (data.status == "failure" && data.reason == "Please ask for valid pass key") {
              ionicToast.show('Please ask for valid pass key', 3000, '#42b');
            } else if (data.status == 'failure' && data.inhouseId != undefined) {
              $Session.inHouse.setId(data.inhouseId);
              $state.go("app.inHouseFoodMenu");
            }
          }
          var config = {
            typeRequest: "POST",
            urlRequest: inHouseCheckRestaurantAPI,
            dataRequest: params,
            sucesCallbackRequest: callback
          };
          $Easyajax.startRequest(config);
        }
      }

      // A confirm dialog
      $scope.showConfirm = function (data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Complete Order',
          cancelText: "Pay Now",
          cancelType: "button-assertive",
          okText: "Continue Shopping",
          okType: "button-balanced",
          template: data.message + ", and " + data.paymentType + " , Make your choice"
        });
        confirmPopup.then(function (res) {
          if (res) {
            $Session.inHouse.setId(data.inhouseId);
            $Session.inHouse.setSellerId($scope.inhouseData.restNum);
            $Session.inHouse.setTableNumber($scope.inhouseData.tableNum);
            $state.go("app.inHouseFoodMenu");
          } else {
            $state.go("app.inhouseCheckout");
          }
        });
      };

    }
  }
})();
