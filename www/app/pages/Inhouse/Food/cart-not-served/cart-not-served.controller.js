(function () {
  // 'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodCartNotServedCTRL', controller)

  controller.$inject = ['$scope', '$stateParams', 'Loading', '$Session', '$Easyajax', '$state', '$ionicModal', '$ionicPopup', 'ionicToast'];

  function controller($scope, $stateParams, Loading, $Session, $Easyajax, $state, $ionicModal, $ionicPopup, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var tableNum = $Session.inHouse.getTableNumber();
    var userId = $Session.user.getId() || "0";



    activate();

    function activate() {

      $scope.$on('$ionicView.enter', function (e) {
        inhouseCartList();
      });

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.cartList = data.userCartList.list;
            $scope.CartTaxes = data.userCartList.listInfo;
            $scope.CartAdTaxes = data.userCartList.adTaxList;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;
            for (var i in $scope.cartList) {
              var objectElement = $scope.cartList[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            for (var j in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[j];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }
            // updateCartBadge($scope.cartList, $scope.cartListOld);
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.increaseDecrease = function (cartId, type) {
        var params = {
          inhouseId: inhouseId,
          cartId: cartId,
          calcType: type
        };
        var callback = function (data) {
          console.log("increaseDecrease : ", data);
          inhouseCartList();
        };
        var config = {
          typeRequest: "GET",
          urlRequest: userCartInHouseIncDecAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.submitInhouseOrder = function () {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId,
          afterEdit: 'AF'
        };
        var callback = function (data) {
          Loading.hide();
          console.log("submitInhouseOrder : ", data);
          if (data.status == 'success') {
            $state.go("app.viewCartInhouse");
          } else {
            alert("Minimum order");
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: completeInHouseOredrAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.goToCart = function () {
        $state.go("app.viewCartInhouse");
      }

    }
  }
})();
