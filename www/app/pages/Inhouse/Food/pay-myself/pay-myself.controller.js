(function () {
  'use strict';

  angular
    .module('inhouse-food-module')
    .controller('InhouseFoodPayMyselfCTRL', controller)

  controller.$inject = ['$scope', '$state', '$Easyajax', 'Loading', '$Session', 'ionicToast'];

  function controller($scope, $state, $Easyajax, Loading, $Session, ionicToast) {
    /* jshint validthis:true */
    var vm = this;
    var inhouseId = $Session.inHouse.getId();
    var sellerId = $Session.inHouse.getSellerId();
    var userId = $Session.user.getId() ||"0" ;


    activate();

    function activate() {


      $scope.paymentType = "";


      $scope.payCash = function (howa, type) {
        console.log("Order Type value:", type);
        // console.log("howa:", howa);
        howa.langToggle.EN = !false;
        howa.langToggle.FR = false;
        $scope.paymentType = type;
      };

      $scope.payOnline = function (howa, type) {
        console.log("Order Type value:", type);
        // console.log("howa:", howa);
        howa.langToggle.EN = false;
        howa.langToggle.FR = !false;
        $scope.paymentType = type;
      }


      inhouseCartList();

      function inhouseCartList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          inhouseId: inhouseId
        };

        var callback = function (data) {
          Loading.hide();
          console.log("inhouseCartList : ", data);
          if (data.status == 'success') {
            $scope.noItems = false;
            $scope.cartListOld = data.userCartTaxOld.list;
            $scope.CartTaxesOld = data.userCartTaxOld.listInfo;
            $scope.CartAdTaxesOld = data.userCartTaxOld.adTaxList;

            for (var i in $scope.cartListOld) {
              var objectElement = $scope.cartListOld[i];
              objectElement.totalItemPrice = objectElement.originalPrice * objectElement.proQty;
            }

          } else {
            $scope.noItems = true;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cartListInHouseAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.completeOrder = function () {
        if ($scope.paymentType == "") {
          ionicToast.show("Please select payment type", 3000, '#42B');
          return false;
        }
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          sId: sellerId,
          uId: userId,
          delType: $scope.paymentType,
          inhouseId: inhouseId
        };
        var callback = function (data) {
          Loading.hide();

          if (data.status == "failure") {
            ionicToast.show("Please contact restaurant regarding this issue", 3000, '#42B');
          } else if (data.status == "failure" && data.reason == "inhouse order items is missing") {
            ionicToast.show("You have already Paid this Bill through Online payment", 3000, '#42B');
            $state.go("app.history");
          } else if (data.status == "success") {
            if ($scope.paymentType == "cod") {
              ionicToast.show("Bill Paid", 3000, '#249E21');
              $state.go("thankyou");
              $Session.order.setType("inHouseFood");
            } else if ($scope.paymentType == "online") {
              localStorage.setItem("InhousePaymentLink", data.link);
              $state.go("app.inhouseFoodPayOnline");
            } else {
              ionicToast.show("you are trtying something wrong .. please check deltype", 3000, '#249E21');
            }
          }

        }
        var config = {
          typeRequest: "POST",
          urlRequest: inHouseOederPayNowAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

    }
  }
})();
