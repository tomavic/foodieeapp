(function () {
  'use strict';

  angular
    .module('order-history-details-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
    .state('app.orderHistory', {
      url: '/history/:orderId',
      views: {
        'menuContent': {
          templateUrl: 'app/pages/History/order-history-details/order-history-details.html',
          controller: 'OrderHistoryDetailsCTRL',
          controllerAs: 'vm'
        }
      }
    })
  }

}());
