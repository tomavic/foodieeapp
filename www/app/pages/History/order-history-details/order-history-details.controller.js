(function () {
  'use strict';

  angular
    .module('order-history-details-module')
    .controller('OrderHistoryDetailsCTRL', HistoryDetailsController)

    HistoryDetailsController.$inject = ['$scope', '$Session', '$Easyajax', 'Loading', '$stateParams', 'ionicToast', '$ionicPopup'];

  function HistoryDetailsController($scope, $Session, $Easyajax, Loading, $stateParams, ionicToast, $ionicPopup) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    $scope.orderisCancelable = false;
    $scope.restName = '';

    activate();

    function activate() {

      getOrderDetails();

      function getOrderDetails() {
        Loading.show();
        var params = {
          orderId: $stateParams.orderId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            $scope.orderDetails = data.orderListAndDetail.orderDetail;
            $scope.orderDetailsTaxes = data.orderListAndDetail.adTaxList;
            $scope.cartList = data.orderListAndDetail.orderList;
            $scope.listToppings = data.orderListAndDetail.topping;
            $scope.restName = data.orderListAndDetail.restaurantName;
            
            ($scope.orderDetails.deliveryType == "cod") ? $scope.orderDetails.deliveryType = "Cash On Delivery" : $scope.orderDetails.deliveryType = "Online";
            ($scope.orderDetails.review == "no") ? $scope.orderDetails.review = "Order has not been reviewed" : $scope.orderDetails.review = "Order has been reviewed";
            ($scope.orderDetails.status.toLocaleLowerCase() == 'pending') ? $scope.orderisCancelable = true : $scope.orderisCancelable = false;


          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: oredrHistoryDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };


      $scope.sendEmail = function () {
        Loading.show();
        var params = {
          orderId: $stateParams.orderId
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('Receipt sent to your email!', 3000, '#68a581') : ionicToast.show('Sorry! Please try to send again', 3000, '#33cd5f');
        };
        var config = {
          typeRequest: "GET",
          urlRequest: SendReceiptViaEmailAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      $scope.cancelOrder = function () {
        $scope.cancelOrderData = {};
        $ionicPopup.show({
          template: '<label class="item item-input item-floating-label"><textarea ng-model="cancelOrderData.message" placeholder="Why you want to cancel your order ..?" rows="4" cols="50"></textarea></label>',
          title: 'Cancel Order',
          subTitle: 'Please mention cancellation reason',
          cssClass: 'cancel-order',
          scope: $scope,
          buttons: [{
              text: 'Cancel'
            },
            {
              text: '<b>Submit</b>',
              type: 'button-assertive',
              onTap: function (e) {
                if (!$scope.cancelOrderData.message) {
                  e.preventDefault();
                  ionicToast.show('Please mention cancellation reason!', 3000, '#b50905');
                } else {
                  cancelMyOrder();
                }
              }
            }
          ]
        });
      }

      function cancelMyOrder() {
        if(userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          orderId: $stateParams.orderId,
          uId: userId,
          event: "1",
          reason: $scope.cancelOrderData.message
        };
        var callback = function (data) {
          Loading.hide();
          data.status === "success" ? ionicToast.show('Order cancelled!', 3000, '#33cd5f') : ionicToast.show('Could not cancel order!', 3000, '#b50905');
        };
        var config = {
          typeRequest: "POST",
          urlRequest: cancelOrderAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      

    }
  }
})();
