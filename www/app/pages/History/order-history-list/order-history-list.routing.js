(function () {
  'use strict';

  angular
    .module('order-history-list-module')
    .config(Config)

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
    $stateProvider
      .state('app.history', {
        url: '/history',
        views: {
          'menuContent': {
            templateUrl: 'app/pages/History/order-history-list/order-history-list.html',
            controller: 'OrderHistoryListCTRL',
            controllerAs: 'vm'
          }
        }
      });
  }

}());
