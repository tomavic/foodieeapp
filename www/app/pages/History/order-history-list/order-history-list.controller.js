(function () {
  'use strict';

  angular
    .module('order-history-list-module')
    .controller('OrderHistoryListCTRL', HistoryController)

  HistoryController.$inject = ['$scope', '$Session', '$Easyajax', 'Loading'];

  function HistoryController($scope, $Session, $Easyajax, Loading) {
    /* jshint validthis:true */
    var vm = this;
    var userId = $Session.user.getId() || "0";
    vm.errorMessage = 'No active orders!';

    activate();

    function activate() {


      // Calls
      getAllHistories();


      $scope.doRefreshOrder = function () {
        getHistoryList();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.doRefreshBarOrder = function () {
        getInhouseBarHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }
      $scope.doRefreshBarExOrder = function () {
        getInhouseBarExHistory();
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getAllHistories() {
        getHistoryList();
        getInhouseBarHistory();
        getInhouseBarExHistory();
      }

      function getHistoryList() {
        if (userId === "0") {
          console.log("Guest User mode is ON!");
          vm.errorMessage = 'Please login to check the order history!';
          return false;
        }
        Loading.show();
        var params = {
          uId: userId
        };
        var callback = function (data) {
          Loading.hide();
          if (data.status = "success") {
            $scope.orderListOld = data.oldOrderList;
            $scope.orderListNew = data.newOrderList;
            $scope.orderListInhouse = data.completeInhouseList;
          }
        };
        var config = {
          typeRequest: "POST",
          urlRequest: orderHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      };

      function getInhouseBarHistory() {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          userId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListInhouseBar = data.listBars || {};
          for (var i in $scope.orderListInhouseBar) {
            var objectElement = $scope.orderListInhouseBar[i];
            switch (objectElement.barStatus) {
              case 0:
                objectElement.orderStatus = "Not Paid";
                break;
              case 1:
                objectElement.orderStatus = "Paid by myself";
                break;
              case 2:
                objectElement.orderStatus = "Paid Cash to restaurant";
                break;
              case 3:
                objectElement.orderStatus = "Choose Online";
                break;
              case 4:
                objectElement.orderStatus = "Paid Online";
                break;
              case 5:
                objectElement.orderStatus = "Paid by Bill Split";
                break;
              default:
                console.log('Sorry, we are out of ' + objectElement.barStatus + '.');
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarOrderHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }

      function getInhouseBarExHistory() {
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        Loading.show();
        var params = {
          userId: userId
        };
        var callback = function (data) {
          Loading.hide();
          $scope.orderListInhouseBarEx = data.listBars;
          for (var i in $scope.orderListInhouseBarEx) {
            var objectElement = $scope.orderListInhouseBarEx[i];
            if (objectElement.barStatus == 0) {
              objectElement.orderStatus = "Not Paid";
            } else if (objectElement.barStatus == 1) {
              objectElement.orderStatus = "Paid by myself";
            } else if (objectElement.barStatus == 2) {
              objectElement.orderStatus = "Paid Cash to restaurant";
            } else if (objectElement.barStatus == 3) {
              objectElement.orderStatus = "Choose Online";
            } else if (objectElement.barStatus == 4) {
              objectElement.orderStatus = "Paid Online";
            } else if (objectElement.barStatus == 5) {
              objectElement.orderStatus = "Paid by Bill Split";
            }
          }
        };
        var config = {
          typeRequest: "GET",
          urlRequest: BarExchangeHistoryAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


    }
  }
})();
