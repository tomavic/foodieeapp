(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('httpRequest', service)

  service.$inject = ['Loading', 'ionicToast', '$q'];

  function service(Loading, ionicToast, $q) {
    var headers = {
      'contentType': 'application/x-www-form-urlencoded',
      'username': 'foodiee',
      'password': 'MTIzNDU=',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, username, password',
      'Access-Control-Allow-Credentials': 'true'
            // .append('Access-Control-Allow-Origin', '*')
      // .append('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
      // .append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization')
      // .append('Access-Control-Allow-Credentials', 'true')
    };
    /**
     * @function makeRequest
     * @param configRequest: configirations is being passed to this function
     * @todo assign values of config to private variables, and check if the request is not for token , so we can
     *       insert it on every AJAX request 
     * @return value of request is returned to makeRequest() function
     * */
    var makeRequest = function (configRequest) {

      var typeRequest = configRequest.typeRequest;
      var urlRequest = configRequest.urlRequest;
      var dataRequest = configRequest.dataRequest || {};
      var sucesCallbackRequest = configRequest.sucesCallbackRequest || function (response) {
        console.log(response)
      };
      var errorCallbackRequest = configRequest.errorCallbackRequest || function (err) {
        console.error("Error while trying to make AJAX request.", err);
        Loading.hide();
        ionicToast.show("Opss! Try again in a while!", 3000, '#A71212');
      };
      var token = localStorage.getItem("Token");
      if (urlRequest != tokenInfoAPI) {
        dataRequest.Token = token;
      }

      $.ajax({
        type: typeRequest,
        url: urlRequest,
        headers: headers,
        data: dataRequest,
        success: sucesCallbackRequest,
        error: errorCallbackRequest,
        timeout: 10000000,
        dataType: "json",
        crossDomain: true,
      });

    };


    this.validateNotNullData = function (DataObjectToTest, typeRequest) {
      if (typeRequest === "POST") {
        if (typeof DataObjectToTest !== "object") {
          throw new Error("POST request, you are trying to sent data that is not an object");
        }
      }
    }

    this.validateTypeRequest = function (typeRequest) {
      if (typeRequest !== "POST" && typeRequest !== "GET") {
        throw new Error("The request Type should be POST/GET");
      }
    }

    this.validateCallback = function (callback) {
      if (typeof callback !== "undefined") {
        if (typeof callback !== "function") {
          throw new Error("The callback function is not a function, make sure that callback is really a function");
        }
      }
    }

    this.getResponse = function (configRequest) {
      makeRequest(configRequest);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('$Easyajax', factory)

  factory.$inject = ['httpRequest', '$ionicPopup', 'Loading'];

  function factory(httpRequest, $ionicPopup, Loading) {
    var service = {
      startRequest: startRequest
    }

    return service;

    function startRequest(configRequest) {



      //Test if the data are valid in case of a POST
      httpRequest.validateNotNullData(configRequest.dataRequest, configRequest.typeRequest);

      //Test if the Request Type is POST or GET
      httpRequest.validateTypeRequest(configRequest.typeRequest);

      //Test if the callback is a function
      httpRequest.validateCallback(configRequest.sucesCallbackRequest);

      //Finally, get configurations and start doing request and return the response
      if (!navigator.onLine) {
        // console.log("Device connected to Internet? >> " + navigator.onLine);
        Loading.hide();
        showAlert();
        return false;
      } else {
        // console.log("Device connected to Internet? >> " + navigator.onLine);
        var response = httpRequest.getResponse(configRequest);
        return response;
      }

    }

    function showAlert() {
      $ionicPopup.show({
        title: 'Oops!',
        template: 'You are offline! Check your internet connection',
        buttons: [{
          text: 'REFRESH',
          type: 'button-assertive',
          onTap: function (e) {
            console.log("e > ", e);
            // e.preventDefault()
            // location.reload();
            startRequest(configRequest);
          }
        }]
      });
    }



  }
})();
