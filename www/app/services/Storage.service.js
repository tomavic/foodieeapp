
(function () {
    "use strict";
  
    angular.module("MinkFoodiee").service("Session", service);
  
    service.$inject = ["localStorageService"];
  
    function service(localStorageService) {
  
      this.get = get;
      this.save = save;
      this.remove = remove;
  
      function checkValue(value) {
        if (!checkParam(value)) return "";
        return value;
      }
  
      function checkParam(param) {
        if (typeof param !== "undefined" && param.toString() !== "undefined")
          return true;
        else return false;
      }
  
      function get(name) {
        if (checkParam(name)) return localStorageService.get(name);
      }
  
      function save(name, value) {
        if (!checkParam(name)) return false;
        value = checkValue(value);
        localStorageService.set(name, value);
      }
  
      function remove(name) {
        if (checkParam(name)) localStorageService.remove(name);
      }
    }
  })();