(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('Loading', factory)

  factory.$inject = ['$ionicLoading'];

  function factory($ionicLoading) {
    var service = {
      show: show,
      hide: hide,
      showDish: showDish,
      hideDish: hideDish
    };

    return service;

    function show() {
      $ionicLoading.show({
        template: '',
        animation: 'fade-in',
        showBackdrop: false
      });
    };

    function hide() {
      $ionicLoading.hide();
    }

    function showDish() {
      $ionicLoading.show({
        // <ion-spinner class="spinner-balanced"></ion-spinner>
        template: '',
        animation: 'fade-in',
        showBackdrop: false
      }).then(function () {
        //console.log("loading is shown");
      });
    };

    function hideDish() {
      $ionicLoading.hide().then(function () {
        //console.log("loading is hidden");
      });
    }


  }
})();
