(function() {
  "use strict";

  angular.module("MinkFoodiee").factory("Seller", SellerFactory);

  SellerFactory.$inject = ["Session"];

  function SellerFactory(Session) {
    var data = {
      session: {
        id: "mf_seller_seller_id",
        name: "mf_seller_name",
        type: "mf_seller_veg_type",
        cuisineName: "mf_seller_cuisine_name",
        minDeliveryCharges: "mf_seller_min_delivery_charges",
        minPrice: "mf_seller_min_price",
        deliveryTime: "mf_seller_delivery_time",
        deliveryAvailable: "mf_seller_delivery_available",
        orderAvailable: "mf_seller_open_or_closed",
        tableBookOption: "mf_seller_reserve_table",
        vatPercent: "mf_seller_vat_percent",
        servicePercent: "mf_seller_service_percent",
        rate: "mf_seller_rate",
        coupon: "mf_seller_coupon",
        menu: "mf_seller_active_menu",
        SurgeIndication: "mf_seller_surge_indication",
        SurgeMode: "mf_seller_surge_mode",
        SurgePricePercent: "mf_seller_surge_percent",
        Images: "mf_seller_images",
        UserCanAddProduct: "mf_seller_user_can_add",
        attendanceTime: {
          from: "mf_seller_attendance_time_from",
          to: "mf_seller_attendance_time_to"
        }
      }
    };

    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getCuisineName: getCuisineName,
      setCuisineName: setCuisineName,
      removeCuisineName: removeCuisineName,

      getName: getName,
      setName: setName,
      removeName: removeName,

      getType: getType,
      setType: setType,
      removeType: removeType,

      getMinDeliveryCharges: getMinDeliveryCharges,
      setMinDeliveryCharges: setMinDeliveryCharges,
      removeMinDeliveryCharges: removeMinDeliveryCharges,

      getMinPrice: getMinPrice,
      setMinPrice: setMinPrice,
      removeMinPrice: removeMinPrice,

      getDeliveryTime: getDeliveryTime,
      setDeliveryTime: setDeliveryTime,
      removeDeliveryTime: removeDeliveryTime,

      isDeliveryAvailable: isDeliveryAvailable,
      setDeliveryAvailable: setDeliveryAvailable,
      removeDeliveryAvailable: removeDeliveryAvailable,

      getAttendanceTime: getAttendanceTime,
      setAttendanceTime: setAttendanceTime,
      removeAttendanceTime: removeAttendanceTime,
      extractAttendanceTime: extractAttendanceTime,

      getOrderAvailable: getOrderAvailable,
      setOrderAvailable: setOrderAvailable,
      removeOrderAvailable: removeOrderAvailable,

      getTableBookOption: getTableBookOption,
      setTableBookOption: setTableBookOption,
      removeTableBookOption: removeTableBookOption,

      getVatPercent: getVatPercent,
      setVatPercent: setVatPercent,
      removeVatPercent: removeVatPercent,

      getServicePercent: getServicePercent,
      setServicePercent: setServicePercent,
      removeServicePercent: removeServicePercent,

      getRating: getRating,
      setRating: setRating,
      removeRating: removeRating,

      getCouponExist: getCouponExist,
      setCouponExist: setCouponExist,
      removeCouponExist: removeCouponExist,

      getActiveMenuList: getActiveMenuList,
      setActiveMenuList: setActiveMenuList,
      removeActiveMenuList: removeActiveMenuList,

      getSurgeIndication: getSurgeIndication,
      setSurgeIndication: setSurgeIndication,
      removeSurgeIndication: removeSurgeIndication,

      getSurgeMode: getSurgeMode,
      setSurgeMode: setSurgeMode,
      removeSurgeMode: removeSurgeMode,

      getSurgePricePercent: getSurgePricePercent,
      setSurgePricePercent: setSurgePricePercent,
      removeSurgePricePercent: removeSurgePricePercent,

      getImages: getImages,
      setImages: setImages,
      removeImages: removeImages,

      getUserCanAddProduct: getUserCanAddProduct,
      setUserCanAddProduct: setUserCanAddProduct,
      removeUserCanAddProduct: removeUserCanAddProduct,

      clearSession: clearSession
    };

    return service;

    /**
     * @private {member}
     * @function convertAttendanceTime
     * @description  Getting the response of the server with many properties, figuring out which day is today
     * and returning an object with just the beginning and the end values of the today attendance time
     * @param {object} data - Response coming from the server
     * @return {object} - Formatted date with from and to properties
     */
    function convertAttendanceTime(data) {
      // Figuring out which day is today
      var currentDate = new Date();
      var today = currentDate.getDay();

      if (today == 0) return formatFromTo(data.sunTimeFrom, data.sunTimeTo);
      if (today == 1) return formatFromTo(data.monTimeFrom, data.monTimeTo);
      if (today == 2) return formatFromTo(data.tueTimeFrom, data.tueTimeTo);
      if (today == 3) return formatFromTo(data.wedTimeFrom, data.wedTimeTo);
      if (today == 4) return formatFromTo(data.thuTimeFrom, data.thuTimeTo);
      if (today == 5) return formatFromTo(data.friTimeFrom, data.friTimeTo);
      if (today == 6) return formatFromTo(data.satTimeFrom, data.satTimeTo);
    }

    /**
     * @private {member}
     * @function formatFromTo
     * @description Avoiding boilerplate to format the response, putting into a pattern that
     * could be changed easily
     * @param {string} dateFrom
     * @param {string} dateTo
     * @returns {object}
     */
    function formatFromTo(dateFrom, dateTo) {
      return {
        from: dateFrom,
        to: dateTo
      };
    }

    /**
     * Getting the id of the seller, generally when the user is at the restaurant page
     * @function getId
     * @memberof Seller
     * @returns {string}
     */
    function getId() {
      return Session.get(data.session.id);
    }

    /**
     * Setting the seller id, generally when the user is gonna be redirected to
     * the restaurant screen
     * @memberof Seller
     * @function setId
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    }

    /**
     * Removing seller id, this information is dynamic, it's highly recommended to
     * call this method when the information it's not been used anymore
     * @memberof Seller
     * @function removeId
     */
    function removeId() {
      Session.remove(data.session.id);
    }

    /**
     * Getting the cuisine name of the seller, which means the type of the cuisine,
     * the restaurant specialities, like Chinese, South Indian, and so on
     * @memberof Seller
     * @function getCuisineName
     * @returns {string}
     */
    function getCuisineName() {
      return Session.get(data.session.cuisineName);
    }

    /**
     * Setting the cuisine name, generally when the user is gonna be redirected to
     * the restaurant screen
     * @memberof Seller
     * @function setCuisineName
     * @returns {string}
     */
    function setCuisineName(name) {
      Session.save(data.session.cuisineName, name);
    }

    /**
     * Removing the cuisine name, this value is dynicamic, so this method should be
     * called whenever the user has a potential change of changing the current restaurant,
     * going to a restaurant list or clicking on In House modal for example
     * @memberof Seller
     * @function removeCuisineName
     */
    function removeCuisineName() {
      Session.remove(data.session.cuisineName);
    }

    /**
     * Setting if this restaurant is currently accepting delivery
     * @memberof Seller
     * @function setDeliveryAvailable
     * @param {boolean} available
     */
    function setDeliveryAvailable(available) {
      Session.save(data.session.deliveryAvailable, available);
    }

    /**
     * Checking in the localStorage if the restaurant is available for delivery
     * @memberof Seller
     * @function isDeliveryAvailable
     * @returns {string}
     */
    function isDeliveryAvailable() {
      return Session.get(data.session.deliveryAvailable);
    }

    /**
     * Removing the key that tells if a restaurant is available for delivery
     * from the localStorage, this is recommended when the user is gonna access
     * a screen that potentially will make him go to another restaurant
     * @memberof Seller
     * @function removeDeliveryAvailable
     */
    function removeDeliveryAvailable() {
      Session.remove(data.session.deliveryAvailable);
    }

    /**
     * Getting the attendance time of the restaurant, this will be used to
     * decide if the restaurant can or cannot receive order at that day and time
     * @memberof Seller
     * @function getAttendanceTime
     * @returns {string}
     */
    function getAttendanceTime() {
      return {
        from: Session.get(data.session.attendanceTime.from),
        to: Session.get(data.session.attendanceTime.to)
      };
    }

    /**
     * Setting the time the restaurant is opened, this will be considered to
     * accept or not orders at that time
     * @memberof Seller
     * @function setAttendanceTime
     * @param {object} time
     */
    function setAttendanceTime(time) {
      Session.save(data.session.attendanceTime.from, time.from);
      Session.save(data.session.attendanceTime.to, time.to);
    }

    /**
     * Removing the attendance time of a restaurant when the user is going out
     * of the checkout flow
     * @memberof Seller
     * @function removeAttendanceTime
     */
    function removeAttendanceTime() {
      Session.remove(data.session.attendanceTime.from);
      Session.remove(data.session.attendanceTime.to);
    }

    /**
     * @memberof Seller
     * @function extractAttendanceTime
     * @param {object} data - Response coming from the server with the date unformatted
     * @returns {object} - Today attendance period into an object with "from" and "to" properties
     */
    function extractAttendanceTime(data) {
      // Getting the response that has all days besides other stuffs and extracting the today's data
      var date = convertAttendanceTime(data);
      return date;
    }

    function setOrderAvailable(available) {
      Session.save(data.session.orderAvailable, available);
    }

    function getOrderAvailable() {
      return Session.get(data.session.orderAvailable);
    }

    function removeOrderAvailable() {
      Session.remove(data.session.orderAvailable);
    }

    function setMinPrice(price) {
      Session.save(data.session.minPrice, price);
    }

    function getMinPrice() {
      return Session.get(data.session.minPrice);
    }

    function removeMinPrice() {
      Session.remove(data.session.minPrice);
    }

    function setMinDeliveryCharges(charge) {
      Session.save(data.session.minDeliveryCharges, charge);
    }

    function getMinDeliveryCharges() {
      return Session.get(data.session.minDeliveryCharges);
    }

    function removeMinDeliveryCharges() {
      Session.remove(data.session.minDeliveryCharges);
    }

    function setDeliveryTime(time) {
      Session.save(data.session.deliveryTime, time);
    }

    function getDeliveryTime() {
      return Session.get(data.session.deliveryTime);
    }

    function removeDeliveryTime() {
      Session.remove(data.session.deliveryTime);
    }

    function setName(name) {
      Session.save(data.session.name, name);
    }

    function getName() {
      return Session.get(data.session.name);
    }

    function removeName() {
      Session.remove(data.session.name);
    }

    function setType(type) {
      Session.save(data.session.type, type);
    }

    function getType() {
      return Session.get(data.session.type);
    }

    function removeType() {
      Session.remove(data.session.type);
    }

    function setTableBookOption(option) {
      Session.save(data.session.tableBookOption, option);
    }

    function getTableBookOption() {
      return Session.get(data.session.tableBookOption);
    }

    function removeTableBookOption() {
      Session.remove(data.session.tableBookOption);
    }

    function setTableBookOption(option) {
      Session.save(data.session.tableBookOption, option);
    }

    function getTableBookOption() {
      return Session.get(data.session.tableBookOption);
    }

    function removeTableBookOption() {
      Session.remove(data.session.tableBookOption);
    }

    function setRating(rate) {
      Session.save(data.session.rate, rate);
    }

    function getRating() {
      return Session.get(data.session.rate);
    }

    function removeRating() {
      Session.remove(data.session.rate);
    }

    function setServicePercent(taxPercent) {
      Session.save(data.session.servicePercent, taxPercent);
    }

    function getServicePercent() {
      return Session.get(data.session.servicePercent);
    }

    function removeServicePercent() {
      Session.remove(data.session.servicePercent);
    }

    function setVatPercent(taxPercent) {
      Session.save(data.session.vatPercent, taxPercent);
    }

    function getVatPercent() {
      return Session.get(data.session.vatPercent);
    }

    function removeVatPercent() {
      Session.remove(data.session.vatPercent);
    }

    function setCouponExist(coupon) {
      Session.save(data.session.coupon, coupon);
    }

    function getCouponExist() {
      return Session.get(data.session.coupon);
    }

    function removeCouponExist() {
      Session.remove(data.session.coupon);
    }

    function setActiveMenuList(menu) {
      Session.save(data.session.menu, menu);
    }

    function getActiveMenuList() {
      return Session.get(data.session.menu);
    }

    function removeActiveMenuList() {
      Session.remove(data.session.menu);
    }

    function setSurgeIndication(value) {
      Session.save(data.session.surgeIndication, value);
    }

    function getSurgeIndication() {
      return Session.get(data.session.surgeIndication);
    }

    function removeSurgeIndication() {
      Session.remove(data.session.surgeIndication);
    }

    function setSurgeMode(value) {
      Session.save(data.session.SurgeMode, value);
    }

    function getSurgeMode() {
      return Session.get(data.session.SurgeMode);
    }

    function removeSurgeMode() {
      Session.remove(data.session.SurgeMode);
    }

    function setSurgePricePercent(value) {
      Session.save(data.session.SurgePricePercent, value);
    }

    function getSurgePricePercent() {
      return Session.get(data.session.SurgePricePercent);
    }

    function removeSurgePricePercent() {
      Session.remove(data.session.SurgePricePercent);
    }

    function setImages(value) {
      Session.save(data.session.Images, value);
    }

    function getImages() {
      return Session.get(data.session.Images);
    }

    function removeImages() {
      Session.remove(data.session.Images);
    }

    function setUserCanAddProduct(value) {
      Session.save(data.session.UserCanAddProduct, value);
    }

    function getUserCanAddProduct() {
      return Session.get(data.session.UserCanAddProduct);
    }

    function removeUserCanAddProduct() {
      Session.remove(data.session.UserCanAddProduct);
    }

    /**
     * Remove all data related to Seller in the current session
     * @function clearSession
     * @memberof Seller
     */
    function clearSession() {
      removeDeliveryAvailable();
      removeAttendanceTime();
      removeOrderAvailable();
      removeMinDeliveryCharges();
      removeName();
      removeType();
      removeMinPrice();
      removeDeliveryTime();
      removeTableBookOption();
      removeRating();
      removeServicePercent();
      removeVatPercent();
      removeActiveMenuList();
      removeCouponExist();
      removeImages();
      removeId();
      removeSurgeIndication();
      removeSurgeMode();
      removeSurgePricePercent();
      removeUserCanAddProduct();
    }
  }
})();
