(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('Geolocation', service)

  service.$inject = ['$ionicPopup'];

  function service($ionicPopup) {
    this.validateCallbackFunction = function (callback) {
      if (typeof callback !== "function") {
        throw new Error("You must pass a function as a parameter");
      }
    }

    this.validateGpsActivation = function (callback) {

      function success_getLocation(enabled) {
        if (!enabled) {

          var confirmPopup = $ionicPopup.confirm({
            title: 'Location detect',
            template: 'We need to access your location to show near restaurants',
            cancelText: 'Cancel',
            cancelType: 'button-default',
            okText: 'OK',
            okType: 'button-balanced'
          });

          confirmPopup.then(function (res) {
            if (res) {
              cordova.plugins.diagnostic.switchToLocationSettings();
            } else {
              console.log('You are not sure');
            }
          });

        } else {
          //alert("Location is already ENABLED");
          // //getting stored values of lang and lat from localStorage
          // var storagelatitude = localStorage.getItem("user_first_latitude");
          // var storagelongitude = localStorage.getItem("user_first_longitude");
          // callback(storagelatitude, storagelongitude);
        }
      }

      function error_getLocation(error) {
        console.error("The following error occurred while trying to open Location: " + error);
      }

      cordova.plugins.diagnostic.isLocationEnabled(success_getLocation, error_getLocation);

    }

    this.getGeolocation = function (callback) {
      function successCallback(position) {
        //alert("Position Object for First Time After success oopening location: ", position);
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        localStorage.setItem("user_first_latitude", latitude);
        localStorage.setItem("user_first_longitude", longitude);
        //alert("latitude Value stored in local: " + latitude);
        //alert("longitude Value stored in local: " + longitude);
        callback(latitude, longitude);
      }

      function errorCallback(error) {
        alert(JSON.stringify(data));
        /*switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("The user has not allowed access to his position");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("The user's location could not be determined");
                break;
            case error.TIMEOUT:
                alert("The service did not respond in time");
                break;
        }*/
        return;
      }

      var options = {
        enableHighAccuracy: true
      };

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback, options);
      } else {
        alert("Something went wrong @getGeolocation   navigator.geolocation.getCurrentPosition ");
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .factory('$Geolocation', factory)

  factory.$inject = ['Geolocation'];

  function factory(Geolocation) {

    return {
      requestGeolocation: requestGetGeolocation
    }

    function requestGetGeolocation(callback) {
      Geolocation.validateGpsActivation();
      Geolocation.validateCallbackFunction(callback);
      Geolocation.getGeolocation(callback);
    }

  }
})();
