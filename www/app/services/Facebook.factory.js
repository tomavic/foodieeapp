(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .service('Facebook', service)

  service.$inject = ['$q'];

  function service($q) {

    // return 
    this.getFacebookProfileInfo = getFacebookProfileInfo;
    this.login = login;
    this.logout = logout;


    function getFacebookProfileInfo(authResponse) {
      var info = $q.defer();
      facebookConnectPlugin.api('/me?fields=email,name,picture,first_name,last_name&access_token=' + authResponse.accessToken, null,
        function (response) {
          console.log(response);
          info.resolve(response);
        },
        function (response) {
          console.log(response);
          info.reject(response);
        }
      );
      return info.promise;
    };


    function login(successCallback, errorCallback) {
      facebookConnectPlugin.login(["email", "public_profile"], successCallback, errorCallback);
    }


    function logout(successCallback, errorCallback) {
      facebookConnectPlugin.logout(successCallback, errorCallback);
    }

  }
})();
