(function() {
  "use strict";

  angular.module("MinkFoodiee").factory("$Session", factory);

  factory.$inject = ["City", "Area", "User", "Order", "Seller", "SellerInhouse", "SellerBar"];

  function factory(City, Area, User, Order, Seller, SellerInhouse, SellerBar) {

    return {
      user: User,
      order: Order,
      city: City,
      area: Area,
      seller: Seller,
      inHouse: SellerInhouse,
      inHouseBar: SellerBar,
      kill: kill
    };

    function kill() {
        // user session
        User.clearSession();
  
        // city location session
        Area.clearSession();
  
        // area location session
        City.clearSession();
  
        // seller session
        Seller.clearSession();
  
        // order session
        Order.clearSession();
  
        // inhouse session
        SellerInhouse.clearSession();
    };
  }
})();
