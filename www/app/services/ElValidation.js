(function () {
  'use strict';

  angular
    .module('ElValidation', [])
    .config(['$validatorProvider', function ($validatorProvider) {

      $validatorProvider.setDefaults({
        debug: true,
        errorElement: 'span',
        errorClass: 'alert'
      });

      $.validator.addMethod('integer', function (value, element) {
        return this.optional(element) || /^\d+$/.test(value);
      });

      $.validator.addMethod('string', function (value, element) {
        return this.optional(element) || /^[a-zA-Z_ ]*$/g.test(value);
      });

      $validatorProvider.addMethod("validEmail", function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $validatorProvider.addMethod("validMobileNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });

      $validatorProvider.addMethod("indianMobNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });
      $.validator.addMethod('strongPassword', function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9]{8,21}$/.test(value);
      });
      $.validator.addMethod('emailOrMobile', function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}|^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $.validator.addMethod('selectRequired', function (value) {
        return value != '0';
      });

      $.validator.addMethod('queuePeople', function (value, element) {
        return (value > 0 && value <= 30 && /^\d+$/.test(value));
      });

      $.validator.addMethod('greaterThanToday', function (value, element) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        var date = new Date(value.split('-').join('/'));
        return (date >= today);
      });
    }]);

})();
