(function() {
    "use strict";
  
    angular.module("MinkFoodiee").factory("City", cityFactory);
  
    cityFactory.$inject = ["Session"];
  
    function cityFactory(Session) {
      /**
       * @private {data}
       * @member {cityFactory}
       */
      var data = {
        session: {
          id: "mf_location_city_id",
          name: "mf_location_city_name"
        }
      };
  
      return {
        getId: getId,
        setId: setId,
        removeId: removeId,
        getName: getName,
        setName: setName,
        removeName: removeName,
        clearSession: clearSession
      };
  
      function getId() {
        return Session.get(data.session.id);
      }
  
      function setId(id) {
        Session.save(data.session.id, id);
      }
  
      function removeId() {
        Session.remove(data.session.id);
      }
  
      function getName() {
        return Session.get(data.session.name);
      }
  
      function setName(name) {
        Session.save(data.session.name, name);
      }
  
      function removeName() {
        Session.remove(data.session.name);
      }
  
      /**
       * Remove all data related to city location in the current session
       * @function clearSession
       * @memberof City
       */
      function clearSession() {
        removeId();
        removeName();
      }
    }
  })();