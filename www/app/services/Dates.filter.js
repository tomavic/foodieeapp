(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .filter('Dates', filter)

  function filter() {

    var errorMessages = {
      invalidDate: "An attempt of defining a new date failed due a wrong date string being passed as a parameter",
      invalidDay: "It wasn't possible to get the day",
      invalidMonth: "It wasn't possible to get the month",
      invalidYear: "It wasn't possible to get the year"
    };

    return dateFilter;

     function dateFilter(input, day) {

      /**
       * Declaring the months as a string
       * @param {*} month 
       */
      function convertMonthAtString(month) {
        var months = [
          "JAN", // 0
          "FEB", // 1
          "MAR", // 2
          "APR", // 3
          "MAY", // 4
          "JUN", // 5
          "JULY", // 6
          "AUG", // 7
          "SEP", // 8
          "OCT", // 9
          "NOV", // 10
          "DEC" // 11
        ];
        return months[month];
      }

      function buildDate(date) {
        var d = new Date(date);
        if (d.toString().toLowerCase() === "invalid date") {
          var correctDate = moment(date, 'DD.MM.YYYY').format();
          // throw new Error(errorMessages.invalidDate);
          return new Date(correctDate);
        }
        return d;
      };

      function getDay(date) {
        var day = date.getDate();
        if (!$.isNumeric(day)) throw new Error(errorMessages.invalidDay);
        return day;
      };

      function getMonth(date) {
        var month = date.getMonth();
        if (!$.isNumeric(month)) throw new Error(errorMessages.invalidMonth);
        return month;
      };

      function getFullYear(date) {
        var year = date.getFullYear();
        if (!$.isNumeric(year)) throw new Error(errorMessages.invalidYear);
        return year;
      };

      function joinMonthYear(month, year) {
        return convertMonthAtString(month) + ", " + year;
      };

      function getTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        return {
          hours: hours,
          minutes: minutes
        }
      };

      function convertHours(hours) {
        // AM
        if (hours < 12) {
          // Adjust if the hour is equal midnight
          hours = (hours === 0) ? 12 : hours;
          // Just returns the number + am
          return {
            hours: hours,
            period: "am"
          };
        }
        // PM
        else {
          // Adjusting if the hours is equal noon
          hours = (hours === 12) ? hours : hours - 12;
          // Returns the number in a 12 hours format + pm
          return {
            hours: hours,
            period: "pm"
          };
        }
      }

      function addLeftZero(number) {
        // Slide -2 will return the last two digits of the number
        return ("0" + number).slice(-2);
      }

      function stringfyTime(time) {
        // Converting hours in a 12 hours format
        var hours = convertHours(time.hours);
        // Returns the result as a string
        return addLeftZero(hours.hours) + ":" + addLeftZero(time.minutes) + hours.period;
      };

      function extractDay(stringDate) {
        var date = buildDate(stringDate);
        var day = getDay(date);
        return day;
      }

      function extractMonthYear(data) {
        var date = buildDate(data);
        var month = getMonth(date);
        var year = getFullYear(date);
        var monthYear = joinMonthYear(month, year);
        return monthYear;
      }

      function extractTime(data) {
        var date = buildDate(data);
        var time = getTime(date);
        var stringTime = stringfyTime(time);
        return stringTime;
      }

      /**
       * @return {value} depending on filter {day} variable
       ***********************************/
      switch (day) {
        case 'mmyy':
          var outputmy = extractMonthYear(input);
          return outputmy;
        case 'd':
          var outputday = extractDay(input);
          return outputday;
        case 'time':
          var outputtime = extractTime(input);
          return outputtime;
        default:
          console.log('Sorry, we are out of ' + day + '.');
      }

    }
  }

}());