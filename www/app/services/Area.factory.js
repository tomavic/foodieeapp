(function() {
    "use strict";
  
    angular.module("MinkFoodiee").factory("Area", AreaFactory);
  
    AreaFactory.$inject = ["Session"];
  
    function AreaFactory(Session) {
      var data = {
        session: {
          id: "mf_location_city_area_id",
          name: "mf_location_city_area_name"
        }
      };
  
      var service = {
        getId: getId,
        setId: setId,
        removeId: removeId,
        getName: getName,
        setName: setName,
        removeName: removeName,
        clearSession: clearSession
      };
  
      return service;
  
      function getId() {
        return Session.get(data.session.id);
      };
  
      function setId(id) {
        Session.save(data.session.id, id);
      };
  
      function removeId() {
        Session.remove(data.session.id);
      };
  
      function getName() {
        return Session.get(data.session.name);
      };
  
      function setName(name) {
        Session.save(data.session.name, name);
      };
  
      function removeName() {
        Session.remove(data.session.name);
      };
  
      /**
       * Remove all data related to city location in the current session
       * @function clearSession
       * @memberof City
       */
      function clearSession() {
        this.removeId();
        this.removeName();
      };
    }
  })();