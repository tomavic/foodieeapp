(function () {
  "use strict";

  angular.module("MinkFoodiee").factory("Order", OrderFactory);

  OrderFactory.$inject = ["Session"];

  function OrderFactory(Session) {

    var data = {
      session: {
        id: "mf_order_id",
        reviewOrderId: "mf_order_review_id",
        menuItem: "mf_order_menu_item_for_data",
        totalAmount: "mf_total_amount",
        minCharge: "mf_min_charge",
        type: "mf_order_type",
        cart: "mf_order_cart"
      }
    };

    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getReviewOrderId: getReviewOrderId,
      setReviewOrderId: setReviewOrderId,
      removeReviewOrderId: removeReviewOrderId,

      getMinDeliveryCharges: getMinDeliveryCharges,
      setMinDeliveryCharges: setMinDeliveryCharges,
      removeMinDeliveryCharges: removeMinDeliveryCharges,

      getTotalAmount: getTotalAmount,
      setTotalAmount: setTotalAmount,
      removeTotalAmount: removeTotalAmount,

      getType: getType,
      setType: setType,
      removeType: removeType,

      getMenuItemType: getMenuItemType,
      setMenuItemType: setMenuItemType,
      removeMenuItemType: removeMenuItemType,

      getCartObject: getCartObject,
      setCartObject: setCartObject,
      removeCartObject: removeCartObject,

      clearSession: clearSession
    };

    return service;

    /**
     * Responding the id of the order, this value is filled when the user adds
     * the first item to the cart
     * @function getId
     * @memberof OrderEntity
     * @returns {integer}
     */
    function getId() {
      return Session.get(data.session.id);
    };

    /**
     * Setting the id of the order in the localStorage, this information should be
     * set when the user adds the first item to the cart
     * @function setId
     * @memberof OrderEntity
     * @param {integer} id - Id coming from the backend
     */
    function setId(id) {
      Session.save(data.session.id, id);
    };

    /**
     * Removing order id from the localStorage, generally because the user
     * got to the end of the Checkout flow, this information should be deleted
     * to avoid inconsistency in the future
     * @function removeId
     * @memberof OrderEntity
     */
    function removeId() {
      Session.remove(data.session.id);
    };

    /**
     * Responding the id of the order, this value is filled when the user adds
     * the first item to the cart
     * @function getReviewOrderId
     * @memberof Order
     * @returns {integer} reviewOrderId
     */
    function getReviewOrderId() {
      return Session.get(data.session.reviewOrderId);
    };

    /**
     * Setting the id of the order in the localStorage, this information should be
     * set when the user adds the first item to the cart
     * @function setReviewOrderId
     * @memberof Order
     * @param {integer} id - Id coming from the backend
     */
    function setReviewOrderId(id) {
      Session.save(data.session.reviewOrderId, id);
    };

    /**
     * Removing order id from the localStorage, generally because the user
     * got to the end of the Checkout flow, this information should be deleted
     * to avoid inconsistency in the future
     * @function removeReviewOrderId
     * @memberof OrderEntity
     */
    function removeReviewOrderId() {
      Session.remove(data.session.reviewOrderId);
    };

    /**
     * Getting the minimum delivery charge from the localStorage before proceed to
     * Checkout Order screen. This value comes originally from the database and
     * change for each restaurant.
     * @function getMinDeliveryCharges
     * @memberof OrderEntity
     * @returns {string}
     */
    function getMinDeliveryCharges() {
      return Session.get(data.session.minCharge);
    };

    /**
     * Setting the Minimum Delivery Charges on the localStorage, this value
     * comes from backend and changes according to the restaurant.
     * @function setMinDeliveryCharges
     * @param {integer} minCharge
     */
    function setMinDeliveryCharges(minCharge) {
      Session.save(data.session.minCharge, minCharge);
    };

    /**
     * Removing the Minimum Delivery Charges from the localStorage, due avoiding
     * inconsistencies or locked values, this value is dynamic and change for
     * each restaurant, it's highly recommended to remove if it's not in use anymore
     * @function removeMinDeliveryCharges
     * @memberof OrderEntity
     */
    function removeMinDeliveryCharges() {
      Session.remove(data.session.minCharge);
    };

    /**
     * Getting the sum of all items the user added to the cart.
     * @function getTotalAmount
     * @returns {float}
     */
    function getTotalAmount() {
      return Session.get(data.session.totalAmount);
    };

    /**
     * Setting the sum of the price of all items the user has on the cart
     * @function setTotalAmount
     * @memberof OrderEntity
     * @param {totalAmount}
     */
    function setTotalAmount(totalAmount) {
      Session.save(data.session.totalAmount, totalAmount);
    };

    /**
     * Removing the total amount from the localStorage, this value is associated
     * to a dynamic value representing the sum of the items in a certain transaction,
     * it's highly recommended to delete this value if it's not been used anymore
     * @function removeTotalAmount
     * @memberof OrderEntity
     */
    function removeTotalAmount() {
      Session.remove(data.session.totalAmount);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getType() {
      return Session.get(data.session.type);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setType(type) {
      Session.save(data.session.type, type);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeType() {
      Session.remove(data.session.type);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getMenuItemType() {
      return Session.get(data.session.menuItem);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setMenuItemType(type) {
      Session.save(data.session.menuItem, type);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeMenuItemType() {
      Session.remove(data.session.menuItem);
    };

    /**
     * Getting the type of the order
     * @function getType
     * @memberof OrderEntity
     * @returns {string}
     */
    function getCartObject() {
      return Session.get(data.session.cart);
    };

    /**
     * Setting the type of the order
     * @function setType
     * @memberof OrderEntity
     * @param {integer} type
     */
    function setCartObject(cartString) {
      Session.save(data.session.cart, cartString);
    };

    /**
     * Removing the order type from the localStorage
     * @function removeType
     * @memberof OrderEntity
     */
    function removeCartObject() {
      Session.remove(data.session.cart);
    };

    /**
     * Remove all data related to order in the current session
     * @function clearSession
     * @memberof Order
     */
    function clearSession() {
      this.removeMenuItemType();
      this.removeMinDeliveryCharges();
      this.removeId();
      this.removeReviewOrderId();
      this.removeType();
      this.removeTotalAmount();
    };



  }
})();
