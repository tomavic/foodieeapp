(function () {
  "use strict";

  angular.module("MinkFoodiee").factory("User", UserFactory);

  UserFactory.$inject = ["Session"];

  function UserFactory(Session) {

    var data = {
      session: {
        id: "mf_user_id",
        accessToken: "mf_fb_user_access_token",
        name: "mf_user_name",
        email: "mf_user_email",
        address: "mf_user_address",
        address2: "mf_user_address_2",
        city: "mf_user_city",
        area: "mf_user_area",
        mobile: "mf_user_mobile",
        image: "mf_user_image",
        pin: "mf_user_pin",
        deviceToken: "mf_user_device_token",
        canOrderCOD: "mf_user_can_order"
      }
    };

    var service = {
      getAccessToken: getAccessToken,
      setAccessToken: setAccessToken,
      removeAccessToken: removeAccessToken,

      getId: getId,
      setId: setId,
      removeId: removeId,

      getName: getName,
      setName: setName,
      removeName: removeName,

      getEmail: getEmail,
      setEmail: setEmail,
      removeEmail: removeEmail,

      getAddress: getAddress,
      setAddress: setAddress,
      removeAddress: removeAddress,

      getAddress2: getAddress2,
      setAddress2: setAddress2,
      removeAddress2: removeAddress2,

      getCity: getCity,
      setCity: setCity,
      removeCity: removeCity,

      getArea: getArea,
      setArea: setArea,
      removeArea: removeArea,

      getMobile: getMobile,
      setMobile: setMobile,
      removeMobile: removeMobile,

      getPin: getPin,
      setPin: setPin,
      removePin: removePin,

      getImage: getImage,
      setImage: setImage,
      removeImage: removeImage,

      getUserCanCod: getUserCanCod,
      setUserCanCod: setUserCanCod,
      removeUserCanCod: removeUserCanCod,

      getDeviceToken: getDeviceToken,
      setDeviceToken: setDeviceToken,
      removeDeviceToken: removeDeviceToken,

      isLoggedIn: isLoggedIn,
      isLoggedInFromFacebook: isLoggedInFromFacebook,
      
      setUserInfo: setUserInfo,
      clearSession: clearSession,
    };

    return service;

    // Facebook Part

    /**
     * Getting the Id of the logged user from the localStorage
     * @function getAccessToken
     * @memberof User
     * @returns {accessToken}
     */
    function getAccessToken() {
      return Session.get(data.session.accessToken);
    };

    /**
     * Set userId in the localStorage, generally after the login
     * @function setAccessToken
     * @memberof User
     * @param {integer} token
     */
    function setAccessToken(token) {
      Session.save(data.session.accessToken, token);
    };

    /**
     * Removing userId from the localStorage, generally when the user logout
     * @function removeAccessToken
     * @memberof User
     */
    function removeAccessToken() {
      Session.remove(data.session.accessToken);
    };

    /**
     * Getting the Id of the logged user from the localStorage
     * @function getId
     * @memberof User
     * @returns {string}
     */
    function getId() {
      return Session.get(data.session.id);
    };

    /**
     * Set userId in the localStorage, generally after the login
     * @function setId
     * @memberof User
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    };

    /**
     * Removing userId from the localStorage, generally when the user logout
     * @function removeId
     * @memberof User
     */
    function removeId() {
      Session.remove(data.session.id);
    };

    /**
     * Getting name of the logged user from the localStorage
     * @function getName
     * @memberof User
     * @returns {string}
     */
    function getName() {
      return Session.get(data.session.name);
    };

    /**
     * Setting the user name in the localStorage, generally after login
     * @function
     * @memberof User
     * @param {string} name
     */
    function setName(name) {
      Session.save(data.session.name, name);
    };

    /**
     * Removing the userName from the localStorage, generally after logout
     * @function
     * @memberof User
     */
    function removeName() {
      Session.remove(data.session.name);
    };

    /**
     * Getting the userEmail from the localStorage
     * @function getEmail
     * @memberof User
     * @returns {string}
     */
    function getEmail() {
      return Session.get(data.session.email);
    };

    /**
     * Setting email of the user in the localStorage, generally after login
     * @function setEmail
     * @memberof User
     * @param {string} email
     */
    function setEmail(email) {
      Session.save(data.session.email, email);
    };

    /**
     * Removing email from the localStorage, generally after logout
     * @function removeEmail
     * @memberof User
     */
    function removeEmail() {
      Session.remove(data.session.email);
    };

    /**
     * Getting the main user address from the localStorage
     * @function getAddress
     * @memberof User
     * @returns {string}
     */
    function getAddress() {
      return Session.get(data.session.address);
    };

    /**
     * Setting the user address in the localStorage, generally after login
     * @function setAddress
     * @memberof User
     * @param {string} address
     */
    function setAddress(address) {
      Session.save(data.session.address, address);
    };

    /**
     * Remove main address from the localStorage, generally after logout
     * @function removeAddress
     * @memberof User
     */
    function removeAddress() {
      Session.remove(data.session.address);
    };

    /**
     * Getting the second user address from the localStorage
     * @function getAddress2
     * @memberof User
     * @returns {string}
     */
    function getAddress2() {
      return Session.get(data.session.address2);
    };

    /**
     * Setting the second user address in the localStorage, generally after login
     * @function setAddress2
     * @memberof User
     * @param {string} address
     */
    function setAddress2(address) {
      Session.save(data.session.address2, address);
    };

    /**
     * Remove second address from the localStorage, generally after logout
     * @function removeAddress2
     * @memberof User
     */
    function removeAddress2() {
      Session.remove(data.session.address2);
    };

    /**
     * Getting the user city from the locaStorage
     * @function getCity
     * @memberof User
     * @returns {string}
     */
    function getCity() {
      return Session.get(data.session.city);
    };

    /**
     * Setting the user city in the localStorage, generally after login
     * @function setCity
     * @memberof User
     * @param {string} city
     */
    function setCity(city) {
      Session.save(data.session.city, city);
    };

    /**
     * Removing user city from the localStorage, generally after logout
     * @function removeCity
     * @memberof User
     */
    function removeCity() {
      Session.remove(data.session.city);
    };

    /**
     * Getting user area from the localStorage
     * @function getArea
     * @memberof User
     * @returns {string}
     */
    function getArea() {
      return Session.get(data.session.area);
    };

    /**
     * Setting user area in the localStorage, generally after login
     * @function setArea
     * @memberof User
     * @param {string} area
     */
    function setArea(area) {
      Session.save(data.session.area, area);
    };

    /**
     * Removing user area from the localStorage, generally after logout
     * @function removeArea
     * @memberof User
     */
    function removeArea() {
      Session.remove(data.session.area);
    };

    /**
     * Getting the user mobile number from the localStorage
     * @function getMobile
     * @memberof User
     * @returns {string}
     */
    function getMobile() {
      return Session.get(data.session.mobile);
    };

    /**
     * Set the user mobile number, generally after login
     * @function setMobile
     * @memberof User
     * @param {string} mobile
     */
    function setMobile(mobile) {
      Session.save(data.session.mobile, mobile);
    };

    /**
     * Removing user mobile number from the localStorage, generally after logout
     * @function removeMobile
     * @memberof User
     */
    function removeMobile() {
      Session.remove(data.session.mobile);
    };

    /**
     * Getting path to the user image from the localStorage
     * @function getImage
     * @memberof User
     * @returns {string}
     */
    function getImage() {
      return Session.get(data.session.image);
    };

    /**
     * Set the path of the user image in the localStorage, after login or edit picture
     * @function setImage
     * @memberof User
     * @param {string} image
     */
    function setImage(image) {
      Session.save(data.session.image, image);
    };

    /**
     * Removing path of the user image from the localStorage, generally after logout
     * @function removeImage
     * @memberof User
     */
    function removeImage() {
      Session.remove(data.session.image);
    };

    /**
     * Getting pin code of the user from the localStorage
     * @function getPin
     * @memberof User
     * @returns {string}
     */
    function getPin() {
      return Session.get(data.session.pin);
    };

    /**
     * Setting the user pin code in the localStorage, generally after login
     * @function setPin
     * @memberof User
     * @param {string} pin
     */
    function setPin(pin) {
      Session.save(data.session.pin, pin);
    };

    /**
     * Removing the user pin code from the localStorage, generally after logout
     * @function removePin
     * @memberof User
     */
    function removePin() {
      Session.remove(data.session.pin);
    };

    

        /**
     * Getting deviceToken of the user from the localStorage
     * @function getDeviceToken
     * @memberof User
     * @returns {string}
     */
    function getDeviceToken() {
      return Session.get(data.session.deviceToken);
    };

    /**
     * Setting the user getDeviceToken in the localStorage, generally after login
     * @function setDeviceToken
     * @memberof User
     * @param {string} deviceToken
     */
    function setDeviceToken(deviceToken) {
      Session.save(data.session.deviceToken, deviceToken);
    };

    /**
     * Removing the user deviceToken from the localStorage, generally after disbaling push notification
     * @function deviceToken
     * @memberof User
     */
    function removeDeviceToken() {
      Session.remove(data.session.deviceToken);
    };

    /**
     * Getting pin code of the user from the localStorage
     * @function getUserCanCod
     * @memberof User
     * @returns {string}
     */
    function getUserCanCod() {
      return Session.get(data.session.canOrderCOD);
    }

    /**
     * Setting the user pin code in the localStorage, generally after login
     * @function setUserCanCod
     * @memberof User
     * @param {boolean} canCod
     */
    function setUserCanCod(canCod) {
      Session.save(data.session.canOrderCOD, canCod);
    }

    /**
     * Removing the user pin code from the localStorage, generally after logout
     * @function removeUserCanCod
     * @memberof User
     */
    function removeUserCanCod() {
      Session.remove(data.session.canOrderCOD);
    }

    /**
     * @function isLoggedIn
     * @memberof User
     * @return {boolean}
     */
    function isLoggedIn() {
      return getId() ? true : false;
    };

    /**
     * @function isLoggedInFromFacebook
     * @memberof User
     * @return {boolean}
     */
    function isLoggedInFromFacebook() {
      return getAccessToken() ? true : false;
    };

    /**
     * Setting a bunch of common user configuration in just one shot
     * This function receives a parameter with a key, value structure that
     * allows that interaction with multiple user session values at once
     * @function setUserInfo
     * @memberof User
     * @param {array} list
     * @example
     * $session.user.setUserInfo([
     *   { key: 'name', value: 'John' },
     *   { key: 'email', value: 'example@email.com' }
     * ]);
     */
    function setUserInfo(list) {
      Object.keys(list).forEach(function (key) {
        var value = list[key];
        Session.save(data.session[key], value);
      });
    };

    /**
     * Remove all data related to user in the current session
     * @function clearSession
     * @memberof User
     */
    function clearSession() {
      
      removeId();
      removeName();
      removeEmail();
      removeMobile();
      removeAddress();
      removeAddress2();
      removeImage();
      removeCity();
      removeArea();
      removeUserCanCod();
      removePin();
      removeDeviceToken();

      //user Facebook session
      removeAccessToken();
    }


  }


})();
