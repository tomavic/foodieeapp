(function() {
  "use strict";

  angular
    .module("MinkFoodiee")
    .factory("SellerInhouse", SellerInhouseFactory);

  SellerInhouseFactory.$inject = ["Session"];

  function SellerInhouseFactory(Session) {
    var data = {
      session: {
        id: "mf_seller_inhouse_id",
        tableNumber: "mf_seller_inhouse_table_number",
        sellerId: "mf_seller_inhouse_rest_id",
        sellerName: "mf_seller_inhouse_name",
        sellerMinCharge: "mf_seller_inhouse_min_charge",
        sellerMinDiscount: "mf_seller_inhouse_min_discount",
        sellerDiscountPercent: "mf_seller_inhouse_discount_percent",
        sellerDiscountAmount: "mf_seller_inhouse_discount_amount",
        orderTotalAmount: "mf_seller_inhouse_total_amount"
      }
    };
    var service = {
      getId: getId,
      setId: setId,
      removeId: removeId,

      getTableNumber: getTableNumber,
      setTableNumber: setTableNumber,
      removeTableNumber: removeTableNumber,

      getSellerId: getSellerId,
      setSellerId: setSellerId,
      removeSellerId: removeSellerId,

      getSellerName: getSellerName,
      setSellerName: setSellerName,
      removeSellerName: removeSellerName,

      getSellerDiscountAmount: getSellerDiscountAmount,
      setSellerDiscountAmount: setSellerDiscountAmount,
      removeSellerDiscountAmount: removeSellerDiscountAmount,

      getSellerDiscountPercent: getSellerDiscountPercent,
      setSellerDiscountPercent: setSellerDiscountPercent,
      removeSellerDiscountPercent: removeSellerDiscountPercent,

      getSellerMinDiscount: getSellerMinDiscount,
      setSellerMinDiscount: setSellerMinDiscount,
      removeSellerMinDiscount: removeSellerMinDiscount,

      getSellerMinCharge: getSellerMinCharge,
      setSellerMinCharge: setSellerMinCharge,
      removeSellerMinCharge: removeSellerMinCharge,

      getOrderTotalAmount: getOrderTotalAmount,
      setOrderTotalAmount: setOrderTotalAmount,
      removeOrderTotalAmount: removeOrderTotalAmount,

      clearSession: clearSession
    };

    return service;

    /**
     * Getting the InHouse id, generally to identify the transaction and
     * search data from the database, such as items on the cart.
     * @function getId
     * @memberof InHouseEntity
     * @returns {integer} - Returns inHouse id used at the inHouse transactions
     */
    function getId() {
      return Session.get(data.session.id);
    }

    /**
     * Saving the inHouse id in the localStorage, this information will be used
     * to identify which transaction the user is executing.
     * @function setId
     * @memberof InHouseEntity
     * @param {integer} id
     */
    function setId(id) {
      Session.save(data.session.id, id);
    }

    /**
     * Removing InHouse id from the localStorage, generally called at the end
     * of a transaction to avoid conflicts with future transactions
     * @memberof InHouseEntity
     * @function removeId
     */
    function removeId() {
      Session.remove(data.session.id);
    }

    /**
     * Getting the InHouse Seller id from the localStorage, this information is
     * used to consulte data from the restaurant, like products available
     * in the InHouse flow for that particular restaurant
     * @memberof InHouseEntity
     * @function getSellerId
     * @returns {string}
     */
    function getSellerId() {
      return Session.get(data.session.sellerId);
    }

    /**
     * Setting the Seller Id of the current transaction, this function
     * generally is called at the beginning of the flow, when the user
     * selected a restaurant after clicking in In House menu
     * @function setSellerId
     * @memberof InHouseEntity
     * @param {integer} sellerId
     */
    function setSellerId(sellerId) {
      Session.save(data.session.sellerId, sellerId);
    }

    /**
     * Removing Seller Id from the localStorage, this function generally is
     * called at the end of the InHouse flow, avoiding inconsistency when
     * the user executes the same flow
     * @function removeSellerId
     * @memberof InHouseEntity
     */
    function removeSellerId() {
      Session.remove(data.session.sellerId);
    }

    function getTableNumber() {
      return Session.get(data.session.tableNumber);
    }
    function setTableNumber(tableNumber) {
      Session.save(data.session.tableNumber, tableNumber);
    }
    function removeTableNumber() {
      Session.remove(data.session.tableNumber);
    }

    function getSellerName() {
      return Session.get(data.session.sellerName);
    }
    function setSellerName(name) {
      Session.save(data.session.sellerName, name);
    }
    function removeSellerName() {
      Session.remove(data.session.sellerName);
    }

    function getSellerMinCharge() {
      return Session.get(data.session.sellerMinCharge);
    }
    function setSellerMinCharge(amount) {
      Session.save(data.session.sellerMinCharge, amount);
    }
    function removeSellerMinCharge() {
      Session.remove(data.session.sellerMinCharge);
    }

    function getSellerMinDiscount() {
      return Session.get(data.session.sellerMinDiscount);
    }
    function setSellerMinDiscount(discount) {
      Session.save(data.session.sellerMinDiscount, discount);
    }
    function removeSellerMinDiscount() {
      Session.remove(data.session.sellerMinDiscount);
    }

    function getSellerDiscountPercent() {
      return Session.get(data.session.sellerDiscountPercent);
    }
    function setSellerDiscountPercent(percent) {
      Session.save(data.session.sellerDiscountPercent, percent);
    }
    function removeSellerDiscountPercent() {
      Session.remove(data.session.sellerDiscountPercent);
    }

    function getSellerDiscountAmount() {
      return Session.get(data.session.sellerDiscountAmount);
    }
    function setSellerDiscountAmount(amount) {
      Session.save(data.session.sellerDiscountAmount, amount);
    }
    function removeSellerDiscountAmount() {
      Session.remove(data.session.sellerDiscountAmount);
    }

    function getOrderTotalAmount() {
      return Session.get(data.session.orderTotalAmount);
    }
    function setOrderTotalAmount(amount) {
      Session.save(data.session.orderTotalAmount, amount);
    }
    function removeOrderTotalAmount() {
      Session.remove(data.session.orderTotalAmount);
    }

    /**
     * Remove all data related to city location in the current session
     * @function clearSession
     * @memberof City
     */
    function clearSession() {
      removeId();
      removeSellerId();
      removeTableNumber();
      removeSellerName();
      removeSellerMinCharge();
      removeSellerMinDiscount();
      removeSellerDiscountPercent();
      removeSellerDiscountAmount();
      removeOrderTotalAmount();
    }
  }
})();
