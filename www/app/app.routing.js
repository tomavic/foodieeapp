(function () {
  "use strict";

  angular
    .module("MinkFoodiee")
    .config(AppConfig);

  AppConfig.$inject = ["$urlRouterProvider", "$ionicConfigProvider", "$stateProvider"];

  function AppConfig($urlRouterProvider, $ionicConfigProvider, $stateProvider) {
    $ionicConfigProvider.backButton.text("").previousTitleText(false);
    $ionicConfigProvider.navBar.alignTitle("center");
    // $ionicConfigProvider.views.transition("ios");

    //App Routing base
    $stateProvider
      .state('app', {
        cache: false,
        url: '/app',
        abstract: true,
        templateUrl: 'app/app.html',
        controller: 'AppController',
        controllerAs: 'vm'
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise("/index");

  }

})();
