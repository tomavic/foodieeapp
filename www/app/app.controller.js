(function () {
  'use strict';

  angular
    .module('MinkFoodiee')
    .controller('AppController', AppController);

  AppController.$inject = ['$scope', '$Easyajax', '$Session', '$state'];

  function AppController($scope, $Easyajax, $Session, $state) {
    /* jshint validthis:true */
    var vm = this;

    activate();

    function activate() {

      $scope.$on('$ionicView.enter', function () {
        getUserDetails();
      });

      function getUserDetails() {
        var userId = $Session.user.getId() || "0";
        if (userId === "0") {
          // console.log("Guest User mode is ON!");
          return false;
        }
        var params = {
          uId: userId
        };
        var callback = function (res) {
          if (res.status == "success") {
            // Append details to show up in the scope
            $scope.userDetail = res.userDetail;
            $Session.user.isLoggedInFromFacebook() ? setUserDetailsForFacebook(res) : setUserDetails(res);

            // Show user image according to 
            var image = $Session.user.getImage();
            if (image == "" || image == "http://minkchatter.com/mink-chatter-images/app/uploads/") {
              $('.userImage').attr('src', 'assets/img/profile.png');
            } else  {
              $('.userImage').attr('src', image);
            }
          } else {
            console.log("Error getting user information!", res);
          }
        }
        var config = {
          typeRequest: "POST",
          urlRequest: getUserDetailsAPI,
          dataRequest: params,
          sucesCallbackRequest: callback
        };
        $Easyajax.startRequest(config);
      }


      $scope.goProfile = function() {
        $state.go("app.profile")
      }

      function setUserDetails(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          image: user.userImage,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }

      function setUserDetailsForFacebook(data) {
        var user = data.userDetail;
        $Session.user.setUserInfo({
          id: user.userId,
          name: user.firstName,
          mobile: user.mobileNumber,
          address: user.address,
          address2: user.shipAddress,
          email: user.email,
          city: user.cityName,
          area: user.cityArea,
          pin: user.pincode
        });
      }


    };
  };
})();
