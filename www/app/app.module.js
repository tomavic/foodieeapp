(function () {
  'use strict';

  angular
    .module('ElValidation', [])
    .config(['$validatorProvider', function ($validatorProvider) {

      $validatorProvider.setDefaults({
        debug: true,
        errorElement: 'span',
        errorClass: 'alert'
      });

      $.validator.addMethod('integer', function (value, element) {
        return this.optional(element) || /^\d+$/.test(value);
      });

      $.validator.addMethod('string', function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z_\s-]+$/.test(value);
      });

      $validatorProvider.addMethod("validEmail", function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $validatorProvider.addMethod("validMobileNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });

      $validatorProvider.addMethod("indianMobNumber", function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value);
      });
      $.validator.addMethod('strongPassword', function (value, element) {
        return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d).{8,}$/.test(value);
      });
      $.validator.addMethod('emailOrMobile', function (value, element) {
        return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}|^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/.test(value);
      });

      $.validator.addMethod('selectRequired', function (value) {
        return value != '0';
      });

      $.validator.addMethod('queuePeople', function (value, element) {
        return (value > 0 && value <= 30 && /^\d+$/.test(value));
      });

      $.validator.addMethod('greaterThanToday', function (value, element) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        var date = new Date(value.split('-').join('/'));
        return (date >= today);
      });
    }]);

})();


(function () {
  'use strict';

  //Waves effect configs
  var wavesConfig = {
    duration: 1200,
    delay: 50
  };
  Waves.init(wavesConfig);

  angular.module('MinkFoodiee', [

    //app dependancies
    'ionic',
    'ngCordova',
    'ngValidate',

    //3rd third-party modules
    'ionic-toast',
    'LocalStorageModule',
    'ngRateIt',
    'ElValidation',
    'angular-md5',
    'countdown',
    "ion-datetime-picker",

    //app pages and services
    'components-module',
    'pages-module',


  ]).run(AppRun);

  AppRun.$inject = ["$ionicPlatform", "$state", "$ionicHistory", "$ionicPopup", "$rootScope", "$Session", 'Loading', '$ionicPickerI18n'];
  
  
  function AppRun($ionicPlatform, $state, $ionicHistory, $ionicPopup, $rootScope, $Session, Loading, $ionicPickerI18n) {

    $ionicPlatform.ready(function () {

      $ionicPickerI18n.okClass = "button-assertive";
      $ionicPickerI18n.cancelClass = "button-stable";
      $ionicPickerI18n.arrowButtonClass = "button-assertive";

      function exitAppConfirm() {
        var confirmPopup = $ionicPopup.confirm({
          title: "Exit",
          template: "Are you sure you want to exit Mink Foodiee?",
          cancelText: "Cancel",
          cancelType: "button-default",
          okText: "I am sure",
          okType: "button-assertive"
        });

        confirmPopup.then(function(res) {
          return (res) ? navigator.app.exitApp() : false; 
        });
      };

      function backFromOnlinePaymentHandler() {
        var confirmPopup = $ionicPopup.confirm({
            title: "Back",
            template: "Order details will be lost if you click back. Are you sure?",
            cancelText: "Close",
            cancelType: "button-assertive",
            okText: "Yes, sure",
            okType: "button-default"
        });
        confirmPopup.then(function (res) {
          return (res) ? $state.go("app.checkout") : false;
        });
    }

      $rootScope.$on('$stateChangeStart', 
        function(event, toState, toParams, fromState, fromParams, options) { 
          // console.log(event,fromState);
          Loading.hide();
      });

      //Disable/Enable Hardware backbutton in Android
      $ionicPlatform.registerBackButtonAction(function (event) {
        Loading.hide();
        if ($state.current.name == "app.restList" || $state.current.name == "home") {
          event.preventDefault();
          exitAppConfirm();
        } else if (
          $state.current.name == "app.profile" ||
          $state.current.name == "app.notification" ||
          $state.current.name == "app.favourites" ||
          $state.current.name == "app.history" ||
          $state.current.name == "app.tableBookingList" ||
          $state.current.name == "app.inhouseFoodBarList" ||
          $state.current.name == "app.queuemanager") {
          $ionicHistory.nextViewOptions({
            historyRoot: true,
            disableBack: true
          });
          $state.go("app.restList");
        } else if ($state.current.name == "app.payment") {
          event.preventDefault();
          backFromOnlinePaymentHandler();
        }
        else if ($state.current.name == "app.checkout") {
          event.preventDefault();
        }
        else if ($state.current.name == "thankyou") {
          event.preventDefault();
        } else {
          // $ionicHistory.backView();
          navigator.app.backHistory();
        }
      }, 100);


      //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
      //Keep in mind the function will return null if the token has not been established yet.
      if (typeof FCMPlugin !== 'undefined') {
        FCMPlugin.getToken(function (registrationId) {
          //alert("registrationId: " + registrationId);
          var oldRegId = $Session.user.getDeviceToken();
          if (oldRegId !== registrationId) {
            // Save new registration ID
            $Session.user.setDeviceToken(registrationId);
            // localStorage.setItem("device_token", registrationId);
          }
        }, function (err) {
          //alert("error retrieving token: " + err);
        });

        FCMPlugin.onNotification(function (data) {
          if (data.wasTapped) {
            //Notification was received on device tray and tapped by the user.
            // alert("Tapped: " + JSON.stringify(data));
          } else {
            //Notification was received in foreground. Maybe the user needs to be notified.
            // alert("Not tapped: " + JSON.stringify(data));
          }
        }, function (msg) {
          //alert("onNotification callback successfully registered: " + msg);
        }, function (err) {
          //alert("Error registering onNotification callback: " + err);
        });
      } else console.warn("FCM is not defined, maybe You are using webview");
      

    });
    
  }

})();