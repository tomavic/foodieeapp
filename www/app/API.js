var isV2 = "V2",
  ReleaseURL = "http://minkfoodiee.com/foodiee/",
  TestingURL = "http://minkfoodiee.com/foodiee-Akay/",
  LocalHostURL = "http://192.168.5.204:8084/foodiee/",
  domain = ReleaseURL,


  tokenInfoAPI = domain + "DateService" + isV2,

  loginAPI = domain + "LoginServiceController" + isV2,
  userRegisterAPI = domain + "UserRegisServiceController" + isV2,
  LoginWithFacebookAPI = domain + "UserRegistrationFBService" + isV2,
  forgetPasswordAPI = domain + "ForgetPassServiceController" + isV2,
  sendOtpApi = domain + "SendOTP" + isV2,

  getUserDetailsAPI = domain + "GetUserDetailServiceController" + isV2,


  cityListAPI = domain + "CityServiceController" + isV2,
  cityAreaListAPI = domain + "CityAreaServiceController" + isV2,


  cuisineAPI = domain + "CuisinesServiceController" + isV2,
  specialCuisineAPI = domain + "SpecialCuisineServiceController" + isV2,


  userUpdateImageAPI = domain + "UpdateUserProfileServiceController" + isV2,
  userUpdateDetailsAPI = domain + "UpdateUserDetailService" + isV2,
  userUpdateMobileAPI = domain + "UpdateMobileService" + isV2,
  userUpdateEmailAPI = domain + "UpdateEmailService" + isV2,
  userChangePasswordAPI = domain + "ChangePasswordService" + isV2,

  RestaurantDetailsOnlyAPI = domain + "RestaurantDetailServiceByRestaurant" + isV2,
  menuDetailsAPI = domain + "RestaurantDetailAndMenuService" + isV2,
  restaurantDetailsAPI = domain + "RestaurantDetailServiceController" + isV2,
  AddUpdateCartItemsAPI = domain + "AddUpdateCartItemService" + isV2,
  reserveTableAPI = domain + "ReserveTableServiceController" + isV2,
  AvailableTableAPI = domain + "ListTableReservationService" + isV2,
  userCartListAPI = domain + "UserCartListServiceController" + isV2,
  userCartItemIncDecAPI = domain + "CartItemIncDecServiceController" + isV2,
  userCartListTaxAPI = domain + "UserCartListTaxServiceController" + isV2,
  addToCartAPI = domain + "AddToCartServiceController" + isV2,
  checkoutAPI = domain + "CheckoutServiceController" + isV2,
  recommendToFriendAPI = domain + "RecommendToFriendService" + isV2,

  toppingCommentAPI = domain + "SaveUpdateToppingCommentService" + isV2,
  listToppingByIdAPI = domain + "ListToppingByCartId" + isV2,
  commonCommentAPI = domain + "CommonCommentService" + isV2,
  preorderAPI = domain + "PreorderServiceController" + isV2,
  placeOrderAPI = domain + "PlaceOrderServiceController" + isV2,
  verificationCodeAPI = domain + "VerificationServiceController" + isV2,
  saveUserOrderAPI = domain + "SaveUserOrderServiceController" + isV2,



  restaurantQMListAPI = domain + "RestaurantQMList" + isV2,
  QMPrefrencesAPI = domain + "QmPreferenceController";
  userQMListAPI = domain + "UserQMList" + isV2,
  QManagerAPI = domain + "QueueManagerService" + isV2,
  QManagerCancelRequestAPI = domain + "QMRequestRejectByUser" + isV2,
  restaurantAPI = domain + "RestaurantServiceController" + isV2,

  restaurantSearchAPI = domain + "SearchRestaurantServiceController" + isV2,
  restaurantSortAPI = domain + "SortRestaurantListService" + isV2,
  listRestaurantByCategoryAPI = domain + "ListRestaurantByCategoryService" + isV2,



  //Inhouse Food Services
  inHouseCheckRestaurantAPI = domain + "InhouseCheckRestaurantService" + isV2,
  inHouseOrderDetailsAPI = domain + "InhouseOrderListDetail",
  inHouseDetailAPI = domain + "GetInhouseDetailService" + isV2,
  cartListInHouseAPI = domain + "UserCartListInhouseServiceController" + isV2,
  completeInHouseOredrAPI = domain + "CompleteInhouseOrderService" + isV2,
  saveUserOrderInHouseAPI = domain + "SaveUserOrderInhouseServiceController" + isV2,
  inHouseOederPayNowAPI = domain + "InhouseOrderPayNowService" + isV2,
  inHouseOrderListById = domain + "InhouseOrderListByInhouseIdService" + isV2,
  userCartInHouseIncDecAPI = domain + "CartItemIncDecInhouseServiceController" + isV2,
  addToCartInHouseAPI = domain + "AddToCartInhouseServiceController" + isV2,
  userCartListTaxInHouseAPI = domain + "UserCartListTaxInhouseServiceController" + isV2,
  PayGenrateInhouseAPI = domain + "PayGenrateInhouseBill" + isV2,
  cartInHouseDetailsAPI = domain + "InhouseOrderDetailByInhouseId" + isV2,
  ItemWiseBillSplitSubmitAPI = domain + "InhouseBillSplitItemwise" + isV2,
  InhouseItemPricewisePayCheckAPI = domain + "InhouseItemPricewisePayersList" + isV2,



  //Inhouse Bartabs (Normal) Services
  BarOrderRestaurantAPI = domain + "BarOrderRestaurantService" + isV2,
  BarOrderRequestAPI = domain + "BarOrderRequestService" + isV2,
  BarOrderRestaurantMenuAPI = domain + "BarOrderRestaurantMenuService" + isV2,
  AddToCartBarOrderServiceAPI = domain + "AddToCartBarOrderService" + isV2,
  UserCartListBarOrderAPI = domain + "UserCartListBarOrder" + isV2,
  BarOrderSendToRestaurantServiceAPI = domain + "BarOrderSendToRestaurantService" + isV2,
  BarOrderBillServiceAPI = domain + "BarOrderBillService" + isV2,
  BarOrderPaymentServiceAPI = domain + "BarOrderPaymentService" + isV2,
  BarOrderHistoryAPI = domain + "BarOrderHistory" + isV2,


  BarCoverChargeAPI = domain + "BarCoverChargeService" + isV2,

  //Inhouse Bar Exchange Services
  BarExchangeRequestAPI = domain + "BarExchangeRequestService" + isV2,
  BarExchangeRestaurantAPI = domain + "BarExchangeRestaurantService" + isV2,
  BarExchangeRestaurantMenuAPI = domain + "BarExchangeRestaurantMenuService" + isV2,
  AddToCartBarExchangeAPI = domain + "AddToCartBarExchangeService" + isV2,
  UserCartListBarExchangeAPI = domain + "UserCartListBarExchange" + isV2,
  BarExchangeSendToRestaurantAPI = domain + "BarExchangeSendToRestaurantService" + isV2,
  BarExchangeBillAPI = domain + "BarExchangeBillService" + isV2,
  BarExchangePaymentAPI = domain + "BarExchangePaymentService" + isV2,
  BarExchangeHistoryAPI = domain + "BarExchangeHistory" + isV2,

  CurrentActiveBarAPI = domain + "CurrentBarStatusService" + isV2,


  wishListAPI = domain + "WishlistServiceController" + isV2,
  addWishListAPI = domain + "AddWishlistServiceController" + isV2,
  userReviewAPI = domain + "UserReviewService" + isV2,
  userReviewListAPI = domain + "ListUsersReviews" + isV2,

  orderHistoryAPI = domain + "OrderHistoryServiceController" + isV2,
  oredrHistoryDetailsAPI = domain + "OrderHisDetailSerController" + isV2,
  tableReservationHistoryAPI = domain + "TableReservationHistoryV2",
  cancelOrderAPI = domain + "CancelOrderServiceControllerV2",


  sendSmsAPI = domain + "SendSMSService" + isV2,
  SendReceiptViaEmailAPI = domain + "SendOrderReceipt" + isV2,



  topCatAPI = domain + "TopCategoriesService" + isV2,
  topCatHomeAPI = domain + "TopCategoriesOnHomeService" + isV2,


  notificationHistoryAPI = domain + "NotificationHistory",


  UpdateUserDeviceTokenAPI = domain + "UpdateUserDeviceTokenV2",
  GeoFencingSendNotificationAPI = domain + "GeoFencingSendNotification" + isV2,
  SellerImagesAPI = domain + "SellerImages" + isV2;



//Deleted services
//  loginFbAPI = domain + "LoginFBService" + isV2,
//reserveTableInHouseAPI = domain + "ReserveInhouseTableServiceController" + isV2,
//    listRestaurantForAPI = domain + "ListRestaurantForV2",
//    paymentResponseAPI = domain + "user/FoodieePaymentResponseService" + isV2,


function caeserCipher(str, num) {
	num = num % 26; 
	var lowerCaseString = str.toLowerCase();
  var alphabet = 'abcdefghiklmnopqrstuvwxyz'.split('');
  var newString = '';
    
  for (var i = 0; i < lowerCaseString.length; i++) {
    var currentLetter = lowerCaseString[i];
      if (currentLetter === ' ') {
        newString += currentLetter;
        continue;
      }
      var currentIndex = alphabet.indexOf(currentLetter);
      var newIndex = currentIndex + num;
      if (newIndex > 25) newIndex = newIndex - 26;
      if (newIndex < 0 ) newIndex = 26 + newIndex;
      
      // if original string has some UPPERCASE letters
      if (str[i] === str[i].toUpperCase()) {
        newString += alphabet[newIndex].toUpperCase();
      } else newString += alphabet[newIndex];
  }
  return newString;
}

// -9 == 17
console.log(caeserCipher('Holaa', 17));