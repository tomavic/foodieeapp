module.exports = {
  "env": {
    "browser": true
  },
  "extends": ["eslint:recommended", "plugin:angular/johnpapa"],
  "plugins": ["angular"],
  "globals": {
    'jQuery': true,
    '$': true,
    "angular": true,
  },
  "parserOptions": {
    "ecmaVersion": 5
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "angular/controller-as-vm": 2,
    "linebreak-style": [
      "error",
      "windows"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ]
  }
};
