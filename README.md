[![Build Status](https://travis-ci.org/akveo/blur-admin.svg?branch=master)](https://travis-ci.org/akveo/blur-admin)

# Mink Foodiee Wep app

A web application, that lets users to order online from nearby restaurants according to user location

 
 ![alt text](./www/assets/img/logo.png "Mink Foodiee")

----

# Documentation
Install all dependencies as follow

 open `cmd` as Administrator and run the following commands

```
	npm install
```

#### ** WARNING **

* MAKE SURE that after running `npm install`, it will run automatically `bower install --force`
* If you had to choose and resolve Angular version, make sure to choose AngularJs version 1.5.3 exactly.

----

# Start Developer Mode

 open `cmd` as Administrator and run the following command

```
	npm run dev
```

then in individual `cmd` run 

```
	ionic serve
```

#### ** WARNING **


* MAKE SURE when running `localhost:8100` on chrome in non-security mode. Our APIs and servers does not support secure browsing.

----

# Deployment

##1.Android:

Use the following commands

```
	npm run build
	ionic cordova build android
```


##2.iOS:

Use the following commands

```
	npm run build
	ionic cordova build ios
```

#### ** WARNING **


1. Make sure all plugins are installed succesfully. 

2. Make sure all files in `index.hml`are inhertied in same order as described.

3. If for first time for Android, make sure to add the platform. Supported Android platform version is Android 6.x and up to 7.1.1 
 `npm run add android` >> ** This will add android 6.2.3 **

4. If for first time for iOS, make sure to add the platform first 
 `npm run add ios`


----

# Libs
* Ionic Framework 1.3.5
* AngularJs (limited to 1.5.3) 
* Jquery
* Cordova
* Bower
* Sass
* Gulp
* Karma
* Mocha

----


License
-------------
MIT

# Credits

Made with ❤️ by Hatem. Follow me on [Twitter](https://twitter.com/toomavic) to get the latest news first! I will be happy to receive your feedback! We're always happy to hear your feedback.
Enjoy!



    ░░░░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ 
    ░░░░░░░░░░█░░░░░░▀█▄▀▄▀██████░░░▀█▄▀▄▀██████ 
    ░░░░░░░░ ░░░░░░░░░░▀█▄█▄███▀░░░░░░▀█▄█▄███▀░

   All copyrights reserved © 2018 | TOmas™ Inc.  
